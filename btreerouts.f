c
c     Binary tree related subroutine
c
c******************************************************
c
c     inittree: initializes the tree structure
c               (a tree with root node only)
c     unitree: generates a uniform tree of a given level
c     quasiunitree: generate a 'quasi'-uniform tree
c                   given the number of leaf nodes
c     getnumqutree: memory query for a quasi-uniform tree
c     mktreef: generate a tree to resolve a given
c              function to a given accuracy
c     mktreelf: generate a tree, given subintervals as leaf
c               nodes
c     subdivide: subdivides a give leaf node, this version
c           doesn't concern itself with the colleague list
c     mergechd: merges children (that are leaf boxes) of 
c               given parent box 
c     getleaflist: returns the list of leaf nodes,
c           in ascending order of position
c     mkgrid: returns the chebyshev nodes of degree
c             ndeg-1, on a given interval of a given
c             binary tree
c     treesort: sorting a given array of points into
c               a given binary tree
c     rvecold2new: reorder points from old order to new order
c              given permutation ipold that maps new indices 
c              to the old ones
c     rvecnew2old: reorder points from new order to old order
c              given permutation ipold that maps new indices 
c              to the old ones
c     printtree: prints out the tree
c     chebyp2k: given function values on the cheby grid on 
c               parent box, interpolate to get the function
c               values on the cheby grids on children boxes
c
c     chebyk2p: given function values on the cheby grids on 
c               the children boxes, interpolate to get the
c               function values on the parent box
c---------------------------------------------------
c
c     (RMK: similar to F. Ethridge's data structure
c          of a quad-tree
c
c     Convention used for adaptive refinement and
c     coarsening: allow for boxid > nboxes
c     and empty spots in arrays:
c     levelbox, icolbox, iparentbox, ichildbox, 
c     while the array iboxlev is always updated
c     and contains only existing nodes in the tree
c
c     For this specific application, there won't be
c     a huge number of coarsening steps and thus not
c     a big problem for storage)
c
c******************************************************





c--------------------------------------------------------
c     subroutine inittree
c--------------------------------------------------------
c
c     This subroutine initializes a binary tree structure
c     which contains only a root node
c
c     output:
c     the binary tree structure:
c     levelbox: an array determining the level of each box
c     icolbox: the column of each box (within its level)
c     nboxes: total number of boxes in the tree
c     maxid: maximum box id used (>=nboxes)
c     nlev: total number of levels in the tree
c     iparentbox: parent of each box
c     ichildbox: the two children of each box
c     nblevel: total number of boxes per level
c     iboxlev: the array in which the boxes are arranged
c              (in ascending order of level) 
c     istartlev: the pointer to where each level begins in
c                the iboxlev array
c
c--------------------------------------------------------
c
      subroutine inittree(levelbox, icolbox, nboxes, maxid,
     1           nlev, iparentbox, ichildbox, nblevel, 
     2           iboxlev, istartlev)
      implicit none
      integer*4 nboxes, maxid, nlev
      integer*4 levelbox(1)
      integer*4 icolbox(1) 
      integer*4 iparentbox(1), ichildbox(2,1)
      integer*4 nblevel(0:1)
      integer*4 iboxlev(1), istartlev(0:1)
c
      levelbox(1)=0
      icolbox(1)=1
      nboxes=1
      maxid=1
      nlev=0
      iparentbox(1)=-1
      ichildbox(1,1)=-1
      ichildbox(2,1)=-1
      nblevel(0)=1
      iboxlev(1)=1
      istartlev(0)=1

      end subroutine




c--------------------------------------------------------
c     subroutine unitree
c--------------------------------------------------------
c
c     This subroutine generates a uniform binary tree
c     of a given level
c
c     input:
c     nlev: total number of levels in the tree
c
c     output:
c     the binary tree structure:
c     levelbox: an array determining the level of each box
c     icolbox: the column of each box (within its level)
c     nboxes: total number of boxes in the tree
c     maxid: maximum box id used (>= nboxes)
c     iparentbox: parent of each box
c     ichildbox: the two children of each box
c     nblevel: total number of boxes per level
c     iboxlev: the array in which the boxes are arranged
c              (in ascending order of level) 
c     istartlev: the pointer to where each level begins in
c                the iboxlev array
c
c--------------------------------------------------------
c
      subroutine unitree(levelbox, icolbox, nboxes, maxid,
     1           nlev, iparentbox, ichildbox, nblevel, 
     2           iboxlev, istartlev)
      implicit none
      integer nboxes, maxid, nlev
      integer levelbox(1)
      integer icolbox(1) 
      integer iparentbox(1), ichildbox(2,1)
      integer nblevel(0:1)
      integer iboxlev(1), istartlev(0:1)
c     local vars
      integer i, l, ii, id
      integer istart, iend, idlev
      
      nboxes=2**(nlev+1)-1
      maxid=nboxes
      do i=1,nboxes
        iparentbox(i)=i/2
        ichildbox(1,i)=i*2
        ichildbox(2,i)=i*2+1
c         simple parent-child relation
        iboxlev(i)=i
c         naturally in ascending order of level
      enddo
      iparentbox(1)=-1
c
      do l=0,nlev
        nblevel(l)=2**l
        istartlev(l)=2**l
      enddo
c
      do l=0,nlev
        istart=istartlev(l)
        iend=istart+nblevel(l)-1
        idlev=1
        do ii=istart,iend
          levelbox(ii)=l
          icolbox(ii)=idlev
          idlev=idlev+1 
        enddo
      enddo
c
      l=nlev
      istart=istartlev(l)
      iend=istart+nblevel(l)-1
      do ii=istart,iend
        id=iboxlev(ii)
        ichildbox(1,id)=-1
        ichildbox(2,id)=-1
      enddo
c
      end subroutine




c--------------------------------------------------------
c     subroutine subdivide
c--------------------------------------------------------
c
c     This subroutine subdivides a given leaf box of a
c     binary tree into two children boxes
c
c     INPUT:
c     iparbox: id of the box being divided
c     levelbox-istartlev: the binary tree structure
c           (see inittree for details)
c     
c     OUTPUT:
c     the tree structure updated to reflect the change
c
c--------------------------------------------------------
c
      subroutine subdivide(iparbox, levelbox, icolbox, nboxes,
     1           maxid, nlev, iparentbox, ichildbox, nblevel, 
     2           iboxlev, istartlev)
      implicit real*8 (a-h,o-z)
      integer*4 iparbox
      integer*4 nboxes, maxid, nlev
      integer*4 levelbox(1)
      integer*4 icolbox(1) 
      integer*4 iparentbox(1), ichildbox(2,1)
      integer*4 nblevel(0:1)
      integer*4 iboxlev(1), istartlev(0:1)

      levp=levelbox(iparbox)
      icolp=icolbox(iparbox)
      if((levp+1) .gt. nlev) then
        istartlev(levp+1)=nboxes+1
        nblevel(levp+1)=0
c       if a new level is created
c       initialize these two numbers as such
      endif
c
c     set up the first child
      ibox=maxid+1
c       attention: maxid > nboxes might happen
      levelbox(ibox)=levp+1
      iparentbox(ibox)=iparbox
      ichildbox(1,ibox)=-1
      ichildbox(2,ibox)=-1
      icolbox(ibox)=2*(icolp-1)+1
c
c     set up the second child
      ibox=maxid+2
      levelbox(ibox)=levp+1
      iparentbox(ibox)=iparbox
      ichildbox(1,ibox)=-1
      ichildbox(2,ibox)=-1
      icolbox(ibox)=2*(icolp-1)+2
c
      ichildbox(1,iparbox)=maxid+1
      ichildbox(2,iparbox)=maxid+2
c
c     if levp+2 exists
c     move things on and below this level forward
      if(nlev .ge. levp+2) then
        do ii=nboxes, istartlev(levp+2),-1
          iboxlev(ii+2)=iboxlev(ii)
        enddo
c       move things 2 spots forward
        
        do l=levp+2,nlev
          istartlev(l)=istartlev(l)+2
        enddo
      endif

      inew=istartlev(levp+1)+nblevel(levp+1)
      iboxlev(inew)=maxid+1
      iboxlev(inew+1)=maxid+2

      nblevel(levp+1)=nblevel(levp+1)+2
      nboxes=nboxes+2
      maxid=maxid+2
      if(levp+1 .gt. nlev) then
        nlev=nlev+1
      endif
      
      end subroutine




      
c--------------------------------------------------------
c     subroutine mergechd
c--------------------------------------------------------
c
c     This subroutine deletes the two child boxes of a 
c     given box where the children are leaves.
c
c     INPUT:
c     iparbox: id of the box whose children are to be 
c              deleted
c     levelbox - istartlev: the binary tree structure
c              (see inittree for details)
c
c     OUTPUT:
c     the tree structure updated to reflect the change
c
c     (RMK: hasn't been tested!)
c
c--------------------------------------------------------
c
      subroutine mergechd(iparbox, levelbox, icolbox, nboxes,
     1           maxid, nlev, iparentbox, ichildbox, nblevel, 
     2           iboxlev, istartlev)
      implicit real*8 (a-h,o-z)
      integer*4 iparbox
      integer*4 nboxes, maxid, nlev
      integer*4 levelbox(1)
      integer*4 icolbox(1) 
      integer*4 iparentbox(1), ichildbox(2,1)
      integer*4 nblevel(0:1)
      integer*4 iboxlev(1), istartlev(0:1)
c
      ichbox1=ichildbox(1,iparbox)
      ichbox2=ichildbox(2,iparbox)
      levp=levelbox(iparbox)
      levc=levp+1
c
      ichildbox(1,iparbox)=-1
      ichildbox(2,iparbox)=-1
c       set the children to -1
c
      levelbox(ichbox1)=-1
      levelbox(ichbox2)=-1

      icolbox(ichbox1)=-1
      icolbox(ichbox2)=-1

      iparentbox(ichbox1)=-1
      iparentbox(ichbox2)=-1

      ichildbox(1,ichbox1)=-1
      ichildbox(2,ichbox1)=-1
      ichildbox(1,ichbox2)=-1
      ichildbox(2,ichbox2)=-1
c      
c     deal with iboxlev and istartlev:
c     1) scan the children's level
c        mark those two spots
c
c     2) delete the spots with -1 entries
c        and move forward things behind them  
c
      istart=istartlev(levc) 
      iend=istart+nblevel(levc)-1
      do ii=istart,iend
        if((iboxlev(ii).eq.ichbox1).or.
     1     (iboxlev(ii).eq.ichbox2)) then
          iboxlev(ii)=-1
        endif
      enddo
c
      movebox=0
      do l=levc,nlev
        istart=istartlev(l)
        iend=istart+nblevel(l)-1
        do ii=istart,iend
          if(iboxlev(ii).lt.0) then
            movebox=movebox+1
          elseif(movebox .gt. 0) then
            iboxlev(ii-movebox)=iboxlev(ii)
          endif          
c           if empty, increase move steps for
c           boxes behind, if movebox>0, move.
        enddo
      enddo 
c
      nboxes=nboxes-2

      lev=0
      maxid=1
      do ii=istartlev(0),nboxes
        ibox=iboxlev(ii)
        if(ibox.gt.maxid) then
          maxid=ibox
        endif
c
        if(levelbox(ibox).ne.lev) then
          lev=lev+1
          istartlev(lev)=ii
        endif
      enddo
c       rewrite istartlev and maxid
c
      nblevel(levc)=nblevel(levc)-2
      if(nblevel(levc) .eq. 0) then
        nlev=nlev-1
      endif
c

      end subroutine




     
c--------------------------------------------------------
c     subroutine mkgrid
c--------------------------------------------------------
c
c     This subroutine returns the Chebyshev nodes of a 
c     given box on a given binary tree
c
c     INPUT:
c     lev: level of the box
c     icol: column index of the box
c     aroot, broot: left and right end points of the root
c           box
c     ndeg: number of chebyshev points
c
c     OUTPUT:
c     a, b: end points of the interval
c     chpts: chebyshev points on the interval
c
c--------------------------------------------------------
c
      subroutine mkgrid(lev, icol, aroot, broot, ndeg,
     1           a, b, chpts)
      implicit real*8 (a-h, o-z)
      integer lev, icol, ndeg
      real*8 aroot, broot, a, b, chpts(ndeg)
      real*8 xlength
c
      xlength=(broot-aroot)/dble(2**lev)
      a=aroot+xlength*(icol-1)
      b=a+xlength
c
      call chnodes(a, b, ndeg, chpts, u, v, u1, v1)

      end subroutine






c--------------------------------------------------------
c     subroutine mkinterval
c--------------------------------------------------------
c
c     This subroutine returns the subinterval of a given 
c     box on a given binary tree
c     (simplified version of subroutien mkgrid)
c
c--------------------------------------------------------
c
      subroutine mkinterval(lev, icol, aroot, broot, a, b)
      implicit real*8 (a-h, o-z)
      integer lev, icol
      real*8 aroot, broot, a, b
c
      xlength=(broot-aroot)/dble(2**lev)
      a=aroot+xlength*(icol-1)
      b=a+xlength
c
      end subroutine






c--------------------------------------------------------
c     subroutine getleaflist
c--------------------------------------------------------
c
c     This subroutine returns the list of ids of leaf nodes
c     of a given binary tree (in ascending order of position)
c
c     INPUT:
c     levelbox-istartlev: the btree structure
c           (see subroutine inittree for details)
c
c     OUTPUT:
c     nleaf: number of leaf boxes
c     leaflist: an array containing ids of the leaf boxes
c           (on input, make sure length >= nboxes)
c
c--------------------------------------------------------
c
      subroutine getleaflist(levelbox, icolbox, nboxes,
     1           maxid, nlev, iparentbox, ichildbox, 
     2           nblevel, iboxlev, istartlev, 
     3           nleaf, leaflist)
      implicit real*8 (a-h,o-z)
      integer*4 nboxes, maxid, nlev, nleaf
      integer*4 levelbox(1)
      integer*4 icolbox(1) 
      integer*4 iparentbox(1), ichildbox(2,1)
      integer*4 nblevel(0:1)
      integer*4 iboxlev(1), istartlev(0:1)
      integer*4 leaflist(1)
      integer*4, allocatable:: istack(:)
      integer*4 nstack
c
      nleaf=0
      allocate(istack(nboxes))
      iroot=iboxlev(1) 
      nstack=1
      istack(nstack)=iroot

      do while(nstack .gt. 0)
        ibox=istack(nstack)
        nstack=nstack-1
        if(ichildbox(1,ibox).lt.0) then
          nleaf=nleaf+1
          leaflist(nleaf)=ibox
c         is a leaf node, add into the leaflist
        else
          icha=ichildbox(1,ibox)
          ichb=ichildbox(2,ibox)
          nstack=nstack+1
          istack(nstack)=ichb
          nstack=nstack+1
          istack(nstack)=icha
c         not a leaf node, add the children (right first)
c         into the stack
        endif
      enddo
      
      deallocate(istack)

      end subroutine






c--------------------------------------------------------
c     subroutine quadiunitree
c--------------------------------------------------------
c
c     This subroutine generates a 'quasi' uniform binary
c     tree with a given number of leaf nodes
c     so that 2**(nlev-1) < nleaf <= 2**nlev
c
c     INPUT:
c     nlev: total numver of levels in the tree
c
c     OUTPUT:
c     the binary tree structure:
c     levelbox: an array determining the level of each box
c     icolbox: the column of each box (within its level)
c     nboxes: total number of boxes in the tree
c     maxid: maximum box id used (>= nboxes)
c     iparentbox: parent of each box
c     ichildbox: the two children of each box
c     nblevel: total number of boxes per level
c     iboxlev: the array in which the boxes are arranged
c              (in ascending order of level) 
c     istartlev: the pointer to where each level begins in
c                the iboxlev array
c
c--------------------------------------------------------
c
      subroutine quasiunitree(levelbox, icolbox, nboxes, 
     1           maxid, nlev, iparentbox, ichildbox, 
     2           nblevel, iboxlev, istartlev, nleaf)
      implicit real*8 (a-h,o-z)
      integer nboxes, maxid, nlev, nleaf
      integer levelbox(1)
      integer icolbox(1) 
      integer iparentbox(1), ichildbox(2,1)
      integer nblevel(0:1)
      integer iboxlev(1), istartlev(0:1)
c
      nlev=0
      nnds=1
      do while(nnds .lt. nleaf) 
        nlev=nlev+1
        nnds=nnds*2
      enddo
c
      if(nnds .eq. nleaf) then
c       nleaf = 2**nlev, a real uniform tree        
        call unitree(levelbox, icolbox, nboxes, maxid,
     1       nlev, iparentbox, ichildbox, nblevel, 
     2       iboxlev, istartlev)
      else
c       some extra nodes on the finest level
        nextra= nleaf-nnds/2
c       make a uniform tree of level nlev-1
        nlevuni=nlev-1
        call unitree(levelbox, icolbox, nboxes, maxid,
     1       nlevuni, iparentbox, ichildbox, nblevel, 
     2       iboxlev, istartlev)
c
        lev=nlevuni
        istart=istartlev(lev)
        iend=istartlev(lev)+nextra-1

c       subdivide the first few boxes 
        do ii=istart,iend
          ibox=iboxlev(ii)
          call subdivide(ibox,levelbox,icolbox,nboxes,
     1         maxid,nlevuni,iparentbox,ichildbox,nblevel, 
     2         iboxlev, istartlev)
        enddo 
      endif

      end subroutine







c--------------------------------------------------------
c     subroutine getnumqutree
c--------------------------------------------------------
c
c     Memory query for a quasi-uniform tree
c     
c     INPUT:
c     nleaf: number of leaf nodes
c     OUTPUT:
c     nboxes: number of boxes
c     nlev: number of levels
c
c--------------------------------------------------------
c
      subroutine getnumqutree(nleaf, nboxes, nlev)
      implicit real*8 (a-h,o-z)
      integer nleaf, nboxes, nlev

      nlev=0
      nnds=1
      nboxes=1
      do while(nnds .lt. nleaf) 
        nlev=nlev+1
        nnds=nnds*2
        nboxes=nboxes+nnds
      enddo
c
      if(nnds .gt. nleaf) then
c        nextra=nleaf-nnds/2
c        nboxes=nboxes-nnds+2*nextra 
        nboxes=nboxes+2*nleaf-2*nnds
      endif


      end subroutine 






c--------------------------------------------------
c     subroutine treesort
c--------------------------------------------------
c
c     Given the tree structure and a set of discrete
c     points, sort the points into boxes of the tree
c
c     INPUT:
c     nlev-istartlev: the tree structure, see
c          subroutine mktree for details
c     xpts: coordinates of the points
c     npts: number of points
c     cent0: center of the root box
c     xsize0: size of the root box
c     
c     OUTPUT:
c     npbox: number of sources in each box 
c                     (including non-leaf boxes)
c     ipold: ids of points on input
c            (before the overwriting)
c     istartbox: starting point in the array xpts
c                for each box
c     ibofp: indices of leaf boxes that the point
c            belongs to (after overwriting)
c
c     (xpts: overwritten, reordered so that points in
c            a box are stored in adjacent places)
c
c--------------------------------------------------
c
      subroutine treesort(nlev,levelbox,iparentbox,ichildbox,
     1           icolbox,nboxes,maxid, nblevel,iboxlev,
     2           istartlev,xpts,npts,npbox,ipold,istartbox,
     3           ibofp,cent0,xsize0)
      implicit real*8 (a-h,o-z)
c     global vars
      integer nlev, nboxes, npts, maxid
      integer levelbox(1), iparentbox(1)
      integer ichildbox(2,1)
      integer icolbox(1) 
      integer nblevel(0:nlev), iboxlev(1)
      integer istartlev(0:nlev)
      integer npbox(maxid), ipold(npts)
      integer istartbox(maxid), ibofp(npts)
      real*8 xpts(npts), cent0, xsize0
c     local vars
      integer nsinchild(2)
      integer, allocatable:: isinchild(:,:)
      integer, allocatable:: itmp(:)
      real*8, allocatable:: xtmp(:)
c
      allocate(isinchild(npts,2))
      allocate(itmp(npts))
      allocate(xtmp(npts))
c
      do i=1,maxid
        npbox(i)=0
        istartbox(i)=-1
      enddo
c      
      do j=1,npts
        ipold(j)=j
      enddo
c
      iroot=iboxlev(istartlev(0))

      npbox(iroot)=npts
      istartbox(iroot)=1
c
c    the root box contains all the points
c    RMK: in the current version, root box doesn't
c         necessarily have index 1
c         use iroot=iboxlev(istartlev(0))
c         instead of 1
c
c----------------------------------
c
      ix=1
      xlength=xsize0
      x0=cent0-xsize0/2.0d0
      nside=1
c
      do l=0,nlev-1
c       go through all the levels
        xlength=xlength/2.0d0
        nside=nside*2

        istart=istartlev(l)
        iend=istartlev(l)+nblevel(l)-1

        do ii=istart,iend
          ibox=iboxlev(ii)
c         go through all non-empty boxes that have children
          if((npbox(ibox) .gt. 0) .and. 
     1               (ichildbox(1,ibox)).gt.0) then
c-------------------
            iss=istartbox(ibox)
            ies=istartbox(ibox)+npbox(ibox)-1
c
            do kk=1,2
              nsinchild(kk)=0
              ic=ichildbox(kk,ibox)
              npbox(ic)=0
            enddo
c
c             go through all the sources points in the box
c             (which occupy adjacent spots in issorted)      
            do j=iss,ies
              xx=xpts(j)
              xtmp(j)=xx
              itmp(j)=ipold(j)
c                 iss-th to ies-th entries of xpts
c                 and ipold copied to xtmp and itmp at
c                 corresponding places
              ix=ceiling((xx-x0)/xlength)

              if(ix.le.0) then
                ix=1
              endif

              if(ix.gt.nside) then
                ix=nside
              endif
c
              do kk=1,2
                ic=ichildbox(kk,ibox)
                if(icolbox(ic).eq.ix) then
                  nsinchild(kk)=nsinchild(kk)+1
                  npbox(ic)=npbox(ic)+1
                  isinchild(nsinchild(kk),kk)=j
                endif
              enddo
            enddo
c
c           all points in box ibox sorted into isinchild,
c           indicating which child the points belong to. 
c           now overwrite the iss-th to ies-th spot in issorted
c
            jpt=iss
            do jc=1,2
              ic=ichildbox(jc,ibox)
              istartbox(ic)=jpt
              do js=1,nsinchild(jc)
                idnow=isinchild(js,jc)
                xpts(jpt)=xtmp(idnow)  
                ipold(jpt)=itmp(idnow)
                jpt=jpt+1
              enddo
            enddo
c-------------------
          endif
        enddo
c       go to the next level
      enddo
c
c     now get information for ibofp
      do l=0,nlev
        istart=istartlev(l)
        iend=istartlev(l)+nblevel(l)-1
        do ii=istart,iend
          ibox=iboxlev(ii)
          if((ichildbox(1,ibox).lt.0).and.(npbox(ibox).gt.0)) then
            iss=istartbox(ibox)
            ies=iss+npbox(ibox)-1
            do kk=iss,ies
              ibofp(kk)=ibox
            enddo
          endif
        enddo
      enddo
c
c
      deallocate(isinchild)
      deallocate(itmp)
      deallocate(xtmp)


      end subroutine






c-----------------------------------------------------
c     subroutine rvecnew2old
c-----------------------------------------------------
c
c Reorder a vector from the new order to the old order
c given permutation ipold that maps new indices 
c to the old ones
c
c INPUT:
c ndim: dimension of the vector
c nvec: length of the vector
c rvec: the real vector  
c ipold: permutation vector that maps the new indices
c        to the old ones
c
c OUTPUT:
c rvec: rewritten in the new order
c
c-----------------------------------------------------
c
      subroutine rvecnew2old(ndim, nvec, rvec, ipold)
      implicit real*8 (a-h,o-z)
      integer ndim, nvec
      integer ipold(nvec)
      real*8 rvec(ndim,nvec)    
      real*8, allocatable:: rnew(:,:)
c
      allocate(rnew(ndim,nvec))
c
      do i=1,nvec
      do j=1,ndim
        rnew(j,i)=rvec(j,i)
      enddo
      enddo
c
      do i=1,nvec
        io=ipold(i)
        do j=1,ndim
          rvec(j,io)=rnew(j,i)
        enddo
      enddo
c
      deallocate(rnew)
      
      end subroutine






c-----------------------------------------------------
c     subroutine rvecold2new
c-----------------------------------------------------
c
c Reorder a vector from the old order to the new order
c given permutation ipold that maps new indices 
c to the old ones
c
c INPUT:
c ndim: dimension of the vector
c nvec: length of the vector
c rvec: the real vector  
c ipold: permutation vector that maps the new indices
c        to the old ones
c
c OUTPUT:
c rvec: rewritten in the old order
c
c-----------------------------------------------------
c
      subroutine rvecold2new(ndim, nvec, rvec, ipold)
      implicit real*8 (a-h,o-z)
      integer ndim, nvec
      integer ipold(nvec)
      real*8 rvec(ndim,nvec)    
      real*8, allocatable:: rold(:,:)
c
      allocate(rold(ndim,nvec))
c
      do i=1,nvec
      do j=1,ndim
        rold(j,i)=rvec(j,i)
      enddo
      enddo
c
      do i=1,nvec
        io=ipold(i)
        do j=1,ndim
          rvec(j,i)=rold(j,io)
        enddo
      enddo
c
      deallocate(rold)
      
      end subroutine





c--------------------------------------------------
c     subroutine posbox
c--------------------------------------------------
c
c     This subroutine gives the end points of a 
c     given box in a given tree
c     
c     INPUT:
c     icol: number of column (within the current level)
c     level: the current level
c
c     OUTPUT:
c     xa, xb: end points of the box
c      
c--------------------------------------------------
c
      subroutine posbox(xa,xb,icol,level,cent0,xsize0)
      implicit real*8 (a-h,o-z)
      integer icol, level
      real*8 cent0, xsize0, xa, xb
      
      xlength = xsize0 / dble(2**level)
      x0 = cent0-xsize0/2.0d0

      xa=x0+(icol-1)*xlength
      xb = xa+xlength

      end subroutine





c--------------------------------------------------
c     subroutine printtree
c--------------------------------------------------
c
c     this subroutine prints end points of leaf
c     boxes into a given file, it also returns
c     the number of leaf boxes in the tree
c
c--------------------------------------------------
c
      subroutine printtree(levelbox,icolbox,nboxes,
     1           nlev,iparentbox,ichildbox,nblevel,
     2           iboxlev,istartlev,iprint,nleaves,
     3           cent0,xsize0)
      implicit real*8 (a-h,o-z)
      integer levelbox(1)
      integer nlev, nboxes
      integer icolbox(1) 
      integer iparentbox(1), ichildbox(2,1)
      integer nblevel(0:1), iboxlev(1)
      integer istartlev(0:1)
      real*8 cent0, xsize0 
c
      nleaves=0
      do l=0,nlev
        istart=istartlev(l)
        iend=istart+nblevel(l)-1
        do i=istart,iend
          ibox=iboxlev(i)
          if(ichildbox(1,ibox).lt.0) then
            nleaves=nleaves+1
            if(iprint .gt. 0) then
c             if a leaf node, print out end points
              call posbox(xa,xb,icolbox(ibox),l,
     1             cent0,xsize0)
              write(iprint,*) xa,xb
            endif
          endif
        enddo
      enddo
c
      if(iprint .gt. 0) then
        write(*,*) 'tree data saved in file',iprint
        close(iprint)
      endif
c
      end subroutine





c--------------------------------------------------
c     subroutine printbylev
c--------------------------------------------------
c
c     given a binary tree,
c     this subroutine prints end points of boxes 
c     and level info boxes into a given file, 
c     it also returns the number of leaf boxes in 
c     the tree
c
c--------------------------------------------------
c
      subroutine printbylev(levelbox,icolbox,nboxes,
     1           nlev,iparentbox,ichildbox,nblevel,
     2           iboxlev,istartlev,iprint,nleaves,
     3           cent0,xsize0)
      implicit real*8 (a-h,o-z)
      integer levelbox(1)
      integer nlev, nboxes
      integer icolbox(1) 
      integer iparentbox(1), ichildbox(2,1)
      integer nblevel(0:1), iboxlev(1)
      integer istartlev(0:1)
      real*8 cent0, xsize0 
c
      nleaves=0
      do l=0,nlev
        istart=istartlev(l)
        iend=istart+nblevel(l)-1
        do i=istart,iend
          ibox=iboxlev(i)
          if(iprint .gt. 0) then
c           if a leaf node, print out end points
            call posbox(xa,xb,icolbox(ibox),l,
     1           cent0,xsize0)
            write(iprint,*) xa, -1.0d0*l
            write(iprint,*) xb, -1.0d0*l
          endif
c
          if(ichildbox(1,ibox).lt.0) then
            nleaves=nleaves+1
          endif
        enddo
      enddo
c
      if(iprint .gt. 0) then
        write(*,*) 'tree data saved in file',iprint
        close(iprint)
      endif
c
      end subroutine






c--------------------------------------------------------
c     subroutine isibling
c--------------------------------------------------------
c
c     determines if two given boxes of a tree are 
c     siblings, quite self explanatory
c
c     trivial, but makes the code more readable
c
c--------------------------------------------------------
c
      subroutine isibling(ib1, ib2, iparentbox, isib)
      implicit real*8 (a-h,o-z)
      integer ib1, ib2, isib
      integer iparentbox(1)
c
      ip1=iparentbox(ib1)
      ip2=iparentbox(ib2)
c
      if(ip1 .eq. ip2) then
        isib=1
      else
        isib=0
      endif
c
      end subroutine






c--------------------------------------------------
c     subroutine mktreef
c--------------------------------------------------
c
c     the following subroutine is used to generate 
c     a tree given a right hand side, fright.  
c     the algorithm works by testing the function 
c     values vs. an approximating polynomial and 
c     dividing if necessary.  
c
c     it is important to note that the tree generated 
c     by this algorithm may not satisfy the level 
c     restriction.  
c
c     it may be necessary to call the routine fixtree 
c     after this to make it compatible with certain
c     fast transform codes
c
c     input:
c
c     maxboxes denotes the maximum number of boxes allowed
c     maxlevel denotes the deepest level allowed
c     itemparray is just a dummy array
c
c     ndeg denotes the spatial order
c     eps denotes the error bound that determines 
c         when refinement take place
c     h is the real function that is the right hand side
c       of the poisson equation
c
c     cent0: center of the region covered
c     xsize0: side length of the unit box 
c
c     output:
c
c     istartlev is the pointer to where each level
c               begins in the iboxlev array
c     levelbox is an array determining the level of each box
c     nboxes is the total number of boxes
c     maxid is the max of box id (may be different from
c           nboxes)
c     nlev is the finest level
c     icolbox denotes the column of each box
c     iparentbox denotes the parent of each box
c     ichildbox denotes the four children of each box
c     nblevel is the total number of boxes per level
c     iboxlev is the array in which the boxes are arranged
c
c--------------------------------------------------
c
      subroutine mktreef(levelbox, icolbox, nboxes,
     1           maxid, nlev, iparentbox, ichildbox,
     2           nblevel, iboxlev, istartlev, maxboxes,
     3           itemparray, maxlevel, eps, h, ndeg, 
     4           cent0, xsize0)
c-----global variables
      implicit real*8 (a-h,o-z)
      integer nlev, nboxes, maxid
      integer maxlevel, maxboxes, ndeg
      integer levelbox(1), icolbox(1)
      integer iparentbox(1), ichildbox(2,1)
      integer nblevel(0:1), iboxlev(1)
      integer istartlev(0:1), itemparray(1)
      real *8  eps, h
      real *8 cent0, xsize0
c-----local variables
      real *8  coefftemp(ndeg), xf(ndeg), ftemp(ndeg)
      real *8  wsave2(1000)
c
      do i = 0, maxlevel
        nblevel(i) = 0
        istartlev(i) = 0
      enddo
c
      do i = 1, maxboxes
        iboxlev(i) = 0
      enddo
c
c     first set the big parent box to the 
c     appropriate settings:
c     (initially, there is just one box and
c     it is the big parent box at level 0)
c
      ibox = 1
      levelbox(ibox) = 0
      icolbox(ibox) = 1
      iparentbox(ibox) = -1
      ichildbox(1,ibox) = -1
      ichildbox(2,ibox) = -1
c
      nboxes = 1
      maxid = 1
      nlev = 0
c
c     we also need to initialize the adaptive 'ladder'
c     structures to the correct initial values:
      nblevel(0) = 1
      istartlev(0) = 1
      iboxlev(1) = 1
c
      call chxcin(ndeg,wsave2)
c
      do i = 0, maxlevel - 1
      iflag = 0
      istart = istartlev(i)
      iend = istart + nblevel(i) - 1
      do j = istart, iend
       ibox = iboxlev(j)

       aroot=cent0-xsize0/2.0d0
       broot=cent0+xsize0/2.0d0
       xlength=xsize0/dble(2**levelbox(ibox))
c
       call mkgrid(levelbox(ibox), icolbox(ibox), aroot,
     1      broot, ndeg, aa, bb, xf)
c
       do jj = 1, ndeg
         ftemp(jj) = h(xf(jj))
       end do
c
c      compute chebyshev transforms
       call getcoeff(ndeg,ftemp,coefftemp)

       call geterror(ndeg,coefftemp,error)

       hh = dble(2**levelbox(ibox))
       epscaled = eps * hh

       if(error .ge. epscaled)then
         call subdivide(ibox,levelbox,icolbox,
     1        nboxes,maxid,nlev,iparentbox,ichildbox,
     2        nblevel,iboxlev,istartlev)
         iflag = 1
       endif
      end do
      if(iflag .eq. 0)then
c      nothing was divided at the
c      last level, so exit the loop.
       return
      endif
      end do

      return
      end subroutine





c---------------------------------------------------
c     a wrapper of utils/chebex.f/chexfc
c     for the convenience of the calling seq
c
c     RMK: chxcin is called inside
c          no need to do it before calling getcoeff
c
c---------------------------------------------------
c
      subroutine getcoeff(n, fdat, coeff)
      implicit real *8 (a-h,o-z)
      integer n
      real*8 fdat(n), coeff(n) 
      real *8 wsave2(1000), work(1000)
c
      call chxcin(n, wsave2)
      call chexfc(fdat, n, coeff, wsave2, work)
c
      end subroutine





c---------------------------------------------------
c     subroutine geterror
c---------------------------------------------------
c
      subroutine geterror(n, coeff, error)
      implicit none
      integer n
      real*8 coeff(n), error
c
      error=abs(coeff(n))+abs(coeff(n-1))
c
      end subroutine





c--------------------------------------------------------
c     subroutine chebyp2k
c--------------------------------------------------------
c
c     This subroutine interpolates function values on a 
c     chebyshev grid to the chebyshev grids of the two 
c     children boxes 
c
c     INPUT:
c     ndeg: order of Chebyshev approx
c     valp: function values on parent box
c
c     OUTPUT:
c     valk1: function values on kid 1
c     valk2: function values on kid 2
c
c     (remember to update the function values on children
c      boxes when subdividing, to maintain the function
c      value array that makes sense for the current tree)
c
c--------------------------------------------------------
c
      subroutine chebyp2k(ndeg,valp,valk1,valk2)
      implicit real*8 (a-h,o-z)
      integer ndeg
      real*8 valp(ndeg)
      real*8 valk1(ndeg), valk2(ndeg)
c     local vars
      real*8 wsave(1000), work(1000), valtmp(ndeg)
      real*8 xf(ndeg), coeffp(ndeg), cent0
      real*8 aroot, broot, a, b
c
      cent0=0.0d0
c
      do k1=1,ndeg
        valtmp(k1)=valp(k1)
      enddo
c       not sure if the following calls would destroy
c       the original values, so I copy it over to be safe
c
      call chxcin(ndeg, wsave)
      call chexfc(valtmp,ndeg,coeffp,wsave,work)
c          got the chebyshev coefficients of the 
c          parent box. now it remains to evaluate
c          the chebyshev series on the grids of children
c
      aroot=-0.5d0
      broot=0.5d0
c     
      call mkgrid(1, 1, aroot, broot, ndeg, a, b, xf)  
      do k1=1,ndeg
        x=xf(k1)
        call cheval(x,valk1(k1),coeffp,ndeg,aroot,broot)
      enddo
c          child 1
c
      call mkgrid(1, 2, aroot, broot, ndeg, a, b, xf)  
      do k1=1,ndeg
        x=xf(k1)
        call cheval(x,valk2(k1),coeffp,ndeg,aroot,broot)
      enddo
c          child 2
c
      end subroutine





c----------------------------------------------------------
c     subroutine chebyk2p
c----------------------------------------------------------
c
c     This subroutine generates the Chebyshev coefficients 
c     on a parent box, given function values on the cheby 
c     grids of children boxes. (to be used to determine the 
c     resolution of the function on the parent box)
c
c     INPUT:
c     ndeg: order of Chebyshev approx
c     valk1: function values on kid 1
c     valk2: function values on kid 2
c
c     OUTPUT:
c     valp: function values on parent box
c     coeffp: chebyshev coefficients on parent box
c
c     (remember to save the function values on parent box, when
c      doing a merging)
c
c------------------------------------------------------------------
c
      subroutine chebyk2p(ndeg, valk1, valk2, valp, coeffp)
      implicit real*8 (a-h,o-z)
      integer ndeg
      real*8 valk1(ndeg), valk2(ndeg)
      real*8 valp(ndeg), coeffp(ndeg)
c     local vars
      real*8 wsave(1000), work(1000)
      real*8 xf(ndeg), yf(ndeg)
      real*8 coeffk1(ndeg), coeffk2(ndeg)
      real*8 cent0
c
      call chxcin(ndeg, wsave)
c
      call chexfc(valk1,ndeg,coeffk1,wsave,work)
      call chexfc(valk2,ndeg,coeffk2,wsave,work)
c          got the chebyshev coefficients of each kid
c
      aroot=-0.5d0
      broot=0.5d0
      call mkgrid(0, 1, aroot, broot, ndeg, a, b, xf)
c          pretend that the parent box is the root box
c          (since scale doesn't matter here)
c
      do k1=1,ndeg
        x=xf(k1)
        if(x.gt.0.0d0) then
          a=0.0d0
          b=0.5d0
          call cheval(x,valp(k1),coeffk2,ndeg,a,b)
        else
          a=-0.5d0
          b=0.0d0
          call cheval(x,valp(k1),coeffk1,ndeg,a,b)
        endif
      enddo
c       interpolated to the cheby grid on the parent box
c
      call chexfc(valp,ndeg,coeffp,wsave,work) 
c

      end subroutine





c--------------------------------------------------------
c     subroutine schebyp2k
c--------------------------------------------------------
c
c     This subroutine interpolates function values on a 
c     chebyshev grid to the chebyshev grids of the two 
c     children boxes 
c
c     This is the system's version tailored to subroutine rasysnwtn
c
c     INPUT:
c     ndim: dimension of the system
c     ndeg: order of Chebyshev approx
c     valp: function values on parent box
c
c     OUTPUT:
c     valk1: function values on kid 1
c     valk2: function values on kid 2
c
c     (remember to update the function values on children
c      boxes when subdividing, to maintain the function
c      value array that makes sense for the current tree)
c
c--------------------------------------------------------
c
      subroutine schebyp2k(ndim,ndeg,valp,valk1,valk2)
      implicit real*8 (a-h,o-z)
      integer ndim, ndeg
      real*8 valp(ndim, ndeg)
      real*8 valk1(ndim, ndeg), valk2(ndim, ndeg)
c     local vars
      real*8, allocatable:: tvalp(:,:)
      real*8, allocatable:: tvalk1(:,:), tvalk2(:,:)
      allocate(tvalp(ndeg,ndim))
      allocate(tvalk1(ndeg,ndim))
      allocate(tvalk2(ndeg,ndim))
c
      do j=1, ndim
      do i=1, ndeg
        tvalp(i,j)=valp(j,i)
      enddo
      enddo
c     reorder valp -> tvalp
c
      do j=1,ndim
        call chebyp2k(ndeg,tvalp(1,j),tvalk1(1,j),
     1       tvalk2(1,j))
      enddo
c
      do j=1, ndeg
      do i=1, ndim
        valk1(i,j)=tvalk1(j,i)
        valk2(i,j)=tvalk2(j,i)
      enddo
      enddo
c     reorder tvalk1 -> valk1, tvalk2 -> valk2
c
      deallocate(tvalp)
      deallocate(tvalk1)
      deallocate(tvalk2)

      end subroutine





c----------------------------------------------------------
c     subroutine schebyk2p
c----------------------------------------------------------
c
c     This subroutine generates the Chebyshev coefficients 
c     on a parent box, given function values on the cheby 
c     grids of children boxes. (to be used to determine the 
c     resolution of the function on the parent box)
c
c     This is the system's version
c
c     INPUT:
c     ndim: dimension of the system
c     ndeg: order of Chebyshev approx
c     valk1: function values on kid 1
c     valk2: function values on kid 2
c
c     OUTPUT:
c     valp: function values on parent box
c
c     (remember to save the function values on parent box, when
c      doing a merging)
c
c------------------------------------------------------------------
c
      subroutine schebyk2p(ndim, ndeg, valk1, valk2, valp)
      implicit real*8 (a-h,o-z)
      integer ndim, ndeg
      real*8 valk1(ndim, ndeg), valk2(ndim, ndeg)
      real*8 valp(ndim, ndeg)
c     local vars
      real*8, allocatable:: tvalp(:,:), coeffp(:,:)
      real*8, allocatable:: tvalk1(:,:), tvalk2(:,:)

      allocate(tvalp(ndeg, ndim))
      allocate(coeffp(ndeg, ndim))
      allocate(tvalk1(ndeg, ndim))
      allocate(tvalk2(ndeg, ndim))
c
      do j=1, ndim
      do i=1, ndeg
        tvalk1(i,j)=valk1(j,i)
        tvalk2(i,j)=valk2(j,i)
      enddo
      enddo
c     reorder valk1 -> tvalk1, valk2 -> tvalk2
c
      do j=1, ndim
        call chebyk2p(ndeg, tvalk1(1,j), tvalk2(1,j),
     1       tvalp(1,j), coeffp(1,j))
      enddo
c
      do j=1, ndeg
      do i=1, ndim
        valp(i,j)=tvalp(j,i)
      enddo
      enddo
c     reorder tvalp -> valp
c
      deallocate(tvalp)
      deallocate(coeffp)
      deallocate(tvalk1)
      deallocate(tvalk2)

      end subroutine







c----------------------------------------------------------
c     subroutine mktreelf
c----------------------------------------------------------
c
c     This subroutine generates a tree given subintervals
c     as leaf nodes (stopes when maxboxes or maxlevel is
c     reached)
c
c----------------------------------------------------------
c
      subroutine mktreelf(levelbox, icolbox, nboxes, maxid,
     1           nlev, iparentbox, ichildbox, nblevel, 
     2           iboxlev, istartlev, cent0, xsize0, maxboxes, 
     3           maxlevel, nch, endpts)
c     global variables
      implicit real*8 (a-h,o-z)
      integer nlev, nboxes, maxid, nch
      integer maxlevel, maxboxes
      integer levelbox(1), icolbox(1)
      integer iparentbox(1), ichildbox(2,1)
      integer nblevel(0:1), iboxlev(1)
      integer istartlev(0:1)
      integer nsinchild(2)
      real*8 endpts(nch+1)
      real *8 cent0, xsize0
c     allocatables
      integer, allocatable:: levtmp(:), leaflist(:)
      integer, allocatable:: npbox(:), ipold(:)
      integer, allocatable:: istartbox(:), ibofp(:)
c     tree sorting related
      integer, allocatable:: isinchild(:,:)
      integer, allocatable:: itmp(:)
      real*8, allocatable:: xpts(:)
      real*8, allocatable:: xtmp(:)
c
      allocate(levtmp(nch))
      allocate(xpts(nch+1))
c
      allocate(leaflist(maxboxes))
c
      allocate(npbox(maxboxes))
      allocate(ipold(nch+1))
      allocate(istartbox(maxboxes))
      allocate(ibofp(nch+1))
c
      allocate(isinchild(nch+1,2))
      allocate(itmp(nch+1))
      allocate(xtmp(nch+1))

      aroot = endpts(1)
      broot = endpts(nch+1)
      cent0=(aroot+broot)/2.0d0
      xsize0=broot-aroot
c
c     1. find the min of levels of leafnodes
      xlen=endpts(2)-endpts(1)
      levtmp(1)=nint(log(xsize0/xlen)/log(2.0d0))
      levmin=levtmp(1)
c
c     go over all subintervals
      do l=2, nch
        aa=endpts(l)
        bb=endpts(l+1)
        xlen=bb-aa
        levtmp(l)=nint(log(xsize0/xlen)/log(2.0d0))
        levmin=min(levmin,levtmp(l))
      enddo
c      write(*,*) 'levmin=',levmin
c
c     create a uniform tree of level levmin
      nlev = levmin
      call unitree(levelbox, icolbox, nboxes, maxid,
     1     nlev, iparentbox, ichildbox, nblevel, 
     2     iboxlev, istartlev)
c
c     2. sort the end points of subintervals into the tree
      do i=1, nch+1
        xpts(i)=endpts(i)
      enddo
      npts=nch+1
c
      call treesort(nlev,levelbox,iparentbox,ichildbox,
     1     icolbox,nboxes,maxid, nblevel,iboxlev,
     2     istartlev,xpts,npts,npbox,ipold,istartbox,
     3     ibofp,cent0,xsize0)
c
c
c     3. go over leaf boxes, if there is a point in the middle
c        of the box, subdivide
c        remember to pass the points into newly generated boxes
      idiv =  0
      nstep = 0
      do while((nlev .lt. maxlevel))
        idiv = 0
        nstep = nstep+1
c        write(*,*) ''
c        write(*,*) 'nstep=',nstep
        call getleaflist(levelbox, icolbox, nboxes,
     1       maxid, nlev, iparentbox, ichildbox, 
     2       nblevel, iboxlev, istartlev, nleaf, leaflist)
c
        do ii=1, nleaf
          ibox=leaflist(ii)
          if(npbox(ibox) .gt. 0) then
c            write(*,*) ibox, npbox(ibox)
            lev = levelbox(ibox)
            icol = icolbox(ibox)
            call mkinterval(lev, icol, aroot, broot, aa, bb)
            xlen=bb-aa
            nside=2**(lev+1)
c            write(*,*) lev, icol, aa, bb, nside
c           go through points in box: ibox
            do ipt= 1, npbox(ibox)
              xx = xpts(istartbox(ibox)+ipt-1)
              rdist1 = abs(xx-aa)/xlen
              rdist2 = abs(xx-bb)/xlen
              rdist = min(rdist1, rdist2)
c              write(*,*) '...', ibox, ipt, xx, rdist
              if(rdist .gt. 1.0d-12) then
                iparbox = ibox
                call subdivide(iparbox, levelbox, icolbox, nboxes,
     1               maxid, nlev, iparentbox, ichildbox, nblevel, 
     2               iboxlev, istartlev)
c
c               mark status of division for this round: true
                idiv = 1
c                write(*,*) 'iparbox=',iparbox
c                write(*,*) 'idiv=',idiv
c                write(*,*) 'nlev=',nlev
c
c               sort points into kids (to be modified)
c-------------------
                iss=istartbox(ibox)
                ies=istartbox(ibox)+npbox(ibox)-1
c
                do kk=1,2
                  nsinchild(kk)=0
                  ic=ichildbox(kk,ibox)
                  npbox(ic)=0
                enddo
c
c               go through all the sources points in the box
c               (which occupy adjacent spots in issorted)      
c                write(*,*) 'iss=',iss
c                write(*,*) 'ies=',ies
                do j=iss,ies
                  xx=xpts(j)
                  xtmp(j)=xx
                  itmp(j)=ipold(j)
c                 iss-th to ies-th entries of xpts
c                 and ipold copied to xtmp and itmp at
c                 corresponding places
                  ix=ceiling((xx-aroot)/xlen*2.0d0)
c                  write(*,*) '...',j, xx, ix

                  if(ix.le.0) then
                    ix=1
                  endif

                  if(ix.gt.nside) then
                    ix=nside
                  endif
c
                  do kk=1,2
                    ic=ichildbox(kk,ibox)
                    if(icolbox(ic).eq.ix) then
                      nsinchild(kk)=nsinchild(kk)+1
                      npbox(ic)=npbox(ic)+1
                      isinchild(nsinchild(kk),kk)=j
                    endif
c                    write(*,*) '......' 
c                    write(*,*) kk, ic
c                    write(*,*) nsinchild(kk)
c                    write(*,*) npbox(ic)
c                    write(*,*) isinchild(nsinchild(kk),kk)
c                    write(*,*) '......' 
                  enddo
                enddo
c
c               all points in box ibox sorted into isinchild,
c               indicating which child the points belong to. 
c               now overwrite the iss-th to ies-th spot in issorted
c
                jpt=iss
                do jc=1,2
                  ic=ichildbox(jc,ibox)
                  istartbox(ic)=jpt
                  do js=1,nsinchild(jc)
                    idnow=isinchild(js,jc)
                    xpts(jpt)=xtmp(idnow)  
                    ipold(jpt)=itmp(idnow)
                    jpt=jpt+1
                  enddo
                enddo
c-------------------
                exit
              endif
            enddo
c           done checking points in box: ibox
          endif
        enddo
c
c       stop if this round hasn't done anything
        if(idiv .eq. 0) then
          exit
        endif
c       
      enddo
c     end of the outer loop      
c      
      deallocate(levtmp)
      deallocate(xpts)
c
      deallocate(leaflist)
c
      deallocate(npbox)
      deallocate(ipold)
      deallocate(istartbox)
      deallocate(ibofp)
c
      deallocate(isinchild)
      deallocate(itmp)
      deallocate(xtmp)

      end subroutine




c--------------------------------------------------
c     subroutine printtree2
c--------------------------------------------------
c
c     this subroutine prints end points of all the 
c     boxes into a given file, it also returns
c     the number of leaf boxes in the tree
c
c--------------------------------------------------
c
      subroutine printtree2(levelbox,icolbox,nboxes,
     1           nlev,iparentbox,ichildbox,nblevel,
     2           iboxlev,istartlev,iprint,nleaves,
     3           cent0,xsize0)
      implicit real*8 (a-h,o-z)
      integer levelbox(1)
      integer nlev, nboxes
      integer icolbox(1) 
      integer iparentbox(1), ichildbox(2,1)
      integer nblevel(0:1), iboxlev(1)
      integer istartlev(0:1)
      real*8 cent0, xsize0 
c
      nleaves=0
      do l=0,nlev
        istart=istartlev(l)
        iend=istart+nblevel(l)-1
        do i=istart,iend
          ibox=iboxlev(i)
c
          if(iprint .gt. 0) then
c           if a leaf node, print out end points
            call posbox(xa,xb,icolbox(ibox),l,
     1           cent0,xsize0)
            write(iprint,*) xa, l
            write(iprint,*) xb, l
          endif
c
          if(ichildbox(1,ibox).lt.0) then
            nleaves=nleaves+1
          endif
        enddo
      enddo
c
      if(iprint .gt. 0) then
        write(*,*) 'tree data saved in file',iprint
        close(iprint)
      endif
c
      end subroutine






