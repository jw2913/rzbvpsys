      program noadap
      implicit real*8 (a-h,o-z)
      integer ndeg, nch
      parameter(ndeg=16)
      parameter(nch=18)
      real*8 endpts(nch+1)
      real*8 usol(ndeg,nch), uder(ndeg,nch), eps
      external peval, qeval, feval
c
      write(*,*) 'shock wave example:'
      write(*,*) 'initial grid loaded from endpts.dat.'
      open(unit=12,file='endpts.dat') 
      do i=1,nch+1
        read(12,*) endpts(i)
      enddo
      close(12)
c     endpoints of subintervals
c     manually refined toward x=0
c
      eps=1.0d-5
c     a parameter for the visous shock problem
c     not error tolerance... 

      bcl0=1.0d0
      bcl1=0.0d0
      bclv=-1.0d0

      bcr0=1.0d0
      bcr1=0.0d0
      bcrv=1.0d0
c     boundary conditions
c
      call rbvpnoadap(ndeg, nch, endpts,
     1    bcl0, bcl1, bclv, bcr0, bcr1, bcrv, 
     2    peval, qeval, feval, eps, usol, uder)
c     call the real bvp solver, non-adaptive version
c
      write(*,*) 'solution and derivative written in fort.41'
      do ii=1,nch
        do jj=1,ndeg
          write(41,*) usol(jj,ii), uder(jj,ii)
        enddo
      enddo
 

      end program




c---------------------------------
c     user provided functions
c
      subroutine peval(x, p, eps)
      implicit real*8 (a-h,o-z)
      real*8 x, p, eps

      p=2.0d0*x/eps

      end subroutine




      subroutine qeval(x, q, eps)
      implicit real*8 (a-h,o-z)
      real*8 x, q, eps

      q=0.0d0

      end subroutine




      subroutine feval(x, f, eps)
      implicit real*8 (a-h,o-z)
      real*8 x, f, eps

      f=0.0d0

      end subroutine
