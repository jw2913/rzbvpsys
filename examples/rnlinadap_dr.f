c     testing driver 
c     which tests the subroutine rnlinadap
c
c     example taken from Starr & Rokhlin (1994)
c
c-----------------------------------------
c     the bvp:
c     u'(x) = F(x,u(x)) 
c              (x \in [xa, xb])    (1)
c     bca*u(xa) + bcb*u(xb) = bcv  (2)
c
c     u\in R^2
c     where F is a user-specified function
c     given by feval, with its derivative dF/dy
c     given bt fyeval
c
c     bca, bcb, bcv, xa, xb are given at the beginning
c
c-----------------------------------------
c
      program test
      implicit real*8 (a-h,o-z)
      integer ndim, ndeg, nmax
      parameter(ndim=3)      
      parameter(ndeg=16)
      parameter(nch=2)
      parameter(nmax=100000)
c     tree related 
      integer nboxes, maxid, nlev
      integer levelbox(nmax)
      integer icolbox(nmax) 
      integer iparentbox(nmax), ichildbox(2,nmax)
      integer nblevel(0:nmax)
      integer iboxlev(nmax), istartlev(0:nmax)
      integer leaflist(nmax), itemparray(nmax)
c     the BC matrices and vec
      real*8 bca(ndim,ndim), bcb(ndim,ndim), bcv(ndim)
      real*8 pars(2), ua(ndim), ub(ndim)
c     the solution and its derivative
      real*8 endpts(nch+1)
      real*8 xtarg(nmax)
      real*8 usol(ndim,nmax), uext(ndim,nmax)
      real*8 ud(ndim,nmax), udext(ndim,nmax)
c     testing
      real*8 chpts(ndeg,nmax), fsol(ndim)
      external feval, fyeval, yeval0, ydeval0
      external uexact, uder, ftest
c
c     0. define the BC and constants
      pars(1)=0.5d0
      pars(2)=1.854074677301372d0
c     Elliptic Jacobi functions
c     pars(1)=m, pars(2)=K
c
      do j=1, ndim
      do i=1, ndim
        bca(i,j)=0.0d0
        bcb(i,j)=0.0d0
      enddo
      enddo
c
      bca(1,1)=1.0d0
      bca(2,2)=1.0d0
      bcb(3,3)=1.0d0
c
      xa=0.0d0
      xb=4.0d0*pars(2)
c
      bcv(1)=0.0d0
      bcv(2)=1.0d0
      bcv(3)=1.0d0
c
c      write(*,*) ua(1), ua(2), ub(1), ub(2)
c
c      write(*,*) 'bcv='
c      do i=1, ndim
c        write(*,*) bcv(i)
c      enddo
c     end of the definition of the problem
c
c-------------------------------------------
c
c     prepare endpts (should be clustered near c1 and c2,
c     but a fine enough grid should be okay too)
      endpts(1)=xa
      endpts(nch+1)=xb
      h=(endpts(nch+1)-endpts(1))/nch
      do i=2, nch
        endpts(i)=(i-1)*h
      enddo
c
c-------------------------------------------
c
c     prepare the target points xtarg
      ntarg=1000
      h=(xb-xa)/(ntarg-1)
      do i=1, ntarg
        xtarg(i)=xa+(i-1)*h
      enddo
c
      maxdiv=10
      rtol=1.0d-10
      maxstep=10
c     reduce it to 2 if it doesn't stop
c
c     remember to change feval, fyeval, yeval0, ydeval0 below!
      call rnlinadap(ndim, ndeg, nch, endpts,
     1     bca, bcb, bcv, feval, fyeval, yeval0,
     2     ydeval0, pars, maxdiv, rtol, maxstep,
     3     ntarg, xtarg, usol, ud)
c
      do k=1, ntarg
        x=xtarg(k)
        call feval(x,usol(1,k),fsol,pars)
        write(211,*) x, usol(1,k), usol(2,k), usol(3,k)
      enddo
c     solution written in fort.211
c     use gen_test_data_ellipj.m to generate the true solution
c     (using ellipj in MATLAB)
c     use get_err.m to compare the solutions
c

      end program




c
c-----------------------------------------
c
      subroutine fyeval(x,y,fy,pars) 
      implicit real*8 (a-h,o-z)
      real*8 x, y(3),pars(2)
      real*8 fy(3,3)
c     Elliptic Jacobi functions
c     pars(1)=m, pars(2)=K
c
      fy(1,1)=0.0d0
      fy(1,2)=y(3)
      fy(1,3)=y(2)
c
      fy(2,1)=-y(3)
      fy(2,2)=0.0d0
      fy(2,3)=-y(1)
c
      fy(3,1)=-pars(1)*y(2)
      fy(3,2)=-pars(1)*y(1)
      fy(3,3)=0.0d0
c
      end subroutine




      subroutine feval(x,y,f,pars)
      implicit real*8 (a-h,o-z)
      real*8 x, y(3), pars(2)
      real*8 f(3)
c     Elliptic Jacobi functions
c     pars(1)=m, pars(2)=K
c      
      f(1)=y(2)*y(3)
      f(2)=-y(1)*y(3)
      f(3)=-pars(1)*y(1)*y(2)
c
      end subroutine




c----------------------------------------------
      subroutine yeval0(x,y0,pars)
      implicit real*8 (a-h,o-z)
      real*8 x, y0(3), pars(2)
      real*8 pi
c
      pi=3.1415926535897932d0
      const=pi/2.0d0/pars(2)      
c
c      y0(1)=sin(x)
c      y0(2)=cos(x)-1.0d0
c      y0(3)=1.0d0-1.0d0
c
      y0(1)=sin(const*x)
      y0(2)=cos(const*x)-1.0d0
      y0(3)=1.0d0-1.0d0
c
c      y0(1)=0.0d0
c      y0(2)=0.0d0
c      y0(3)=0.0d0

      end subroutine





      subroutine ydeval0(x,yd0,pars)
      implicit real*8 (a-h,o-z)
      real*8 x, yd0(3), pars(2)
      real*8 pi
c
      pi=3.1415926535897932d0
      const=pi/2.0d0/pars(2)      
c
c      yd0(1)=cos(x)
c      yd0(2)=-sin(x)
c      yd0(3)=0.0d0
c
      yd0(1)=const*cos(const*x)
      yd0(2)=-const*sin(const*x)
      yd0(3)=0.0d0
c
c      yd0(1)=0.0d0
c      yd0(2)=0.0d0
c      yd0(3)=0.0d0


      end subroutine






