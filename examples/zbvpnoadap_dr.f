c-----------------------------------------
c
c     An example driver code for zbvpnoadap
c     (see bvpmain.f/zbvpnoadap for the calling seq)
c
c     which solves the viscous shock problem:
c
c     u''(x)+ u'(x) + u(x) = i*exp(i*x)
c     u(-1)=exp(-i)
c     -i*u(1)=-i*exp(i)
c
c------------------------------------------

      program noadap
      implicit real*8 (a-h,o-z)
      integer ndeg, nch
      parameter(ndeg=16)
      parameter(nch=10)
      real*8 endpts(nch+1)
      complex*16 bcl0, bcl1, bclv
      complex*16 bcr0, bcr1, bcrv
      complex*16 usol(ndeg,nch), uder(ndeg,nch)
      complex*16 eye, pars
      external peval, qeval, feval
c     for tests
      real*8 chpts(ndeg,nch)
      complex*16 uext, uderext
c
      eye=dcmplx(0.0d0,1.0d0)
c
      xa=-1.0d0
      xb=1.0d0
      endpts(1)=xa
      h=2.0d0/nch
      do i=1,nch
        endpts(i+1)=xa+i*h 
      enddo
c     endpoints of subintervals
c     5 equispaced subintervals for example
c
      bcl0=1.0d0
      bcl1=0.0d0
      bclv=exp(-eye)

      bcr0=-eye
      bcr1=0.0d0
      bcrv=-eye*exp(eye)
c     boundary conditions
c
      call zbvpnoadap(ndeg, nch, endpts,
     1    bcl0, bcl1, bclv, bcr0, bcr1, bcrv, 
     2    peval, qeval, feval, pars, usol, uder)
c     call the complex bvp solver, non-adaptive version
c
      do j=1,nch
        aa=endpts(j)
        bb=endpts(j+1)
        call chnodes(aa,bb,ndeg,chpts(1,j),u,v,u1,v1)
      enddo
c
      do ii=1,nch
        do jj=1,ndeg
          uext=exp(eye*chpts(jj,ii))   
          uderext=eye*uext
          write(51,*) abs(usol(jj,ii)-uext),
     1                abs(uder(jj,ii)-uderext)
        enddo
      enddo
 

      end program




c---------------------------------
c     user provided functions
c
      subroutine peval(x, p, pars)
      implicit real*8 (a-h,o-z)
      real*8 x
      complex*16 pars, p

      p=dcmplx(1.0d0,0.0d0)

      end subroutine




      subroutine qeval(x, q, pars)
      implicit real*8 (a-h,o-z)
      real*8 x
      complex*16 pars, q

      q=dcmplx(1.0d0,0.0d0)

      end subroutine




      subroutine feval(x, f, pars)
      implicit real*8 (a-h,o-z)
      real*8 x
      complex*16 pars, f, eye

      eye=dcmplx(0.0d0,1.0d0)

      f=eye*exp(eye*x)

      end subroutine
