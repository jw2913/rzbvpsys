format long
f=chebfun(@(x) 1/sqrt(1-0.5*sin(x)^2),[0,pi/2]);
K=sum(f,0,pi/2)
const=pi/2/K

dat=load('test_ellipj.dat');
dat1=load('fort.211');
dat2=zeros(size(dat));

% the initial data, plot it too
dat2(:,1)=dat(:,1);
dat2(:,2)=sin(const*dat2(:,1));
dat2(:,3)=cos(const*dat2(:,1));
dat2(:,4)=1.0d0;

figure(1)
plot(dat(:,1),dat(:,2),'r-');
hold on;
plot(dat(:,1),dat(:,3),'g-');
plot(dat(:,1),dat(:,4),'b-');

figure(2)
plot(dat1(:,1),dat1(:,2),'r-');
hold on;
plot(dat1(:,1),dat1(:,3),'g-');
plot(dat1(:,1),dat1(:,4),'b-');


plot(dat2(:,1),dat2(:,2),'r-.');
hold on;
plot(dat2(:,1),dat2(:,3),'g-.');
plot(dat2(:,1),dat2(:,4),'b-.');


err_abs=abs(dat-dat1);
err_abs(:,1)=dat(:,1);
format long
err_abs(1:10,:)
%norm(dat1(:,1)-dat(:,1),inf)
err_inf=max(max(max(err_abs(:,2:4))))

save('test_ellipj.err','err_abs','-ascii','-double')
