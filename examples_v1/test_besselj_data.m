dat=load('fort.111');
xx=dat(:,1);
y100=besselj(100,xx);
y99=besselj(99,xx);
y98=besselj(98,xx);
figure(3)
plot(dat(:,1),dat(:,2),'r');
hold on
plot(dat(:,1),dat(:,3),'g');
plot(dat(:,1),dat(:,4),'b');
%
err100=norm(y100-dat(:,2),2)/norm(y100,2)
err99=norm(y99-dat(:,3),2)/norm(y99,2)
err98=norm(y98-dat(:,4),2)/norm(y98,2)

dat2=load('fort.112');
figure(4)
plot(dat2(:,1),dat2(:,2),'r');
hold on
plot(dat2(:,1),dat2(:,3),'g');
plot(dat2(:,1),dat2(:,4),'b');
