%unu = @(nu) chebop(@(x,u) x^2*diff(u,2)+x*diff(u)+(x^2-nu^2)*u,[0,600],0,1)\0;
unu = @(nu) chebop(@(x,u) diff(u,2)+1/x*diff(u)+(x^2-nu^2)/x^2*u,[0,600],0,1)\0;
nu = 100;
tic, u = unu(nu); t = toc;
clf, plot(u), grid on
title(sprintf('nu = %3d   Length =%4d   Time =%6.3f',nu,length(u),t))
