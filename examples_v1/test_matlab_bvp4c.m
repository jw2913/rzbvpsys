% Y = BVP4C(ODEFUN, BCFUN, Y0) applies the standard BVP4C method to solve a
%    boundary-value problem. ODEFUN and BCFUN are as in BVP4C. The Y0 argument is
%    a CHEBFUN that represents the initial guess to the solution Y. Its domain
%    defines the domain of the problem, and the length of the CHEBFUN Y0 is used
%    to set the number of points in an initial equispaced mesh. Note that it is
%    not necessary to call BVPINIT.

% EXAMPLE:
% besselj functions

format long
n=100;
d=[1e-5,600];
% domain of the ode
%
ellipjode = @(x,u) [u(2);-(x^2-n^2-n)/x^2*u(1)-(1/x)*u(3);u(4);-(x^2-n^2+n)/x^2*u(3)-(1/x)*u(5);u(6);(1/x)*u(3)-(x^2-n^2+5*n-6)/x^2*u(5)];
%
g1=0.030598170290372796;
g2=0.015416721257492013;
g3=-0.025526503991812874;
ellipjbc = @(ua,ub) [ua(1);ua(3);ua(5);ub(2)-g1;ub(4)-g2;ub(6)-g3];
solinit.x=d(1):0.01:d(2);
solinit.y=ones(6,numel(solinit.x));

opts=bvpset('reltol',1e-9)
ejsol=bvp4c(ellipjode, ellipjbc, solinit, opts);
yn=ejsol.y(1,:);
yn1=ejsol.y(3,:);
yn2=ejsol.y(5,:);
xx=ejsol.x;
plot(xx,yn,'r')
hold on
plot(xx,yn1,'g')
plot(xx,yn2,'b')


