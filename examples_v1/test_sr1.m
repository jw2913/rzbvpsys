f1=@(t) 12*exp(-t)-5*exp(-1000*t)+6*(t-1)-0.004*(t-0.001+0.001*exp(-1000*t));
f2=@(t) -6*exp(-t)+5*exp(-1000*t)-3*(t-1)+0.004*(t-0.001+0.001*exp(-1000*t));
t=0:0.0001:1;
y1=f1(t);
y2=f2(t);
figure(1)
plot(t,y1,'r-')
hold on
plot(t,y2,'b-')

dat=load('fort.111');
plot(dat(:,1),dat(:,2),'r-.')
plot(dat(:,1),dat(:,3),'b-.')

