c     testing driver (of the global bvp system solver)
c     which tests the subroutines:
c
c     ssetbc, sbctab (dealing with the BC)
c     formsmat, slocsolve, reorderrhs (inside slocsolve) 
c     (forming the linear system and solving it)   
c
c     the bvp:
c
c     u'(x)+p(x)*u(x)=f(x)
c     bca*u(xa)+bcb*u(xb)=bcv
c
c     u\in R^2
c
c     where p and f are functions given by peval and feval
c     bca, bcb, bcv, xa, xb are given at the beginning
c     the exact solution and derivative are given by 
c     subroutines uexact and uder
c
c     RMK: the interfaces of subroutines formsmat and slocsolve
c     are modified now, remember to modify testing drivers
c     test_slocsolve.f test_slocsolve1.f test_slocsolve2.f
c     before running again
c
      program test
      implicit real*8 (a-h,o-z)
      integer ndim, ndeg
      parameter(ndim=2)      
      parameter(ndeg=32)
c     the spectral integration matrices
      real*8 cftm(ndeg,ndeg), citm(ndeg,ndeg)
      real*8 spdef(ndeg)
      real*8 spbx(ndeg,ndeg), spxb(ndeg,ndeg)
      real*8 sdvl(ndim,ndeg*ndim), sdvr(ndim,ndeg*ndim)
      real*8 svlvr(ndeg*ndim,ndeg*ndim)
      real*8 chpts(ndeg)
c     the constant matrices appearing in the eqn
      real*8 vl(ndim,ndim), vr(ndim,ndim)
      real*8 ui(ndim)
c     the rhs mat and vec
      real*8 phi(ndim,ndim,ndeg), fval(ndim,ndeg)
c     the solution mat and vec
      real*8 xi(ndim,ndim,ndeg), eta(ndim,ndeg)
      real*8 xvec(ndim*ndeg), yvec(ndim*ndeg)
      real*8 usol(ndim,ndeg), uext(ndim,ndeg)
      real*8 ud(ndim,ndeg), udext(ndim,ndeg)
c     the BC matrices and vec
      real*8 bca(ndim,ndim), bcb(ndim,ndim), bcv(ndim)
c     misc vars
      real*8 pars(1)
      real*8 sigma(ndim*ndeg)
      real*8 yt(ndim*ndeg), ye(ndim*ndeg)
      external peval, feval, uexact, uder
c
c     0. define the BC
c     (leave them here, but they are not needed in the test of the bie)
      bca(1,1)=4.0d0
      bca(2,1)=0.0d0
      bca(1,2)=1.0d0
      bca(2,2)=5.0d0
c
      bcb(1,1)=0.1d0
      bcb(2,1)=-0.5d0
      bcb(1,2)=-0.3d0
      bcb(2,2)=0.2d0
c
      bcv(1)=0.16d0
      bcv(2)=1.55d0
c
      xa=0.0d0
      xb=1.0d0
      xlength=xb-xa
c
      nn=ndeg*ndim
c
      call setchmat(ndeg, cftm, citm, spdef, spbx, spxb)
      call chnodes(xa, xb, ndeg, chpts, u, v, u1, v1)
c
c     1. call ssetbc to set constants according to the bc
      call ssetbc(ndim, bca, bcb, bcv, ui, vl, vr)
c
c     2. call sbctab to tabulate the rhs and such
      do i=1, ndeg
        x=chpts(i)
        call sbctab(ndim, x, ui, fval(1,i), phi(1,1,i), peval, 
     1       feval, pars)
      enddo
c
c     3. call formsmat to form the spectral integration matrices
c        (system's version)
c     modify the interface
      xlength=xb-xa
      call formsmat(ndim, ndeg, spdef, spbx, spxb,
     1     vl, vr, sdvl, sdvr, svlvr)
c
c     4. call slocsolve to solve the linear system
c     modify the interface
      call slocsolve(ndim, ndeg, svlvr, phi, fval, xlength, eta, xi)
c
c     print out the density functions 
      do j=1, ndeg
        x=chpts(j)
        write(21,*) eta(1,j)
        write(21,*) eta(2,j)
      enddo
c
      do k=1, ndeg
        do j=1, ndim
          do i=1, ndim
            write(22,*) i,j,k,xi(i,j,k)
          enddo
        enddo 
      enddo
c
c     5. recover the solution to the ode
c        at the chebyshev nodes by:
c        u=svlvr(eta)+ui
c
c        assuming that the grid resolves the function
c        then the solution at an arbitary point can be recovered
c        by chebyshev interpolation
c
      do j=1, ndeg
        do i=1, ndim
          xvec((j-1)*ndim+i)=eta(i,j)
        enddo
      enddo
c
      do i=1, nn
        yvec(i)=0.0d0
        do j=1, nn
          yvec(i)=yvec(i)+xlength/2.0d0*svlvr(i,j)*xvec(j)
        enddo 
      enddo
c
      do j=1, ndeg
        do i=1, ndim
          usol(i,j)=yvec((j-1)*ndim+i)+ui(i)
        enddo
      enddo
c
c     compare with the exact solution
      do j=1, ndeg
        x=chpts(j)
        call uexact(x,uext(1,j))
        do i=1, ndim
          write(23,*) usol(i,j), uext(i,j), 
     1                abs(uext(i,j)-usol(i,j))
        enddo
      enddo
c
c     6. recover the derivatives
      do j=1, ndeg
        do i=1, ndim
          ud(i,j)=0.0d0
          do k=1, ndim
            ud(i,j)=ud(i,j)+(vl(i,k)-vr(i,k))*eta(k,j)
          enddo
        enddo
      enddo
c
c     compare with the exact derivatives
      do j=1, ndeg
        x=chpts(j)
        call uder(x,udext(1,j))
        do i=1, ndim
          write(24,*) ud(i,j), udext(i,j), 
     1                abs(udext(i,j)-ud(i,j))
        enddo
      enddo
c

      

      end program




c
c-----------------------------------------
c

      subroutine peval(x,p,pars) 
      implicit real*8 (a-h,o-z)
      real*8 x, pars(1)
      real*8 p(2,2)
c
      p(1,1)=2.0d0
      p(2,1)=0.5d0
      p(1,2)=0.1d0
      p(2,2)=3.0d0
c
      end subroutine




      subroutine feval(x,f,pars)
      implicit real*8 (a-h,o-z)
      real*8 x, pars(1)
      real*8 f(2)

      f(1)=0.22d0*x+0.13d0
      f(2)=0.65d0*x+1.1d0

      end subroutine




      subroutine uexact(x,u)
      implicit real*8 (a-h,o-z)
      real*8 x, u(2)
      
      u(1)=0.1d0*x
      u(2)=0.2d0*x+0.3d0

      end subroutine





      subroutine uder(x,ud)
      implicit real*8 (a-h,o-z)
      real*8 s, ud(2)
c
      ud(1)=0.1d0
      ud(2)=0.2d0
c
      end subroutine 
