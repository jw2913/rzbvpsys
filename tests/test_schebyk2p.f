c     tests the subroutine schebyk2p
c     by assigning a function to the cheb pts on the
c     leaf nodes of a given tree, 
c     then interp to the parent node
c     then compare  
      program test
      implicit real*8 (a-h,o-z)
      integer ndim, nmax, ndeg
      parameter(nmax=100000)
      parameter(ndeg=8)
      parameter(ndim=3)
      integer nboxes, maxid, nlev
      integer levelbox(nmax)
      integer icolbox(nmax) 
      integer iparentbox(nmax), ichildbox(2,nmax)
      integer nblevel(0:nmax)
      integer iboxlev(nmax), istartlev(0:nmax)
      integer leaflist(nmax), itemparray(nmax)
      real*8 chpts(ndeg,nmax), fch(ndim,ndeg,nmax), y(ndim)
      real*8 coeffp(ndeg)
      real*8 ftest, xgrid(nmax), finterp(nmax)
      external ftest
c
      cent0=0.0d0
      xsize0=1.0d0
c
      eps=1.0d-10
      maxboxes=nmax/10
      maxlevel=20
c
      call mktreef(levelbox, icolbox, nboxes,
     1     maxid, nlev, iparentbox, ichildbox,
     2     nblevel, iboxlev, istartlev, maxboxes,
     3     itemparray, maxlevel, eps, ftest, ndeg, 
     4     cent0, xsize0)
      write(*,*) 'nboxes=',nboxes
      write(*,*) 'nlev=',nlev
c
      call getleaflist(levelbox, icolbox, nboxes,
     1     maxid, nlev, iparentbox, ichildbox, 
     2     nblevel, iboxlev, istartlev, 
     3     nleaf, leaflist)
c
      write(*,*) 'nleaf=',nleaf
c
      aroot=cent0-xsize0/2.0d0
      broot=cent0+xsize0/2.0d0
      do i=1, nleaf
        ibox=leaflist(i)
c
        lev=levelbox(ibox)
        icol=icolbox(ibox)
        xlength=xsize0/dble(2**levelbox(ibox))
c
        call mkgrid(lev, icol, aroot, broot, ndeg,
     1       aa, bb, chpts(1,ibox))
        do j=1, ndeg
          xx=chpts(j,ibox)
ccc          fch(j,ibox)=ftest(xx)
          call ftest3(xx,y)
          do k=1, ndim
            fch(k,j,ibox)=y(k)
          enddo
        enddo
      enddo
c
c-----------------------------------------------------
c     test subroutine schebyp2k
c
      ibox=leaflist(1)
c
      call subdivide(ibox, levelbox, icolbox, nboxes,
     1     maxid, nlev, iparentbox, ichildbox, nblevel, 
     2     iboxlev, istartlev)
c
      ikid1=ichildbox(1,ibox)
      ikid2=ichildbox(2,ibox)
c
      call schebyp2k(ndim,ndeg,fch(1,1,ibox),fch(1,1,ikid1),
     1     fch(1,1,ikid2))
c     call schebyp2k to interpolate to the kids
c
c--------------------------
c     test function values on kid1      
      lev=levelbox(ikid1)
      icol=icolbox(ikid1)
      call mkgrid(lev, icol, aroot, broot, ndeg, aa, bb,
     1     chpts(1,ikid1))
      do j=1, ndeg
        xx=chpts(j,ikid1)
cc        fext=ftest(xx)
        call ftest3(xx,y)
        do k=1,ndim
          write(21,*) xx, y(k), fch(k,j,ikid1), 
     1              abs(y(k)-fch(k,j,ikid1))
        enddo
      enddo 
c--------------------------
c     test function values on kid2
      lev=levelbox(ikid2)
      icol=icolbox(ikid2)
      call mkgrid(lev, icol, aroot, broot, ndeg, aa, bb,
     1     chpts(1,ikid2))
      do j=1, ndeg
        xx=chpts(j,ikid2)
cc        fext=ftest(xx)
        call ftest3(xx,y)
        do k=1,ndim
          write(21,*) xx, y(k), fch(k,j,ikid2), 
     1              abs(y(k)-fch(k,j,ikid2))
        enddo
      enddo 

c-----------------------------------------------------
c     test subroutine schebyk2p
      do k=1, ndeg
      do j=1, ndim
        fch(j,k,ibox)=0.0d0
      enddo
      enddo
c     erase the fch value for ibox
c
      call schebyk2p(ndim, ndeg, fch(1,1,ikid1),
     1     fch(1,1,ikid2), fch(1,1,ibox))

c
c--------------------------
c     test function values on ibox
      lev=levelbox(ibox)
      icol=icolbox(ibox)
      call mkgrid(lev, icol, aroot, broot, ndeg, aa, bb,
     1     chpts(1,ibox))
      do j=1, ndeg
        xx=chpts(j,ibox)
        call ftest3(xx,y)
        do k=1,ndim
          write(22,*) xx, y(k), fch(k,j,ibox), 
     1              abs(y(k)-fch(k,j,ibox))
        enddo
      enddo 




      end program




      function ftest(x)
      implicit real*8 (a-h,o-z)
      real*8 x, ftest
c
      ftest=sin(0.5d0*x**2+3.0d0)
c      
      end function



      subroutine ftest3(x,y)
      implicit real*8 (a-h,o-z)
      real*8 x, y(3)
c
      y(1)=sin(0.5d0*x**2+3.0d0)
      y(2)=1.0d0
      y(3)=sin(1.5d0*x**2+3.0d0)
c      
      end subroutine
