c     tests the subroutine chebyk2p
c     by assigning a function to the cheb pts on the
c     leaf nodes of a given tree, 
c     then interp to the parent node
c     then compare  
      program test
      implicit real*8 (a-h,o-z)
      integer nmax, ndeg
      parameter(nmax=100000)
      parameter(ndeg=8)
      integer nboxes, maxid, nlev
      integer levelbox(nmax)
      integer icolbox(nmax) 
      integer iparentbox(nmax), ichildbox(2,nmax)
      integer nblevel(0:nmax)
      integer iboxlev(nmax), istartlev(0:nmax)
      integer leaflist(nmax), itemparray(nmax)
      real*8 chpts(ndeg,nmax), fch(ndeg,nmax)
      real*8 coeffp(ndeg)
      real*8 ftest, xgrid(nmax), finterp(nmax)
      external ftest
c
      cent0=0.0d0
      xsize0=1.0d0
c
      eps=1.0d-10
      maxboxes=nmax/10
      maxlevel=20
c
      call mktreef(levelbox, icolbox, nboxes,
     1     maxid, nlev, iparentbox, ichildbox,
     2     nblevel, iboxlev, istartlev, maxboxes,
     3     itemparray, maxlevel, eps, ftest, ndeg, 
     4     cent0, xsize0)
      write(*,*) 'nboxes=',nboxes
      write(*,*) 'nlev=',nlev
c
      call getleaflist(levelbox, icolbox, nboxes,
     1     maxid, nlev, iparentbox, ichildbox, 
     2     nblevel, iboxlev, istartlev, 
     3     nleaf, leaflist)
c
      write(*,*) 'nleaf=',nleaf
c
      aroot=cent0-xsize0/2.0d0
      broot=cent0+xsize0/2.0d0
      do i=1, nleaf
        ibox=leaflist(i)
c
        lev=levelbox(ibox)
        icol=icolbox(ibox)
        xlength=xsize0/dble(2**levelbox(ibox))
c
        call mkgrid(lev, icol, aroot, broot, ndeg,
     1       aa, bb, chpts(1,ibox))
        do j=1, ndeg
          xx=chpts(j,ibox)
          fch(j,ibox)=ftest(xx)
        enddo
      enddo
c
c-----------------------------------------------------
c     test subroutine chebyp2k
c
      ibox=leaflist(1)
c
      call subdivide(ibox, levelbox, icolbox, nboxes,
     1     maxid, nlev, iparentbox, ichildbox, nblevel, 
     2     iboxlev, istartlev)
c
      ikid1=ichildbox(1,ibox)
      ikid2=ichildbox(2,ibox)
c
      call chebyp2k(ndeg,fch(1,ibox),fch(1,ikid1),fch(1,ikid2))
c     call chebyp2k to interpolate to the kids
c
c--------------------------
c     test function values on kid1      
      lev=levelbox(ikid1)
      icol=icolbox(ikid1)
      call mkgrid(lev, icol, aroot, broot, ndeg, aa, bb,
     1     chpts(1,ikid1))
      do j=1, ndeg
        xx=chpts(j,ikid1)
        fext=ftest(xx)
        write(21,*) xx, fext, fch(j,ikid1), 
     1              abs(fext-fch(j,ikid1))
      enddo 
c--------------------------
c     test function values on kid2
      lev=levelbox(ikid2)
      icol=icolbox(ikid2)
      call mkgrid(lev, icol, aroot, broot, ndeg, aa, bb,
     1     chpts(1,ikid2))
      do j=1, ndeg
        xx=chpts(j,ikid2)
        fext=ftest(xx)
        write(21,*) xx, fext, fch(j,ikid2), 
     1              abs(fext-fch(j,ikid2))
      enddo 

c-----------------------------------------------------
c     test subroutine chebyk2p
      do j=1, ndeg
        fch(j,ibox)=0.0d0
      enddo
c     erase function values for ibox
c     then interpolate from children boxes
c     to recover the values
c
      call chebyk2p(ndeg, fch(1,ikid1), fch(1,ikid2),
     1     fch(1,ibox), coeffp)
c     interpolated back on
c
c     test the function values
      lev=levelbox(ibox)
      icol=icolbox(ibox)
      call mkgrid(lev, icol, aroot, broot, ndeg, aa, bb,
     1     chpts(1,ibox))
      do j=1, ndeg
        xx=chpts(j,ibox)
        fext=ftest(xx)
        write(21,*) xx, fext, fch(j,ibox), 
     1              abs(fext-fch(j,ibox))
      enddo 



      end program




      function ftest(x)
      implicit real*8 (a-h,o-z)
      real*8 x, ftest
c
      ftest=sin(0.5d0*x**2+3.0d0)
c      
      end function
