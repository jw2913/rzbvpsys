c     tests the subroutine sfinterp
c     by assigning a function to the cheb pts on the
c     leaf nodes of a given tree, 
c     then interp to a regular grid,
c     then compare  
      program test
      implicit real*8 (a-h,o-z)
      integer nmax, ndeg, ndim
      parameter(nmax=100000)
      parameter(ndeg=8)
      parameter(ndim=3)
      integer nboxes, maxid, nlev
      integer levelbox(nmax)
      integer icolbox(nmax) 
      integer iparentbox(nmax), ichildbox(2,nmax)
      integer nblevel(0:nmax)
      integer iboxlev(nmax), istartlev(0:nmax)
      integer leaflist(nmax), itemparray(nmax)
      real*8 chpts(ndeg,nmax), fch(ndim,ndeg,nmax)
      real*8 ftest, xgrid(nmax), finterp(ndim,nmax)
      real*8 fg(ndim)
      external ftest
c
      cent0=0.0d0
      xsize0=1.0d0
c
      eps=1.0d-10
      maxboxes=nmax/10
      maxlevel=20
c
      call mktreef(levelbox, icolbox, nboxes,
     1     maxid, nlev, iparentbox, ichildbox,
     2     nblevel, iboxlev, istartlev, maxboxes,
     3     itemparray, maxlevel, eps, ftest, ndeg, 
     4     cent0, xsize0)
      write(*,*) 'nboxes=',nboxes
      write(*,*) 'nlev=',nlev
c
      call getleaflist(levelbox, icolbox, nboxes,
     1     maxid, nlev, iparentbox, ichildbox, 
     2     nblevel, iboxlev, istartlev, 
     3     nleaf, leaflist)
c
      write(*,*) 'nleaf=',nleaf
c
      aroot=cent0-xsize0/2.0d0
      broot=cent0+xsize0/2.0d0
      do i=1, nleaf
        ibox=leaflist(i)
c
        lev=levelbox(ibox)
        icol=icolbox(ibox)
        xlength=xsize0/dble(2**levelbox(ibox))
c
        call mkgrid(lev, icol, aroot, broot, ndeg,
     1       aa, bb, chpts(1,i))
        do j=1, ndeg
          xx=chpts(j,i)
          call feval(xx,fch(1,j,i))
          do k=1, ndim
            write(201,*) k,j,i, xx, fch(k,j,i)
          enddo
        enddo
      enddo
c
      ngrid=1000
      h=xsize0/ngrid
c
      do i=1, ngrid
        xgrid(i)=aroot+h*i
      enddo
c
      call sinterpf(nlev, levelbox, iparentbox,
     1     ichildbox, icolbox, nboxes, maxid,
     2     nblevel, iboxlev, istartlev, cent0,
     3     xsize0, nleaf, leaflist, ndim, ndeg, 
     4     fch, ngrid, xgrid, finterp)
c
      do i=1, ngrid
        xg=xgrid(i)
        call feval(xg,fg)
        do k=1, ndim
          fi=finterp(k,i)
          write(202,*) xg, fg(k), fi, abs(fg(k)-fi)
        enddo
      enddo



      end program




c------------------------------------
c     a testing function, to make the tree
c     taken to be the first component of
c     feval
      function ftest(x)
      implicit real*8 (a-h,o-z)
      real*8 x, ftest
c
      ftest=sin(0.5d0*x**2+3.0d0)
c      
      end function






c------------------------------------
c     the function to be interpolated
c
      subroutine feval(x,fval)
      implicit real*8 (a-h,o-z)
      real*8 x, fval(3)
c
      fval(1)=sin(0.5d0*x**2+3.0d0)
      fval(2)=2.0d0*x+3.0d0
      fval(3)=exp(-x**2/2.0d0)


      end subroutine
