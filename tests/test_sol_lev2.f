c     testing driver (of the global/2-level bvp system solver)
c     which tests the subroutines:
c
c     schd2par, spar2chd, sevaldens
c     spreqompq, squadloc
c
c     the bvp:
c
c     u'(x)+p(x)*u(x)=f(x)
c     bca*u(xa)+bcb*u(xb)=bcv
c
c     u\in R^2
c
c     where p and f are functions given by peval and feval
c     bca, bcb, bcv, xa, xb are given at the beginning
c     the exact solution and derivative are given by 
c     subroutines uexact and uder
c
c     it works by comparing the global solution and the 2-level
c     local solution patched together

      program test
      implicit real*8 (a-h,o-z)
      integer ndim, ndeg, nmax
      parameter(ndim=2)      
      parameter(ndeg=32)
      parameter(nmax=100)
      integer idbox(nmax), leaflist(nmax)
      real*8 xlength, xlen(2)
c     the spectral integration matrices
      real*8 cftm(ndeg,ndeg), citm(ndeg,ndeg)
      real*8 spdef(ndeg)
      real*8 spbx(ndeg,ndeg), spxb(ndeg,ndeg)
      real*8 sdvl(ndim,ndeg*ndim), sdvr(ndim,ndeg*ndim)
      real*8 svlvr(ndeg*ndim,ndeg*ndim)
      real*8 chpts(ndeg), chpts2(ndeg,2)
c     the constant matrices appearing in the eqn
      real*8 vl(ndim,ndim), vr(ndim,ndim)
      real*8 ui(ndim)
c     the rhs mat and vec
      real*8 phi(ndim,ndim,ndeg), fval(ndim,ndeg)
      real*8 phi2(ndim,ndim,ndeg,2), fval2(ndim,ndeg,2)
c     the solution mat and vec
      real*8 xi(ndim,ndim,ndeg), eta(ndim,ndeg)
      real*8 xi2(ndim,ndim,ndeg,2), eta2(ndim,ndeg,2)
      real*8 eta1(ndim,ndeg,2)
      real*8 xvec(ndim*ndeg), yvec(ndim*ndeg)
      real*8 usol(ndim,ndeg,2), uext(ndim,ndeg,2)
      real*8 ud(ndim,ndeg,2), udext(ndim,ndeg,2)
c     the BC matrices and vec
      real*8 bca(ndim,ndim), bcb(ndim,ndim), bcv(ndim)
c     the inner products and coupling coeffs
      real*8 alist(ndim,ndim,2,3), dlist(ndim,2,3)
      real*8 alist2(ndim,ndim,2,3), dlist2(ndim,2,3)
      real*8 clist(ndim,3)
c     the local quadrature related
      real*8 quadl(ndim,nmax), quadr(ndim,nmax)
      real*8 qloc(ndim,ndeg,nmax)
c     misc vars
      real*8 pars(1)
      real*8 sigma(ndim*ndeg)
      real*8 yt(ndim*ndeg), ye(ndim*ndeg)
      external peval, feval, uexact, uder
c
c     0. define the BC
c     (leave them here, but they are not needed in the test of the bie)
      bca(1,1)=4.0d0
      bca(2,1)=0.0d0
      bca(1,2)=1.0d0
      bca(2,2)=5.0d0
c
      bcb(1,1)=0.1d0
      bcb(2,1)=-0.5d0
      bcb(1,2)=-0.3d0
      bcb(2,2)=0.2d0
c
      bcv(1)=0.16d0
      bcv(2)=1.55d0
c
      xa=0.0d0
      xb=1.0d0
      xc=0.5d0 
      xlength=xb-xa
      xlen(1)=0.5d0
      xlen(2)=0.5d0
c
      nn=ndeg*ndim
c
      call setchmat(ndeg, cftm, citm, spdef, spbx, spxb)
      call chnodes(xa, xb, ndeg, chpts, u, v, u1, v1)
c
c     1. call ssetbc to set constants according to the bc
      call ssetbc(ndim, bca, bcb, bcv, ui, vl, vr)
      do i=1, ndim
        write(*,*) i,ui(i)
      enddo
c
      write(*,*) ' '
      do j=1, ndim
      do i=1, ndim
        write(*,*) i,j,vl(i,j), vr(i,j)
      enddo
      enddo


c
c     2. call sbctab to tabulate the rhs and such
      do i=1, ndeg
        x=chpts(i)
        call sbctab(ndim, x, ui, fval(1,i), phi(1,1,i), peval, 
     1       feval, pars)
      enddo
c
c     3. call formsmat to form the spectral integration matrices
c        (system's version)
c     modify the interface
      xlength=xb-xa
      call formsmat(ndim, ndeg, spdef, spbx, spxb,
     1     vl, vr, sdvl, sdvr, svlvr)
c
c     4. call slocsolve to solve the linear system
c     modify the interface
      call slocsolve(ndim, ndeg, svlvr, phi, fval, xlength, eta, xi)
c
c     print out the density functions 
      do j=1, ndeg
        x=chpts(j)
        write(21,*) x, eta(1,j), eta(2,j)
      enddo
c
      do k=1, ndeg
        do j=1, ndim
          do i=1, ndim
            write(22,*) i,j,k,xi(i,j,k)
          enddo
        enddo 
      enddo
c
c--------------------------------------------
c
c     now do the two level solve
      nch=2
c     1. call chnodes on each subinterval
      call chnodes(xa, xc, ndeg, chpts2(1,1), u, v, u1, v1)
      call chnodes(xc, xb, ndeg, chpts2(1,2), u, v, u1, v1)
c
c     test printout chpts2
c      do j=1, nch
c      do i=1, ndeg
c        write(101,*) chpts2(i,j), 0.0d0
c      enddo
c      enddo
c
c     2. ui, vl, vr remain the same
c     call sbctab on the two intervals to tabulate fval2 and phi2
      do j=1, nch
      do i=1, ndeg
        x=chpts2(i,j)
        call sbctab(ndim, x, ui, fval2(1,i,j), phi2(1,1,i,j), peval, 
     1       feval, pars)
c        write(102,*) x,fval2(1,i,j), fval2(2,i,j)
c        write(103,*) x,phi2(1,1,i,j), phi2(2,1,i,j),
c     1                   phi2(1,2,i,j), phi2(2,2,i,j)
      enddo
      enddo
c
c     3. formsmat is also global, no change needed
c
c     4. call slocsolve on each subinterval to do the two local solves
      do j=1, nch
        call slocsolve(ndim, ndeg, svlvr, phi2(1,1,1,j), fval2(1,1,j),
     1       xlen(j), eta2(1,1,j),xi2(1,1,1,j)) 
      enddo
c      
c     test print out of eta2 and xi2
c      do ich=1, nch
c      do j=1, ndeg
c        x=chpts(j)
c        write(121,*) eta2(1,j,ich)
c        write(121,*) eta2(2,j,ich)
c      enddo
c
c      do k=1, ndeg
c        do j=1, ndim
c          do i=1, ndim
c            write(122,*) i,j,k,ich,xi2(i,j,k,ich)
c          enddo
c        enddo 
c      enddo
c      enddo
c
c--------------------------------------------
c     compute the inner products on each interval directly
c
      call smkprod(ndim, ndeg, xlength, eta, xi, sdvl, sdvr, 
     1     alist(1,1,1,1), alist(1,1,2,1), dlist(1,1,1), dlist(1,2,1))
c
      call smkprod(ndim, ndeg, xlen(1), eta2(1,1,1), xi2(1,1,1,1), 
     1     sdvl, sdvr, alist(1,1,1,2), alist(1,1,2,2), 
     2     dlist(1,1,2), dlist(1,2,2))
c
      call smkprod(ndim, ndeg, xlen(2), eta2(1,1,2), xi2(1,1,1,2), 
     1     sdvl, sdvr, alist(1,1,1,3), alist(1,1,2,3), 
     2     dlist(1,1,3), dlist(1,2,3))
c
c     copy over alist and dlist on children intervals
      do ibox=2,3
        do ilr=1,2
          do j=1, ndim
            do i=1, ndim
              alist2(i,j,ilr,ibox)=alist(i,j,ilr,ibox)
            enddo
            dlist2(j,ilr,ibox)=dlist(j,ilr,ibox)
          enddo
        enddo
      enddo 
c
      ipar=1
      icha=2
      ichb=3
      call schd2par(ndim, ipar, icha, ichb, nmax, alist2, dlist2)
c
      do ibox=1, 3
        do ilr=1, 2
          write(103,*) alist(1,1,ilr,ibox), alist(2,1,ilr,ibox),
     1                 alist(1,2,ilr,ibox), alist(2,2,ilr,ibox)
          write(104,*) alist2(1,1,ilr,ibox), alist2(2,1,ilr,ibox),
     1                 alist2(1,2,ilr,ibox), alist2(2,2,ilr,ibox)

          write(105,*) dlist(1,ilr,ibox), dlist(2,ilr,ibox)
          write(106,*) dlist2(1,ilr,ibox), dlist2(2,ilr,ibox)
        enddo
      enddo
c
c     subroutine schd2par passed the test. proceed on with spar2chd
c
c--------------------------------------------
c
c     test the subroutine spar2chd
c     set the coupling coeffs for the root box to be zero
      clist(1,1)=0.0d0
      clist(2,1)=0.0d0
c     
      ipar=1
      icha=2
      ichb=3
      call spar2chd(ndim, ipar, icha, ichb, nmax,
     1     alist, dlist, clist)
c
      do ibox=1, 3
        do i=1, ndim
          write(107,*) i, ibox, clist(i,ibox)
        enddo
      enddo
c
c     test the subroutine sevaldens
      idbox(1)=2
      idbox(2)=3
      do ich=1, nch
        call sevaldens(ndim, ndeg, clist(1,idbox(ich)), eta2(1,1,ich),
     1       xi2(1,1,1,ich), eta1(1,1,ich))
      enddo
c
c     eta1: the final density function value at chpts2
      do ich=1, nch
      do ipt=1, ndeg
        x=chpts2(ipt,ich)
        write(108,*) x, eta1(1,ipt,ich), eta1(2,ipt,ich)
      enddo
      enddo
c
c     subroutine spar2chd passed the test too!
c     so is subroutine sevaldens!
c
c--------------------------------------------
c
c     test the subroutines sprecompq and squadloc
c
      nleaf=2
      leaflist(1)=2
      leaflist(2)=3
c
      call sprecompq(ndim, nleaf, leaflist, alist, dlist, 
     1     clist, quadl, quadr)
      do i=1, nleaf
        do k=1, ndim
          write(201,*) k,i,quadl(k,i),quadr(k,i)
        enddo
      enddo
c
      do i=1, nleaf
        call squadloc(ndim, ndeg, xlen(i), eta1(1,1,i), svlvr, 
     1       qloc(1,1,i))
        do k=1, ndeg
        do j=1, ndim
          write(202,*) qloc(j,k,i)
        enddo
        enddo
      enddo
c
      do i=1, nleaf
        do k=1, ndeg
          x=chpts2(k,i)
          usol(1,k,i)=qloc(1,k,i)+quadl(1,i)+quadr(1,i)+ui(1)
          usol(2,k,i)=qloc(2,k,i)+quadl(2,i)+quadr(2,i)+ui(2)
c
          call uexact(x,uext(1,k,i))           
          err1=abs(usol(1,k,i)-uext(1,k,i))
          err2=abs(usol(2,k,i)-uext(2,k,i))
          write(203,*) x, usol(1,k,i), uext(1,k,i), err1 
          write(204,*) x, usol(2,k,i), uext(2,k,i), err2 
        enddo
      enddo
c
c     subroutines sprecompq and squadloc passed the test!
c     the derivative is simply vl*eta1-vr*eta1, btw.
c
      do i=1, nleaf
        do j=1, ndeg
          x=chpts2(j,i)
          do k=1, ndim
            ud(k,j,i)=0.0d0
            do kk=1, ndim
              ud(k,j,i)=ud(k,j,i)+vl(k,kk)*eta1(kk,j,i)
     1                           -vr(k,kk)*eta1(kk,j,i)
c 
            enddo
          enddo
          call uder(x,udext(1,j,i))           
          err1=abs(ud(1,j,i)-udext(1,j,i))
          err2=abs(ud(2,j,i)-udext(2,j,i))
c 
          write(205,*) x, ud(1,j,i), udext(1,j,i), err1 
          write(206,*) x, ud(2,j,i), udext(2,j,i), err2 
c
        enddo
      enddo
c
c     the derivatives are also correct!
c     done with the 2-level solver!
c     all clear!



      end program




c
c-----------------------------------------
c

      subroutine peval(x,p,pars) 
      implicit real*8 (a-h,o-z)
      real*8 x, pars(1)
      real*8 p(2,2)
c
      p(1,1)=2.0d0
      p(2,1)=0.5d0
      p(1,2)=0.1d0
      p(2,2)=3.0d0
c
      end subroutine




      subroutine feval(x,f,pars)
      implicit real*8 (a-h,o-z)
      real*8 x, pars(1)
      real*8 f(2)

      f(1)=0.22d0*x+0.13d0
      f(2)=0.65d0*x+1.1d0

      end subroutine




      subroutine uexact(x,u)
      implicit real*8 (a-h,o-z)
      real*8 x, u(2)
      
      u(1)=0.1d0*x
      u(2)=0.2d0*x+0.3d0

      end subroutine





      subroutine uder(x,ud)
      implicit real*8 (a-h,o-z)
      real*8 s, ud(2)
c
      ud(1)=0.1d0
      ud(2)=0.2d0
c
      end subroutine 
