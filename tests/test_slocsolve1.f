c     testing driver
c     which tests the subroutines:
c
c     ssetbc, sbctab (dealing with the BC)
c     formsmat, slocsolve, reorderrhs (inside slocsolve) 
c     (forming the linear system and solving it)   
c
      program test
      implicit real*8 (a-h,o-z)
      integer ndim, ndeg
      parameter(ndim=2)      
      parameter(ndeg=32)
c     the spectral integration matrices
      real*8 cftm(ndeg,ndeg), citm(ndeg,ndeg)
      real*8 spdef(ndeg)
      real*8 spbx(ndeg,ndeg), spxb(ndeg,ndeg)
      real*8 sdvl(ndim,ndeg*ndim), sdvr(ndim,ndeg*ndim)
      real*8 slvl(ndeg*ndim,ndeg*ndim)
      real*8 srvr(ndeg*ndim,ndeg*ndim)
      real*8 chpts(ndeg)
c     the constant matrices appearing in the eqn
      real*8 vl(ndim,ndim), vr(ndim,ndim)
      real*8 ui(ndim)
c     the rhs mat and vec
      real*8 phi(ndim,ndim,ndeg), fval(ndim,ndeg)
c     the solution mat and vec
      real*8 xi(ndim,ndim,ndeg), eta(ndim,ndeg)
c     the BC matrices and vec
      real*8 bca(ndim,ndim), bcb(ndim,ndim), bcv(ndim)
c     misc vars
      real*8 pars(1)
      real*8 sigma(ndim*ndeg)
      real*8 yt(ndim*ndeg), ye(ndim*ndeg)
      external peval, feval
c
c     0. define the BC
c     (leave them here, but they are not needed in the test of the bie)
      bca(1,1)=4.0d0
      bca(2,1)=0.0d0
      bca(1,2)=1.0d0
      bca(2,2)=5.0d0
c
      bcb(1,1)=0.1d0
      bcb(2,1)=-0.5d0
      bcb(1,2)=-0.3d0
      bcb(2,2)=0.2d0
c
      bcv(1)=0.16d0
      bcv(2)=1.55d0
c
      xa=0.0d0
      xb=1.0d0
      xlength=xb-xa
c
      call setchmat(ndeg, cftm, citm, spdef, spbx, spxb)
      call chnodes(xa, xb, ndeg, chpts, u, v, u1, v1)
c
c     1. call ssetbc to set constants according to the bc
c      call ssetbc(ndim, bca, bcb, bcv, ui, vl, vr)
c     instead of calling subroutine ssetbc, let's
c     set vl and vr manually... first let's try vl=vr=eye(2)
c
      do j=1, ndim
        do i=1, ndim
          vl(i,j)=0.0d0
          vr(i,j)=0.0d0
        enddo
        vl(j,j)=1.0d0
        vr(j,j)=1.0d0
      enddo
c
c      do i=1, ndim
c      do j=1, ndim
c        write(*,*) i,j,vl(i,j), vr(i,j)
c      enddo
c      enddo
c
c----------
c
c
c     2. call sbctab to tabulate the rhs and such
c        (RMK: change this subroutine to test the integral eqn alone)
c        (assign fval and phi manually)
c      do i=1, ndeg
c        x=chpts(i)
c        call sbctab(ndim, x, ui, fval(1,i), phi(1,1,i), peval, 
c     1       feval, pars)
c      enddo
c
      do i=1, ndeg
        x=chpts(i) 
        write(31,*) x
        fval(1,i)=2.0d0*x+2.15d0
        fval(2,i)=x+6.0d0
c
        phi(1,1,i)=2.0d0
        phi(2,1,i)=0.5d0
        phi(1,2,i)=0.1d0
        phi(2,2,i)=3.0d0
      enddo
c----------
c
c     3. call formsmat to form the spectral integration matrices
c        (system's version)
      call formsmat(ndim, ndeg, spdef, spbx, spxb, vl, vr,
     1     sdvl, sdvr, slvl, srvr)
c
c     4. call slocsolve to solve the linear system
      xlength=xb-xa
      call slocsolve(ndim, ndeg, xlength, sdvl, sdvr, slvl, srvr,
     1     phi, fval, eta, xi)
c
      do j=1, ndeg
        x=chpts(j)
        write(21,*) eta(1,j), 2.0d0*x, abs(eta(1,j)-2.0d0*x)
        write(21,*) eta(2,j), x+1.0d0, abs(eta(2,j)-x-1.0d0)
      enddo
c
      do k=1, ndeg
        do j=1, ndim
          do i=1, ndim
            write(22,*) i,j,k,xi(i,j,k)
c           in this case, vl=vr=eye(2), phi=const matrix
c           xi = inv(I-phi)*phi
          enddo
        enddo 
      enddo
c
c----------
      

      end program




c
c-----------------------------------------
c

      subroutine peval(x,p,pars) 
      real*8 x, pars(1)
      real*8 p(2,2)
c
      p(1,1)=2.0d0
      p(2,1)=0.5d0
      p(1,2)=0.1d0
      p(2,2)=3.0d0
c
      end subroutine




      subroutine feval(x,f,pars)
      real*8 x, pars(1)
      real*8 f(2)

      f(1)=0.22d0*x+0.13d0
      f(2)=0.65d0*x+1.1d0

      end subroutine





      subroutine fdens(x,sigma,pars)
      real*8 x, pars(1)
      real*8 sigma(2)

      sigma(1)=2.0d0*x
      sigma(2)=x+1.0d0
c
c      sigma(1)=1.0d0
c      sigma(2)=0.0d0

      end subroutine
