format long
f=chebfun(@(x) 1/sqrt(1-0.5*sin(x)^2),[0,pi/2]);
K=sum(f,0,pi/2)

% generate a plot
M=0.5;
x=0:0.01:4*K;
%x=0:0.01:10;
[sn,cn,dn]=ellipj(x,M);
plot(x,sn,x,cn,x,dn);
legend('SN','CN','DN','Location','best')
grid on
title('Jacobi Elliptic Functions sn,cn,dn')


% generate testing data
% on [0,4*K]
M=0.5;
N=1000;
h=(4*K)/(N-1);
x=0:h:4*K;
x=x.';
[sn,cn,dn]=ellipj(x,M);
data1=[x,sn,cn,dn];
save('test_ellipj.dat','data1','-ascii','-double');


