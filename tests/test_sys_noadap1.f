c     testing driver 
c     which tests the subroutine rsysnoadap
c
c     the bvp:
c
c     u'(x)+p(x)*u(x)=f(x)
c     bca*u(xa)+bcb*u(xb)=bcv
c
c     u\in R^2
c
c     where p and f are functions given by peval and feval
c     bca, bcb, bcv, xa, xb are given at the beginning
c     the exact solution and derivative are given by 
c     subroutines uexact and uder
c
      program test
      implicit real*8 (a-h,o-z)
      integer ndim, ndeg, nmax
      parameter(ndim=2)      
      parameter(ndeg=16)
      parameter(nch=40)
      real*8 endpts(nch+1)
c     the BC matrices and vec
      real*8 bca(ndim,ndim), bcb(ndim,ndim), bcv(ndim)
      real*8 pars(1), ua(ndim), ub(ndim)
c     the solution and its derivative
      real*8 usol(ndim,ndeg,nch), uext(ndim,ndeg,nch)
      real*8 ud(ndim,ndeg,nch), udext(ndim,ndeg,nch)
c     testing
      real*8 chpts(ndeg,nch)
      external peval, feval, uexact, uder
c
c     0. define the BC
c     (leave them here, but they are not needed in the test of the bie)
      bca(1,1)=4.0d0
      bca(2,1)=0.0d0
      bca(1,2)=1.0d0
      bca(2,2)=5.0d0
c
      bcb(1,1)=0.1d0
      bcb(2,1)=-0.5d0
      bcb(1,2)=-0.3d0
      bcb(2,2)=0.2d0
c
      xa=0.0d0
      xb=1.0d0
c
c     define the solution to be two gaussians with c1, d1, c2, d2
c     see also subroutines uexact and uder 
c
      c1=0.0d0
      d1=1.0d-3
c
      c2=1.0d0
      d2=1.0d-2
c
      ua(1)=exp(-(xa-c1)**2/d1)
      ua(2)=exp(-(xa-c2)**2/d2)
c
      ub(1)=exp(-(xb-c1)**2/d1)
      ub(2)=exp(-(xb-c2)**2/d2)
c
      bcv(1)=bca(1,1)*ua(1)+bca(1,2)*ua(2)
     1      +bcb(1,1)*ub(1)+bcb(1,2)*ub(2)
c
      bcv(2)=bca(2,1)*ua(1)+bca(2,2)*ua(2)
     1      +bcb(2,1)*ub(1)+bcb(2,2)*ub(2)
c
      write(*,*) ua(1), ua(2), ub(1), ub(2)
c
      write(*,*) 'bcv='
      do i=1, ndim
        write(*,*) bcv(i)
      enddo
c     end of the definition of the problem
c
c
c     prepare endpts (should be clustered near c1 and c2,
c     but a fine enough grid should be okay too)
      endpts(1)=xa
      endpts(nch+1)=xb
      h=(endpts(nch+1)-endpts(1))/nch
      do i=2, nch
        endpts(i)=(i-1)*h
      enddo
c
      do i=1, nch
        xa=endpts(i)
        xb=endpts(i+1)
        call chnodes(xa, xb, ndeg, chpts(1,i), u, v, u1, v1)
      enddo
c
      call rsysnoadap(ndim, ndeg, nch, endpts,
     1     bca, bcb, bcv, peval, feval, pars,
     2     usol, ud)
c
      do k=1, nch
        do j=1, ndeg
          x=chpts(j,k)
c         write(111,*) usol(1,j,k), ud(1,j,k)
c         write(112,*) usol(2,j,k), ud(2,j,k)
          call uexact(x,uext(1,j,k)) 
          err1=abs(usol(1,j,k)-uext(1,j,k))
          err2=abs(usol(2,j,k)-uext(2,j,k))
          write(111,*) x, usol(1,j,k), uext(1,j,k), err1 
          write(112,*) x, usol(2,j,k), uext(2,j,k), err2 
        enddo
      enddo
     


      end program




c
c-----------------------------------------
c

      subroutine peval(x,p,pars) 
      implicit real*8 (a-h,o-z)
      real*8 x, pars(1)
      real*8 p(2,2)
c
      p(1,1)=2.0d0
      p(2,1)=0.5d0+x
      p(1,2)=0.1d0-x
      p(2,2)=3.0d0
c
      end subroutine




      subroutine feval(x,f,pars)
      implicit real*8 (a-h,o-z)
      real*8 x, pars(1)
      real*8 f(2)
c      
      c1=0.0d0
      d1=1.0d-3
c
      c2=1.0d0
      d2=1.0d-2
c
      exp1=exp(-(x-c1)**2/d1)
      exp2=exp(-(x-c2)**2/d2)
c
      f(1)=-2.0d0*(x-c1)/d1*exp1+2.0d0*exp1
     1    +(0.1d0-x)*exp2

      f(2)=-2.0d0*(x-c2)/d2*exp2+(0.5d0+x)*exp1
     1    +3.0d0*exp2
c

      end subroutine




      subroutine uexact(x,u)
      implicit real*8 (a-h,o-z)
      real*8 x, u(2)
c      
      c1=0.0d0
      d1=1.0d-3
c
      c2=1.0d0
      d2=1.0d-2
c
      u(1)=exp(-(x-c1)**2/d1)
      u(2)=exp(-(x-c2)**2/d2)
c
      end subroutine





      subroutine uder(x,ud)
      implicit real*8 (a-h,o-z)
      real*8 s, ud(2)
c
      c1=0.0d0
      d1=1.0d-3
c
      c2=1.0d0
      d2=1.0d-2
c
      ud(1)=-2.0d0*(x-c1)/d1*exp(-(x-c1)**2/d1)
      ud(2)=-2.0d0*(x-c2)/d2*exp(-(x-c2)**2/d2)
c
      end subroutine 




