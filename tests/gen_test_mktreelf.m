endpts=[-1;0;1];
rng(1);
format long
for idiv = 1:10
  n=length(endpts)-1;
  i=ceil(n*rand());
  i=min([i,n-1]);
  i=max([i,1]);
  xmid=(endpts(i)+endpts(i+1))/2.0;
  endpts=[endpts(1:i);xmid;endpts(i+1:end)];
end
ypts=zeros(size(endpts));
endpts
plot(endpts,ypts,'r*')
save('endpts0.dat','-ascii','-double','endpts');
