c     tests the adaptive solver
c
      program test
      implicit real*8 (a-h,o-z)
      integer ndeg, nch
      parameter(ndeg=16)
      parameter(nch=15)
      real*8 endpts(nch+1), pars(1), eps
      real*8, allocatable:: xtarg(:), usol(:), uder(:)
      external peval, qeval, feval
c
      bcl0=1.0d0
      bcl1=0.0d0
      bclv=-1.0d0

      bcr0=1.0d0
      bcr1=0.0d0
      bcrv=1.0d0
c     boundary conditions
c
      endpts(1)=-1.0d0
      endpts(nch+1)=1.0d0
      h=2.0d0/nch
c
      do i=2, nch
        endpts(i)=endpts(1)+(i-1)*h
      enddo
c     set end points
c
      ntarg=1000
      hh=2.0d0/(ntarg+1)
      allocate(xtarg(ntarg))      
      allocate(usol(ntarg))      
      allocate(uder(ntarg))      
c
      do i=1, ntarg
        xtarg(i)=-1.0d0+hh*i
      enddo
c     set the target points
c
      eps=1.0d-5
      pars(1)=eps     
c
cccccc      maxdiv=12
      maxdiv=6
      rtol=1.0d-9
c
      call rbvpadap(ndeg, nch, endpts,
     1     bcl0, bcl1, bclv, bcr0, bcr1, bcrv, 
     2     peval, qeval, feval, pars, maxdiv, 
     3     rtol, ntarg, xtarg, usol, uder)
c
      do i=1, ntarg
        write(21, *) xtarg(i), usol(i)
      enddo
      write(*,*) 'solution written in fort.21'


      deallocate(xtarg)
      deallocate(usol)
      deallocate(uder)
c
c 
      end program      





c---------------------------------
c     user provided functions
c
      subroutine peval(x, p, eps)
      implicit real*8 (a-h,o-z)
      real*8 x, p, eps

      p=2.0d0*x/eps

      end subroutine




      subroutine qeval(x, q, eps)
      implicit real*8 (a-h,o-z)
      real*8 x, q, eps

      q=0.0d0

      end subroutine




      subroutine feval(x, f, eps)
      implicit real*8 (a-h,o-z)
      real*8 x, f, eps

      f=0.0d0

      end subroutine
