c     tests the adaptive solver
c     this is to be compared with the system's case
c     (tests how the adaptivity works)
c
      program test
      implicit real*8 (a-h,o-z)
      integer ndeg, nch
      parameter(ndeg=16)
      parameter(nch=2)
      real*8 endpts(nch+1), pars(1), eps
      real*8, allocatable:: xtarg(:), usol(:), uder(:)
      external peval, qeval, feval
c
c     coefficients of the BC
      bcl0=1.0d0
      bcl1=0.0d0
      bcr0=1.0d0
      bcr1=0.0d0
c
c     end of the interval
      xa=0.0d0
      xc=1.0d0      
c
c     a bunch of parameters (two gaussians)
      c1=0.0d0
      d1=1.0d-3
      c2=1.0d0
      d2=1.0d-2
c
c     rhs of the BC
      bclv=exp(-(xa-c1)**2/d1)+exp(-(xa-c2)**2/d2)
      bcrv=exp(-(xc-c1)**2/d1)+exp(-(xc-c2)**2/d2)
c
c
c     define the endpoints of subintervals
      endpts(1)=xa
      endpts(nch+1)=xc
      h=2.0d0/nch
c
      do i=2, nch
        endpts(i)=endpts(1)+(i-1)*h
      enddo
c     set end points
c
c
c     define the targets
      ntarg=1000
      allocate(xtarg(ntarg))      
      allocate(usol(ntarg))      
      allocate(uder(ntarg))      
c
      hh=(xc-xa)/(ntarg+1)
      do i=1, ntarg
        xtarg(i)=xa+hh*i
      enddo
c     set the target points
c
      maxdiv=10
      rtol=1.0d-10
c
      call rbvpadap(ndeg, nch, endpts,
     1     bcl0, bcl1, bclv, bcr0, bcr1, bcrv, 
     2     peval, qeval, feval, pars, maxdiv, 
     3     rtol, ntarg, xtarg, usol, uder)
c
      do i=1, ntarg
        x=xtarg(i)
        call uexact(x,uext)
        err=dabs(uext-usol(i))
        write(21, *) xtarg(i), usol(i), uext, err
      enddo
      write(*,*) 'solution and error written in fort.21'


      deallocate(xtarg)
      deallocate(usol)
      deallocate(uder)
c
c 
      end program      





c---------------------------------
c     user provided functions
c
      subroutine peval(x, p, eps)
      implicit real*8 (a-h,o-z)
      real*8 x, p, eps

      p=0.0d0

      end subroutine




      subroutine qeval(x, q, eps)
      implicit real*8 (a-h,o-z)
      real*8 x, q, eps

      q=1.0d0

      end subroutine




      subroutine feval(x, f, eps)
      implicit real*8 (a-h,o-z)
      real*8 x, f, eps
c
      c1=0.0d0
      d1=1.0d-3
c
      c2=1.0d0
      d2=1.0d-2
c
      exp1=exp(-(x-c1)**2/d1)
      exp2=exp(-(x-c2)**2/d2)
c
c     p=0, q=1 => f=u''+u      
c
      f=(1.0d0-2.0d0/d1+4.0d0*(x-c1)**2/d1**2)*exp1
     1 +(1.0d0-2.0d0/d2+4.0d0*(x-c2)**2/d2**2)*exp2
c

      end subroutine





c------------------------------------------
c
      subroutine uexact(x,u)
      implicit real*8 (a-h,o-z)
      real*8 x, u
c
      c1=0.0d0
      d1=1.0d-3
c
      c2=1.0d0
      d2=1.0d-2
c
      u=exp(-(x-c1)**2/d1)+exp(-(x-c2)**2/d2)

      end subroutine
