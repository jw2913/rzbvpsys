c     testing driver 
c     which tests the subroutine rnlinadap
c
c-----------------------------------------
c     the bvp:
c     u'(x) = F(x,u(x)) 
c              (x \in [xa, xb])    (1)
c     bca*u(xa) + bcb*u(xb) = bcv  (2)
c
c     u\in R^2
c     where F is a user-specified function
c     given by feval, with its derivative dF/dy
c     given bt fyeval
c
c     bca, bcb, bcv, xa, xb are given at the beginning
c     the exact solution and derivative are given by 
c     subroutines uexact and uder
c
c-----------------------------------------
c
c     RMK:
c     this first testing case tests
c     a linear bvp in disguise:
c
c     u'(x)+p(x)*u(x)=f(x)
c     bca*u(xa)+bcb*u(xb)=bcv
c
c     we use the nonlinear interface with
c
c     F(x,y)= f(x)-p(x)*y(x)
c     dF/dy(x,y) = -p(x)
c     y0(x)=y0'(x)=0
c
c     y0 and y0' are defined in subroutines 
c     yeval0 and ydeval0
c
c
c     let's see if it does the obvious
c-----------------------------------------
c
c
      program test
      implicit real*8 (a-h,o-z)
      integer ndim, ndeg, nmax
      parameter(ndim=2)      
      parameter(ndeg=16)
      parameter(nch=2)
      parameter(nmax=100000)
c     tree related 
      integer nboxes, maxid, nlev
      integer levelbox(nmax)
      integer icolbox(nmax) 
      integer iparentbox(nmax), ichildbox(2,nmax)
      integer nblevel(0:nmax)
      integer iboxlev(nmax), istartlev(0:nmax)
      integer leaflist(nmax), itemparray(nmax)
c     the BC matrices and vec
      real*8 bca(ndim,ndim), bcb(ndim,ndim), bcv(ndim)
      real*8 pars(1), ua(ndim), ub(ndim)
c     the solution and its derivative
      real*8 endpts(nch+1)
      real*8 xtarg(nmax)
      real*8 usol(ndim,nmax), uext(ndim,nmax)
      real*8 ud(ndim,nmax), udext(ndim,nmax)
c     testing
      real*8 chpts(ndeg,nmax)
      external feval, fyeval, yeval0, ydeval0
      external uexact, uder
c
c     0. define the BC
c     (leave them here, but they are not needed in the test of the bie)
      bca(1,1)=4.0d0
      bca(2,1)=0.0d0
      bca(1,2)=1.0d0
      bca(2,2)=5.0d0
c
      bcb(1,1)=0.1d0
      bcb(2,1)=-0.5d0
      bcb(1,2)=-0.3d0
      bcb(2,2)=0.2d0
c
      xa=0.0d0
      xb=1.0d0
c
c     define the solution to be two gaussians with c1, d1, c2, d2
c     see also subroutines uexact and uder 
c
      c1=0.0d0
      d1=1.0d-3
c
      c2=1.0d0
      d2=1.0d-2
c
      ua(1)=exp(-(xa-c1)**2/d1)
      ua(2)=exp(-(xa-c2)**2/d2)
c
      ub(1)=exp(-(xb-c1)**2/d1)
      ub(2)=exp(-(xb-c2)**2/d2)
c
      bcv(1)=bca(1,1)*ua(1)+bca(1,2)*ua(2)
     1      +bcb(1,1)*ub(1)+bcb(1,2)*ub(2)
c
      bcv(2)=bca(2,1)*ua(1)+bca(2,2)*ua(2)
     1      +bcb(2,1)*ub(1)+bcb(2,2)*ub(2)
c
c      write(*,*) ua(1), ua(2), ub(1), ub(2)
c
c      write(*,*) 'bcv='
c      do i=1, ndim
c        write(*,*) bcv(i)
c      enddo
c     end of the definition of the problem
c
c-------------------------------------------
c
c     prepare endpts (should be clustered near c1 and c2,
c     but a fine enough grid should be okay too)
      endpts(1)=xa
      endpts(nch+1)=xb
      h=(endpts(nch+1)-endpts(1))/nch
      do i=2, nch
        endpts(i)=(i-1)*h
      enddo
c
c-------------------------------------------
c
c     prepare the target points xtarg
      ntarg=1000
      h=(xb-xa)/ntarg
      do i=1, ntarg
        xtarg(i)=xa+i*h
      enddo
c
      maxdiv=10
      rtol=1.0d-10
      maxstep=1000
c     reduce it to 2 if it doesn't stop
c     correct when maxstep = 1
c     incorrect when maxstep = 2
c
c     remember to change feval, fyeval, yeval0, ydeval0 below!
      call rnlinadap(ndim, ndeg, nch, endpts,
     1     bca, bcb, bcv, feval, fyeval, yeval0,
     2     ydeval0, pars, maxdiv, rtol, maxstep,
     3     ntarg, xtarg, usol, ud)
c
      do k=1, ntarg
        x=xtarg(k)
c       write(111,*) usol(1,k), ud(1,k)
c       write(112,*) usol(2,k), ud(2,k)
        call uexact(x,uext(1,k)) 
        err1=abs(usol(1,k)-uext(1,k))
        err2=abs(usol(2,k)-uext(2,k))
        write(211,*) x, usol(1,k), uext(1,k), err1 
        write(212,*) x, usol(2,k), uext(2,k), err2 
      enddo
c

      end program




c
c-----------------------------------------
c

      subroutine fyeval(x,y,fy,pars) 
      implicit real*8 (a-h,o-z)
      real*8 x, y(2),pars(1)
      real*8 fy(2,2)
c
c     df/dy = -p(x)
      fy(1,1)=-2.0d0
      fy(2,1)=-(0.5d0+x)
      fy(1,2)=-(0.1d0-x)
      fy(2,2)=-3.0d0
c
      end subroutine




      subroutine feval(x,y,f,pars)
      implicit real*8 (a-h,o-z)
      real*8 x, y(2), pars(1)
      real*8 f(2), p(2,2)
c      
      c1=0.0d0
      d1=1.0d-3
c
      c2=1.0d0
      d2=1.0d-2
c
      exp1=exp(-(x-c1)**2/d1)
      exp2=exp(-(x-c2)**2/d2)
c
      f(1)=-2.0d0*(x-c1)/d1*exp1+2.0d0*exp1
     1    +(0.1d0-x)*exp2

      f(2)=-2.0d0*(x-c2)/d2*exp2+(0.5d0+x)*exp1
     1    +3.0d0*exp2
c
      p(1,1)=2.0d0
      p(2,1)=0.5d0+x
      p(1,2)=0.1d0-x
      p(2,2)=3.0d0
c
      f(1)=f(1)-p(1,1)*y(1)-p(1,2)*y(2)
      f(2)=f(2)-p(2,1)*y(1)-p(2,2)*y(2)
c     f = f - p(x)*y(x)
c
      end subroutine




c----------------------------------------------
      subroutine yeval0(x,y0,pars)
      implicit real*8 (a-h,o-z)
      real*8 x, y0(2), pars(1)
c
      y0(1)=0.0d0
      y0(2)=0.0d0


      end subroutine





      subroutine ydeval0(x,yd0,pars)
      implicit real*8 (a-h,o-z)
      real*8 x, yd0(2), pars(1)
c
      yd0(1)=0.0d0
      yd0(2)=0.0d0


      end subroutine






c----------------------------------------------
      subroutine uexact(x,u)
      implicit real*8 (a-h,o-z)
      real*8 x, u(2)
c      
      c1=0.0d0
      d1=1.0d-3
c
      c2=1.0d0
      d2=1.0d-2
c
      u(1)=exp(-(x-c1)**2/d1)
      u(2)=exp(-(x-c2)**2/d2)
c
      end subroutine





      subroutine uder(x,ud)
      implicit real*8 (a-h,o-z)
      real*8 s, ud(2)
c
      c1=0.0d0
      d1=1.0d-3
c
      c2=1.0d0
      d2=1.0d-2
c
      ud(1)=-2.0d0*(x-c1)/d1*exp(-(x-c1)**2/d1)
      ud(2)=-2.0d0*(x-c2)/d2*exp(-(x-c2)**2/d2)
c
      end subroutine 



