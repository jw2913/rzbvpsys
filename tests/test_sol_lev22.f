c     testing driver (of the global/2-level bvp system solver)
c     which tests the subroutine rsysnoadap
c
c
c     the bvp:
c
c     u'(x)+p(x)*u(x)=f(x)
c     bca*u(xa)+bcb*u(xb)=bcv
c
c     u\in R^2
c
c     where p and f are functions given by peval and feval
c     bca, bcb, bcv, xa, xb are given at the beginning
c     the exact solution and derivative are given by 
c     subroutines uexact and uder
c
c     it works by comparing the global solution and the 2-level
c     local solution patched together

      program test
      implicit real*8 (a-h,o-z)
      integer ndim, ndeg, nmax
      parameter(ndim=2)      
      parameter(ndeg=32)
      parameter(nch=2)
      real*8 endpts(nch+1)
c     the BC matrices and vec
      real*8 bca(ndim,ndim), bcb(ndim,ndim), bcv(ndim)
      real*8 pars(1)
c     the solution and its derivative
      real*8 usol(ndim,ndeg,nch), uext(ndim,ndeg,nch)
      real*8 ud(ndim,ndeg,nch), udext(ndim,ndeg,nch)
c     testing
      real*8 chpts(ndeg,nch)
      external peval, feval, uexact, uder
c
c     0. define the BC
c     (leave them here, but they are not needed in the test of the bie)
      bca(1,1)=4.0d0
      bca(2,1)=0.0d0
      bca(1,2)=1.0d0
      bca(2,2)=5.0d0
c
      bcb(1,1)=0.1d0
      bcb(2,1)=-0.5d0
      bcb(1,2)=-0.3d0
      bcb(2,2)=0.2d0
c
      bcv(1)=0.16d0
      bcv(2)=1.55d0
c
      endpts(1)=0.0d0
      endpts(2)=0.5d0
      endpts(3)=1.0d0
c
      do i=1, nch
        xa=endpts(i)
        xb=endpts(i+1)
        call chnodes(xa, xb, ndeg, chpts(1,i), u, v, u1, v1)
      enddo
c
      call rsysnoadap(ndim, ndeg, nch, endpts,
     1     bca, bcb, bcv, peval, feval, pars,
     2     usol, ud)
c
      do k=1, nch
        do j=1, ndeg
          x=chpts(j,k)
c         write(111,*) usol(1,j,k), ud(1,j,k)
c         write(112,*) usol(2,j,k), ud(2,j,k)
          call uexact(x,uext(1,j,k)) 
          err1=abs(usol(1,j,k)-uext(1,j,k))
          err2=abs(usol(2,j,k)-uext(2,j,k))
          write(111,*) x, usol(1,j,k), uext(1,j,k), err1 
          write(112,*) x, usol(2,j,k), uext(2,j,k), err2 
        enddo
      enddo
     


      end program




c
c-----------------------------------------
c

      subroutine peval(x,p,pars) 
      implicit real*8 (a-h,o-z)
      real*8 x, pars(1)
      real*8 p(2,2)
c
      p(1,1)=2.0d0
      p(2,1)=0.5d0
      p(1,2)=0.1d0
      p(2,2)=3.0d0
c
      end subroutine




      subroutine feval(x,f,pars)
      implicit real*8 (a-h,o-z)
      real*8 x, pars(1)
      real*8 f(2)

      f(1)=0.22d0*x+0.13d0
      f(2)=0.65d0*x+1.1d0

      end subroutine




      subroutine uexact(x,u)
      implicit real*8 (a-h,o-z)
      real*8 x, u(2)
      
      u(1)=0.1d0*x
      u(2)=0.2d0*x+0.3d0

      end subroutine





      subroutine uder(x,ud)
      implicit real*8 (a-h,o-z)
      real*8 s, ud(2)
c
      ud(1)=0.1d0
      ud(2)=0.2d0
c
      end subroutine 
