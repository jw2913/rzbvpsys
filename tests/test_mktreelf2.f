c     tests the subroutine mktreelf
      program test
      implicit real*8 (a-h,o-z)
      integer ndim, nmax, ndeg, maxlev
      integer nch
      parameter(nmax=1000000)
      parameter(maxlevel=100)
      parameter(maxch=1000)
      integer nboxes, maxid, nlev
      integer levelbox(nmax)
      integer icolbox(nmax) 
      integer iparentbox(nmax), ichildbox(2,nmax)
      integer nblevel(0:nmax)
      integer iboxlev(nmax), istartlev(0:nmax)
      integer leaflist(nmax), itemparray(nmax)
      real*8 endpts(maxch+1)
c
c     read the endpts from a file
c     there seems to be problems with data from endpts1.dat
      open(unit=12,file='endpts1.dat') 
      nch=18
      do i=1,nch+1
        read(12,*) endpts(i)
      enddo
      close(12)
c
      write(*,*) 'calling mktreelf'
      maxboxes=nmax
      call mktreelf(levelbox, icolbox, nboxes, maxid,
     1     nlev, iparentbox, ichildbox, nblevel, 
     2     iboxlev, istartlev, cent0, xsize0, maxboxes, 
     3     maxlevel, nch, endpts)

      write(*,*) 'nboxes=',nboxes
      write(*,*) 'nlev=',nlev
c
      call getleaflist(levelbox, icolbox, nboxes,
     1     maxid, nlev, iparentbox, ichildbox, 
     2     nblevel, iboxlev, istartlev, 
     3     nleaf, leaflist)
c
      write(*,*) 'nleaf=',nleaf
      aroot=cent0-xsize0/2.0d0
      broot=cent0+xsize0/2.0d0
      write(*,*) 'aroot=',aroot
      write(*,*) 'broot=',broot
c
      do ii=1, nleaf
        ibox=leaflist(ii)
        lev=levelbox(ibox)
        icol=icolbox(ibox)
        call mkinterval(lev, icol, aroot, broot,
     1       aa, bb)
        write(101,*) aa
      enddo
      write(101,*) bb
c
c
      iprint=102
      call printtree2(levelbox,icolbox,nboxes,
     1     nlev,iparentbox,ichildbox,nblevel,
     2     iboxlev,istartlev,iprint,nleaves,
     3     cent0,xsize0)
      write(*,*) 'the whole tree written in fort.102'


      end program





