c     testing driver
c     which tests the subroutines:
c
c     ssetbc, sbctab (dealing with the BC)
c     formsmat, slocsolve, reorderrhs (inside slocsolve) 
c     (forming the linear system and solving it)   
c
      program test
      implicit real*8 (a-h,o-z)
      integer ndim, ndeg
      parameter(ndim=2)      
      parameter(ndeg=32)
c     the spectral integration matrices
      real*8 cftm(ndeg,ndeg), citm(ndeg,ndeg)
      real*8 spdef(ndeg)
      real*8 spbx(ndeg,ndeg), spxb(ndeg,ndeg)
      real*8 sdvl(ndim,ndeg*ndim), sdvr(ndim,ndeg*ndim)
      real*8 slvl(ndeg*ndim,ndeg*ndim)
      real*8 srvr(ndeg*ndim,ndeg*ndim)
      real*8 chpts(ndeg)
c     the constant matrices appearing in the eqn
      real*8 vl(ndim,ndim), vr(ndim,ndim)
      real*8 ui(ndim)
c     the rhs mat and vec
      real*8 phi(ndim,ndim,ndeg), fval(ndim,ndeg)
c     the solution mat and vec
      real*8 xi(ndim,ndim,ndeg), eta(ndim,ndeg)
c     the BC matrices and vec
      real*8 bca(ndim,ndim), bcb(ndim,ndim), bcv(ndim)
c     misc vars
      real*8 pars(1)
      real*8 sigma(ndim*ndeg)
      real*8 yt(ndim*ndeg), ye(ndim*ndeg)
      external peval, feval
c
c     0. define the BC
      bca(1,1)=4.0d0
      bca(2,1)=0.0d0
      bca(1,2)=1.0d0
      bca(2,2)=5.0d0
c
      bcb(1,1)=0.1d0
      bcb(2,1)=-0.5d0
      bcb(1,2)=-0.3d0
      bcb(2,2)=0.2d0
c
      bcv(1)=0.16d0
      bcv(2)=1.55d0
c
      xa=0.0d0
      xb=1.0d0
      xlength=xb-xa
c
      call setchmat(ndeg, cftm, citm, spdef, spbx, spxb)
      call chnodes(xa, xb, ndeg, chpts, u, v, u1, v1)
c
c     1. call ssetbc to set constants according to the bc
      call ssetbc(ndim, bca, bcb, bcv, ui, vl, vr)
c     2. call sbctab to tabulate the rhs and such
c        (RMK: change this subroutine to test the integral eqn alone)
      do i=1, ndeg
        x=chpts(i)
        call sbctab(ndim, x, ui, fval(1,i), phi(1,1,i), peval, 
     1       feval, pars)
      enddo
c
c      do i=1, ndeg
c        write(31,*) i, chpts(i), fval(1,i), fval(2,i)
c      enddo
c
c      do i=1, ndeg
c        write(32,*) phi(1,1,i), phi(1,2,i), phi(2,1,i), phi(2,2,i)
c      enddo
c
c     correct up to here
c
c     3. call formsmat to form the spectral integration matrices
c     for test only, let vl=vr=eye(2,2)
c      do j=1,ndim
c      do i=1, ndim
c        vl(i,j)=0.0d0
c        vr(i,j)=0.0d0
c      enddo
c      enddo
c
c      vl(1,1)=1.0d0
c      vl(2,1)=2.0d0
c      vl(2,2)=1.0d0
c      vr(2,1)=1.0d0
c
      call formsmat(ndim, ndeg, spdef, spbx, spxb, vl, vr,
     1     sdvl, sdvr, slvl, srvr)
c
c      do j=1,ndeg*ndim
c      do i=1, ndim
c        write(31,*) i,j,sdvl(i,j),sdvr(i,j)
c      enddo
c      enddo
c
      do j=1,ndeg*ndim
      do i=1, ndeg*ndim
        slvl(i,j)=slvl(i,j)*xlength/2.0d0
        srvr(i,j)=srvr(i,j)*xlength/2.0d0
c        write(32,*) i,j,slvl(i,j),srvr(i,j)
      enddo
      enddo
c
c-----------------------------------------------------
c     looking like sensible numbers
c     try applying the operators
c
c     sample sigma(ndim*ndeg)
      do i=1, ndeg
        call fdens(chpts(i),sigma((i-1)*ndim+1),pars)
c        write(41,*) i,chpts(i), sigma((i-1)*ndim+1), 
c     1                sigma((i-1)*ndim+2)
      enddo
c
c     test: application of sdvl
c      write(*,*) 'stest='
c      do i=1, ndim
c        stest=0.0d0
c        do j=1, ndim*ndeg
c          stest=stest+sdvl(i,j)*sigma(j)
c          write(42,*) i,j,sdvl(i,j), stest, sigma(j)
c        enddo
c        write(*,*) i, stest
c      enddo
c
c      write(*,*) 'sexact='
c      write(*,*) vl(1,1)+1.5d0*vl(1,2)
c      write(*,*) vl(2,1)+1.5d0*vl(2,2)
c
c     test: application of sdvr
c      write(*,*) 'stest='
c      do i=1, ndim
c        stest=0.0d0
c        do j=1, ndim*ndeg
c          stest=stest+sdvr(i,j)*sigma(j)
c          write(42,*) i,j,sdvr(i,j), stest, sigma(j)
c        enddo
c        write(*,*) i, stest
c      enddo
c
c      write(*,*) 'sexact='
c      write(*,*) vr(1,1)+1.5d0*vr(1,2)
c      write(*,*) vr(2,1)+1.5d0*vr(2,2)
c
c     sdvl,sdvr passed the test, continue with slvl
c      do i=1, ndeg*ndim
c        yt(i)=0.0d0
c        do j=1, ndeg*ndim
c          yt(i)=yt(i)+slvl(i,j)*sigma(j)
c        enddo
c        write(41,*) yt(i)
c      enddo
c
c      do i=1, ndeg
c        x=chpts(i)
c        ye(2*i-1)=(vl(1,1)+0.5d0*vl(1,2))*x**2
c     1            +vl(1,2)*x
c        ye(2*i)=(vl(2,1)+0.5d0*vl(2,2))*x**2
c     1            +vl(2,2)*x
c
c        write(42,*) ye(2*i-1) 
c        write(42,*) ye(2*i) 
c      enddo
c
c     slvl passed the test, continue with srvr
      do i=1, ndeg*ndim
        yt(i)=0.0d0
        do j=1, ndeg*ndim
          yt(i)=yt(i)+srvr(i,j)*sigma(j)
        enddo
        write(41,*) yt(i)
      enddo
c
      do i=1, ndeg
        x=chpts(i)
        ye(2*i-1)=-(vr(1,1)+0.5d0*vr(1,2))*x**2
     1            -vr(1,2)*x+vr(1,1)+1.5d0*vr(1,2)
        ye(2*i)=-(vr(2,1)+0.5d0*vr(2,2))*x**2
     1          -vr(2,2)*x+vr(2,1)+1.5d0*vr(2,2)
        write(42,*) ye(2*i-1) 
        write(42,*) ye(2*i) 
      enddo
c
c     RMK: move the scaling outside of formsmat
c          (it's already in slocsolve)
c
c     RMK: construct an integral equation and solve
c          to test the subroutine slocsolve
c
c     RMK: eventually test the whole thing as a global
c          solver
c
c-----------------------------------------------------
c
c     4. call slocsolve to solve the linear system
      xlength=xb-xa
      call slocsolve(ndim, ndeg, xlength, sdvl, sdvr, slvl, srvr,
     1     phi, fval, eta, xi)
c
      do j=1, ndeg
        do i=1, ndim
          write(21,*) i,j,eta(i,j)
        enddo
      enddo
c
      do k=1, ndeg
        do j=1, ndim
          do i=1, ndim
            write(22,*) i,j,k,xi(i,j,k)
          enddo
        enddo 
      enddo
c

      end program




c
c-----------------------------------------
c

      subroutine peval(x,p,pars) 
      real*8 x, pars(1)
      real*8 p(2,2)
c
      p(1,1)=2.0d0
      p(2,1)=0.5d0
      p(1,2)=0.1d0
      p(2,2)=3.0d0
c
      end subroutine




      subroutine feval(x,f,pars)
      real*8 x, pars(1)
      real*8 f(2)

      f(1)=0.22d0*x+0.13d0
      f(2)=0.65d0*x+1.1d0

      end subroutine





      subroutine fdens(x,sigma,pars)
      real*8 x, pars(1)
      real*8 sigma(2)

      sigma(1)=2.0d0*x
      sigma(2)=x+1.0d0
c
c      sigma(1)=1.0d0
c      sigma(2)=0.0d0

      end subroutine
