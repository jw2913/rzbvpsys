c     testing driver 
c     which tests the subroutine rsysadap
c
c     the bvp:
c
c     u'(x)+p(x)*u(x)=f(x)
c     bca*u(xa)+bcb*u(xb)=bcv
c
c     u\in R^2
c
c     where p and f are functions given by peval and feval
c     bca, bcb, bcv, xa, xb are given at the beginning
c     the exact solution and derivative are given by 
c     subroutines uexact and uder
c
      program test
      implicit real*8 (a-h,o-z)
      integer ndim, ndeg, nmax
      parameter(ndim=2)      
      parameter(ndeg=16)
      parameter(nch=2)
      parameter(nmax=100000)
c     tree related 
      integer nboxes, maxid, nlev
      integer levelbox(nmax)
      integer icolbox(nmax) 
      integer iparentbox(nmax), ichildbox(2,nmax)
      integer nblevel(0:nmax)
      integer iboxlev(nmax), istartlev(0:nmax)
      integer leaflist(nmax), itemparray(nmax)
c     the BC matrices and vec
      real*8 bca(ndim,ndim), bcb(ndim,ndim), bcv(ndim)
      real*8 pars(1), ua(ndim), ub(ndim)
c     the solution and its derivative
      real*8 endpts(nch+1)
      real*8 xtarg(nmax)
      real*8 usol(ndim,nmax), uext(ndim,nmax)
      real*8 ud(ndim,nmax), udext(ndim,nmax)
c     testing
      real*8 chpts(ndeg,nmax)
      external peval, feval, uexact, uder, ftest
c
c     0. define the BC
c     (leave them here, but they are not needed in the test of the bie)
      bca(1,1)=4.0d0
      bca(2,1)=0.0d0
      bca(1,2)=1.0d0
      bca(2,2)=5.0d0
c
      bcb(1,1)=0.1d0
      bcb(2,1)=-0.5d0
      bcb(1,2)=-0.3d0
      bcb(2,2)=0.2d0
c
      xa=0.0d0
      xb=1.0d0
c
c     define the solution to be two gaussians with c1, d1, c2, d2
c     see also subroutines uexact and uder 
c
      c1=0.0d0
      d1=1.0d-3
c
      c2=1.0d0
      d2=1.0d-2
c
      ua(1)=exp(-(xa-c1)**2/d1)
      ua(2)=exp(-(xa-c2)**2/d2)
c
      ub(1)=exp(-(xb-c1)**2/d1)
      ub(2)=exp(-(xb-c2)**2/d2)
c
      bcv(1)=bca(1,1)*ua(1)+bca(1,2)*ua(2)
     1      +bcb(1,1)*ub(1)+bcb(1,2)*ub(2)
c
      bcv(2)=bca(2,1)*ua(1)+bca(2,2)*ua(2)
     1      +bcb(2,1)*ub(1)+bcb(2,2)*ub(2)
c
c      write(*,*) ua(1), ua(2), ub(1), ub(2)
c
c      write(*,*) 'bcv='
c      do i=1, ndim
c        write(*,*) bcv(i)
c      enddo
c     end of the definition of the problem
c
c-------------------------------------------
c
c     prepare endpts (should be clustered near c1 and c2,
c     but a fine enough grid should be okay too)
      endpts(1)=xa
      endpts(nch+1)=xb
      h=(endpts(nch+1)-endpts(1))/nch
      do i=2, nch
        endpts(i)=(i-1)*h
      enddo
c
c-------------------------------------------
c
c     prepare the target points xtarg
      ntarg=1000
      h=(xb-xa)/ntarg
      do i=1, ntarg
        xtarg(i)=xa+i*h
      enddo
c
      maxdiv=10
      rtol=1.0d-10
      call rsysadap(ndim, ndeg, nch, endpts,
     1     bca, bcb, bcv, peval, feval, pars,
     2     maxdiv, rtol, ntarg, xtarg, usol, ud)
c
      do k=1, ntarg
        x=xtarg(k)
c       write(111,*) usol(1,k), ud(1,k)
c       write(112,*) usol(2,k), ud(2,k)
        call uexact(x,uext(1,k)) 
        err1=abs(usol(1,k)-uext(1,k))
        err2=abs(usol(2,k)-uext(2,k))
        write(311,*) x, usol(1,k), uext(1,k), err1 
        write(312,*) x, usol(2,k), uext(2,k), err2 
      enddo
c
c-----------------------------------------
c-----------------------------------------
c     how about resolving the exact solution itself
c     on a tree? generate such a tree and compare
c     with the one obtained from the solution of the ode
c
c     conclusion: approx the same, if without doubling
c     (the same if we set rtol=rtol/40)
c     (pretty good for refinement)
c     (the coarsening is not so good, i.e. if given an overresolved
c      grid, not enough coarsening is done, it seems)
c
      cent0=0.5d0*(endpts(1)+endpts(nch+1))
      xsize0=endpts(nch+1)-endpts(1)
c
      maxboxes=nmax
      maxlevel=100
c
      rtol=rtol/40.0d0
      call mktreef(levelbox, icolbox, nboxes,
     1     maxid, nlev, iparentbox, ichildbox,
     2     nblevel, iboxlev, istartlev, maxboxes,
     3     itemparray, maxlevel, rtol, ftest, ndeg, 
     4     cent0, xsize0)
      write(*,*) ' '
      write(*,*) 'nboxes=',nboxes
      write(*,*) 'nlev=',nlev
c
      call getleaflist(levelbox, icolbox, nboxes,
     1     maxid, nlev, iparentbox, ichildbox, 
     2     nblevel, iboxlev, istartlev, 
     3     nleaf, leaflist)
c
      aroot=endpts(1)
      broot=endpts(1+nch)
      do i=1, nleaf
        ibox=leaflist(i)
        call mkgrid(levelbox(ibox), icolbox(ibox), aroot, 
     1       broot, ndeg, a, b, chpts)
        write(557,*) a, 0.0d0

      enddo      



      end program




c
c-----------------------------------------
c

      subroutine peval(x,p,pars) 
      implicit real*8 (a-h,o-z)
      real*8 x, pars(1)
      real*8 p(2,2)
c
      p(1,1)=2.0d0
      p(2,1)=0.5d0+x
      p(1,2)=0.1d0-x
      p(2,2)=3.0d0
c
      end subroutine




      subroutine feval(x,f,pars)
      implicit real*8 (a-h,o-z)
      real*8 x, pars(1)
      real*8 f(2)
c      
      c1=0.0d0
      d1=1.0d-3
c
      c2=1.0d0
      d2=1.0d-2
c
      exp1=exp(-(x-c1)**2/d1)
      exp2=exp(-(x-c2)**2/d2)
c
      f(1)=-2.0d0*(x-c1)/d1*exp1+2.0d0*exp1
     1    +(0.1d0-x)*exp2

      f(2)=-2.0d0*(x-c2)/d2*exp2+(0.5d0+x)*exp1
     1    +3.0d0*exp2
c

      end subroutine




      subroutine uexact(x,u)
      implicit real*8 (a-h,o-z)
      real*8 x, u(2)
c      
      c1=0.0d0
      d1=1.0d-3
c
      c2=1.0d0
      d2=1.0d-2
c
      u(1)=exp(-(x-c1)**2/d1)
      u(2)=exp(-(x-c2)**2/d2)
c
      end subroutine





      subroutine uder(x,ud)
      implicit real*8 (a-h,o-z)
      real*8 s, ud(2)
c
      c1=0.0d0
      d1=1.0d-3
c
      c2=1.0d0
      d2=1.0d-2
c
      ud(1)=-2.0d0*(x-c1)/d1*exp(-(x-c1)**2/d1)
      ud(2)=-2.0d0*(x-c2)/d2*exp(-(x-c2)**2/d2)
c
      end subroutine 



c----------------------------------------------
c
      function ftest(x)
      implicit real*8 (a-h,o-z)
      real*8 x, ftest
c
      c1=0.0d0
      d1=1.0d-3
c
      c2=1.0d0
      d2=1.0d-2
c
      ftest=exp(-(x-c1)**2/d1)+exp(-(x-c2)**2/d2)
c

      end function
