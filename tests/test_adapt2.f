c testing environment for the adaptivity strategy 
c of the 2pt bvp solver
c
c we create a tree by hand, small but non-trivial
c then assign a value to each leaf node, saving them
c in an array sval, and do the following:
c
c subdivide a leaf node ib, if sval(ib)>1.0,
c   and let sval(ichild(1,ib))=sval(ib)/2.0d0,
c           sval(ichild(2,ib))=sval(ib)/2.0d0.
c
c merge leaf nodes ib and ir, if 
c 1) ib and ir are siblings
c 2) max(sval(ib), sval(ir))<0.8, 
c   and let sval(iparent(ib))=max(sval(ib), sval(ir))
c
c the final result is figured out manually and compared
c against the result of the code
c (the current result is correct!)
c
c RMK: this one uses the same tree as in testadapt.f,
c      but a different initial array sval
c
c ----------------------------------------------------
c
      program test
      implicit real*8 (a-h,o-z)
      integer nmax
      parameter(nmax=100000)
      integer nboxes, maxid, nlev
      integer levelbox(nmax)
      integer icolbox(nmax) 
      integer iparentbox(nmax), ichildbox(2,nmax)
      integer nblevel(0:nmax)
      integer iboxlev(nmax), istartlev(0:nmax)
      integer leaflist(nmax)
      real*8 sval(nmax)
c
      nlev=2
      call unitree(levelbox, icolbox, nboxes, maxid,
     1     nlev, iparentbox, ichildbox, nblevel, 
     2     iboxlev, istartlev)
c     begin by creating a uniform tree of nlev=2
c
      cent0=0.0d0
      xsize0=1.0d0
c
      call getleaflist(levelbox, icolbox, nboxes,
     1     maxid, nlev, iparentbox, ichildbox, 
     2     nblevel, iboxlev, istartlev, 
     3     nleaf, leaflist)
c
      do i=1, nleaf
        ib=leaflist(i)
        call posbox(xa,xb,icolbox(ib),levelbox(ib),
     1       cent0,xsize0)
        write(*,*) i, ib, xa, xb
      enddo
c     test the leaflist before proceeding on
c
c     subdivide box 5, 7, 9, 12
c
      iparbox=5
      call subdivide(iparbox, levelbox, icolbox, nboxes,
     1     maxid, nlev, iparentbox, ichildbox, nblevel, 
     2     iboxlev, istartlev)
c
      iparbox=7
      call subdivide(iparbox, levelbox, icolbox, nboxes,
     1     maxid, nlev, iparentbox, ichildbox, nblevel, 
     2     iboxlev, istartlev)
c
      iparbox=9
      call subdivide(iparbox, levelbox, icolbox, nboxes,
     1     maxid, nlev, iparentbox, ichildbox, nblevel, 
     2     iboxlev, istartlev)
c
      iparbox=12
      call subdivide(iparbox, levelbox, icolbox, nboxes,
     1     maxid, nlev, iparentbox, ichildbox, nblevel, 
     2     iboxlev, istartlev)

c
      call getleaflist(levelbox, icolbox, nboxes,
     1     maxid, nlev, iparentbox, ichildbox, 
     2     nblevel, iboxlev, istartlev, 
     3     nleaf, leaflist)
c
      write(*,*) ' '
      write(*,*) 'after subdivision:'
      do i=1, nleaf
        ib=leaflist(i)
        call posbox(xa,xb,icolbox(ib),levelbox(ib),
     1       cent0,xsize0)
        write(*,*) i, ib, xa, xb
      enddo
c
      iprint=21
      call printbylev(levelbox,icolbox,nboxes,
     1     nlev,iparentbox,ichildbox,nblevel,
     2     iboxlev,istartlev,iprint,nleaves,
     3     cent0,xsize0)
      write(*,*) 'nleaves=', nleaves
c     the tree is correct, now test the adaptivity
c
c-------------------------------------------------------
c
c     initialize the sval array
c     a hand picked example
c     not too trivial hopefully
      sval(leaflist(1))=2.8d0
      sval(leaflist(2))=2.2d0
      sval(leaflist(3))=0.6d0
      sval(leaflist(4))=0.7d0
      sval(leaflist(5))=0.6d0
      sval(leaflist(6))=1.8d0
      sval(leaflist(7))=1.4d0
      sval(leaflist(8))=1.8d0
c
      iflag=1
      nstep=0
c
c     the adaptivity process
      do while((nboxes .gt. 1).and.(iflag.gt.0))
        iflag=0
        call getleaflist(levelbox, icolbox, nboxes,
     1       maxid, nlev, iparentbox, ichildbox, 
     2       nblevel, iboxlev, istartlev, 
     3       nleaf, leaflist)
c
        write(*,*) ' '
        write(*,*) 'nstep:', nstep
        do i=1, nleaf
          write(*,*) i, sval(leaflist(i))
        enddo
c
        nstep=nstep+1
        do i=1, nleaf
          ib=leaflist(i)
          si=sval(ib)
          if(si .gt. 1.0d0) then
            iparbox=ib 
            call subdivide(iparbox, levelbox, icolbox, 
     1           nboxes, maxid, nlev, iparentbox, 
     2           ichildbox, nblevel, iboxlev, istartlev)
            do ic=1, 2
              sval(ichildbox(ic,ib))=si/2.0d0
            enddo 
            iflag=1
          else
            if(i .lt. nleaf) then
              ir=leaflist(i+1)
              call isibling(ib, ir, iparentbox, isib)
              sr=sval(ir)
              smax=max(si,sr)
              if((smax .lt. 0.8d0)
     1           .and.(isib .gt. 0)) then
                iparbox=iparentbox(ib)
                call mergechd(iparbox,levelbox,icolbox, 
     1               nboxes,maxid,nlev,iparentbox, 
     2               ichildbox,nblevel,iboxlev,istartlev)
                sval(iparbox)=smax
                iflag=1
              endif
            endif
          endif
        enddo
c       
      enddo
c
c     at the final step, do this again:
      call getleaflist(levelbox, icolbox, nboxes,
     1     maxid, nlev, iparentbox, ichildbox, 
     2     nblevel, iboxlev, istartlev, 
     3     nleaf, leaflist)

      write(*,*) ' '
      write(*,*) 'nstep:', nstep
      do i=1, nleaf
        write(*,*) i, sval(leaflist(i))
      enddo
c


      end program
