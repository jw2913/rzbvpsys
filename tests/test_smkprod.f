c     testing driver
c     which tests the subroutine smkprod
c
c
      program test
      implicit real*8 (a-h,o-z)
      integer ndim, ndeg
      parameter(ndim=2)      
      parameter(ndeg=32)
c     the spectral integration matrices
      real*8 cftm(ndeg,ndeg), citm(ndeg,ndeg)
      real*8 spdef(ndeg)
      real*8 spbx(ndeg,ndeg), spxb(ndeg,ndeg)
      real*8 sdvl(ndim,ndeg*ndim), sdvr(ndim,ndeg*ndim)
      real*8 svlvr(ndeg*ndim,ndeg*ndim)
      real*8 chpts(ndeg)
c     the constant matrices appearing in the eqn
      real*8 vl(ndim,ndim), vr(ndim,ndim)
      real*8 ui(ndim)
c     the rhs mat and vec
      real*8 phi(ndim,ndim,ndeg), fval(ndim,ndeg)
c     the solution mat and vec
      real*8 xi(ndim,ndim,ndeg), eta(ndim,ndeg)
c     the BC matrices and vec
      real*8 bca(ndim,ndim), bcb(ndim,ndim), bcv(ndim)
c     misc vars
      real*8 pars(1)
      real*8 sigma(ndim*ndeg)
      real*8 al(ndim,ndim), ar(ndim,ndim)
      real*8 ale(ndim,ndim), are(ndim,ndim)
      real*8 dl(ndim), dr(ndim)
      real*8 dle(ndim), dre(ndim)
      external peval, feval, fdens, fdens2, prod_ext
c
c     0. define the BC
      bca(1,1)=4.0d0
      bca(2,1)=0.0d0
      bca(1,2)=1.0d0
      bca(2,2)=5.0d0
c
      bcb(1,1)=0.1d0
      bcb(2,1)=-0.5d0
      bcb(1,2)=-0.3d0
      bcb(2,2)=0.2d0
c
      bcv(1)=0.16d0
      bcv(2)=1.55d0
c
      xa=0.0d0
      xb=1.0d0
      xlength=xb-xa
c
      call setchmat(ndeg, cftm, citm, spdef, spbx, spxb)
      call chnodes(xa, xb, ndeg, chpts, u, v, u1, v1)
c
c     1. call ssetbc to set constants according to the bc
      call ssetbc(ndim, bca, bcb, bcv, ui, vl, vr)
c
c      do i=1, ndeg
c        write(31,*) i, chpts(i), fval(1,i), fval(2,i)
c      enddo
c
c
c     3. call formsmat to form the spectral integration matrices
c     for test only, let vl=vr=eye(2,2)
c
c      vl(1,1)=1.0d0
c      vl(2,1)=0.0d0
c      vl(1,2)=0.0d0
c      vl(2,2)=1.0d0
c
c      vr(1,1)=1.0d0
c      vr(2,1)=0.0d0
c      vr(1,2)=0.0d0
c      vr(2,2)=1.0d0
c
      write(*,*) 'vl, vr='
      do j=1,ndim
      do i=1, ndim
        write(*,*) i,j,vl(i,j), vr(i,j)
      enddo
      enddo
c
      call formsmat(ndim, ndeg, spdef, spbx, spxb, vl, vr,
     1     sdvl, sdvr, svlvr)
c
c      do j=1,ndeg*ndim
c      do i=1, ndim
c        write(31,*) i,j,sdvl(i,j),sdvr(i,j)
c      enddo
c      enddo
c
c      do j=1,ndeg*ndim
c      do i=1, ndeg*ndim
c        write(32,*) i,j,slvl(i,j),srvr(i,j)
c      enddo
c      enddo
c
c-----------------------------------------------------
c     looking like sensible numbers
c     try applying the operators
c
c     sample the density functions:
      do i=1, ndeg
        x=chpts(i)
        call fdens(x,eta(1,i),pars)
        call fdens2(x,xi(1,1,i),pars)
      enddo
c
c     call the subroutine smkprod
      call smkprod(ndim, ndeg, xlength, eta, xi, sdvl, sdvr,
     1     al, ar, dl, dr)
c
c     compute the exact inner products
      call prod_ext(vl, vr, ale, are, dle, dre)
c
c
      write(*,*) 'al:'
      do j=1, ndim
      do i=1, ndim
        errij=dabs(al(i,j)-ale(i,j))
        write(*,*) i,j,al(i,j), ale(i,j), errij
      enddo
      enddo
c
      write(*,*) ' '
      write(*,*) 'ar:'
      do j=1, ndim
      do i=1, ndim
        errij=dabs(ar(i,j)-are(i,j))
        write(*,*) i,j,ar(i,j), are(i,j), errij
      enddo
      enddo

c
      write(*,*) ' '
      write(*,*) 'dl:'
      do j=1, ndim
        errj=dabs(dl(j)-dle(j))
        write(*,*) i,j,dl(j), dle(j), errj
      enddo
c
      write(*,*) ' '
      write(*,*) 'dr:'
      do j=1, ndim
        errj=dabs(dr(j)-dre(j))
        write(*,*) i,j,dr(j), dre(j), errj
      enddo
c





      end program




c
c-----------------------------------------
c

      subroutine peval(x,p,pars) 
      implicit real*8 (a-h,o-z)
      real*8 x, pars(1)
      real*8 p(2,2)
c
      p(1,1)=2.0d0
      p(2,1)=0.5d0
      p(1,2)=0.1d0
      p(2,2)=3.0d0
c
      end subroutine




      subroutine feval(x,f,pars)
      implicit real*8 (a-h,o-z)
      real*8 x, pars(1)
      real*8 f(2)

      f(1)=0.22d0*x+0.13d0
      f(2)=0.65d0*x+1.1d0

      end subroutine





      subroutine fdens(x,sigma,pars)
      implicit real*8 (a-h,o-z)
      real*8 x, pars(1)
      real*8 sigma(2)

      sigma(1)=2.0d0*x
      sigma(2)=x+1.0d0
c
      end subroutine




      subroutine fdens2(x,phi,pars)
      implicit real*8 (a-h,o-z)
      real*8 x, pars(1)
      real*8 phi(2,2)

      phi(1,1)=2.0d0*x
      phi(2,1)=3.0d0
      phi(1,2)=1.0d0
      phi(2,2)=x+1.0d0
c
      end subroutine
c




      subroutine prod_ext(vl,vr,ale,are,dle,dre)
      implicit real*8 (a-h,o-z)
      real*8 vl(2,2), vr(2,2)
      real*8 ale(2,2), are(2,2)
      real*8 dle(2), dre(2)
c
      dle(1)=vl(1,1)+1.5d0*vl(1,2)
      dle(2)=vl(2,1)+1.5d0*vl(2,2)
c
      dre(1)=vr(1,1)+1.5d0*vr(1,2)
      dre(2)=vr(2,1)+1.5d0*vr(2,2)
c
      ale(1,1)=vl(1,1)+3.0d0*vl(1,2)
      ale(2,1)=vl(2,1)+3.0d0*vl(2,2)
      ale(1,2)=1.5d0*vl(1,2)+vl(1,1)
      ale(2,2)=1.5d0*vl(2,2)+vl(2,1)
c
      are(1,1)=vr(1,1)+3.0d0*vr(1,2)
      are(2,1)=vr(2,1)+3.0d0*vr(2,2)
      are(1,2)=1.5d0*vr(1,2)+vr(1,1)
      are(2,2)=1.5d0*vr(2,2)+vr(2,1)
c


      end subroutine
