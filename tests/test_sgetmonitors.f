c     tests the subroutine sgetmonitors
c     by sampling a given function (vector) at the cheb pts
c     of the leaf nodes of a given tree
c     then calling sgetmonitors
c     
      program test
      implicit real*8 (a-h,o-z)
      integer nmax, ndeg, ndim
      parameter(nmax=100000)
      parameter(ndeg=8)
      parameter(ndim=3)
      integer nboxes, maxid, nlev
      integer levelbox(nmax)
      integer icolbox(nmax) 
      integer iparentbox(nmax), ichildbox(2,nmax)
      integer nblevel(0:nmax)
      integer iboxlev(nmax), istartlev(0:nmax)
      integer leaflist(nmax), itemparray(nmax)
      real*8 chpts(ndeg,nmax), fch(ndim,ndeg,nmax)
      real*8 ftest, xgrid(nmax), finterp(ndim,nmax)
      real*8 fg(ndim), smoni(nmax), chloc(ndeg)
c
      cent0=0.0d0
      xsize0=1.0d0
c
      eps=1.0d-10
      maxboxes=nmax/10
      maxlevel=20
c
      aroot=cent0-xsize0/2.0d0
      broot=cent0+xsize0/2.0d0
c
      nlev=2
      call unitree(levelbox, icolbox, nboxes, maxid,
     1     nlev, iparentbox, ichildbox, nblevel, 
     2     iboxlev, istartlev)
      write(*,*) 'a uniform tree made'
      write(*,*) 'nlev=', nlev
      write(*,*) 'nboxes=', nboxes
c
      call getleaflist(levelbox, icolbox, nboxes,
     1     maxid, nlev, iparentbox, ichildbox, 
     2     nblevel, iboxlev, istartlev, 
     3     nleaf, leaflist)
c
      write(*,*) 'nleaf=',nleaf
      do i=1, nleaf
        ibox=leaflist(i)
        lev=levelbox(ibox)
        icol=icolbox(ibox)
        call mkgrid(lev, icol, aroot, broot, ndeg,
     1       a, b, chloc)
        write(*,*) i, a, b
      enddo
      write(*,*) ' '
c
      do i=1, nleaf
        ibox=leaflist(i)
c
        lev=levelbox(ibox)
        icol=icolbox(ibox)
        xlength=xsize0/dble(2**levelbox(ibox))
c
        call mkgrid(lev, icol, aroot, broot, ndeg,
     1       aa, bb, chpts(1,i))
        do j=1, ndeg
          xx=chpts(j,i)
          call feval(xx,fch(1,j,i))
          do k=1, ndim
            write(201,*) k,j,i, xx, fch(k,j,i)
          enddo
        enddo
      enddo
c
c----------------------------------------------------
c

      call sgetmonitors(ndim, ndeg, nleaf, fch, 
     1     smoni, sdiv)
c
      do i=1, nleaf
        write(*,*) i,smoni(i)
        write(302,*) smoni(i)
      enddo





      end program





c------------------------------------
c     the function to be sampled
c
      subroutine feval(x,fval)
      implicit real*8 (a-h,o-z)
      real*8 x, fval(3)
c
      fval(1)=exp(-(x+0.125d0)**2/1d-4)+
     1        exp(-(x+0.135d0)**2/1d-3)   
      fval(2)=2.0d0*x+3.0d0
      fval(3)=exp(-(x-0.125d0)**2/1d-3)


      end subroutine
