%
% 4 eq model for tangential velocity and SA turbulent viscosity using diffusion model
%

function [y,ufinal] = ns2lf()

global my_dpdx 
global uinitguess 
global nu_sa_initguess 
global y
global uedge
global nusaEdge
global mu
global b
global rho
global ReL
global Minf
global balanceApproach

% plotting params
set(0,'defaultlinelinewidth',5);
set(0,'defaultlinemarkersize',2);
set(0,'defaulttextfontsize',18);

% problem params
balanceApproach = true; 
%balanceApproach = false;
my_dpdx  = -.009888 % 0.;
uedge    =  0.16814;
vedge = -.0011
nusaEdge = 225.14; 
ubal = 0.004

ReL = 3000000; 
Minf = .2;

nsub = 80;
b        =  .003125; % linelet endpoint 

%y = logspace(-8,log10(b),nsub);     % vector of points between ~ 0 and b
%y = linspace(0,b,nsub);
% initialize with nonuniform logarithmic spacing but start at 0
y(1) = 0.;
i = 1;
h = 1;
sr = 1.1;
for i=2:nsub
    y(i) = y(i-1)+h;
    h = h * (sr-.1*i/(2*nsub));
end;
% now scale so last y ends at b
y = y * b/y(nsub);
  
mu       = .99857;
rho      = 0.9998; 

  
% initialize, but seems we really need to do it in separate function
uinitguess  = uedge/b.*y;     % initial guess linear between 0 and ub
nu_sa_initguess =  nusaEdge/b.*y;
%solinit = bvpinit(y, [1 0 0 0 ]);   % initialize solution structure
solinit = bvpinit(y, @initFun);   % initialize solution structure


 % call 2pt. bval solver with fn. names as arguments
options = bvpset('Stats','on','RelTol',.1);
soln = bvp4c(@fourode, @fourbc, solinit,options);                                          

ufinal = deval(soln, y);    % evaluate and plot

qtang = ufinal(1,:);

dudyAtZero = ufinal(2,1);
new_utau = sqrt((Minf/ReL)*dudyAtZero/rho);

figure(1)
hold off
semilogx(y,ufinal(1,:),'r+');
xlabel('y');
ylabel('u velocity');
hold on
semilogx(y,ufinal(3,:)/200,'b*');


analytic = load('SAModelPlus.dat');
SA_UPlus = analytic(:,2);
Yplus = analytic(:,1);
%nu = 1;
nu = mu/rho;
yanalytic = Yplus*(Minf/ReL)* nu / new_utau;
uanalytic = new_utau*SA_UPlus;

semilogx(yanalytic,uanalytic,'g');
legend('u velocity', 'nu_{sa}/200','SA u_{analytic}','location','NorthWest');
grid on

figure(2)
hold off
yp = new_utau*y*rho*ReL/Minf/mu;  
semilogx(yp,ufinal(1,:)*new_utau);
xlabel('y+');
ylabel('u+');
grid on

hold off
figure(4);plot(y,ufinal(3,:),'b-*');
xlabel('y');
ylabel('nusa');
grid on



% -------------------------------------------------------------------

function dudy = fourode(y,u)

global my_dpdx
global mu
global rho
global b
global ReL
global Minf
global balanceApproach


Ka = .41;
Re_nondim = ReL/Minf;
nu = mu/rho;
utau = .0107; %.0279;
% try adjusting utau along with solution
dudyAtZero = u(2,1);
utau = sqrt((Minf/ReL)*dudyAtZero/rho);

%dpdx = my_dpdx * ReL/Minf;
dpdx = my_dpdx;


nu_sa = u(3);
if (y == 0) 
    nu_sa = 0; 
end

cv1 = 7.1;
chi = nu_sa/nu;
fv1 =  chi^3/(chi^3 + cv1^3);
fv2 = 1. - chi/(1.+chi*fv1);
nuturb = nu_sa * fv1;
p = (nu + nuturb)*rho;

cb2 = .622;
sigma = .66666666667;

cb1 = .1355;
cw1 = cb1/(Ka^2) + (1.+ cb2)/sigma;
cw2 = .3;
cw3 = 2.;
cv2 = .7;
cv3 = .9;

dudy = u(2)/p;
omega = abs(dudy);  %approx vorticity by dudy - dont have vx at least for now


if (y < 1.D-4)
    prod = cb1*nu_sa*omega + cb1*utau*utau*Re_nondim;
    fw = 1.d0 ;
    nu_sa_div_y = Ka * utau * Re_nondim;
else

    S_bar = (nu_sa/Re_nondim)*fv2/(Ka*Ka*y*y);
    if (S_bar >= -cv2 * omega)
        S_hat = omega + S_bar;
    else
        S_hat = omega + (omega*(cv2*cv2*omega + cv3*S_bar))/(omega*(cv3-2*cv2)-S_bar);
    end

    r       = min( (Minf/ReL) * nu_sa/(S_hat*Ka*Ka*y*y), 10);
    g       = r*(1. +  cw2*(r*r*r*r*r - 1.));
    cw3p6 = 64;

    if (abs(g) > 1.d10)
        fw = 0.d0;
    else if (abs(g) < 1.d-20)
            gp6 = 0.d0;
            fw = g* ((1. + cw3p6)/(gp6 + cw3p6)) ^(.16666666666666667);
        else
            gp6Inv = (1.d0/g)^6 ;
            gp6 = 1.d0/gp6Inv;
            fw =  g* ((1. + cw3p6)/(gp6 + cw3p6)) ^(.16666666666666667);
        end
    end
    prod = cb1*S_hat*nu_sa;
    nu_sa_div_y = nu_sa/y;
end

dest = cw1*fw*(nu_sa_div_y)^2;
F(1) = dudy;

if (balanceApproach)
    ubal =  -.01650; % = uedge*dudx + vedge*dudy + dpdx
    %val = -0.000752978574518157
    
    Ampl_x = (ubal - dpdx)/exp(-0.000375/(2.*b)); 
    if (y > 0) 
       rhs_u = dpdx + Ampl_x * exp(-0.000375/(2.*y));
    else 
       rhs_u = dpdx;
    end
    F(2) = Re_nondim *rhs_u;
else
    F(2) = dpdx *Re_nondim;
end

dnusa_dy = u(4)/(nu+nu_sa);
F(3) = dnusa_dy;
F(4) = -cb2*dnusa_dy^2 - sigma*(Re_nondim*prod-dest);



dudy = [  F(1)
    F(2)
    F(3)
    F(4) ];


% -------------------------------------------------------------------

function res = fourbc(ua,ub)   %bcs for BVP solver

global nusaEdge
global uedge


 res = [ ua(1)                 % residual should be 0 at the endpoints
         uedge-ub(1)   %ua(2)-11456   % calc based on utau from cfl3d         
         ua(3)                % at y=0,  nuturb=0
         nusaEdge-ub(3)];     % take from data

   
% -------------------------------------------------------------------

  function u = initFun(yguess)  % initial guess for BVP solver

  global uinitguess 
  global y
  global nusaEdge
  global uedge
  global b

  u(1) = uedge/b*yguess;
  index = find(y==yguess);
  if (index==1) 
      index=2;  %for first point do forward diff
   end

   u(3) = nusaEdge/b*yguess;  % nu_sa init guess  (not nu_turb but close)

   u(2) = (uinitguess(index)-uinitguess(index-1))/(y(index)-y(index-1)) * (1.+u(3)); 

   u(4) =  (1.+u(3))*nusaEdge/b;
% -------------------------------------------------------------------

