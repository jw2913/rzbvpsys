c subroutines used in the two point BVP solver:
c
c real version:
c locsolve: local integral equation solver
c mkprod: calculates inner products on leaf nodes
c chd2par: inner product to inner product
c par2chd: coupling coeffs to coupling coeffs
c evaldens: recovers the density function from
c           local solutions and coupling coeffs
c precompq: precomputes a global integral for each
c           subinterval (to recover the solution)
c quadloc: computes a local integral at each
c          grid point on each subinterval
c setbc: given bc, computes the coefficients in
c        the green's functions and inhomo term
c bctab: given the coefficients, tabulate the 
c        green's function and inhomogeneous term
c interpf: given a tree and function values on the
c          leaf nodes of the tree, interpolate the
c          function to a given grid 
c
c----------------
c
c complex version
c zlocsolve: local integral equation solver
c zmkprod: calculates inner products on leaf nodes
c zchd2par: inner product to inner product
c zpar2chd: coupling coeffs to coupling coeffs
c zevaldens: recovers the density function from
c           local solutions and coupling coeffs
c zprecompq: precomputes a global integral for each
c           subinterval (to recover the solution)
c zquadloc: computes a local integral at each
c          grid point on each subinterval
c zsetbc: given bc, computes the coefficients in
c        the green's functions and inhomo term
c zbctab: given the coefficients, tabulate the 
c        green's function and inhomo term
c
c
c-----------------------------------------------




c-----------------------------------------------
c     subroutine locsolve
c-----------------------------------------------
c
c     This subroutine forms the local integral
c     equation and solves it (given an interval)
c
c     INPUT:
c     nnd: degree of chebyshev approx
c     xlength: length of the interval
c     gl, gr, phil, phir, fval: the function values
c         (ordered the same way as the cheb nodes)
c    
c     OUTPUT:
c     phil: P^{-1} phil
c     phir: P^{-1} phir
c     fval: P^{-1} fval 
c
c-----------------------------------------------
c
      subroutine locsolve(nnd, xlength, spbx, spxb,
     1           gl, gr, phil, phir, fval)
      implicit real*8 (a-h,o-z)
      integer nnd, nmxnd
      parameter(nmxnd=100)
      real*8 xlength, gl(nnd), gr(nnd)
      real*8 spbx(nnd,nnd), spxb(nnd,nnd)
      real*8 phil(nnd), phir(nnd), fval(nnd)
c     local vars
      integer ipvt(nmxnd)
      real*8 pcc(nmxnd,nmxnd), z(nmxnd), condloc
c
      do i=1,nnd
        do k=1,nnd
c         pcc: the system's matrix
          pcc(k,i)=xlength/2.0d0 *
     1            (phil(k)*spbx(k,i)*gl(i)
     2            +phir(k)*spxb(k,i)*gr(i))
        enddo
        pcc(i,i)=1.0d0+pcc(i,i)     
      enddo
c     make sure you are passing in the right parameters
c     with the right ordering
c
c     now solve P(fval)=fval
c               P(phil)=phil
c               P(phir)=phir
c
      call dgeco(pcc,nmxnd,nnd,ipvt,condloc,z) 
c      write(*,*) 'rcond=',condloc
c
      call dgesl(pcc,nmxnd,nnd,ipvt,fval,0)
      call dgesl(pcc,nmxnd,nnd,ipvt,phil,0)
      call dgesl(pcc,nmxnd,nnd,ipvt,phir,0)


      end subroutine




c-----------------------------------------------
c     subroutine mkprod
c-----------------------------------------------
c
c     This subroutine evaluates the inner product
c     on leaf nodes 
c
c-----------------------------------------------
c
      subroutine mkprod(nnd, xlength, gl, gr, eta, 
     1           phil, phir, spdef, alpha11, alpha21, 
     2           alpha12, alpha22, deltal, deltar)
      implicit real*8 (a-h,o-z)
      integer nnd
      real*8 gl(nnd), gr(nnd), spdef(nnd)
      real*8 eta(nnd), phil(nnd), phir(nnd)
      real*8 alpha11, alpha21, alpha12, alpha22
      real*8 deltal, deltar, sc
c
      sc=xlength/2.0d0
      alpha11=0.0d0
      alpha12=0.0d0
      alpha21=0.0d0
      alpha22=0.0d0
      deltal=0.0d0
      deltar=0.0d0
c
      do i=1,nnd
        alpha11=alpha11+gl(i)*phil(i)*spdef(i)*sc
        alpha12=alpha12+gl(i)*phir(i)*spdef(i)*sc
        alpha21=alpha21+gr(i)*phil(i)*spdef(i)*sc
        alpha22=alpha22+gr(i)*phir(i)*spdef(i)*sc
        deltal = deltal+gl(i)*eta(i)*spdef(i)*sc
        deltar = deltar+gr(i)*eta(i)*spdef(i)*sc
      enddo

      end subroutine





c-----------------------------------------------
c     subroutine chd2par
c-----------------------------------------------
c
c     This subroutine computes the inner products 
c     for a parent node from the inner products
c     for its children nodes
c
c     alist: list of alpha and beta for all the nodes
c     dlist: list of delta for all the nodes
c
c-----------------------------------------------
c
      subroutine chd2par(ipar, icha, ichb, maxid,
     1           alist, dlist, cond)
      implicit real*8 (a-h,o-z)
      integer ipar, icha, ichb, maxid
      real*8 alist(2,2,maxid), dlist(2,maxid)
      real*8 cond

      deter = 1.0d0-alist(2,1,ichb)*alist(1,2,icha)
      cond=dabs(deter)
c
c     the recurrence relation (lemma 2.5)      
c
      alist(1,1,ipar)=alist(1,1,ichb)+
     1        (1.0d0-alist(1,1,ichb))*
     2  (alist(1,1,icha)-alist(1,2,icha)*alist(2,1,ichb))
     3  /deter
c
      alist(2,1,ipar)=alist(2,1,icha)+alist(2,1,ichb)*
     1  (1.0d0-alist(2,2,icha))*(1.0d0-alist(1,1,icha))
     2  /deter
c
      alist(1,2,ipar)=alist(1,2,ichb)+alist(1,2,icha)*
     1  (1.0d0-alist(2,2,ichb))*(1.0d0-alist(1,1,ichb))
     2  /deter
c
      alist(2,2,ipar)=alist(2,2,icha)+
     1         (1.0d0-alist(2,2,icha))*
     2  (alist(2,2,ichb)-alist(1,2,icha)*alist(2,1,ichb))
     3  /deter
c
      dlist(1,ipar)=(1.0d0-alist(1,1,ichb))/deter*
     1  dlist(1,icha)+dlist(1,ichb)+
     2  (alist(1,1,ichb)-1.0d0)*alist(1,2,icha)/deter*
     3  dlist(2,ichb)
c
      dlist(2,ipar)=(1.0d0-alist(2,2,icha))/deter*
     1  dlist(2,ichb)+dlist(2,icha)+
     2  (alist(2,2,icha)-1.0d0)*alist(2,1,ichb)/deter*
     3  dlist(1,icha)

      end subroutine




c-----------------------------------------------
c     subroutine par2chd
c-----------------------------------------------
c
c     This subroutine computes the coupling 
c     coefficients fot the children nodes
c     of a given parent node
c     
c     alist: list of alpha and beta for all nodes
c     dlist: list of delta for all nodes
c     clist: list of coupling coefficients (mu)
c            for all nodes
c
c-----------------------------------------------
c
      subroutine par2chd(ipar, icha, ichb, maxid,
     1           alist, dlist, clist)
      implicit real*8 (a-h,o-z)
      integer ipar, icha, ichb, maxid
      real*8 alist(2,2,maxid), dlist(2,maxid)
      real*8 clist(2,maxid), deter
c
      deter=1.0d0-alist(2,1,ichb)*alist(1,2,icha)
c
      clist(1,icha)=clist(1,ipar)
c
      clist(2,icha)=-(1.0d0-alist(1,1,icha))*
     1  alist(2,1,ichb)/deter*clist(1,ipar)+
     2  (1.0d0-alist(2,2,ichb))/deter*clist(2,ipar)+
     3  alist(2,1,ichb)/deter*
     4  (dlist(1,icha)-alist(1,2,icha)*dlist(2,ichb))-
     5  dlist(2,ichb)
c
      clist(1,ichb)=+(1.0d0-alist(1,1,icha))/deter*
     1  clist(1,ipar)-(1.0d0-alist(2,2,ichb))*
     2  alist(1,2,icha)/deter*clist(2,ipar)+
     3  alist(1,2,icha)/deter*
     4  (dlist(2,ichb)-alist(2,1,ichb)*dlist(1,icha))-
     5  dlist(1,icha)
c
      clist(2,ichb)=clist(2,ipar)
     
      end subroutine





c-----------------------------------------------
c     subroutine evaldens
c-----------------------------------------------
c
c     This subroutine recovers the density
c     function from local solutions and 
c     coupling coefficients
c
c     INPUT:
c     ndeg - degree of Cheby approx
c     coef1, coef2: left and right coupling coeffs
c     phil = P_{loc}^{-1} psil
c     phir = P_{loc}^{-1} psir
c     eta = P_{loc}^{-1} fval
c       (all the above are on a given interval)
c
c     OUTPUT:
c     sigma: density function values at Chebyshev
c            nodes on the given interval
c
c-----------------------------------------------
c
      subroutine evaldens(ndeg, sigma, coef1, coef2,
     1           phil, phir, eta)
      implicit real*8 (a-h,o-z)
      integer ndeg
      real*8 sigma(ndeg), coef1, coef2
      real*8 phil(ndeg), phir(ndeg), eta(ndeg)

      do i=1,ndeg
        sigma(i)=eta(i)+coef1*phil(i)+coef2*phir(i)
      enddo

      end subroutine




c-----------------------------------------------
c     suborutine precompq
c-----------------------------------------------
c
c     This subroutine precomputes quadratures
c     quadl and quadr for each leaf interval
c     (to be used in recovering the solution)
c
c-----------------------------------------------
c
      subroutine precompq(nleaf, leaflist, 
     1           alist, dlist, clist, quadl, quadr)
      implicit real*8 (a-h,o-z)
      integer nleaf
      integer leaflist(nleaf)
      real*8 alist(2,2,1), dlist(2,1)
      real*8 clist(2,1)
      real*8 quadl(nleaf), quadr(nleaf)
c
      quadl(1)=0.0d0
      do i=1,nleaf-1
        ibox=leaflist(i) 
        qloc=dlist(1,ibox)+clist(1,ibox)*alist(1,1,ibox)
     1                    +clist(2,ibox)*alist(1,2,ibox)
        quadl(i+1)=quadl(i)+qloc
      enddo
c
      quadr(nleaf)=0.0d0
      do i=nleaf,2,-1
        ibox=leaflist(i)
        qloc=dlist(2,ibox)+clist(1,ibox)*alist(2,1,ibox)
     1                    +clist(2,ibox)*alist(2,2,ibox)
        quadr(i-1)=quadr(i)+qloc
      enddo

      end subroutine






c-----------------------------------------------
c     subroutine quadloc
c-----------------------------------------------
c
c     This subroutine computes a local integral
c     (on [b_i, tau_j^i] and [tau_j^i, b_i])
c     to be added to the precomputed global
c     integral to recover the solution
c
c-----------------------------------------------
c
      subroutine quadloc(ndeg, xlength, sigma, 
     1           gl, gr, spbx, spxb, qlocl, qlocr)
      implicit real*8(a-h,o-z)
      integer ndeg
      real*8 xlength, sigma(ndeg)
      real*8 gl(ndeg), gr(ndeg)
      real*8 spbx(ndeg,ndeg), spxb(ndeg,ndeg)
      real*8 qlocl(ndeg), qlocr(ndeg)
c
      sc=xlength/2.0d0
      do n=1,ndeg
        qlocl(n)=0.0d0
        do m=1,ndeg
          qlocl(n)=qlocl(n)+spbx(n,m)*gl(m)*sigma(m)
        enddo
        qlocl(n)=qlocl(n)*sc
      enddo      
c
      do n=1,ndeg
        qlocr(n)=0.0d0
        do m=1,ndeg
          qlocr(n)=qlocr(n)+spxb(n,m)*gr(m)*sigma(m)
        enddo
        qlocr(n)=qlocr(n)*sc
      enddo      
           
      end subroutine





c-----------------------------------------------
c     subroutine setbc
c-----------------------------------------------
c
c     This subroutine sets the correct constants
c     in the green's function and inhomogeneous 
c     term according to a given BC
c
c     INPUT:
c     a - gc: the boundary condition:
c         za1 u'(a) + za0 u(a) = ga
c         zc1 u'(c) + zc0 u(c) = gc
c
c     OUTPUT:
c     itype: itype = 0 - Dirichlet type
c            itype = 1 - Neumann type
c            itype = 2 - not handled yet
c
c     gl0, gl1, gr0, gr1: constants in green's fun
c     ui0, ui1, ui2: constants in the inhomo term
c     wron: another constant,
c           wron= gl(x)*gr'(x)-gl'(x)*gr(x)
c
c -----------------------
c  How to choose Green's functions:
c
c  If za0 * zc0 * (c-a) + ( zc1*za0 - za1*zc0 ) > 1e-4 then use
c    # linear green's functions (q0=0) and linear inhomogeneous term
c    gl = gl1*x + gl0
c    gr = gr1*x + gr0
c    ui = ui1*x + ui0
c
c  otherwise (for Neumann type problems)
c    # cosh,sinh for green's functions (q0=-1) and quadratic u_i(x)
c    gl = gl1 * dcosh(x) - gl0 * dsinh(x)
c    gr = gr1 * dcosh(x) - gr0 * dsinh(x)
c
c    ui = ui2*x**2 + ui1*x + ui0 where
c    ( 2 a za1 + a^2 za0 , za1 + a za0 , za0 ) (ui2)   (ga)
c    (                                       ) (ui1) = (  )
c    ( 2 c za1 + c^2 zc0 , zc1 + c zc0 , zc0 ) (ui0)   (gc)
c
c    - It solves (ui2,ui0) first to get (ui0) and then
c         solves (ui2,ui1) with (ga - za0 ui0, gc - zc0 ui0)
c    - This will be performed only when | det(ui1,ui0) | < 1d-4
c         i.e. (za1 + a za0, zc1 + c zc0) // (za0, zc0)
c      So det(ui2,ui1)=0 implies that
c         (2a za1 + a^2 za0, 2c za1 + c^2 zc0 ) // (za0, zc0)
c         and the rank of the linear system is ONE.
c      Therefore it is solvable only when (ga, gc) is in the range space.
c
c-----------------------------------------------
c
      subroutine setbc(a, c, za0, za1, ga, zc0, 
     1           zc1, gc, itype, gl0, gl1, gr0, gr1,
     2           ui0, ui1, ui2, wron)
      implicit real*8 (a-h,o-z)
      real*8 za0, za1, a, zc0, zc1, c
      real*8 x, gl, gr, gl1, gr1, ui, uip, wron
      real*8 det
c
      wron = za0 * zc0 * (c-a) + ( zc1*za0 - za1*zc0 )
      if ( abs(wron) .ge. 1d-4 ) then
         itype=0
         q0 = 0d0
         gl1 = za0
         gr1 = zc0
         gl0 = -a * za0 - za1
         gr0 = -c * zc0 - zc1

         ui2 = 0d0
         ui1 = ( gc*za0 - ga*zc0 ) / wron
         ui0 = ( (c*zc0+zc1)*ga - (a*za0+za1)*gc ) / wron

         return
      end if

      wron = (za0*zc0-za1*zc1)*dsinh(c-a) + (zc1*za0-za1*zc0)*dcosh(c-a)
      if ( abs(wron) .ge. 1d-4 ) then
         q0 = -1d0
         itype=1
         gl1 = za1*dcosh(a) + za0*dsinh(a)
         gl0 = za1*dsinh(a) + za0*dcosh(a)

         gr1 = zc1*dcosh(c) + zc0*dsinh(c)
         gr0 = zc1*dsinh(c) + zc0*dcosh(c)
c     
         det = za0*zc0*(a*a-c*c) + ( 2d0*a*za1*zc0 - 2d0*c*zc1*za0 )
         if ( abs(det) .lt. 1d-4 ) then
            ui0 = 0d0
         else
            ui0 = ((a*a*za0+2d0*a*za1)*gc-(c*c*zc0+2d0*c*zc1)*ga) / det
         end if
         det = a*c*(a-c)*za0*zc0 + 2d0*(a-c)*za1*zc1
     &       + c*(2d0*a-c)*za1*zc0 + a*(a-2d0*c)*za0*zc1
         ui2 = (  (zc1+c*zc0)*(ga-ui0*za0)
     &          - (za1+a*za0)*(gc-ui0*zc0) ) / det
         ui1 = (  (2d0*a*za1+a*a*za0)*(gc-ui0*zc0)
     &          - (2d0*c*zc1+c*c*zc0)*(ga-ui0*za0) ) / det
c
         return
      end if

      print*, 'Given boundary conditions are Neumann Type', wron
      itype=2
      print*, 'This mixed boundary condition make some troubles'
      print*, 'to form two linearly independent Green''s functions'
      print*, 'using q0 = 0 or -1. Please make a new choice'
      stop

      end subroutine





c-----------------------------------------------
c     subroutine bctab
c-----------------------------------------------
c
c     This subroutine computes the green's functions
c     and ihomogeneous term,  given corresponding
c     coefficients returned by subroutine setbc 
c
c     INPUT:
c     x: the evaluation point
c     itype: itype = 0 - Dirichlet type
c            itype = 1 - Neumann type
c            itype = 2 - not handled yet
c
c     gl0, gl1, gr0, gr1: constants in green's fun
c     ui0, ui1, ui2: constants in the inhomo term
c
c     OUTPUT:
c     gl, gr: the green's functions
c     glp, grp: the first derivatives of gl and gr 
c     ui: the inhomegeneous function that satisfies
c         the BC
c     uip: the first derivative of ui
c         (all the above are evaluated at x)   
c
c-----------------------------------------------
c
      subroutine bctab(x, itype, gl0, gl1, gr0, gr1,
     1           ui0, ui1, ui2, wron, gl, gr, glp, grp,
     2           ui, uip, phil, phir, fval, 
     3           peval, qeval, feval, pars)
      implicit real*8 (a-h,o-z)
      integer itype
      real*8 x, gl0, gl1, gr0, gr1
      real*8 gl, gr, glp, grp
      real*8 ui0, ui1, ui2, wron
      real*8 ui, uip
      real*8 phil, phir, fval
      real*8 pv, qv, fv, pars(1)
      external peval, qeval, feval
c
      if(itype .eq. 0) then
c       Dirichlet type
        gl=gl1*x+gl0
        gr=gr1*x+gr0
        glp=gl1
        grp=gr1
c
c       the inhomogeneous term and its derivative
        ui=ui1*x+ui0
        uip=ui1
        uipp=0.0d0

c       the phil and phir functions and rhs
        call peval(x,pv,pars)
        call qeval(x,qv,pars)
        call feval(x,fv,pars)

        pt=pv
        qt=qv+1.0d0*itype
        phil=(pt*grp+qt*gr)/wron
        phir=(pt*glp+qt*gl)/wron
        fval=fv-(uipp+pv*uip+qv*ui)

      elseif(itype .eq. 1) then
c       Neumann type
        gl=gl1*dcosh(x)-gl0*dsinh(x)
        gr=gr1*dcosh(x)-gr0*dsinh(x)
        glp=gl1*dsinh(x)-gl0*dcosh(x)
        grp=gr1*dsinh(x)-gr0*dcosh(x)
c
        ui=ui2*x**2+ui1*x+ui0
        uip=2.0d0*ui2*x+ui1
        uipp=2.0d0*ui2
c
c       the phil and phir functions and rhs
        call peval(x,pv,pars)
        call qeval(x,qv,pars)
        call feval(x,fv,pars)

        pt=pv
        qt=qv+1.0d0*itype
        phil=(pt*grp+qt*gr)/wron
        phir=(pt*glp+qt*gl)/wron
        fval=fv-(uipp+pv*uip+qv*ui)
c
      else
        write(*,*) 'unknown BC type'
      endif
      
      end subroutine






c***********************************************
c     COMPLEX VERSION
c***********************************************




c-----------------------------------------------
c     subroutine zlocsolve
c-----------------------------------------------
c
c     This subroutine forms the local integral
c     equation and solves it (given an interval)
c
c     INPUT:
c     nnd: degree of chebyshev approx
c     xlength: length of the interval (I think)
c     gl, gr, phil, phir, fval: the function values
c         (ordered the same way as the cheb nodes)
c    
c     OUTPUT:
c     phil: P^{-1} phil
c     phir: P^{-1} phir
c     fval: P^{-1} fval 
c
c-----------------------------------------------
c
      subroutine zlocsolve(nnd, xlength, spbx, spxb,
     1           gl, gr, phil, phir, fval)
      implicit real*8 (a-h,o-z)
      integer nnd, nmxnd
      parameter(nmxnd=100)
      real*8 xlength
      complex*16 gl(nnd), gr(nnd)
      complex*16 spbx(nnd,nnd), spxb(nnd,nnd)
      complex*16 phil(nnd), phir(nnd), fval(nnd)
c     local vars
      integer ipvt(nmxnd)
      real*8 condloc
      complex*16 pcc(nmxnd,nmxnd), z(nmxnd)
c
      do i=1,nnd
        do k=1,nnd
          pcc(k,i)=xlength/2.0d0 *
     1            (phil(k)*spbx(k,i)*gl(i)
     2            +phir(k)*spxb(k,i)*gr(i))
        enddo
        pcc(i,i)=1.0d0+pcc(i,i)     
      enddo
c     make sure you are passing in the right parameters
c     with the right ordering
c
c     now solve P(fval)=fval
c               P(phil)=phil
c               P(phir)=phir
c
      call zgeco(pcc,nmxnd,nnd,ipvt,condloc,z) 
c
      call zgesl(pcc,nmxnd,nnd,ipvt,fval,0)
      call zgesl(pcc,nmxnd,nnd,ipvt,phil,0)
      call zgesl(pcc,nmxnd,nnd,ipvt,phir,0)


      end subroutine






c-----------------------------------------------
c     subroutine zmkprod
c-----------------------------------------------
c
c     This subroutine evaluates the inner product
c     on leaf nodes 
c
c-----------------------------------------------
c
      subroutine zmkprod(nnd, xlength, gl, gr, eta, 
     1           phil, phir, spdef, alpha11, alpha21, 
     2           alpha12, alpha22, deltal, deltar)
      implicit real*8 (a-h,o-z)
      integer nnd
      real*8 xlength, sc
      complex*16 gl(nnd), gr(nnd), spdef(nnd)
      complex*16 eta(nnd), phil(nnd), phir(nnd)
      complex*16 alpha11, alpha21, alpha12, alpha22
      complex*16 deltal, deltar
c
      sc=xlength/2.0d0
      alpha11=0.0d0
      alpha12=0.0d0
      alpha21=0.0d0
      alpha22=0.0d0
      deltal=0.0d0
      deltar=0.0d0
c
      do i=1,nnd
        alpha11=alpha11+gl(i)*phil(i)*spdef(i)*sc
        alpha12=alpha12+gl(i)*phir(i)*spdef(i)*sc
        alpha21=alpha21+gr(i)*phil(i)*spdef(i)*sc
        alpha22=alpha22+gr(i)*phir(i)*spdef(i)*sc
        deltal = deltal+gl(i)*eta(i)*spdef(i)*sc
        deltar = deltar+gr(i)*eta(i)*spdef(i)*sc
      enddo

      end subroutine





c-----------------------------------------------
c     subroutine zchd2par
c-----------------------------------------------
c
c     This subroutine computes the inner products 
c     for a parent node from the inner products
c     for its children nodes
c
c     alist: list of alpha and beta for all the nodes
c     dlist: list of delta for all the nodes
c
c-----------------------------------------------
c
      subroutine zchd2par(ipar, icha, ichb, maxid,
     1           alist, dlist, cond)
      implicit real*8 (a-h,o-z)
      integer ipar, icha, ichb, maxid
      real*8 cond
      complex*16 deter
      complex*16 alist(2,2,maxid), dlist(2,maxid)

      deter = 1.0d0-alist(2,1,ichb)*alist(1,2,icha)
      cond=abs(deter)
c
c     the recurrence relation (lemma 2.5)      
c
      alist(1,1,ipar)=alist(1,1,ichb)+
     1        (1.0d0-alist(1,1,ichb))*
     2  (alist(1,1,icha)-alist(1,2,icha)*alist(2,1,ichb))
     3  /deter
c
      alist(2,1,ipar)=alist(2,1,icha)+alist(2,1,ichb)*
     1  (1.0d0-alist(2,2,icha))*(1.0d0-alist(1,1,icha))
     2  /deter
c
      alist(1,2,ipar)=alist(1,2,ichb)+alist(1,2,icha)*
     1  (1.0d0-alist(2,2,ichb))*(1.0d0-alist(1,1,ichb))
     2  /deter
c
      alist(2,2,ipar)=alist(2,2,icha)+
     1         (1.0d0-alist(2,2,icha))*
     2  (alist(2,2,ichb)-alist(1,2,icha)*alist(2,1,ichb))
     3  /deter
c
      dlist(1,ipar)=(1.0d0-alist(1,1,ichb))/deter*
     1  dlist(1,icha)+dlist(1,ichb)+
     2  (alist(1,1,ichb)-1.0d0)*alist(1,2,icha)/deter*
     3  dlist(2,ichb)
c
      dlist(2,ipar)=(1.0d0-alist(2,2,icha))/deter*
     1  dlist(2,ichb)+dlist(2,icha)+
     2  (alist(2,2,icha)-1.0d0)*alist(2,1,ichb)/deter*
     3  dlist(1,icha)

      end subroutine






c-----------------------------------------------
c     subroutine zpar2chd
c-----------------------------------------------
c
c     This subroutine computes the coupling 
c     coefficients fot the children nodes
c     of a given parent node
c     
c     alist: list of alpha and beta for all nodes
c     dlist: list of delta for all nodes
c     clist: list of coupling coefficients (mu)
c            for all nodes
c
c-----------------------------------------------
c
      subroutine zpar2chd(ipar, icha, ichb, maxid,
     1           alist, dlist, clist)
      implicit real*8 (a-h,o-z)
      integer ipar, icha, ichb, maxid
      complex*16 alist(2,2,maxid), dlist(2,maxid)
      complex*16 clist(2,maxid), deter
c
      deter=1.0d0-alist(2,1,ichb)*alist(1,2,icha)
c
      clist(1,icha)=clist(1,ipar)
c
      clist(2,icha)=-(1.0d0-alist(1,1,icha))*
     1  alist(2,1,ichb)/deter*clist(1,ipar)+
     2  (1.0d0-alist(2,2,ichb))/deter*clist(2,ipar)+
     3  alist(2,1,ichb)/deter*
     4  (dlist(1,icha)-alist(1,2,icha)*dlist(2,ichb))-
     5  dlist(2,ichb)
c
      clist(1,ichb)=+(1.0d0-alist(1,1,icha))/deter*
     1  clist(1,ipar)-(1.0d0-alist(2,2,ichb))*
     2  alist(1,2,icha)/deter*clist(2,ipar)+
     3  alist(1,2,icha)/deter*
     4  (dlist(2,ichb)-alist(2,1,ichb)*dlist(1,icha))-
     5  dlist(1,icha)
c
      clist(2,ichb)=clist(2,ipar)
     
      end subroutine






c-----------------------------------------------
c     subroutine zevaldens
c-----------------------------------------------
c
c     This subroutine recovers the density
c     function from local solutions and 
c     coupling coefficients
c
c     INPUT:
c     ndeg - degree of Cheby approx
c     coef1, coef2: left and right coupling coeffs
c     phil = P_{loc}^{-1} psil
c     phir = P_{loc}^{-1} psir
c     eta = P_{loc}^{-1} fval
c       (all the above are on a given interval)
c
c     OUTPUT:
c     sigma: density function values at Chebyshev
c            nodes on the given interval
c
c-----------------------------------------------
c
      subroutine zevaldens(ndeg, sigma, coef1, coef2,
     1           phil, phir, eta)
      implicit real*8 (a-h,o-z)
      integer ndeg
      complex*16 sigma(ndeg), coef1, coef2
      complex*16 phil(ndeg), phir(ndeg), eta(ndeg)

      do i=1,ndeg
        sigma(i)=eta(i)+coef1*phil(i)+coef2*phir(i)
      enddo

      end subroutine






c-----------------------------------------------
c     suborutine zprecompq
c-----------------------------------------------
c
c     This subroutine precomputes quadratures
c     quadl and quadr for each leaf interval
c     (to be used in recovering the solution)
c
c-----------------------------------------------
c
      subroutine zprecompq(nleaf, leaflist, 
     1           alist, dlist, clist, quadl, quadr)
      implicit real*8 (a-h,o-z)
      integer nleaf
      integer leaflist(nleaf)
      complex*16 alist(2,2,1), dlist(2,1)
      complex*16 clist(2,1), qloc
      complex*16 quadl(nleaf), quadr(nleaf)
c
      quadl(1)=0.0d0
      do i=1,nleaf-1
        ibox=leaflist(i) 
        qloc=dlist(1,ibox)+clist(1,ibox)*alist(1,1,ibox)
     1                    +clist(2,ibox)*alist(1,2,ibox)
        quadl(i+1)=quadl(i)+qloc
      enddo
c
      quadr(nleaf)=0.0d0
      do i=nleaf,2,-1
        ibox=leaflist(i)
        qloc=dlist(2,ibox)+clist(1,ibox)*alist(2,1,ibox)
     1                    +clist(2,ibox)*alist(2,2,ibox)
        quadr(i-1)=quadr(i)+qloc
      enddo

      end subroutine






c-----------------------------------------------
c     subroutine zquadloc
c-----------------------------------------------
c
c     This subroutine computes a local integral
c     (on [b_i, tau_j^i] and [tau_j^i, b_i])
c     to be added to the precomputed global
c     integral to recover the solution
c
c-----------------------------------------------
c
      subroutine zquadloc(ndeg, xlength, sigma, 
     1           gl, gr, spbx, spxb, qlocl, qlocr)
      implicit real*8(a-h,o-z)
      integer ndeg
      real*8 xlength, sc
      complex*16 sigma(ndeg), gl(ndeg), gr(ndeg)
      complex*16 spbx(ndeg,ndeg), spxb(ndeg,ndeg)
      complex*16 qlocl(ndeg), qlocr(ndeg)
c
      sc=xlength/2.0d0
      do n=1,ndeg
        qlocl(n)=0.0d0
        do m=1,ndeg
          qlocl(n)=qlocl(n)+spbx(n,m)*gl(m)*sigma(m)
        enddo
        qlocl(n)=qlocl(n)*sc
      enddo      
c
      do n=1,ndeg
        qlocr(n)=0.0d0
        do m=1,ndeg
          qlocr(n)=qlocr(n)+spxb(n,m)*gr(m)*sigma(m)
        enddo
        qlocr(n)=qlocr(n)*sc
      enddo      
           
      end subroutine





c-----------------------------------------------
c     subroutine zsetbc
c-----------------------------------------------
c
c     This subroutine sets the correct constants
c     in the green's function and inhomogeneous 
c     term according to a given BC
c
c     INPUT:
c     a - gc: the boundary condition:
c         za1 u'(a) + za0 u(a) = ga
c         zc1 u'(c) + zc0 u(c) = gc
c
c     OUTPUT:
c     itype: itype = 0 - Dirichlet type
c            itype = 1 - Neumann type
c            itype = 2 - not handled yet
c
c     gl0, gl1, gr0, gr1: constants in green's fun
c     ui0, ui1, ui2: constants in the inhomo term
c     wron: another constant,
c           wron= gl(x)*gr'(x)-gl'(x)*gr(x)
c
c -----------------------
c  How to choose Green's functions:
c
c  If za0 * zc0 * (c-a) + ( zc1*za0 - za1*zc0 ) > 1e-4 then use
c    # linear green's functions (q0=0) and linear inhomogeneous term
c    gl = gl1*x + gl0
c    gr = gr1*x + gr0
c    ui = ui1*x + ui0
c
c  otherwise (for Neumann type problems)
c    # cosh,sinh for green's functions (q0=-1) and quadratic u_i(x)
c    gl = gl1 * dcosh(x) - gl0 * dsinh(x)
c    gr = gr1 * dcosh(x) - gr0 * dsinh(x)
c
c    ui = ui2*x**2 + ui1*x + ui0 where
c    ( 2 a za1 + a^2 za0 , za1 + a za0 , za0 ) (ui2)   (ga)
c    (                                       ) (ui1) = (  )
c    ( 2 c za1 + c^2 zc0 , zc1 + c zc0 , zc0 ) (ui0)   (gc)
c
c    - It solves (ui2,ui0) first to get (ui0) and then
c         solves (ui2,ui1) with (ga - za0 ui0, gc - zc0 ui0)
c    - This will be performed only when | det(ui1,ui0) | < 1d-4
c         i.e. (za1 + a za0, zc1 + c zc0) // (za0, zc0)
c      So det(ui2,ui1)=0 implies that
c         (2a za1 + a^2 za0, 2c za1 + c^2 zc0 ) // (za0, zc0)
c         and the rank of the linear system is ONE.
c      Therefore it is solvable only when (ga, gc) is in the range space.
c
c-----------------------------------------------
c
      subroutine zsetbc(a, c, za0, za1, ga, zc0, 
     1           zc1, gc, itype, gl0, gl1, gr0, gr1,
     2           ui0, ui1, ui2, wron)
      implicit real*8 (a-h,o-z)
      integer itype
      real*8 a, c
      complex*16 za0, za1, ga, zc0, zc1, gc
      complex*16 gl0, gl1, gr0, gr1
      complex*16 ui0, ui1, ui2, wron
      complex*16 det
c
      wron = za0 * zc0 * (c-a) + ( zc1*za0 - za1*zc0 )
      if ( abs(wron) .ge. 1d-4 ) then
         itype=0
         q0 = 0d0
         gl1 = za0
         gr1 = zc0
         gl0 = -a * za0 - za1
         gr0 = -c * zc0 - zc1

         ui2 = 0d0
         ui1 = ( gc*za0 - ga*zc0 ) / wron
         ui0 = ( (c*zc0+zc1)*ga - (a*za0+za1)*gc ) / wron

         return
      end if

      wron = (za0*zc0-za1*zc1)*dsinh(c-a) + (zc1*za0-za1*zc0)*dcosh(c-a)
      if ( abs(wron) .ge. 1d-4 ) then
         q0 = -1d0
         itype=1
         gl1 = za1*dcosh(a) + za0*dsinh(a)
         gl0 = za1*dsinh(a) + za0*dcosh(a)
         gr1 = zc1*dcosh(c) + zc0*dsinh(c)
         gr0 = zc1*dsinh(c) + zc0*dcosh(c)
c     
         det = za0*zc0*(a*a-c*c) + ( 2d0*a*za1*zc0 - 2d0*c*zc1*za0 )
         if ( abs(det) .lt. 1d-4 ) then
            ui0 = 0d0
         else
            ui0 = ((a*a*za0+2d0*a*za1)*gc-(c*c*zc0+2d0*c*zc1)*ga) / det
         end if
         det = a*c*(a-c)*za0*zc0 + 2d0*(a-c)*za1*zc1
     &       + c*(2d0*a-c)*za1*zc0 + a*(a-2d0*c)*za0*zc1
         ui2 = (  (zc1+c*zc0)*(ga-ui0*za0)
     &          - (za1+a*za0)*(gc-ui0*zc0) ) / det
         ui1 = (  (2d0*a*za1+a*a*za0)*(gc-ui0*zc0)
     &          - (2d0*c*zc1+c*c*zc0)*(ga-ui0*za0) ) / det
c
         return
      end if

      print*, 'Given boundary conditions are Neumann Type', wron
      itype=2
      print*, 'This mixed boundary condition make some troubles'
      print*, 'to form two linearly independent Green''s functions'
      print*, 'using q0 = 0 or -1. Please make a new choice'
      stop

      end subroutine





c-----------------------------------------------
c     subroutine zbctab
c-----------------------------------------------
c
c     This subroutine computes the green's functions
c     and ihomogeneous term,  given corresponding
c     coefficients returned by subroutine setbc 
c
c     INPUT:
c     x: the evaluation point
c     itype: itype = 0 - Dirichlet type
c            itype = 1 - Neumann type
c            itype = 2 - not handled yet
c
c     gl0, gl1, gr0, gr1: constants in green's fun
c     ui0, ui1, ui2: constants in the inhomo term
c
c     OUTPUT:
c     gl, gr: the green's functions
c     glp, grp: the first derivatives of gl and gr 
c     ui: the inhomegeneous function that satisfies
c         the BC
c     uip: the first derivative of ui
c         (all the above are evaluated at x)   
c
c-----------------------------------------------
c
      subroutine zbctab(x, itype, gl0, gl1, gr0, gr1,
     1           ui0, ui1, ui2, wron, gl, gr, glp, grp,
     2           ui, uip, phil, phir, fval, 
     3           peval, qeval, feval, pars)
      implicit real*8 (a-h,o-z)
      integer itype
      real*8 x
      complex*16 gl0, gl1, gr0, gr1
      complex*16 gl, gr, glp, grp
      complex*16 ui0, ui1, ui2, wron
      complex*16 ui, uip, uipp
      complex*16 phil, phir, fval
      complex*16 pv, qv, fv, pt, qt, pars(1)
      external peval, qeval, feval
c
      if(itype .eq. 0) then
c       Dirichlet type
        gl=gl1*x+gl0
        gr=gr1*x+gr0
        glp=gl1
        grp=gr1
c
c       the inhomogeneous term and its derivative
        ui=ui1*x+ui0
        uip=ui1
        uipp=0.0d0
c
c       the phil and phir functions and rhs
        call peval(x,pv,pars)
        call qeval(x,qv,pars)
        call feval(x,fv,pars)

        pt=pv
        qt=qv+1.0d0*itype
        phil=(pt*grp+qt*gr)/wron
        phir=(pt*glp+qt*gl)/wron
        fval=fv-(uipp+pv*uip+qv*ui)

      elseif(itype .eq. 1) then
c       Neumann type
        gl=gl1*dcosh(x)-gl0*dsinh(x)
        gr=gr1*dcosh(x)-gr0*dsinh(x)
        glp=gl1*dsinh(x)-gl0*dcosh(x)
        grp=gr1*dsinh(x)-gr0*dcosh(x)
c
        ui=ui2*x**2+ui1*x+ui0
        uip=2.0d0*ui2*x+ui1
        uipp=2.0d0*ui2
c
c       the phil and phir functions and rhs
        call peval(x,pv,pars)
        call qeval(x,qv,pars)
        call feval(x,fv,pars)

        pt=pv
        qt=qv+1.0d0*itype
        phil=(pt*grp+qt*gr)/wron
        phir=(pt*glp+qt*gl)/wron
        fval=fv-(uipp+pv*uip+qv*ui)
c
      else
        write(*,*) 'unknown BC type'
      endif
      
      end subroutine







c-----------------------------------------------
c     subroutine interpf
c-----------------------------------------------
c
c  given a tree and function values on the leaf 
c  nodes of the tree, interpolate the function to 
c  a given grid 
c
c-----------------------------------------------
c
      subroutine interpf(nlev, levelbox, iparentbox,
     1           ichildbox, icolbox, nboxes, maxid,
     2           nblevel, iboxlev, istartlev, cent0,
     3           xsize0, nleaf, leaflist, ndeg, fvals,
     4           ngrid, xgrid, finterp)
      implicit real*8 (a-h,o-z)
      integer nboxes, maxid, nlev
      integer nleaf, ndeg, ngrid
      integer levelbox(1)
      integer icolbox(1) 
      integer iparentbox(1), ichildbox(2,1)
      integer nblevel(0:1)
      integer iboxlev(1), istartlev(0:1)
      integer leaflist(nleaf)
      real*8 fvals(ndeg,nleaf), xgrid(ngrid)
      real*8 finterp(ngrid), cent0, xsize0
c     local variables, allocatable
      integer, allocatable:: npbox(:), ipold(:)
      integer, allocatable:: istartbox(:), ibofp(:)
      real*8, allocatable:: xpts(:)
      real*8 coeff(1000)
      complex*16 wsave(1000), work(1000)
c
      allocate(npbox(maxid))
      allocate(ipold(ngrid))
      allocate(istartbox(maxid))
      allocate(ibofp(ngrid))
      allocate(xpts(ngrid))
c
      npts=ngrid
      do i=1, npts
        xpts(i)=xgrid(i)
      enddo
c     copy over xgrid, so as not to overwrite it
c
c     1. sort the points into the tree
      call treesort(nlev,levelbox,iparentbox,ichildbox,
     1     icolbox,nboxes,maxid, nblevel,iboxlev,
     2     istartlev,xpts,npts,npbox,ipold,istartbox,
     3     ibofp,cent0,xsize0)
c
c     2. go thru the leaf nodes, do the cheby transf
c        fval -> coeffs, then eval coeffs -> finterp 
c        at xgrid in the node
c
      call chxcin(ndeg,wsave)
      x0=cent0-xsize0/2.0d0
c
      do i=1, nleaf
        ibox=leaflist(i)
        if(npbox(ibox) .gt. 0) then
          lev=levelbox(ibox)
          icol=icolbox(ibox)
c
c         set the left and right enpoints of the 
c         interval ibox
          xlength=0.5d0**lev*xsize0
          a=dble(icol-1)*xlength+x0
          b=a+xlength
c          
          call chexfc(fvals(1,i),ndeg,coeff,wsave,work) 
c          call chexfc(fvals(1,ibox),ndeg,coeff,wsave,work) 
          istart=istartbox(ibox)
          iend=istart+npbox(ibox)-1
          do ipt=istart,iend
            xx=xpts(ipt)
            call cheval(xx,finterp(ipt),coeff,ndeg,a,b)
          enddo
        endif
      enddo
c
c     3. reverse the ordering of finterp to the
c        order of xgrid
      call rvecnew2old(1, ngrid, finterp, ipold)
c
c
      deallocate(npbox)
      deallocate(ipold)
      deallocate(istartbox)
      deallocate(ibofp)
      deallocate(xpts)
c
      end subroutine





c-----------------------------------------------
c     subroutine getmonitors
c-----------------------------------------------
c
c     Given function values at cheby pts, eval
c     the monitor function
c
c     input:
c     ndeg: order
c     nleaf: number of leaf nodes (where fvals are
c            sampled)
c     fvals: function values at chey pts on the leaf
c            nodes (dim(ndeg,nleaf))
c
c     output:
c     smoni: monitor function, one value for each
c            leaf node, array of length nleaf
c     sdiv: a real number, defined by:
c           max(smoni)/2**4 
c
c-----------------------------------------------
      subroutine getmonitors(ndeg, nleaf, fvals, smoni, sdiv)
      implicit real*8 (a-h,o-z)
      integer ndeg, nleaf
      real*8 fvals(ndeg,nleaf), smoni(nleaf), sdiv
      real*8 coeff(1000)
      complex*16 wsave(1000), work(1000)
c
      call chxcin(ndeg,wsave)
c
      do i=1, nleaf
        call chexfc(fvals(1,i),ndeg,coeff,wsave,work)
        smoni(i)=abs(coeff(ndeg-1))+
     1           abs(coeff(ndeg-2)-coeff(ndeg))
      enddo
c
      sdiv=0.0d0
      do i=1, nleaf
        si=smoni(i)/16.0d0
        if(si .gt. sdiv) then
          sdiv=si
        endif
      enddo
      
      end subroutine






c-----------------------------------------------
c     subroutine ssetbc
c-----------------------------------------------
c
c     This subroutine sets the correct constant
c     matrices in the green's function and 
c     inhomogeneous term according to a given BC.
c     This is the system's case 
c
c     INPUT:
c     ndim: dimension of the system
c     bc**: the boundary condition matrices/vector,
c           as in:
c           bca*u(xa) + bcb*u(xb) = bcv  (2)
c
c     OUTPUT:
c     ui: the inhomogeneous term, in this case, we
c         set it to be a constant vector:
c         ui=inv(bca+bcb)*bcv
c     vl, vr: matrices related to the fundamental
c           matrix of the background problem.
c           In this case, we FIX the background 
c           problem to be:
c           phi'(x)=0, and the fundamental matrix is 
c           the identity matrix Id(ndim,ndim)
c     vl: the matrix: Id - inv(bca+bcb)*bcb
c     vr: the matrix: - inv(bca+bcb)*bcb
c
c-----------------------------------------------
c
      subroutine ssetbc(ndim, bca, bcb, bcv, ui, vl, vr)
      implicit real*8 (a-h,o-z)
      integer ndim
      real*8 bca(ndim,ndim), bcb(ndim,ndim)
      real*8 bcv(ndim), ui(ndim)
      real*8 vl(ndim,ndim), vr(ndim,ndim)
      integer, allocatable:: ipvt(:)
      real*8, allocatable:: rwork(:)
      real*8, allocatable:: dmat(:,:)
c
      allocate(ipvt(ndim))
      allocate(rwork(ndim))
      allocate(dmat(ndim,ndim))
c
c     copy over the matrices and vectors
c     so as not to change the input
      do j=1, ndim
        do i=1, ndim
          dmat(i,j)=bca(i,j)+bcb(i,j)
          vr(i,j)=-bcb(i,j)
        enddo
        ui(j)=bcv(j)
      enddo
c
      call dgeco(dmat,ndim,ndim,ipvt,rcond,rwork)
c      write(*,*) '*** rcond=',rcond
c
      call dgesl(dmat,ndim,ndim,ipvt,ui,0)
      do j=1, ndim
        call dgesl(dmat,ndim,ndim,ipvt,vr(1,j),0)
      enddo
c
      do j=1, ndim
      do i=1, ndim
        vl(i,j)=vr(i,j)
      enddo
      enddo
c
      do i=1, ndim
        vl(i,i)=vl(i,i)+1.0d0
      enddo
c     add an id matrix to it


      deallocate(ipvt)
      deallocate(rwork)
      deallocate(dmat)
c
c
      end subroutine





c-----------------------------------------------
c     subroutine sbctab
c-----------------------------------------------
c
c     This subroutine computes the RHS of the 
c     integral equations,  given corresponding
c     matrices returned by subroutine ssetbc 
c
c     INPUT:
c     ndim: dimension of the system
c     x: the evaluation point
c     ui: the inhomogeneous term, a vec returned by 
c         subroutine ssetbc
c     peval: subroutine for the evaluation of p(x)
c     feval: subroutine for the evaluation of f(x) 
c            as in the equation
c            u'(x)+ p(x)*u(x)  = f(x) 
c            with the calling seq:
c            peval(x,p,pars), feval(x,f,pars)
c     pars: the parameters needed by peval, feval
c
c     OUTPUT:
c     fval: the rhs (vec) of the integral eqn,
c           fval(x)=f(x)-p(x)*ui(x)
c     phival: the rhs (mat) of the integral eqn,
c           phival(x)= (p(x)-p0(x))*gam0, 
c
c     RMK: in this case we fix p0(x)=0 and gam0=Id, 
c          thus we have phival(x)=p(x)
c
c-----------------------------------------------
c
      subroutine sbctab(ndim,x,ui,fval,phival,peval,
     1           feval,pars)
      implicit real*8 (a-h,o-z)
      integer ndim
      real*8 x, ui(ndim), fval(ndim)
      real*8 pars(1), phival(ndim,ndim)
      integer, allocatable:: ipvt(:)
      real*8, allocatable:: rwork(:)
      real*8, allocatable:: rvec(:), rmat(:,:)
      external peval, feval
c
      allocate(ipvt(ndim))
      allocate(rwork(ndim))
      allocate(rvec(ndim))
      allocate(rmat(ndim,ndim))
c
      call peval(x,phival,pars)
      call feval(x,fval,pars)
c
      do i=1, ndim
        rvec(i)=0.0d0
        do j=1, ndim
          rvec(i)=rvec(i)+phival(i,j)*ui(j)
        enddo
      enddo
c
c
      do j=1, ndim
        fval(j)=fval(j)-rvec(j)
      enddo
c
c
      deallocate(ipvt)
      deallocate(rwork)
      deallocate(rvec)
      deallocate(rmat)
c
      end subroutine






c-----------------------------------------------
c     subroutine formsmat
c-----------------------------------------------
c
c     This subroutine forms the spectral integration
c     matrices for the following operator/functional,
c     given the plain spectral integration matrices
c     spdef, spbx, spxb.
c
c     RMKs:
c     functionals:
c     sdvl[sigma] =\int_a^b vl*sigma(t) dt  (1)
c     sdvr[sigma] =\int_a^b vr*sigma(t) dt  (2)
c
c     operators:
c     svlvr[sigma] = \int_a^x vl*sigma(t) dt 
c                  + \int_x^b vr*sigma(t) dt (3)
c
c     intermediate results:
c     slvl[sigma] = \int_a^x vl*sigma(t) dt 
c     srvr[sigma] = \int_x^b vr*sigma(t) dt 
c
c     vectors are stacked in the order of
c     (sigma_1(x_1),...,sigma_d(x_1), ...,
c      sigma_1(x_n),...,sigma_d(x_n))
c     upon input and output
c
c     INPUT:
c     ndim: dimension of the system
c     ndeg: degree of the method
c     spdef: the definite chebyshev integration vec
c     spbx: the left chebyshev integration mat
c     spxb: the right chebyshev integration mat
c     vl: a constant matrix of dimension ndim x ndim
c     vr: a constant matrix of dimension ndim x ndim
c
c     OUTPUT:
c     sdvl: the discretization of the functional (1)  
c     sdvr: the discretization of the functional (2)  
c     svlvr: the discretization of the operator (3)  
c
c-----------------------------------------------
c
      subroutine formsmat(ndim, ndeg, spdef, spbx, spxb,
     1           vl, vr, sdvl, sdvr, svlvr)
      implicit real*8 (a-h,o-z)
      integer ndim, ndeg
      real*8 spdef(ndeg) 
      real*8 spbx(ndeg,ndeg), spxb(ndeg,ndeg)
      real*8 sdvl(ndim,ndeg*ndim), sdvr(ndim,ndeg*ndim)
      real*8 svlvr(ndeg*ndim,ndeg*ndim)
      real*8 vl(ndim,ndim), vr(ndim,ndim)
      real*8, allocatable:: slmat(:,:), srmat(:,:)
      real*8, allocatable:: slvl(:,:), srvr(:,:)
      real*8, allocatable:: vldiag(:,:)
      real*8, allocatable:: vrdiag(:,:)
      real*8, allocatable:: sdmat(:,:)
c
      allocate(slmat(ndeg*ndim,ndeg*ndim))
      allocate(srmat(ndeg*ndim,ndeg*ndim))
      allocate(slvl(ndeg*ndim,ndeg*ndim))
      allocate(srvr(ndeg*ndim,ndeg*ndim))
      allocate(vldiag(ndeg*ndim,ndeg*ndim))
      allocate(vrdiag(ndeg*ndim,ndeg*ndim))
      allocate(sdmat(ndim,ndeg*ndim))
c
c     the (big) spectral integration matrices,
c     for the system's case, initialize to zero
      do j=1, ndeg*ndim
        do i=1, ndeg*ndim
          slmat(i,j)=0.0d0
          srmat(i,j)=0.0d0
        enddo
        do k=1, ndim
          sdmat(k,j)=0.0d0
        enddo
      enddo

c
c     spbx -> slmat, spxb -> srmat
      do i=1, ndim
        do k=1, ndeg
        do j=1, ndeg
          slmat((j-1)*ndim+i,(k-1)*ndim+i)=spbx(j,k)
          srmat((j-1)*ndim+i,(k-1)*ndim+i)=spxb(j,k)
        enddo
        enddo
      enddo
c
      do k=1, ndeg
        kk=(k-1)*ndim
        do i=1, ndim
          sdmat(i,kk+i)=spdef(k)
        enddo
      enddo
c
c
c     form the block diagonal matrices 
c     vldiag and vrdiag
      do k=1, ndeg
        kk=(k-1)*ndim
        do j=1, ndim
        do i=1, ndim
          vldiag(kk+i,kk+j)=vl(i,j)
          vrdiag(kk+i,kk+j)=vr(i,j)
        enddo
        enddo
      enddo
c
c     slvl = slmat * diag(vl, ..., vl)
c     srvr = srmat * diag(vr, ..., vr)
      nn=ndim*ndeg
      do j=1, nn
        do i=1, nn
          slvl(i,j)=0.0d0
          srvr(i,j)=0.0d0
          do k=1, nn
            slvl(i,j)=slvl(i,j)+slmat(i,k)*vldiag(k,j)
            srvr(i,j)=srvr(i,j)+srmat(i,k)*vrdiag(k,j)
          enddo
          svlvr(i,j)=(slvl(i,j)+srvr(i,j))
        enddo
      enddo
c
c     sdvl = sdmat * diag(vl, ..., vl)
c     sdvr = sdmat * diag(vr, ..., vr)
      nn=ndim*ndeg
      do j=1, nn
        do i=1, ndim
          sdvl(i,j)=0.0d0
          sdvr(i,j)=0.0d0
          do k=1, nn
            sdvl(i,j)=sdvl(i,j)+sdmat(i,k)*vldiag(k,j)
            sdvr(i,j)=sdvr(i,j)+sdmat(i,k)*vrdiag(k,j)
          enddo
        enddo
      enddo
c
c-----------------------------------------------
c     print out slvl, srvr, svlvr for debugging
      do j=1, ndim
      do i=1, ndim
        write(303,*) vl(i,j), vr(i,j), i, j
      enddo
      enddo
c
      do j=1, nn
      do i=1, nn
        write(302,*) slvl(i,j), srvr(i,j),
     1               svlvr(i,j), i, j
      enddo
      enddo
c
      deallocate(slmat)
      deallocate(srmat)
      deallocate(slvl)
      deallocate(srvr)
      deallocate(vldiag)
      deallocate(vrdiag)
      deallocate(sdmat)
c
      end subroutine







c-----------------------------------------------
c     subroutine slocsolve
c-----------------------------------------------
c
c     This subroutine forms the local integral
c     equation and solves it (given an interval)
c     (the system's version)
c
c     INPUT:
c     ndim: dimension of the system
c     ndeg: degree of the method
c     xlength: length of the interval
c     sdvl, sdvr: the spectral (definite) integration
c           matrices, returned by formsmat
c     slvl, srvr: the spectral (indef) integration
c           matrices, returned by formsmat
c     phi: the matrix in the operator P/Q, sampled
c          at the local Cheby nodes
c     fval: the rhs vec, sampled at the local Cheby
c           nodes
c
c     OUTPUT:
c     eta: the density vec, solving P[eta]=f,
c          sampled at the local Cheby nodes
c     xi: the density mat, solving Q[xi]=phi,
c          sampled at the local Cheby nodes
c
c-----------------------------------------------
c
      subroutine slocsolve(ndim, ndeg, svlvr, phi, 
     1           fval, xlength, eta, xi)
      implicit real*8 (a-h,o-z)
      integer ndim, ndeg
      real*8 xlength
      real*8 svlvr(ndeg*ndim,ndeg*ndim)
      real*8 phi(ndim,ndim,ndeg), fval(ndim,ndeg)
      real*8 xi(ndim,ndim,ndeg), eta(ndim,ndeg)
      integer, allocatable:: ipvt(:)
      real*8, allocatable:: phidiag(:,:)
      real*8, allocatable:: pcc(:,:)
      real*8, allocatable:: xvec(:), rwork(:)
      real*8, allocatable:: xmat(:,:,:)
c
c     the system's matrix is 
c     I+xlength/2.0d0*(diag(phi(x_1),...,phi(x_n))*slvl
c                     +diag(phi(x_1),...,phi(x_n))*srvr)
c     = I+xlength/2.0d0*diag(phi(x_1),...,phi(x_n))*svlvr
c
c--------------------------------------------
c     big condition numbers for certain cases
c     stop here to debug:
c     print out the matrices: sdvl, sdvr, svlvr
c     xlength, {x_i}
c     try to retrieve these matrices to see
c     why this happens
c     print out in 301: rsysnoadap
c     print out in 302: formsmat
c--------------------------------------------
c
      nn=ndeg*ndim
      allocate(phidiag(nn,nn))
      allocate(pcc(nn,nn))
      allocate(xvec(nn))
      allocate(ipvt(nn))
      allocate(rwork(nn))
      allocate(xmat(ndim,ndeg,ndim))
c
c     initialize phidiag
      do j=1, nn
      do i=1, nn
        phidiag(i,j)=0.0d0
      enddo
      enddo
c
c     phi -> phidiag
      do k=1, ndeg
        kk=(k-1)*ndim
        do j=1, ndim
        do i=1, ndim
          phidiag(kk+i,kk+j)=phi(i,j,k)
        enddo
        enddo
      enddo
c
c     print out phidiag for debugging
      do j=1, nn
      do i=1, nn
        write(304,*) phidiag(i,j),i,j
      enddo
      enddo
c
c     form the system's matrix pcc
      do j=1, nn
        do i=1,nn
          pcc(i,j)=0.0d0
          do k=1,nn
            pcc(i,j)=pcc(i,j)+phidiag(i,k)*svlvr(k,j)*xlength/2.0d0
          enddo 
        enddo
        pcc(j,j)=pcc(j,j)+1.0d0
      enddo
c
c     reorder the the rhs mat phi
c     (not the rhs vec fval)
      call reorderrhs(ndim,ndeg,phi,xmat,0)
c
c     call dgeco with input matrix pcc
      call dgeco(pcc,nn,nn,ipvt,condloc,rwork)
c      write(*,*) 'xlength=',xlength
c      write(*,*) 'rcond=',condloc
c     for certain cases, there seems to be an 
c     ill conditioned matrix?
c
c     followed by dgesl (on fval and phi)
      ii=1
      do j=1, ndeg
        do i=1, ndim
          xvec(ii)=fval(i,j)
          ii=ii+1
        enddo
      enddo 
c
      call dgesl(pcc,nn,nn,ipvt,xvec,0)
      ii=1
      do j=1, ndeg
        do i=1, ndim
          eta(i,j)=xvec(ii)
          ii=ii+1
        enddo
      enddo
c
c     call dgesl on xmat
      do k=1, ndim
        call dgesl(pcc,nn,nn,ipvt,xmat(1,1,k),0)
      enddo
c
c     reorder xmat -> xi
      call reorderrhs(ndim,ndeg,xi,xmat,1)
c
      deallocate(phidiag)
      deallocate(pcc)
      deallocate(xvec)
      deallocate(ipvt)
      deallocate(rwork)
      deallocate(xmat)

      end subroutine






c-----------------------------------------------
c     subroutine reorderrhs
c-----------------------------------------------
c     a utility subroutine that reorders the 
c     rhs matrix
c     
c     input:
c     ndim: dimension of the system
c     ndeg: degree of the method
c     rmat0: the rhs matrix in the natural ordering
c            of input, dim(ndim,ndim,ndeg) 
c     rmat1: the rhs matrix in the natural ordering
c            of computation, dim(ndim,ndeg,ndim)
c     itype: type of the permutation
c            itype = 0: rmat0 -> rmat1
c            itype = 1: rmat1 -> rmat0
c
c     output:
c     rmat0 or rmat1 overwritten, depending on 
c     the value of itype
c
c     RMK: the relation is:
c     rmat1(i,j,k)=rmat0(i,k,j)
c
c-----------------------------------------------
c
      subroutine reorderrhs(ndim,ndeg,rmat0,rmat1,itype)
      implicit real*8 (a-h,o-z)
      integer ndim, ndeg, itype
      real*8 rmat0(ndim,ndim,ndeg)
      real*8 rmat1(ndim,ndeg,ndim)
c
      if (itype .eq. 0) then
        do k=1, ndim
          do j=1, ndeg
            do i=1, ndim
              rmat1(i,j,k)=rmat0(i,k,j)
            enddo
          enddo 
        enddo
      endif
c
      if (itype .eq. 1) then
        do k=1, ndeg
          do j=1, ndim
            do i=1, ndim
              rmat0(i,j,k)=rmat1(i,k,j)
            enddo
          enddo
        enddo
      endif
c
c

      end subroutine






c-----------------------------------------------
c     subroutine smkprod
c-----------------------------------------------
c
c     This subroutine evaluates the inner product
c     on leaf nodes (the system's version)
c
c     input:
c     ndim: dimension of the system
c     ndeg: degree of chebyshev approx
c     xlength: length of the subinterval
c     eta: density function (vector)
c     xi: density function (matrix)
c     sdvl, sdvr: the left and right spectral integration
c           matrices (returned by formsmat)
c
c     output:
c     the inner products al, ar, dl, dr
c     al, ar: \in R^{ndim,ndim}
c     dl, dr: \in R^{ndim}
c
c-----------------------------------------------
c
      subroutine smkprod(ndim, ndeg, xlength, eta, xi,
     1           sdvl, sdvr, al, ar, dl, dr)
      implicit real*8 (a-h,o-z)
      integer ndim, ndeg
      real*8 xlength
      real*8 sdvl(ndim,ndeg*ndim), sdvr(ndim,ndeg*ndim)
      real*8 xi(ndim,ndim,ndeg), eta(ndim,ndeg)
      real*8 al(ndim, ndim), ar(ndim, ndim)
      real*8 dl(ndim), dr(ndim)
      real*8, allocatable:: xvec(:), xvec2(:,:), xmat(:,:,:)
c
      nn=ndeg*ndim
      allocate(xvec(nn))
      allocate(xvec2(nn,ndim))
      allocate(xmat(ndim,ndeg,ndim))
c
      sc=xlength/2.0d0
c
c     reshape eta as an length nn vector xvec
      ii=1
      do j=1, ndeg
        do i=1, ndim
          xvec(ii)=eta(i,j)
          ii=ii+1
        enddo
      enddo 
c
c     reshape xi -> xmat(ndim,ndeg,ndim)->xvec2
      call reorderrhs(ndim,ndeg,xi,xmat,0)
      do k=1, ndim
        ii=1
        do j=1, ndeg
          do i=1, ndim
            xvec2(ii,k)=xmat(i,j,k)
            ii=ii+1
          enddo
        enddo 
      enddo
c     
c     dl=sc*sdvl*xvec, dr=sc*sdvr*xvec
      do i=1, ndim
        dl(i)=0.0d0
        dr(i)=0.0d0
        do j=1, nn
          dl(i)=dl(i)+sc*sdvl(i,j)*xvec(j)
          dr(i)=dr(i)+sc*sdvr(i,j)*xvec(j)
        enddo
      enddo
c
c     al=sc*sdvl*xvec2, ar=sc*sdvr*xvec2
      do j=1, ndim
        do i=1, ndim
          al(i,j)=0.0d0
          ar(i,j)=0.0d0
          do k=1, nn
            al(i,j)=al(i,j)+sc*sdvl(i,k)*xvec2(k,j)
            ar(i,j)=ar(i,j)+sc*sdvr(i,k)*xvec2(k,j)
c            write(*,*) i,j,k,sc, sdvl(i,k), xvec2(k,j)
c            write(*,*) i,j,k,sc, sdvr(i,k), xvec2(k,j)
          enddo
        enddo
      enddo


      deallocate(xvec)
      deallocate(xvec2)
      deallocate(xmat)

      end subroutine






c-----------------------------------------------
c     subroutine schd2par
c-----------------------------------------------
c
c     This subroutine computes the inner products 
c     for a parent node from the inner products
c     for its children nodes (system's version)
c
c     alist: list of alpha and beta for all the nodes
c     dlist: list of delta for all the nodes
c
c-----------------------------------------------
c
      subroutine schd2par(ndim, ipar, icha, ichb, maxid,
     1           alist, dlist)
      implicit real*8 (a-h,o-z)
      integer ndim, ipar, icha, ichb, maxid
      real*8 alist(ndim,ndim,2,maxid), dlist(ndim,2,maxid)
      integer, allocatable:: ipvt1(:), ipvt2(:)
      real*8, allocatable:: delta1(:,:), delta2(:,:), rwork(:)
      real*8, allocatable:: ala(:,:), ara(:,:), alb(:,:), arb(:,:)
      real*8, allocatable:: dla(:), dlb(:), dra(:), drb(:)
      real*8, allocatable:: x1(:), x2(:), y1(:,:), y2(:,:)
c
      allocate(delta1(ndim,ndim))
      allocate(delta2(ndim,ndim))
c
      allocate(ala(ndim,ndim))
      allocate(ara(ndim,ndim))
      allocate(alb(ndim,ndim))
      allocate(arb(ndim,ndim))
c
      allocate(dla(ndim))
      allocate(dra(ndim))
      allocate(dlb(ndim))
      allocate(drb(ndim))
c
      allocate(x1(ndim))
      allocate(x2(ndim))
      allocate(y1(ndim,ndim))
      allocate(y2(ndim,ndim))
c
      allocate(ipvt1(ndim))
      allocate(ipvt2(ndim))
      allocate(rwork(ndim))
c
c     define ala, ara, alb, arb      
      do j=1, ndim
      do i=1, ndim
        ala(i,j)=alist(i,j,1,icha)
        ara(i,j)=alist(i,j,2,icha)
        alb(i,j)=alist(i,j,1,ichb)
        arb(i,j)=alist(i,j,2,ichb)
      enddo
      enddo
c
c     define dla, dra, dlb, drb
      do j=1, ndim
        dla(j)=dlist(j,1,icha)
        dra(j)=dlist(j,2,icha)
        dlb(j)=dlist(j,1,ichb)
        drb(j)=dlist(j,2,ichb)
      enddo
c
c     delta1=I-ala*arb, delta2=I-arb*ala
      do j=1, ndim
        do i=1, ndim
          delta1(i,j)=0.0d0
          delta2(i,j)=0.0d0
          do k=1, ndim
            delta1(i,j)=delta1(i,j)-ala(i,k)*arb(k,j)
            delta2(i,j)=delta2(i,j)-arb(i,k)*ala(k,j)
          enddo
        enddo
        delta1(j,j)=delta1(j,j)+1.0d0
        delta2(j,j)=delta2(j,j)+1.0d0
      enddo
c
c     factorize delta1, delta2
      call dgeco(delta1,ndim,ndim,ipvt1,rcond1,rwork)
      call dgeco(delta2,ndim,ndim,ipvt2,rcond2,rwork)
c      write(*,*) 'rcond1=',rcond1
c      write(*,*) 'rcond2=',rcond2
c
c     x1=arb*dla-drb, x2=ala*drb-dla
      do j=1, ndim
        x1(j)=-drb(j)
        x2(j)=-dla(j)
        do k=1, ndim
          x1(j)=x1(j)+arb(j,k)*dla(k)
          x2(j)=x2(j)+ala(j,k)*drb(k)
        enddo 
      enddo
c
c     y1=I-arb, y2=I-ala
      do j=1, ndim
        do i=1, ndim
          y1(i,j)=-arb(i,j)
          y2(i,j)=-ala(i,j)
        enddo
        y1(j,j)=y1(j,j)+1.0d0
        y2(j,j)=y2(j,j)+1.0d0
      enddo
c
c     x1=inv(delta2)*x1, x2=inv(delta1)*x2
      call dgesl(delta2,ndim,ndim,ipvt2,x1,0)
      call dgesl(delta1,ndim,ndim,ipvt1,x2,0)
c
c     y1=inv(delta2)*y1, y2=inv(delta2)*y2
      do j=1, ndim
        call dgesl(delta2,ndim,ndim,ipvt2,y1(1,j),0)
        call dgesl(delta1,ndim,ndim,ipvt1,y2(1,j),0)
      enddo
c
c     formulae for dl and dr
      do j=1, ndim
        dlist(j,1,ipar)=dla(j)+dlb(j)
        dlist(j,2,ipar)=dra(j)+drb(j)
        do k=1, ndim
          dlist(j,1,ipar)=dlist(j,1,ipar)
     1                   +ala(j,k)*x1(k)+alb(j,k)*x2(k)
          dlist(j,2,ipar)=dlist(j,2,ipar)
     1                   +ara(j,k)*x1(k)+arb(j,k)*x2(k)
        enddo
      enddo
c
c     formulae for al and ar
      do j=1, ndim
        do i=1, ndim
          alist(i,j,1,ipar)=0.0d0
          alist(i,j,2,ipar)=0.0d0
          do k=1, ndim
            alist(i,j,1,ipar)=alist(i,j,1,ipar)
     1                       +ala(i,k)*y1(k,j)+alb(i,k)*y2(k,j)
            alist(i,j,2,ipar)=alist(i,j,2,ipar)
     1                       +ara(i,k)*y1(k,j)+arb(i,k)*y2(k,j)
          enddo
        enddo
      enddo
c

      deallocate(delta1)
      deallocate(delta2)
c
      deallocate(ala)
      deallocate(alb)
      deallocate(ara)
      deallocate(arb)
c
      deallocate(dla)
      deallocate(dlb)
      deallocate(dra)
      deallocate(drb)
c
      deallocate(x1)
      deallocate(x2)
      deallocate(y1)
      deallocate(y2)
c
      deallocate(ipvt1)
      deallocate(ipvt2)
      deallocate(rwork)

      end subroutine






c-----------------------------------------------
c     subroutine spar2chd
c-----------------------------------------------
c
c     This subroutine computes the coupling 
c     coefficients fot the children nodes
c     of a given parent node (system's version)
c     
c     input:
c     alist: list of alpha and beta for all nodes
c     dlist: list of delta for all nodes
c
c     output:
c     clist: list of coupling coefficients (mu)
c            for all nodes
c
c-----------------------------------------------
c
      subroutine spar2chd(ndim, ipar, icha, ichb, maxid,
     1           alist, dlist, clist)
      implicit real*8 (a-h,o-z)
      integer ndim, ipar, icha, ichb, maxid
      real*8 alist(ndim,ndim,2,maxid), dlist(ndim,2,maxid)
      real*8 clist(ndim,maxid)
      integer, allocatable:: ipvt1(:), ipvt2(:)
      real*8, allocatable:: delta1(:,:), delta2(:,:), rwork(:)
      real*8, allocatable:: ala(:,:), ara(:,:), alb(:,:), arb(:,:)
      real*8, allocatable:: dla(:), dlb(:), dra(:), drb(:)
      real*8, allocatable:: xvec(:)
c
      allocate(delta1(ndim,ndim))
      allocate(delta2(ndim,ndim))
c
      allocate(ala(ndim,ndim))
      allocate(ara(ndim,ndim))
      allocate(alb(ndim,ndim))
      allocate(arb(ndim,ndim))
c
      allocate(dla(ndim))
      allocate(dra(ndim))
      allocate(dlb(ndim))
      allocate(drb(ndim))
c
      allocate(ipvt1(ndim))
      allocate(ipvt2(ndim))
      allocate(rwork(ndim))
c
      allocate(xvec(ndim))
c
c     define ala, ara, alb, arb      
      do j=1, ndim
      do i=1, ndim
        ala(i,j)=alist(i,j,1,icha)
        ara(i,j)=alist(i,j,2,icha)
        alb(i,j)=alist(i,j,1,ichb)
        arb(i,j)=alist(i,j,2,ichb)
      enddo
      enddo
c
c     define dla, dra, dlb, drb
      do j=1, ndim
        dla(j)=dlist(j,1,icha)
        dra(j)=dlist(j,2,icha)
        dlb(j)=dlist(j,1,ichb)
        drb(j)=dlist(j,2,ichb)
      enddo
c
c     delta1=I-ala*arb, delta2=I-arb*ala
      do j=1, ndim
        do i=1, ndim
          delta1(i,j)=0.0d0
          delta2(i,j)=0.0d0
          do k=1, ndim
            delta1(i,j)=delta1(i,j)-ala(i,k)*arb(k,j)
            delta2(i,j)=delta2(i,j)-arb(i,k)*ala(k,j)
          enddo
        enddo
        delta1(j,j)=delta1(j,j)+1.0d0
        delta2(j,j)=delta2(j,j)+1.0d0
      enddo
c
c     factorize delta1, delta2
      call dgeco(delta1,ndim,ndim,ipvt1,rcond1,rwork)
      call dgeco(delta2,ndim,ndim,ipvt2,rcond2,rwork)
c
c     clist(:,icha):
c     define xvec
      do j=1, ndim
        xvec(j)=clist(j,ipar)-dla(j)
      enddo
c
c     formula for clist(:,icha)
      do j=1, ndim
        clist(j,icha)=0.0d0
        do k=1, ndim
          clist(j,icha)=clist(j,icha)-arb(j,k)*xvec(k)
        enddo
        clist(j,icha)=clist(j,icha)+clist(j,ipar)-drb(j)
      enddo
c
c     clist(:,icha)=inv(delta2)*clist(:,icha)
      call dgesl(delta2,ndim,ndim,ipvt2,clist(1,icha),0)
c
c     clist(:,ichb):
c     define xvec
      do j=1, ndim
        xvec(j)=clist(j,ipar)-drb(j)
      enddo
c
c     formula for clist(:,ichb)
      do j=1, ndim
        clist(j,ichb)=0.0d0
        do k=1, ndim
          clist(j,ichb)=clist(j,ichb)-ala(j,k)*xvec(k)
        enddo
        clist(j,ichb)=clist(j,ichb)+clist(j,ipar)-dla(j)
      enddo
c
c     clist(:,ichb)=inv(delta1)*clist(:,ichb)
      call dgesl(delta1,ndim,ndim,ipvt1,clist(1,ichb),0)


      deallocate(delta1)
      deallocate(delta2)
c
      deallocate(ala)
      deallocate(alb)
      deallocate(ara)
      deallocate(arb)
c
      deallocate(dla)
      deallocate(dlb)
      deallocate(dra)
      deallocate(drb)
c
      deallocate(ipvt1)
      deallocate(ipvt2)
      deallocate(rwork)
c
      deallocate(xvec)
c
      end subroutine






c-----------------------------------------------
c     subroutine sevaldens
c-----------------------------------------------
c
c     This subroutine recovers the density
c     function from local solutions and 
c     coupling coefficients
c
c     INPUT:
c     ndim: dimension of the system
c     ndeg: degree of Cheby approx
c     coeff: dim(ndim), the coupling coefficients.
c            a single vector in this case
c     eta: P_{loc}^{-1} fval
c     xi: Q_{loc}^{-1} phival
c
c     OUTPUT:
c     sigma: dim(ndim, ndeg), the density function values at 
c            the Chebyshev nodes on the given interval
c
c-----------------------------------------------
c
      subroutine sevaldens(ndim, ndeg, coeff, eta, xi, sigma) 
      implicit real*8 (a-h,o-z)
      integer ndim, ndeg
      real*8 coeff(ndim)
      real*8 eta(ndim, ndeg), xi(ndim, ndim, ndeg)
      real*8 sigma(ndim, ndeg)
c
      do ipt=1, ndeg
        do i=1, ndim
          sigma(i,ipt)=eta(i,ipt)
          do j=1, ndim
            sigma(i,ipt)=sigma(i,ipt)+xi(i,j,ipt)*coeff(j)
          enddo
        enddo
      enddo 


      end subroutine





c-----------------------------------------------
c     suborutine sprecompq
c-----------------------------------------------
c
c     This subroutine precomputes quadratures
c     quadl and quadr for each leaf interval
c     (to be used in recovering the solution)
c     (the system's version)
c
c-----------------------------------------------
c
      subroutine sprecompq(ndim, nleaf, leaflist, 
     1           alist, dlist, clist, quadl, quadr)
      implicit real*8 (a-h,o-z)
      integer ndim, nleaf
      integer leaflist(nleaf)
      real*8 alist(ndim,ndim,2,1), dlist(ndim,2,1)
      real*8 clist(ndim,1)
      real*8 quadl(ndim,nleaf), quadr(ndim,nleaf)
      real*8, allocatable:: qloc(:)
c
      allocate(qloc(ndim))
c
c     initialize quadl(:,1)=0
      do i=1, ndim
        quadl(i,1)=0.0d0
      enddo
c
c     eval quadl
      do i=1, nleaf-1
        ibox=leaflist(i)
        do k=1, ndim
          qloc(k)=dlist(k,1,ibox)
          do j=1, ndim
            qloc(k)=qloc(k)+alist(k,j,1,ibox)*clist(j,ibox)
          enddo
          quadl(k,i+1)=quadl(k,i)+qloc(k)
        enddo 
      enddo
c
c     initialize quadr(:,nleaf)=0
      do i=1, ndim
        quadr(i,nleaf)=0.0d0
      enddo
c
c     eval quadr      
      do i=nleaf, 2, -1
        ibox=leaflist(i)
        do k=1, ndim
          qloc(k)=dlist(k,2,ibox)
          do j=1, ndim
            qloc(k)=qloc(k)+alist(k,j,2,ibox)*clist(j,ibox)
          enddo
          quadr(k,i-1)=quadr(k,i)+qloc(k) 
        enddo
      enddo
c
      deallocate(qloc)
c
      end subroutine






c-----------------------------------------------
c     subroutine squadloc
c-----------------------------------------------
c
c     This subroutine computes a local integral
c     (on [b_i, tau_j^i] and [tau_j^i, b_i])
c     to be added to the precomputed global
c     integral to recover the solution
c     (the system's version)
c
c-----------------------------------------------
c
      subroutine squadloc(ndim, ndeg, xlength, sigma,
     1           svlvr, qloc)
      implicit real*8(a-h,o-z)
      integer ndim, ndeg
      real*8 xlength, sigma(ndim, ndeg)
      real*8 svlvr(ndeg*ndim, ndeg*ndim)
      real*8 qloc(ndim, ndeg)
      real*8, allocatable:: xvec(:), yvec(:)
c
      sc=xlength/2.0d0
      nn=ndeg*ndim
      allocate(xvec(nn))
      allocate(yvec(nn))
c
c     reshape sigma-> xvec
      ii=1
      do j=1, ndeg
        do i=1, ndim
          xvec(ii)=sigma(i,j)
          ii=ii+1
        enddo
      enddo 
c
c     yvec=svlvr*xvec
      do i=1, nn
        yvec(i)=0.0d0
        do j=1, nn
          yvec(i)=yvec(i)+sc*svlvr(i,j)*xvec(j)
        enddo
      enddo
c
c     reshape yvec->qloc
      ii=1
      do j=1, ndeg
        do i=1, ndim
          qloc(i,j)=yvec(ii)
          ii=ii+1
        enddo
      enddo 


      deallocate(xvec)
      deallocate(yvec)

      end subroutine






c-----------------------------------------------
c     subroutine sinterpf
c-----------------------------------------------
c
c  given a tree and function values on the leaf 
c  nodes of the tree, interpolate the function to 
c  a given grid (the system's version)
c
c-----------------------------------------------
c
      subroutine sinterpf(nlev, levelbox, iparentbox,
     1           ichildbox, icolbox, nboxes, maxid,
     2           nblevel, iboxlev, istartlev, cent0,
     3           xsize0, nleaf, leaflist, ndim, ndeg, 
     4           fvals, ngrid, xgrid, finterp)
      implicit real*8 (a-h,o-z)
      integer nboxes, maxid, nlev
      integer nleaf, ngrid, ndim, ndeg
      integer levelbox(1)
      integer icolbox(1) 
      integer iparentbox(1), ichildbox(2,1)
      integer nblevel(0:1)
      integer iboxlev(1), istartlev(0:1)
      integer leaflist(nleaf)
      real*8 fvals(ndim,ndeg,nleaf), xgrid(ngrid)
      real*8 chpts(ndeg,nleaf)
      real*8 finterp(ndim,ngrid), cent0, xsize0
c     local variables, allocatable
      integer, allocatable:: npbox(:), ipold(:)
      integer, allocatable:: istartbox(:), ibofp(:)
      real*8, allocatable:: xpts(:)
      real*8, allocatable:: fv(:)
      real*8 coeff(1000)
      complex*16 wsave(1000), work(1000)
c
      allocate(npbox(maxid))
      allocate(ipold(ngrid))
      allocate(istartbox(maxid))
      allocate(ibofp(ngrid))
      allocate(xpts(ngrid))
c
      allocate(fv(ndeg))
c
      npts=ngrid
      do i=1, npts
        xpts(i)=xgrid(i)
c        write(333,*) xpts(i), 0.0d0
      enddo
c     copy over xgrid, so as not to overwrite it
c
c     1. sort the points into the tree
      call treesort(nlev,levelbox,iparentbox,ichildbox,
     1     icolbox,nboxes,maxid, nblevel,iboxlev,
     2     istartlev,xpts,npts,npbox,ipold,istartbox,
     3     ibofp,cent0,xsize0)
c
c     2. go thru the leaf nodes, do the cheby transf
c        fval -> coeffs, then eval coeffs -> finterp 
c        at xgrid in the node.
c        in the system's case, fvals(ndim,ndeg,nleaf)  
c        should be processed one dimension at a time
c
      call chxcin(ndeg,wsave)
      x0=cent0-xsize0/2.0d0
      aroot=x0
      broot=x0+xsize0
c
      do i=1, nleaf
        ibox=leaflist(i)
        if(npbox(ibox) .gt. 0) then
          lev=levelbox(ibox)
          icol=icolbox(ibox)
c
c         set the left and right enpoints of the 
c         interval ibox
          xlength=0.5d0**lev*xsize0
          a=dble(icol-1)*xlength+x0
          b=a+xlength
c
c          call mkgrid(lev,icol,aroot,broot,ndeg,
c     1    aa,bb,chpts(1,i))
          call chnodes(a, b, ndeg, chpts(1,i), u, v, u1, v1)
c          
c         loop over the dimensions
          do k=1, ndim
            do j=1, ndeg
              fv(j)=fvals(k,j,i)
c              if(k .eq. 1) then
c                write(501,*) chpts(j,i), fv(j), a, b, 
c     1          lev, xlength, xsize0
c              else 
c                write(502,*) chpts(j,i), fv(j)
c              endif
            enddo
c
c           copy over the k-th dimension
            call chexfc(fv,ndeg,coeff,wsave,work) 
            istart=istartbox(ibox)
            iend=istart+npbox(ibox)-1
            do ipt=istart,iend
              xx=xpts(ipt)
              call cheval(xx,finterp(k,ipt),coeff,ndeg,a,b)
c              if(k .eq. 1) then
c                write(401,*) xx, finterp(k,ipt) 
c              else 
c                write(402,*) xx, finterp(k,ipt) 
c              endif
            enddo
          enddo
        endif
      enddo
c
c     3. reverse the ordering of finterp to the
c        order of xgrid
      call rvecnew2old(ndim, ngrid, finterp, ipold)
c

      deallocate(npbox)
      deallocate(ipold)
      deallocate(istartbox)
      deallocate(ibofp)
      deallocate(xpts)
      deallocate(fv)
c
      end subroutine





c-----------------------------------------------
c     subroutine sinterpf2
c-----------------------------------------------
c
c  given a tree and function values on the leaf 
c  nodes of the tree, interpolate the function to 
c  a given grid (the system's version)
c
c  modification from sinterpf:
c  the input fvals is saved in the order of the tree,
c  not in the order of leaflist
c  unify them into sinterpf2 later!
c
c-----------------------------------------------
c
      subroutine sinterpf2(nlev, levelbox, iparentbox,
     1           ichildbox, icolbox, nboxes, maxid,
     2           nblevel, iboxlev, istartlev, cent0,
     3           xsize0, nleaf, leaflist, ndim, ndeg, 
     4           fvals, ngrid, xgrid, finterp)
      implicit real*8 (a-h,o-z)
      integer nboxes, maxid, nlev
      integer nleaf, ngrid, ndim, ndeg
      integer levelbox(1)
      integer icolbox(1) 
      integer iparentbox(1), ichildbox(2,1)
      integer nblevel(0:1)
      integer iboxlev(1), istartlev(0:1)
      integer leaflist(nleaf)
      real*8 fvals(ndim,ndeg,nboxes), xgrid(ngrid)
      real*8 finterp(ndim,ngrid), cent0, xsize0
c     local variables, allocatable
      integer, allocatable:: npbox(:), ipold(:)
      integer, allocatable:: istartbox(:), ibofp(:)
      real*8, allocatable:: xpts(:)
      real*8, allocatable:: fv(:)
      real*8 coeff(1000)
      complex*16 wsave(1000), work(1000)
c
      allocate(npbox(maxid))
      allocate(ipold(ngrid))
      allocate(istartbox(maxid))
      allocate(ibofp(ngrid))
      allocate(xpts(ngrid))
c
      allocate(fv(ndeg))
c
      npts=ngrid
      do i=1, npts
        xpts(i)=xgrid(i)
      enddo
c     copy over xgrid, so as not to overwrite it
c
c     1. sort the points into the tree
      call treesort(nlev,levelbox,iparentbox,ichildbox,
     1     icolbox,nboxes,maxid, nblevel,iboxlev,
     2     istartlev,xpts,npts,npbox,ipold,istartbox,
     3     ibofp,cent0,xsize0)
c
c     2. go thru the leaf nodes, do the cheby transf
c        fval -> coeffs, then eval coeffs -> finterp 
c        at xgrid in the node.
c        in the system's case, fvals(ndim,ndeg,nleaf)  
c        should be processed one dimension at a time
c
      call chxcin(ndeg,wsave)
      x0=cent0-xsize0/2.0d0
c
      do i=1, nleaf
        ibox=leaflist(i)
        if(npbox(ibox) .gt. 0) then
          lev=levelbox(ibox)
          icol=icolbox(ibox)
c
c         set the left and right enpoints of the 
c         interval ibox
          xlength=0.5d0**lev*xsize0
          a=dble(icol-1)*xlength+x0
          b=a+xlength
c          
c         loop over the dimensions
          do k=1, ndim
            do j=1, ndeg
cccccc              fv(j)=fvals(k,j,i)
              fv(j)=fvals(k,j,ibox)
            enddo
c           copy over the k-th dimension
            call chexfc(fv,ndeg,coeff,wsave,work) 
            istart=istartbox(ibox)
            iend=istart+npbox(ibox)-1
            do ipt=istart,iend
              xx=xpts(ipt)
              call cheval(xx,finterp(k,ipt),coeff,ndeg,a,b)
cccccc        problem with cheval when xx=a or xx=b?
cccccc        xx slightly outside of the interval [a,b]
c              if (abs(xx-0.5d0).le.1.0d-9) then
c                write(411,*) a, b, ibox
c                do jj=1, ndeg
c                  write(412,*) fv(jj), k
c                  write(413,*) coeff(jj), k
c                  write(414,*) a, b, ibox, xx, finterp(k,ipt), k, ipt
c                enddo
c              endif
cccccc         end of the debugging code
            enddo
          enddo
        endif
      enddo
c
c     3. reverse the ordering of finterp to the
c        order of xgrid
      call rvecnew2old(ndim, ngrid, finterp, ipold)
c

      deallocate(npbox)
      deallocate(ipold)
      deallocate(istartbox)
      deallocate(ibofp)
      deallocate(xpts)
      deallocate(fv)
c
      end subroutine





c-----------------------------------------------
c     subroutine sgetmonitors0
c-----------------------------------------------
c
c     Given function values at cheby pts, eval
c     the monitor function
c
c     input:
c     ndim: dimension
c     ndeg: order
c     nleaf: number of leaf nodes (where fvals are
c            sampled)
c     fvals: function values at chey pts on the leaf
c            nodes (dim(ndeg,nleaf))
c
c     output:
c     smoni: monitor function, one value for each
c            leaf node, array of length nleaf
c     sdiv: a real number, defined by:
c           max(smoni)/2**4 
c
c-----------------------------------------------
      subroutine sgetmonitors0(ndim, ndeg, nleaf, fvals, 
     1           smoni, sdiv)
      implicit real*8 (a-h,o-z)
      integer ndim, ndeg, nleaf
      real*8 fvals(ndim,ndeg,nleaf), smoni(nleaf), sdiv
      real*8 coeff(1000)
      real*8, allocatable:: fv(:)
      complex*16 wsave(1000), work(1000)
c
      allocate(fv(ndeg))
c
      call chxcin(ndeg,wsave)
c
c     attention: fvals has dim(ndim,ndeg,nleaf) now
c     instead of dim(ndeg,nleaf)
c
      do i=1, nleaf
        smoni(i)=0.0d0
        do k=1, ndim
          do j=1, ndeg
            fv(j)=fvals(k,j,i)
          enddo
          call chexfc(fv,ndeg,coeff,wsave,work)
          smoniik=abs(coeff(ndeg-1))+
     1             abs(coeff(ndeg-2)-coeff(ndeg))
          if(smoniik .gt. smoni(i)) then
            smoni(i)=smoniik
          endif
        enddo
      enddo
c
      sdiv=0.0d0
      do i=1, nleaf
        si=smoni(i)/16.0d0
        if(si .gt. sdiv) then
          sdiv=si
        endif
      enddo
c

      deallocate(fv)
      
      end subroutine










c-----------------------------------------------
c     subroutine sgetmonitors
c-----------------------------------------------
c
c     Given function values at cheby pts, eval
c     the monitor function
c
c     input:
c     ndim: dimension
c     ndeg: order
c     nleaf: number of leaf nodes (where fvals are
c            sampled)
c     fvals: function values at chey pts on the leaf
c            nodes (dim(ndeg,nleaf))
c
c     output:
c     smoni: monitor function, one value for each dimension on each leaf node, dimention(ndim,nleaf)
c     sdiv: a real array of length ndim, 
c           defined by max(smoni)/2**4, for each 
c           dimension
c
c-----------------------------------------------
c     problem: fvals saved in the order of
c     ibox!!! not ileaf!!!
      subroutine sgetmonitors(ndim, ndeg, nleaf, 
     1           leaflist, fvals, smoni, sdiv)
      implicit real*8 (a-h,o-z)
      integer ndim, ndeg, nleaf
      integer leaflist(nleaf)
      real*8 fvals(ndim,ndeg,nleaf), smoni(ndim,nleaf), sdiv(ndim)
      real*8 coeff(1000)
      real*8, allocatable:: fv(:)
      complex*16 wsave(1000), work(1000)
c
      allocate(fv(ndeg))
c
      call chxcin(ndeg,wsave)
c
c     attention: fvals has dim(ndim,ndeg,nleaf) now
c     instead of dim(ndeg,nleaf)
c
      do i=1, nleaf
c        smoni(i)=0.0d0
        ibox=leaflist(i)
        do k=1, ndim
          smoni(k,i)=0.0d0
          do j=1, ndeg
            fv(j)=fvals(k,j,ibox)
          enddo
          call chexfc(fv,ndeg,coeff,wsave,work)
          smoniik=abs(coeff(ndeg-1))+
     1             abs(coeff(ndeg-2)-coeff(ndeg))
          smoni(k,i)=smoniik
c          if(smoniik .gt. smoni(i)) then
c            smoni(i)=smoniik
c          endif
        enddo
      enddo
c
      do k=1, ndim
        sdiv(k)=0.0d0
        do i=1, nleaf
          ski=smoni(k,i)/16.0d0
          if(ski .gt. sdiv(k)) then
            sdiv(k)=ski
          endif
        enddo
      enddo
c

      deallocate(fv)
      
      end subroutine





c-----------------------------------------------
c     subroutine sbcnwtn
c-----------------------------------------------
c
c     This subroutine computes the RHS of the 
c     linearized integral equations in each
c     newton step of the nonlinear solver,
c     given corresponding matrices returned by 
c     subroutine ssetbc, and subroutines feval
c     and fyeval
c
c     INPUT:
c     ndim: dimension of the system
c     x: the evaluation point
c     ui: the inhomogeneous term, a vec returned by 
c         subroutine ssetbc
c     sigmak: solution of the integral equation of the
c             last newton step
c     uk: solution of the linearized ode of the last
c         newton step
c
c     feval: subroutine for the evaluation of f(x)
c     fyeval: subroutine for the evaluation of df(x,y)/dy 
c            as in the equation:
c
c            u'(x) = f(x, u(x)) 
c            with the calling seq:
c            feval(x,y,f,pars), fyeval(x,y,fy,pars)
c
c     pars: the parameters needed by feval, fyeval
c
c     OUTPUT:
c     fval: the rhs (vec) of the integral eqn,
c           i.e. gk(x) in Starr & Rokhlin's paper,
c           (remember to replace f(x,y) by f(x,y+ui)
c            to deal with the inhomo BC)
c           fval(x)= p0(x)*uk(x)+f(x,uk(x)+ui)-sigmak(x)
c
c     phival: the rhs (mat) of the integral eqn,
c           phival(x)= (Omega_k(x)-p0(x))*gam0, 
c           (Omega_k=-fy(x,uk(x)+ui)-p0(x)
c           (Omega_k as in Starr & Rokhlin's paper)
c
c     RMK: in this case we fix p0(x)=0 and gam0=Id, 
c          thus we have:
c          fval(x)=f(x,uk+ui)-sigmak
c          phival(x)=-fy(x,uk+ui)
c
ccccccc
c
c-----------------------------------------------
c
      subroutine sbcnwtn(ndim,x,ui,sigmak,uk,fval,
     1           phival,feval,fyeval,pars)
      implicit real*8 (a-h,o-z)
      integer ndim
      real*8 x, ui(ndim), fval(ndim)
      real*8 sigmak(ndim), uk(ndim)
      real*8 pars(1), phival(ndim,ndim)
      real*8, allocatable:: yvec(:)
      external feval, fyeval
c
      allocate(yvec(ndim))
c
      do i=1, ndim
        yvec(i)=ui(i)+uk(i)
      enddo
c
      call feval(x,yvec,fval,pars)
      do i=1, ndim
        fval(i)=fval(i)-sigmak(i)
c        write(314,*) i, x, fval(i)
      enddo
c     done with fval
c
      call fyeval(x,yvec,phival,pars)
      do j=1, ndim
      do i=1, ndim
        phival(i,j)=-phival(i,j)
c        write(315,*) i, j, x, phival(i,j)
      enddo
      enddo 

c
      deallocate(yvec)
c
      end subroutine







