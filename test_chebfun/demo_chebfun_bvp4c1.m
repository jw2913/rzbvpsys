% Y = BVP4C(ODEFUN, BCFUN, Y0) applies the standard BVP4C method to solve a
%    boundary-value problem. ODEFUN and BCFUN are as in BVP4C. The Y0 argument is
%    a CHEBFUN that represents the initial guess to the solution Y. Its domain
%    defines the domain of the problem, and the length of the CHEBFUN Y0 is used
%    to set the number of points in an initial equispaced mesh. Note that it is
%    not necessary to call BVPINIT.

% EXAMPLE:
% u''+|u| = 0
% u(0) = 0, u(4) = -2

twoode = @(x,v) [v(2); -abs(v(1))];
twobc = @(va,vb) [va(1); vb(1)+2];
d = [0,4];
one = chebfun(1, d);
v0 = [one 0*one];
% initiall guess 1: v0 = 1
v = bvp4c(twoode, twobc, v0);
u = v(:,1); plot(u,'r');
hold on

v0 = [-one 0*one];
% initiall guess 1: v0 = -1
v = bvp4c(twoode,twobc,v0);
u = v(:,1); plot(u,'b')
