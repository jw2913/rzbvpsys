function ff=ejodelin(x,d,y,yp) 
% the linearized odefun for the elliptic jacobian ode system
% y and yp are chebfuns (solution of the last step)
  yn = y(x);
  ynp = yp(x);
%
  f=[(yn(2)+1).*(yn(3)+1), -yn(1).*(yn(3)+1), -0.5*yn(1).*(yn(2)+1)]; % the f(x,y) vec in y'=f(x,y)
%
  hd=[(yn(3)+1).*d(2)+(yn(2)+1).*d(3), -(yn(3)+1).*d(1)-yn(1).*d(3), -0.5*(yn(2)+1).*d(1)-0.5*yn(1).*d(2)];
% hd=h*d, Hessian of f (h=df/dy) multiplied by d
% be careful about the ordering...
%
  ff= hd+f-ynp;
end
