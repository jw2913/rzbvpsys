function ff=ejodelin(x,d,y,yp) 
% the linearized odefun for the elliptic jacobian ode system
% p=[y;yp] is the solution and derivative of the last step (atacked together)
  h=ejder(x,y);
  f=ejode(x,y);
  ff=h*d+f-yp;
end
