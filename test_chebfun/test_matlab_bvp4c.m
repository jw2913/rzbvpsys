% Y = BVP4C(ODEFUN, BCFUN, Y0) applies the standard BVP4C method to solve a
%    boundary-value problem. ODEFUN and BCFUN are as in BVP4C. The Y0 argument is
%    a CHEBFUN that represents the initial guess to the solution Y. Its domain
%    defines the domain of the problem, and the length of the CHEBFUN Y0 is used
%    to set the number of points in an initial equispaced mesh. Note that it is
%    not necessary to call BVPINIT.

% EXAMPLE:
% Jacobian Elliptic functions from Starr & Rokhlin (1992)

format long
m=0.5;
f=chebfun(@(x) 1/sqrt(1-0.5*sin(x)^2),[0,pi/2]);
K=sum(f,0,pi/2)
d=[0,4*K];
% domain of the ode

ellipjode = @(x,u) [(u(2)+1)*(u(3)+1); -u(1)*(u(3)+1); -0.5*u(1)*(u(2)+1)]
ellipjbc = @(ua,ub) [ua(1);ua(2);ub(3)];
solinit.x=0:0.01:4*K;
solinit.y=ejinit(solinit.x);

opts=bvpset('reltol',1e-14)
ejsol=bvp4c(ellipjode, ellipjbc, solinit, opts);
sn=ejsol.y(1,:);
cn=ejsol.y(2,:)+1;
dn=ejsol.y(3,:)+1;
xx=ejsol.x;
plot(xx,sn,'r')
hold on
plot(xx,cn,'g')
plot(xx,dn,'b')

% print out the residual
% the true solution
const= pi/2/K;
[sne,cne,dne]=ellipj(xx,m);
plot(xx,sne,'r-.')
plot(xx,cne,'g-.')
plot(xx,dne,'b-.')

err_abs1=norm(sn-sne,inf)
err_abs2=norm(dn-dne,inf)
err_abs3=norm(cn-cne,inf)
