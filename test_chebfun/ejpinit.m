function y=ejinit(x)
  f=chebfun(@(x) 1/sqrt(1-0.5*sin(x)^2),[0,pi/2]);
  K=sum(f,0,pi/2);
  const=pi/2/K;
  y(1,:)=const*cos(const*x);
  y(2,:)=-const*sin(const*x);
  y(3,:)=0;
end
