% Y = BVP4C(ODEFUN, BCFUN, Y0) applies the standard BVP4C method to solve a
%    boundary-value problem. ODEFUN and BCFUN are as in BVP4C. The Y0 argument is
%    a CHEBFUN that represents the initial guess to the solution Y. Its domain
%    defines the domain of the problem, and the length of the CHEBFUN Y0 is used
%    to set the number of points in an initial equispaced mesh. Note that it is
%    not necessary to call BVPINIT.

% EXAMPLE:
% Jacobian Elliptic functions from Starr & Rokhlin (1992)
% this version has the Newton iteration written out explicitly
% calling chebfun/bvp4c as a linear solver only
%
% Solving the problem with:
% dy/dx = f(x,y);
% with hessian: h = df/dy(x,y);
% ejodelincheb.m is the linearized odefun, chebfun version
% where F(x,d,yn,ynp) = df/dy(x,yn)*d+f(x,yn)-ynp,
% yn and ynp being y(x) and y'(x) of the last step
% (in this test, we pass them over as chebfuns to simplify the interface, if you know what I mean)
format long
m=0.5;
f=chebfun(@(x) 1/sqrt(1-0.5*sin(x)^2),[0,pi/2]);
K=sum(f,0,pi/2)  % global parameters:
d=[0,4*K];
% domain of the ode
ellipjbc = @(ua,ub) [ua(1);ua(2);ub(3)];

const=pi/2/K;
sn0=chebfun(@(x) sin(const*x), d);
cn0=chebfun(@(x) cos(const*x)-1, d);
dn0=chebfun(@(x) 0, d);
y=[sn0,cn0,dn0];
 % y of the previous step
yp=diff(y);
 % y' of the previous step

rtol=1e-7; % saturates at 7 digits for some reason
err_rel=100.0; % init err_rel as a big number
nstep = 0;
maxstep = 100;
while((err_rel>rtol) & (nstep < maxstep))
  nstep = nstep+1
  ellipjode = @(x,d) ejodelincheb(x,d,y,yp); % define a linearized ode system at each step
  dsol = bvp4c(ellipjode, ellipjbc, y); % solve the ode as a chebfun
  y = y + dsol;
  yp = diff(y);
  err_rel =norm(dsol)/norm(y)
end

sn=y(:,1);
cn=y(:,2)+1;
dn=y(:,3)+1;
plot(sn,'r')
hold on
plot(cn,'g')
plot(dn,'b')

% print out the residual
r1=diff(sn)-cn.*dn; 
r2=diff(cn)+sn.*dn; 
r3=diff(dn)+0.5*sn.*cn;

res1=norm(r1,inf);
res2=norm(r2,inf);
res3=norm(r3,inf);
res=max([res1,res2,res3])

% the true solution
const= pi/2/K;
x=0:0.01:4*K;
[sne,cne,dne]=ellipj(x,m);
plot(x,sne,'r-.')
plot(x,cne,'g-.')
plot(x,dne,'b-.')

snc=sn(x);
cnc=cn(x);
dnc=dn(x);
err1=norm(snc-sne,inf);
err2=norm(dnc-dne,inf);
err3=norm(cnc-cne,inf);
err_inf=max([err1, err2, err3])
