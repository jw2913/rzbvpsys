function f = ejode(x,u)
  f=[(u(2)+1)*(u(3)+1); -u(1)*(u(3)+1); -0.5*u(1)*(u(2)+1)];
end
