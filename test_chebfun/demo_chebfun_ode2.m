contact_rate  = .003;
recovery_rate = .3;

op = @(x,S,I,R) [ ...
    diff(S) + contact_rate*I*S
    diff(I) - contact_rate*I*S + recovery_rate*I
    diff(R) - recovery_rate*I
    ];

N = chebop(op, [0, 30]);

N.lbc = @(S,I,R) [ ...
        S - 500
        I - 1
        R
    ];

[S,I,R] = N\0;

plot([S I R])
legend('S','I','R')
title('SIR model')
xlabel('t')
