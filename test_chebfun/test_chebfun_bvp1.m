% Y = BVP4C(ODEFUN, BCFUN, Y0) applies the standard BVP4C method to solve a
%    boundary-value problem. ODEFUN and BCFUN are as in BVP4C. The Y0 argument is
%    a CHEBFUN that represents the initial guess to the solution Y. Its domain
%    defines the domain of the problem, and the length of the CHEBFUN Y0 is used
%    to set the number of points in an initial equispaced mesh. Note that it is
%    not necessary to call BVPINIT.

% EXAMPLE:
% Jacobian Elliptic functions from Starr & Rokhlin (1992)

format long
m=0.5;
f=chebfun(@(x) 1/sqrt(1-0.5*sin(x)^2),[0,pi/2]);
K=sum(f,0,pi/2)
d=[0,4*K];
% domain of the ode

ellipjode = @(x,u) [(u(2)+1)*(u(3)+1); -u(1)*(u(3)+1); -0.5*u(1)*(u(2)+1)]
ellipjbc = @(ua,ub) [ua(1);ua(2);ub(3)];

const=pi/2/K;
sn0=chebfun(@(x) sin(const*x), d);
cn0=chebfun(@(x) cos(const*x)-1, d);
dn0=chebfun(@(x) 0, d);
u0=[sn0,cn0,dn0];

opts=bvpset('reltol',1e-12)
u=bvp4c(ellipjode, ellipjbc, u0, opts);
sn=u(:,1);
cn=u(:,2)+1;
dn=u(:,3)+1;
plot(sn,'r')
hold on
plot(cn,'g')
plot(dn,'b')

% print out the residual
r1=diff(sn)-cn.*dn; 
r2=diff(cn)+sn.*dn; 
r3=diff(dn)+0.5*sn.*cn;

norm(r1,inf)
norm(r2,inf)
norm(r3,inf)

% the true solution
const= pi/2/K;
x=0:0.01:4*K;
[sne,cne,dne]=ellipj(x,m);
plot(x,sne,'r-.')
plot(x,cne,'g-.')
plot(x,dne,'b-.')

snc=sn(x);
cnc=cn(x);
dnc=dn(x);
norm(snc-sne,inf)
norm(dnc-dne,inf)
norm(cnc-cne,inf)
