function y=ejinit(x)
  f=chebfun(@(x) 1/sqrt(1-0.5*sin(x)^2),[0,pi/2]);
  K=sum(f,0,pi/2);
  const=pi/2/K;
  y(1,:)=sin(const*x);
  y(2,:)=cos(const*x)-1; 
  y(3,:)=0;
end
