c-------------------------------------------------
c Two point boundary value problem solver
c-------------------------------------------------
c
c Subroutines:
c
c user interfaces:
c rbvpnoadap: real bvp solver, non-adaptive version
c rbvpadap: real bvp solver, adaptive version
c rsysnoadap: real bvp system solver, non-adaptive version
c rsysadap: real bvp system solver, adaptive version
c
c real nonlinear version:
c rnlinadap: real nonlinear bvp system solver, adaptive
c
c complex version:
c zbvpnoadap: complex bvp solver, scalar non-adaptive version
c zbvpadap: complex bvp solver, scalar adaptive version
c zsysnoadap: complex bvp system solver, non-adaptive version
c zsysadap: complex bvp system solver, adaptive version
c
c     (RMK: the complex version is incomplete for now,
c           only zbvpnoadap is implemented)
c
c
c Main routines:
c rbvpmain: the subroutine that does the main work
c           (real version)
c rsysmain: the subroutine that does the main work
c           (real system version)
c rasysmain: the subroutine that does the main work 
c           (adaptive, real system version)
c rasysnwtn: the subroutine that does the main work
c            of one newton iteration of the nonlinear
c            system's case, adaptive
c            (similar to rasysmain, with a slightly different interface) 
c zbvpmain: the subroutine that does the main work
c           (complex version)
c zsysmain: the subroutine that does the main work
c           (complex sys version)
c
c     (RMK: the complex version is incomplete for now,
c           zsysmain is not implemented yet)
c
c--------------------------------------------------





c--------------------------------------------------
c     subroutine rbvpnoadap
c--------------------------------------------------
c
c     Real BVP solver, non-adaptive version
c
c     This subroutine solves the two point BVP
c     (1) - (3):
c     
c     u''(x)+ p(x)*u'(x) +q(x)*u(x) = f(x) 
c                  (x \in [xa, xb])    (1)
c     bcl0*u(xa) + bcl1*u'(xa) = bclv  (2)
c     bcr0*u(xb) + bcr1*u'(xb) = bcrv  (3)
c
c     INPUT:
c     ndeg: desired order of the method
c     nch: number of subintervals of the user specified grid
c     endpts: real array of length nch+1, endpoints of 
c             subintervals. endpts(1)=xa, endpts(nch+1)=xb.
c             the user specified grid.
c     bc**: boundary conditions, as in (2) and (3)
c     peval, qeval, feval: function evaluation 
c           subroutines for p, q, and f in (1)
c           which have the calling sequence:
c           peval(x,p,pars), qeval(x,q,pars), feval(x,f,pars)
c           where x is the eval point, p/q/f is the value
c           returned, and pars is the list of parameters
c
c     OUTPUT:
c     usol: real*8, dim (ndeg,nch), the solution u
c           evaluated at Chebyshev points on each
c           interval (in leaflist)        
c     uder: real*8, dim (ndeg,nch), the derivative
c           of u evaluated at the same grid 
c
c     RMK: the interface is imperfect, 
c          need to specify a grid where output
c          is required -
c          in output, do (xgrid, usol, uder)
c          instead of the current situation!
c
c--------------------------------------------------
c
      subroutine rbvpnoadap(ndeg, nch, endpts,
     1    bcl0, bcl1, bclv, bcr0, bcr1, bcrv, 
     2    peval, qeval, feval, pars, ntarg, 
     3    xtarg, usol, uder)
      implicit real*8 (a-h,o-z)
      integer ndeg, nch, ntarg
      real*8 endpts(nch+1), xtarg(ntarg)
      real*8 bcl0, bcl1, bclv, bcr0, bcr1, bcrv
      real*8 pars(1), ulsol(ndeg,nch), ulder(ndeg,nch)
      real*8 usol(ntarg), uder(ntarg)
      external peval, qeval, feval
c     local vars (allocatable)
c     binary tree and leaflist
      integer nboxes, maxid, nlev, nleaf
      integer,allocatable:: levelbox(:)
      integer,allocatable:: icolbox(:) 
      integer,allocatable:: iparentbox(:)
      integer,allocatable:: ichildbox(:,:)
      integer,allocatable:: nblevel(:)
      integer,allocatable:: iboxlev(:)
      integer,allocatable:: istartlev(:)
      integer,allocatable:: leaflist(:)
      integer,allocatable:: newlist(:)
c     chebyshev matrices      
      real*8, allocatable::cftm(:,:), citm(:,:) 
      real*8, allocatable::spdef(:), spbx(:,:), spxb(:,:)
c     data on leaf nodes
      real*8, allocatable::chpts(:,:), xlength(:)
      real*8, allocatable::gl(:,:), gr(:,:)
      real*8, allocatable::glp(:,:), grp(:,:)
      real*8, allocatable::phil(:,:), phir(:,:)
      real*8, allocatable::ui(:,:), uip(:,:)
      real*8, allocatable::fval(:,:)
c     inner products and coupling coeffs
      real*8, allocatable::alist(:,:,:), dlist(:,:)
      real*8, allocatable::clist(:,:)
c     solution related
      real*8, allocatable::sigma(:,:)
      real*8, allocatable::quadl(:), quadr(:)
      real*8, allocatable::qlocl(:), qlocr(:)
c
c     1) allocate memory
c     memory query, to create a quasiunitree
      nleaf=nch
c      call getnumqutree(nleaf, nboxes, nlev)
c
      maxboxes=1000000
      maxlevel=100
c
      allocate(levelbox(maxboxes))
      allocate(icolbox(maxboxes))
      allocate(iparentbox(maxboxes))
      allocate(ichildbox(2,maxboxes))
      allocate(nblevel(0:nlev))
      allocate(iboxlev(maxboxes))
      allocate(istartlev(0:nlev))
      allocate(leaflist(maxboxes))
      allocate(newlist(maxboxes))
c
      allocate(cftm(ndeg,ndeg))
      allocate(citm(ndeg,ndeg))
      allocate(spdef(ndeg))
      allocate(spbx(ndeg,ndeg))
      allocate(spxb(ndeg,ndeg))
c
      allocate(chpts(ndeg,maxboxes))
      allocate(xlength(maxboxes))
      allocate(gl(ndeg,maxboxes))
      allocate(gr(ndeg,maxboxes))
      allocate(glp(ndeg,maxboxes))
      allocate(grp(ndeg,maxboxes))
      allocate(phil(ndeg,maxboxes))
      allocate(phir(ndeg,maxboxes))
      allocate(ui(ndeg,maxboxes))
      allocate(uip(ndeg,maxboxes))
      allocate(fval(ndeg,maxboxes))
c
      allocate(alist(2,2,maxboxes))
      allocate(dlist(2,maxboxes))
      allocate(clist(2,maxboxes))
c     
      allocate(sigma(ndeg,maxboxes))
      allocate(quadl(maxboxes))
      allocate(quadr(maxboxes))
      allocate(qlocl(ndeg))
      allocate(qlocr(ndeg))
c
c     2) create a quasi-uniform tree whose leaf nodes
c        corresponds to the subintervals
c      call quasiunitree(levelbox, icolbox, nboxes, 
c     1     maxid, nlev, iparentbox, ichildbox, 
c     2     nblevel, iboxlev, istartlev, nleaf)
c
      call mktreelf(levelbox, icolbox, nboxes, 
     1     maxid, nlev, iparentbox, ichildbox, 
     2     nblevel, iboxlev, istartlev, cent0, 
     3     xsize0, maxboxes, maxlevel, 
     4     nch, endpts)
      write(*,*) 'nlev=',nlev
      write(*,*) 'nch=',nch
      write(*,*) 'nboxes=',nboxes
      write(*,*) 'cent0=',cent0
      write(*,*) 'xsize0=',xsize0
c
      call getleaflist(levelbox, icolbox, nboxes,
     1     maxid, nlev, iparentbox, ichildbox, 
     2     nblevel, iboxlev, istartlev, 
     3     nleaf, leaflist)
      write(*,*) 'nleaf=',nleaf
c
c     set the newlist to be the leaflist
      nnew=nleaf
      do i=1, nleaf
        newlist(i)=leaflist(i)
      enddo
c      
c     3) prepare chebyshev matrices and piecewise
c        chebyshev grid on each leaf node
c
      xa=endpts(1)
      xb=endpts(nch+1)
      write(*,*) 'xa,xb=',xa, xb
c
      call setchmat(ndeg, cftm, citm, spdef, spbx, spxb)
      do ii=1,nleaf
        ibox=leaflist(ii)
        aa=endpts(ii)
        bb=endpts(ii+1)
        xlength(ibox)=bb-aa
        call chnodes(aa, bb, ndeg, chpts(1,ibox), u, v, u1, v1)
      enddo
c
c     4) choose the right Green's function and
c        inhomogeneous term, and compute phil, phir,
c        and fval (the rhs)
c
c     call setbc to compute the constants 
      call setbc(xa, xb, bcl0, bcl1, bclv, bcr0,
     1     bcr1, bcrv, itype, gl0, gl1, gr0, gr1,
     2     ui0, ui1, ui2, wron)
c
c     call bctab to compute green's functions and 
c           the inhomogeneous term
      do j=1,nleaf
        ibox=leaflist(j)
        do i=1,ndeg
          call bctab(chpts(i,ibox),itype,gl0,gl1,gr0,gr1,
     1         ui0,ui1,ui2,wron,gl(i,ibox),gr(i,ibox),glp(i,ibox),
     2         grp(i,ibox),ui(i,ibox),uip(i,ibox),phil(i,ibox),
     3         phir(i,ibox),fval(i,ibox),peval,qeval,feval,
     4         pars) 
        enddo
      enddo
c
c     5) call rbvpmain to solve the BVP
c
      call rbvpmain(ndeg, levelbox, icolbox, 
     1    nboxes, maxid, nlev, iparentbox, ichildbox,
     2    nblevel, iboxlev, istartlev, nleaf, leaflist,
     3    nnew, newlist, cftm, citm, spdef, spbx, spxb, 
     4    chpts, xlength, gl, gr, glp, grp, phil, phir, 
     5    fval, ui, uip, wron, alist, dlist, clist,
     6    sigma, quadl, quadr, qlocl, qlocr, ulsol, ulder) 
c
c     RMK: rbvpmain written in the order of the leaflist
c          while interpf written in the order of the tree
c          rbvpnoadap + rbvpmain
c          never used in actual examples
c          modify -> in the order of the tree
c          okay, modified. but interpf is in 
c          the order of the leaflist after all
c
c        write(*,*) 'interpolating to the grid'
        call interpf(nlev, levelbox, iparentbox,
     1       ichildbox, icolbox, nboxes, maxid,
     2       nblevel, iboxlev, istartlev, cent0,
     3       xsize0, nleaf, leaflist, ndeg, ulsol,
     4       ntarg, xtarg, usol)
c
        call interpf(nlev, levelbox, iparentbox,
     1       ichildbox, icolbox, nboxes, maxid,
     2       nblevel, iboxlev, istartlev, cent0,
     3       xsize0, nleaf, leaflist, ndeg, ulder,
     4       ntarg, xtarg, uder)
c
c     6) deallocate memory
c
      deallocate(levelbox)
      deallocate(icolbox)
      deallocate(iparentbox)
      deallocate(ichildbox)
      deallocate(nblevel)
      deallocate(iboxlev)
      deallocate(istartlev)
      deallocate(leaflist)
      deallocate(newlist)
c
      deallocate(cftm)
      deallocate(citm)
      deallocate(spdef)
      deallocate(spbx)
      deallocate(spxb)
c
      deallocate(chpts)
      deallocate(xlength)
      deallocate(gl)
      deallocate(gr)
      deallocate(glp)
      deallocate(grp)
      deallocate(phil)
      deallocate(phir)
      deallocate(ui)
      deallocate(uip)
      deallocate(fval)
c
      deallocate(alist)
      deallocate(dlist)
      deallocate(clist)
c
      deallocate(sigma)
      deallocate(quadl)
      deallocate(quadr)
      deallocate(qlocl)
      deallocate(qlocr)

      end subroutine





c--------------------------------------------------
c     subroutine rbvpmain
c--------------------------------------------------
c
c     This subroutine does the main work of bvp solver
c     (real scalar case)
c
c--------------------------------------------------
c
      subroutine rbvpmain(ndeg, levelbox, icolbox, 
     1    nboxes, maxid, nlev, iparentbox, ichildbox,
     2    nblevel, iboxlev, istartlev, nleaf, leaflist,
     3    nnew, newlist, cftm, citm, spdef, spbx, spxb, 
     4    chpts, xlength, gl, gr, glp, grp, phil, phir, 
     5    fval, ui, uip, wron, alist, dlist, clist, 
     6    sigma, quadl, quadr, qlocl, qlocr, ulsol, ulder) 
      implicit real*8 (a-h,o-z)
      integer ndeg
c     binary tree and leaflist
      integer nboxes, nlev, nleaf, nnew, maxid
      integer levelbox(nboxes), icolbox(nboxes)
      integer iparentbox(nboxes), ichildbox(2,nboxes)
      integer nblevel(0:nlev)
      integer iboxlev(nboxes), istartlev(0:nlev)
      integer leaflist(nleaf), newlist(nnew)
c     chebyshev matrices
      real*8 cftm(ndeg,ndeg), citm(ndeg,ndeg)
      real*8 spdef(ndeg), spbx(ndeg,ndeg), spxb(ndeg,ndeg)
c     data on leaf nodes
      real*8 chpts(ndeg,nboxes), xlength(nboxes)
      real*8 gl(ndeg,nboxes), gr(ndeg,nboxes)
      real*8 glp(ndeg,nboxes), grp(ndeg,nboxes)
      real*8 phil(ndeg,nboxes), phir(ndeg,nboxes)
      real*8 ui(ndeg,nboxes), uip(ndeg,nboxes)
      real*8 fval(ndeg,nboxes), wron
c     inner products and coupling coeffs
      real*8 alist(2,2,nboxes), dlist(2,nboxes)
      real*8 clist(2,nboxes)
c     solution related
      real*8 sigma(ndeg,nboxes)
      real*8 quadl(nboxes), quadr(nboxes)
      real*8 qlocl(ndeg), qlocr(ndeg)
      real*8 ulsol(ndeg,nboxes), ulder(ndeg,nboxes)
c
c     1) on leaf nodes, do the local solve and 
c        compute the inner products
c        update: change to the newlist instead of
c        the leaflist
c
      do ii=1,nnew
        ibox=newlist(ii)
        call locsolve(ndeg, xlength(ibox), spbx, spxb,
     1       gl(1,ibox), gr(1,ibox), phil(1,ibox),
     2       phir(1,ibox), fval(1,ibox))
c
        call mkprod(ndeg, xlength(ibox), gl(1,ibox), 
     1       gr(1,ibox), fval(1,ibox), phil(1,ibox), 
     2       phir(1,ibox), spdef, 
     3       alist(1,1,ibox), alist(2,1,ibox),
     4       alist(1,2,ibox), alist(2,2,ibox), 
     5       dlist(1,ibox), dlist(2,ibox))
      enddo
c
c     2) upward sweep to compute inner products
c
      do l=nlev,0,-1
        istart=istartlev(l)
        iend=istart+nblevel(l)-1
        do ii=istart,iend
          ibox=iboxlev(ii)
          if(ichildbox(1,ibox) .gt.0 ) then
            icha=ichildbox(1,ibox)
            ichb=ichildbox(2,ibox)
            call chd2par(ibox, icha, ichb, maxid,
     1           alist, dlist, cond)
          endif
        enddo
      enddo
c
c     3) downward sweep to compute coupling coefficients
c
      l=0
      iroot=istartlev(l) 
      clist(1,iroot)=0.0d0
      clist(2,iroot)=0.0d0
c
      do l=0,nlev
        istart=istartlev(l)
        iend=istart+nblevel(l)-1
        do ii=istart,iend
          ibox=iboxlev(ii)
          if(ichildbox(1,ibox) .gt. 0) then
            icha=ichildbox(1,ibox)
            ichb=ichildbox(2,ibox)
            call par2chd(ibox, icha, ichb, maxid,
     1           alist, dlist, clist)
          endif
        enddo
      enddo
c
c     4) recover the solution and first derivative
c
      do ii=1,nleaf
        ibox=leaflist(ii)
        call evaldens(ndeg, sigma(1,ii), clist(1,ibox),
     1       clist(2,ibox), phil(1,ibox), phir(1,ibox),
     2       fval(1,ibox))
c       call evaldens to patch together the density
c       sigma is stored in the order of leaflist
      enddo
c
      call precompq(nleaf, leaflist, alist, dlist,
     1     clist, quadl, quadr)
c     call precompq to precompute global integrals
c     quadl and quadr for each (leaf) interval
c
      do ii=1,nleaf
        ibox=leaflist(ii)
        call quadloc(ndeg, xlength(ibox), sigma(1,ii),
     1       gl(1,ibox), gr(1,ibox), spbx, spxb,
     2       qlocl, qlocr)
c       go through the leaflist to compute the local
c       integrals qlocl and qlocr 
c
        do jj=1,ndeg
          ulsol(jj,ii)=ui(jj,ibox)+gr(jj,ibox)/wron*
     1    (quadl(ii)+qlocl(jj))+gl(jj,ibox)/wron*
     2    (quadr(ii)+qlocr(jj))
c
          ulder(jj,ii)=uip(jj,ibox)+grp(jj,ibox)/wron*
     1    (quadl(ii)+qlocl(jj))+glp(jj,ibox)/wron*
     2    (quadr(ii)+qlocr(jj))

        enddo
      enddo 
c
c
      end subroutine






c***************************************************
c     COMPLEX VERSION
c***************************************************





c--------------------------------------------------
c     subroutine zbvpnoadap
c--------------------------------------------------
c
c     Complex BVP solver, non-adaptive version
c
c     this subroutine solves the two point BVP
c     (1) - (3):
c     
c     u''(x)+ p(x)*u'(x) +q(x)*u(x) = f(x) 
c                  (x \in [xa, xb])    (1)
c     bcl0*u(xa) + bcl1*u'(xa) = bclv  (2)
c     bcr0*u(xb) + bcr1*u'(xb) = bcrv  (3)
c
c     INPUT:
c     ndeg: degree of Chebyshev approx on each subinterval
c     nch: number of subintervals
c     endpts: real array of length nch+1, endpoints of 
c             subintervals. endpts(1)=xa, endpts(nch+1)=xb.
c     bc**: boundary conditions, as in (2) and (3)
c     peval, qeval, feval: function evaluation 
c           subroutines for p, q, and f in (1)
c           which have the calling sequence:
c           peval(x,p,pars), qeval(x,q,pars), feval(x,f,pars)
c           where x is the eval point, p/q/f is the value
c           returned, and pars is the list of parameters
c
c     OUTPUT:
c     usol: complex*16, dim (ndeg,nch), the solution u
c           evaluated at Chebyshev points on each
c           interval (in leaflist)        
c     uder: complex*16, dim (ndeg,nch), the derivative
c           of u evaluated at the same grid 
c
c--------------------------------------------------
c
      subroutine zbvpnoadap(ndeg, nch, endpts,
     1    bcl0, bcl1, bclv, bcr0, bcr1, bcrv, 
     2    peval, qeval, feval, pars, usol, uder)
      implicit real*8 (a-h,o-z)
      integer ndeg, nch
      real*8 endpts(nch+1)
      complex*16 bcl0, bcl1, bclv, bcr0, bcr1, bcrv
      complex*16 pars(1), usol(ndeg,nch), uder(ndeg,nch)
      external peval, qeval, feval
c     local vars 
      complex*16 gl0, gl1, gr0, gr1
      complex*16 ui0, ui1, ui2, wron
c     binary tree and leaflist
      integer nboxes, maxid, nlev, nleaf
      integer,allocatable:: levelbox(:)
      integer,allocatable:: icolbox(:) 
      integer,allocatable:: iparentbox(:)
      integer,allocatable:: ichildbox(:,:)
      integer,allocatable:: nblevel(:)
      integer,allocatable:: iboxlev(:)
      integer,allocatable:: istartlev(:)
      integer,allocatable:: leaflist(:)
c     chebyshev matrices      
      complex*16, allocatable::cftm(:,:), citm(:,:) 
      complex*16, allocatable::spdef(:), spbx(:,:), spxb(:,:)
c     data on leaf nodes
      real*8, allocatable::chpts(:,:), xlength(:)
      complex*16, allocatable::gl(:,:), gr(:,:)
      complex*16, allocatable::glp(:,:), grp(:,:)
      complex*16, allocatable::phil(:,:), phir(:,:)
      complex*16, allocatable::ui(:,:), uip(:,:)
      complex*16, allocatable::fval(:,:)
c     inner products and coupling coeffs
      complex*16, allocatable::alist(:,:,:), dlist(:,:)
      complex*16, allocatable::clist(:,:)
c     solution related
      complex*16, allocatable::sigma(:,:)
      complex*16, allocatable::quadl(:), quadr(:)
      complex*16, allocatable::qlocl(:), qlocr(:)
c
c     1) allocate memory
c     memory query, to create a quasiunitree
      nleaf=nch
      call getnumqutree(nleaf, nboxes, nlev)
c
      allocate(levelbox(nboxes))
      allocate(icolbox(nboxes))
      allocate(iparentbox(nboxes))
      allocate(ichildbox(2,nboxes))
      allocate(nblevel(0:nlev))
      allocate(iboxlev(nboxes))
      allocate(istartlev(0:nlev))
      allocate(leaflist(nleaf))
c
      allocate(cftm(ndeg,ndeg))
      allocate(citm(ndeg,ndeg))
      allocate(spdef(ndeg))
      allocate(spbx(ndeg,ndeg))
      allocate(spxb(ndeg,ndeg))
c
      allocate(chpts(ndeg,nleaf))
      allocate(xlength(nleaf))
      allocate(gl(ndeg,nleaf))
      allocate(gr(ndeg,nleaf))
      allocate(glp(ndeg,nleaf))
      allocate(grp(ndeg,nleaf))
      allocate(phil(ndeg,nleaf))
      allocate(phir(ndeg,nleaf))
      allocate(ui(ndeg,nleaf))
      allocate(uip(ndeg,nleaf))
      allocate(fval(ndeg,nleaf))
c
      allocate(alist(2,2,nboxes))
      allocate(dlist(2,nboxes))
      allocate(clist(2,nboxes))
c     
      allocate(sigma(ndeg,nleaf))
      allocate(quadl(nleaf))
      allocate(quadr(nleaf))
      allocate(qlocl(ndeg))
      allocate(qlocr(ndeg))
c
c     2) create a quasi-uniform tree whose leaf nodes
c        corresponds to the subintervals
      call quasiunitree(levelbox, icolbox, nboxes, 
     1     maxid, nlev, iparentbox, ichildbox, 
     2     nblevel, iboxlev, istartlev, nleaf)
c
      call getleaflist(levelbox, icolbox, nboxes,
     1     maxid, nlev, iparentbox, ichildbox, 
     2     nblevel, iboxlev, istartlev, 
     3     nleaf, leaflist)
c      
c     3) prepare chebyshev matrices and piecewise
c        chebyshev grid on each leaf node
c
      xa=endpts(1)
      xb=endpts(nch+1)
      call zsetchmat(ndeg, cftm, citm, spdef, spbx, spxb)
      do ii=1,nleaf
        ibox=leaflist(ii)
c        call mkgrid(levelbox(ibox), icolbox(ibox), xa,
c     1       xb, ndeg, xlength(ii),chpts(1,ii))
        aa=endpts(ii)
        bb=endpts(ii+1)
        xlength(ii)=bb-aa
        call chnodes(aa, bb, ndeg, chpts(1,ii), u, v, u1, v1)
      enddo
c
c     4) choose the right Green's function and
c        inhomogeneous term, and compute phil, phir,
c        and fval (the rhs)
c
c     call setbc to compute the constants 
      call zsetbc(xa, xb, bcl0, bcl1, bclv, bcr0,
     1     bcr1, bcrv, itype, gl0, gl1, gr0, gr1,
     2     ui0, ui1, ui2, wron)
c
c     call bctab to compute green's functions and 
c           the inhomogeneous term
      do j=1,nleaf
        do i=1,ndeg
          call zbctab(chpts(i,j),itype,gl0,gl1,gr0,gr1,
     1         ui0,ui1,ui2,wron,gl(i,j),gr(i,j),glp(i,j),
     2         grp(i,j),ui(i,j),uip(i,j),phil(i,j),
     3         phir(i,j),fval(i,j),peval,qeval,feval,
     4         pars) 
        enddo
      enddo
c
c     5) call zbvpmain to solve the BVP
c
      call zbvpmain(ndeg, levelbox, icolbox, 
     1    nboxes, maxid, nlev, iparentbox, ichildbox,
     2    nblevel, iboxlev, istartlev, nleaf, leaflist,
     3    cftm, citm, spdef, spbx, spxb, chpts, xlength,
     4    gl, gr, glp, grp, phil, phir, fval, ui, uip,
     6    wron, alist, dlist, clist, sigma, quadl, quadr,
     7    qlocl, qlocr, usol, uder) 
c
c     6) deallocate memory
c
      deallocate(levelbox)
      deallocate(icolbox)
      deallocate(iparentbox)
      deallocate(ichildbox)
      deallocate(nblevel)
      deallocate(iboxlev)
      deallocate(istartlev)
      deallocate(leaflist)
c
      deallocate(cftm)
      deallocate(citm)
      deallocate(spdef)
      deallocate(spbx)
      deallocate(spxb)
c
      deallocate(chpts)
      deallocate(xlength)
      deallocate(gl)
      deallocate(gr)
      deallocate(glp)
      deallocate(grp)
      deallocate(phil)
      deallocate(phir)
      deallocate(ui)
      deallocate(uip)
      deallocate(fval)
c
      deallocate(alist)
      deallocate(dlist)
      deallocate(clist)
c
      deallocate(sigma)
      deallocate(quadl)
      deallocate(quadr)
      deallocate(qlocl)
      deallocate(qlocr)

      end subroutine






c--------------------------------------------------
c     subroutine zbvpmain
c--------------------------------------------------
c
c     This subroutine does the main work of bvp solver
c     (complex scalar case)
c
c--------------------------------------------------
c
      subroutine zbvpmain(ndeg, levelbox, icolbox, 
     1    nboxes, maxid, nlev, iparentbox, ichildbox,
     2    nblevel, iboxlev, istartlev, nleaf, leaflist,
     3    cftm, citm, spdef, spbx, spxb, chpts, xlength,
     4    gl, gr, glp, grp, phil, phir, fval, ui, uip,
     6    wron, alist, dlist, clist, sigma, quadl, quadr,
     7    qlocl, qlocr, usol, uder) 
      implicit real*8 (a-h,o-z)
c     binary tree and leaflist
      integer nboxes, nlev, nleaf, maxid
      integer levelbox(nboxes), icolbox(nboxes)
      integer iparentbox(nboxes), ichildbox(2,nboxes)
      integer nblevel(0:nlev)
      integer iboxlev(nboxes), istartlev(0:nlev)
      integer leaflist(nleaf)
c     chebyshev matrices
      complex*16 cftm(ndeg,ndeg), citm(ndeg,ndeg)
      complex*16 spdef(ndeg), spbx(ndeg,ndeg), spxb(ndeg,ndeg)
c     data on leaf nodes
      real*8 chpts(ndeg,nleaf), xlength(nleaf)
      complex*16 gl(ndeg,nleaf), gr(ndeg,nleaf)
      complex*16 glp(ndeg,nleaf), grp(ndeg,nleaf)
      complex*16 phil(ndeg,nleaf), phir(ndeg,nleaf)
      complex*16 ui(ndeg,nleaf), uip(ndeg,nleaf)
      complex*16 fval(ndeg,nleaf), wron
c     inner products and coupling coeffs
      complex*16 alist(2,2,nboxes), dlist(2,nboxes)
      complex*16 clist(2,nboxes)
c     solution related
      complex*16 sigma(ndeg,nleaf)
      complex*16 quadl(nleaf), quadr(nleaf)
      complex*16 qlocl(ndeg), qlocr(ndeg)
      complex*16 usol(ndeg,nleaf), uder(ndeg,nleaf)

c
c     1) on leaf nodes, do the local solve and 
c        compute the inner products
c
      do ii=1,nleaf
        ibox=leaflist(ii)
        call zlocsolve(ndeg, xlength(ii), spbx, spxb,
     1       gl(1,ii), gr(1,ii), phil(1,ii),
     2       phir(1,ii), fval(1,ii))
c
        call zmkprod(ndeg, xlength(ii), gl(1,ii), 
     1       gr(1,ii), fval(1,ii), phil(1,ii), 
     2       phir(1,ii), spdef, 
     3       alist(1,1,ibox), alist(2,1,ibox),
     4       alist(1,2,ibox), alist(2,2,ibox), 
     5       dlist(1,ibox), dlist(2,ibox))
      enddo
c
c     2) upward sweep to compute inner products
c
      do l=nlev,0,-1
        istart=istartlev(l)
        iend=istart+nblevel(l)-1
        do ii=istart,iend
          ibox=iboxlev(ii)
          if(ichildbox(1,ibox) .gt.0 ) then
            icha=ichildbox(1,ibox)
            ichb=ichildbox(2,ibox)
            call zchd2par(ibox, icha, ichb, maxid,
     1           alist, dlist, cond)
          endif
        enddo
      enddo
c
c     3) downward sweep to compute coupling coefficients
c
      l=0
      iroot=istartlev(l) 
      clist(1,iroot)=0.0d0
      clist(2,iroot)=0.0d0
c
      do l=0,nlev
        istart=istartlev(l)
        iend=istart+nblevel(l)-1
        do ii=istart,iend
          ibox=iboxlev(ii)
          if(ichildbox(1,ibox) .gt. 0) then
            icha=ichildbox(1,ibox)
            ichb=ichildbox(2,ibox)
            call zpar2chd(ibox, icha, ichb, maxid,
     1           alist, dlist, clist)
          endif
        enddo
      enddo
c
c     4) recover the solution and first derivative
c
      do ii=1,nleaf
        ibox=leaflist(ii)
        call zevaldens(ndeg, sigma(1,ii), clist(1,ibox),
     1       clist(2,ibox), phil(1,ii), phir(1,ii),
     2       fval(1,ii))
c       call evaldens to patch together the density
c       sigma is stored in the order of leaflist
      enddo
c
      call zprecompq(nleaf, leaflist, alist, dlist,
     1     clist, quadl, quadr)
c     call precompq to precompute global integrals
c     quadl and quadr for each (leaf) interval
c
      do ii=1,nleaf
        ibox=leaflist(ii)
        call zquadloc(ndeg, xlength(ii), sigma(1,ii),
     1       gl(1,ii), gr(1,ii), spbx, spxb,
     2       qlocl, qlocr)
c       go through the leaflist to compute the local
c       integrals qlocl and qlocr 
c
        do jj=1,ndeg
          usol(jj,ii)=ui(jj,ii)+gr(jj,ii)/wron*
     1    (quadl(ii)+qlocl(jj))+gl(jj,ii)/wron*
     2    (quadr(ii)+qlocr(jj))
c
          uder(jj,ii)=uip(jj,ii)+grp(jj,ii)/wron*
     1    (quadl(ii)+qlocl(jj))+glp(jj,ii)/wron*
     2    (quadr(ii)+qlocr(jj))

        enddo
      enddo 
c
c
      end subroutine






c--------------------------------------------------
c     subroutine rbvpadap
c--------------------------------------------------
c
c     Real BVP solver, adaptive version
c
c     this subroutine solves the two point BVP
c     (1) - (3):
c     
c     u''(x)+ p(x)*u'(x) +q(x)*u(x) = f(x) 
c                  (x \in [xa, xb])    (1)
c     bcl0*u(xa) + bcl1*u'(xa) = bclv  (2)
c     bcr0*u(xb) + bcr1*u'(xb) = bcrv  (3)
c
c     INPUT:
c     ndeg: degree of Chebyshev approx on each subinterval
c     nch: number of subintervals of the initial grid.
c     endpts: real array of length nch+1, endpoints of 
c             subintervals of the initial grid.
c             endpts(1)=xa, endpts(nch+1)=xb.
c     bc**: boundary conditions, as in (2) and (3)
c     peval, qeval, feval: function evaluation 
c           subroutines for p, q, and f in (1)
c           which have the calling sequence:
c           peval(x,p,pars), qeval(x,q,pars), feval(x,f,pars)
c           where x is the eval point, p/q/f is the value
c           returned, and pars is the list of parameters
c     maxdiv: maximum rounds of subdivisions and mergings
c             allowed
c     rtol: error tolerance
c     ntarg: length of xtarg
c     xtarg: user specified grid where the solution is needed
c
c     OUTPUT:
c     usol: the solution u evaluated at xtarg
c     uder: the derivative of u evaluated at xtarg
c
c--------------------------------------------------
c
      subroutine rbvpadap(ndeg, nch, endpts,
     1    bcl0, bcl1, bclv, bcr0, bcr1, bcrv, 
     2    peval, qeval, feval, pars, maxdiv, 
     3    rtol, ntarg, xtarg, usol, uder)
      implicit real*8 (a-h,o-z)
      integer ndeg, nch, ntarg
      real*8 endpts(nch+1), xtarg(ntarg)
      real*8 bcl0, bcl1, bclv, bcr0, bcr1, bcrv
      real*8 pars(1), usol(ntarg), uder(ntarg)
      external peval, qeval, feval
c     local vars (allocatable)
c     binary tree and leaflist
      integer nboxes, maxid, nlev, nleaf
      integer,allocatable:: levelbox(:)
      integer,allocatable:: icolbox(:) 
      integer,allocatable:: iparentbox(:)
      integer,allocatable:: ichildbox(:,:)
      integer,allocatable:: nblevel(:)
      integer,allocatable:: iboxlev(:)
      integer,allocatable:: istartlev(:)
      integer,allocatable:: leaflist(:)
      integer,allocatable:: newlist(:)
c     chebyshev matrices      
      real*8, allocatable::cftm(:,:), citm(:,:) 
      real*8, allocatable::spdef(:), spbx(:,:), spxb(:,:)
c     data on leaf nodes
      real*8, allocatable::chpts(:,:), xlength(:)
      real*8, allocatable::gl(:,:), gr(:,:)
      real*8, allocatable::glp(:,:), grp(:,:)
      real*8, allocatable::phil(:,:), phir(:,:)
      real*8, allocatable::ui(:,:), uip(:,:)
      real*8, allocatable::fval(:,:)
c     inner products and coupling coeffs
      real*8, allocatable::alist(:,:,:), dlist(:,:)
      real*8, allocatable::clist(:,:)
c     solution related
      real*8, allocatable::sigma(:,:)
      real*8, allocatable::quadl(:), quadr(:)
      real*8, allocatable::qlocl(:), qlocr(:)
      real*8, allocatable::ulsol(:,:), ulder(:,:)
      real*8, allocatable::smoni(:), upre(:)
c
c---------------------------
c     STEP 0: initialization
c---------------------------
c
c     1) allocate memory, increase nmax and maxlev
c        if necessary
      nmax=1000000
      maxlev=20
c
      allocate(levelbox(nmax))
      allocate(icolbox(nmax))
      allocate(iparentbox(nmax))
      allocate(ichildbox(2,nmax))
      allocate(nblevel(0:maxlev))
      allocate(iboxlev(nmax))
      allocate(istartlev(0:maxlev))
      allocate(leaflist(nmax))
      allocate(newlist(nmax))
c
      allocate(cftm(ndeg,ndeg))
      allocate(citm(ndeg,ndeg))
      allocate(spdef(ndeg))
      allocate(spbx(ndeg,ndeg))
      allocate(spxb(ndeg,ndeg))
c
      allocate(chpts(ndeg,nmax))
      allocate(xlength(nmax))
      allocate(gl(ndeg,nmax))
      allocate(gr(ndeg,nmax))
      allocate(glp(ndeg,nmax))
      allocate(grp(ndeg,nmax))
      allocate(phil(ndeg,nmax))
      allocate(phir(ndeg,nmax))
      allocate(ui(ndeg,nmax))
      allocate(uip(ndeg,nmax))
      allocate(fval(ndeg,nmax))
c
      allocate(alist(2,2,nmax))
      allocate(dlist(2,nmax))
      allocate(clist(2,nmax))
c     
      allocate(sigma(ndeg,nmax))
      allocate(quadl(nmax))
      allocate(quadr(nmax))
      allocate(qlocl(ndeg))
      allocate(qlocr(ndeg))
c
      allocate(ulsol(ndeg,nmax))
      allocate(ulder(ndeg,nmax))
      allocate(smoni(nmax))
      allocate(upre(ntarg))
c     there are two types of ordering:
c     quadl, quadr, sigma, ulsol, ulder are stored
c     in the order of the (current) leaflist,
c     while others are stored in the order of the tree
c
c
c     prepare the chebyshev matrices
      xa=endpts(1)
      xb=endpts(nch+1)
      call setchmat(ndeg, cftm, citm, spdef, spbx, spxb)
c
c     choose the right Green's function and
c     inhomogeneous term, call setbc to compute 
c     the constants 
      call setbc(xa, xb, bcl0, bcl1, bclv, bcr0,
     1     bcr1, bcrv, itype, gl0, gl1, gr0, gr1,
     2     ui0, ui1, ui2, wron)
c
c     2) create a quasi-uniform tree whose leaf nodes
c        corresponds to the subintervals
      nleaf=nch
      call getnumqutree(nleaf, nboxes, nlev)
c
      xsize0=xb-xa
      cent0=xa+xsize0/2.0d0
      call quasiunitree(levelbox, icolbox, nboxes, 
     1     maxid, nlev, iparentbox, ichildbox, 
     2     nblevel, iboxlev, istartlev, nleaf)
c
      call getleaflist(levelbox, icolbox, nboxes,
     1     maxid, nlev, iparentbox, ichildbox, 
     2     nblevel, iboxlev, istartlev, 
     3     nleaf, leaflist)
c
c     set the newlist to be the leaflist
      nnew=nleaf
      do i=1, nleaf
        newlist(i)=leaflist(i)
      enddo
c
c     initialize the solution at the previous time step
c     to be zero
      do i=1, ntarg
        upre(i)=0.0d0
      enddo
c
c-------------------------------------------------
      nstep=0
      idbled=0
      do while((nnew .gt. 0) .and. (nstep .le. maxdiv))
c       adapt control: newlist is nonempty, and
c       the number of adaptive refinement and coarsening
c       hasn't exceeded maxdiv. Do the following steps:
c
        nstep=nstep+1     
        write(*,*) '****** nstep=', nstep
        write(*,*) '****** nboxes=', nboxes
c -------------------------------
c       STEP 1: local solve, for nodes in the newlist
c -------------------------------
c        write(*,*) 'setting up chebyshev nodes' 
c        write(*,*) 'doing the loval solve'
c        write(*,*) 'evaluating innner products'
        do ii=1, nnew
          ibox=newlist(ii)
          lev=levelbox(ibox)
          icol=icolbox(ibox)
c         set the cheby pts
          call mkgrid(lev, icol, xa, xb, ndeg,
     1         aa, bb, chpts(1,ibox))
          xlength(ibox)=bb-aa
c          write(22,*) aa, 0.0d0
c        
c         compute the green's function and 
c         related function values on this node
c         returns:
c         gl,gr,glp,grp,ui,uip,phil,phir,fval
c         at the cheby pts on each of the newlist nodes
          do i=1,ndeg
            call bctab(chpts(i,ibox),itype,gl0,gl1,gr0,gr1,
     1           ui0,ui1,ui2,wron,gl(i,ibox),gr(i,ibox),glp(i,ibox),
     2           grp(i,ibox),ui(i,ibox),uip(i,ibox),phil(i,ibox),
     3           phir(i,ibox),fval(i,ibox),peval,qeval,feval,pars) 
c           these func vals will be needed later
c           should be saved
          enddo
c
c         now do the local solve
          call locsolve(ndeg, xlength(ibox), spbx,
     1         spxb, gl(1,ibox), gr(1,ibox), 
     2         phil(1,ibox), phir(1,ibox), fval(1,ibox))
c
c         eval inner products alist and dlist
          call mkprod(ndeg,xlength(ibox),gl(1,ibox), 
     1         gr(1,ibox),fval(1,ibox),phil(1,ibox), 
     2         phir(1,ibox), spdef, 
     3         alist(1,1,ibox), alist(2,1,ibox),
     4         alist(1,2,ibox), alist(2,2,ibox), 
     5         dlist(1,ibox), dlist(2,ibox))
        enddo 
c
        nnew=0
c       remove the processed nodes from the newlist
c
c -------------------------------
c     STEP 2A: an upward sweep to compute inner products
c -------------------------------
c        write(*,*) 'upward sweep'
        do l=nlev,0,-1
          istart=istartlev(l)
          iend=istart+nblevel(l)-1
          do ii=istart,iend
            ibox=iboxlev(ii)
            if(ichildbox(1,ibox) .gt.0 ) then
              icha=ichildbox(1,ibox)
              ichb=ichildbox(2,ibox)
              call chd2par(ibox, icha, ichb, maxid,
     1             alist, dlist, cond)
            endif
          enddo
        enddo
c
c -------------------------------
c     STEP 2B: a downward sweep to compute 
c              the coupling coeffs
c -------------------------------
c        write(*,*) 'downward sweep'
        l=0
        iroot=istartlev(l) 
        clist(1,iroot)=0.0d0
        clist(2,iroot)=0.0d0
c
        do l=0,nlev
          istart=istartlev(l)
          iend=istart+nblevel(l)-1
          do ii=istart,iend
            ibox=iboxlev(ii)
            if(ichildbox(1,ibox) .gt. 0) then
              icha=ichildbox(1,ibox)
              ichb=ichildbox(2,ibox)
              call par2chd(ibox, icha, ichb, maxid,
     1             alist, dlist, clist)
            endif
          enddo
        enddo
c
c -------------------------------
c       STEP 2C: recover the solution of
c            the global integral equation 
c -------------------------------
c        write(*,*) 'evaluating the sol of the bie'
        do ii=1, nleaf
c         make sure the leaflist is updated after
c         each round of adaptivity
          ibox=leaflist(ii)
          call evaldens(ndeg,sigma(1,ii),
     1         clist(1,ibox),clist(2,ibox),
     2         phil(1,ibox),phir(1,ibox),
     3         fval(1,ibox))
c         call evaldens to patch together the 
c         density stored in sigma
c         attention: sigma is stored in the order
c         of the leaflist
        enddo
c
c -------------------------------
c       STEP 3: recover the solution of the
c       ODE and its derivative
c -------------------------------
c
c        write(*,*) 'precomputing local quads'
        call precompq(nleaf, leaflist, alist, 
     1       dlist, clist, quadl, quadr)
c       call precompq to precompute global 
c       integrals quadl and quadr for each (leaf) 
c       interval
c       (stored in the order of leaflist!)
c
        do ii=1, nleaf
          ibox=leaflist(ii)
          call quadloc(ndeg, xlength(ibox), 
     1         sigma(1,ii), gl(1,ibox), 
     2         gr(1,ibox), spbx, spxb, qlocl, qlocr)
          
c         go through the leaflist to compute the 
c         local integrals qlocl and qlocr 
c
          do jj=1, ndeg
            ulsol(jj,ii)=ui(jj,ibox)+gr(jj,ibox)/wron*
     1      (quadl(ii)+qlocl(jj))+gl(jj,ibox)/wron*
     2      (quadr(ii)+qlocr(jj))
c
            ulder(jj,ii)=uip(jj,ibox)+grp(jj,ibox)/wron*
     1      (quadl(ii)+qlocl(jj))+glp(jj,ibox)/wron*
     2      (quadr(ii)+qlocr(jj))
          enddo
c         recover the solution and its der on
c         the leaf nodes of the tree: 
c         ulsol, ulder
        enddo
c
c       now interpolate to the user-specified grid
c       by calling interpf
c
c        write(*,*) 'interpolating to the grid'
        call interpf(nlev, levelbox, iparentbox,
     1       ichildbox, icolbox, nboxes, maxid,
     2       nblevel, iboxlev, istartlev, cent0,
     3       xsize0, nleaf, leaflist, ndeg, ulsol,
     4       ntarg, xtarg, usol)
c
        call interpf(nlev, levelbox, iparentbox,
     1       ichildbox, icolbox, nboxes, maxid,
     2       nblevel, iboxlev, istartlev, cent0,
     3       xsize0, nleaf, leaflist, ndeg, ulder,
     4       ntarg, xtarg, uder)
c       make sure nothing is changed on output
c       after calling interpf
c
c -------------------------------
c       STEP 4: the adaptivity control
c -------------------------------
c
        etest=0.0d0
        um2=0.0d0
        up2=0.0d0
        do i=1, ntarg
          um2=um2+abs(upre(i)-usol(i))**2
          up2=up2+abs(upre(i)+usol(i))**2
        enddo
        etest=sqrt(um2/up2)
c       etest: l2 norm of the error
c
        write(*,*) 'etest=',etest
        write(*,*) 'rtol=',rtol
c
c       update: upre = usol
        do i=1, ntarg
          upre(i)=usol(i)
        enddo
c
c-------------------------------------
c
        if(etest .gt. rtol) then
c         not convergent yet, do the following:
c
c         1) call getmonitors (in bvprouts.f)
          call getmonitors(ndeg, nleaf, sigma, smoni,
     1         sdiv)
          write(*,*) 'sdiv=',sdiv
c
c         2) do the thing as in testadapt2.f,
c            remember to add nodes to newlist
          do i=1, nleaf
            ibox=leaflist(i)
            si=smoni(i)
c            write(*,*) 'smoni'
c            write(*,*) ibox, smoni(i), sdiv
            if(si .gt. sdiv) then
c             subdivide ibox and add its children
c             into the newlist
              iparbox=ibox
              write(*,*) 'subdividing box', iparbox
ccccccc       check if nboxes>nmax...
              call subdivide(iparbox, levelbox, icolbox, 
     1             nboxes, maxid, nlev, iparentbox, 
     2             ichildbox, nblevel, iboxlev, istartlev)
              do ic=1, 2
                nnew=nnew+1
                newlist(nnew)=ichildbox(ic,ibox)
              enddo
            elseif(i .lt. nleaf) then
              irt=leaflist(i+1)
              sr=smoni(i+1)
              call isibling(ibox, irt, iparentbox, isib)
              ssum=si+sr
              if((ssum .lt. sdiv/2**8)
     1           .and.(isib .gt. 0)) then
c               overresolved, merge siblings
                iparbox=iparentbox(ibox)
c                write(*,*) 'merging kids of box', iparbox
                call mergechd(iparbox,levelbox,icolbox, 
     1               nboxes,maxid,nlev,iparentbox, 
     2               ichildbox,nblevel,iboxlev,istartlev)
c               add the parent box, which newly becomes
c               a leaf box, into the newlist
                nnew=nnew+1
                newlist(nnew)=iparbox
              endif
            endif
          enddo
c
          idbled=0              
        elseif(idbled .le. 0) then
c         converged, double the grid just to be sure
          do i=1, nleaf
            ibox=leaflist(i)
c           subdivide each leaf box
            call subdivide(ibox, levelbox, icolbox, 
     1           nboxes, maxid, nlev, iparentbox, 
     2           ichildbox, nblevel, iboxlev, istartlev)
c
c           add the new leaf nodes into the newlist
            do ic=1, 2
              nnew=nnew+1
              newlist(nnew)=ichildbox(ic,ibox)
            enddo 
          enddo
c
          idbled=1
        endif
c
c        write(*,*) 'converged at nstep=',nstep
c
c-------------------------------------
c
c       end of this round of adaptive refinement 
c       and coarsening
c
c       call getleaflist to get the current leaves
c       after everything
c        write(*,*) 'getting leaf nodes'
        call getleaflist(levelbox, icolbox, nboxes,
     1       maxid, nlev, iparentbox, ichildbox, 
     2       nblevel, iboxlev, istartlev, 
     3       nleaf, leaflist)
c
      enddo
c     
c     for debugging only
c     print out the chebyshev grid
c
c      do ii=1, nleaf
c        ibox=leaflist(ii)
c        lev=levelbox(ibox)
c        icol=icolbox(ibox)
c       set the cheby pts
c        call mkgrid(lev, icol, xa, xb, ndeg,
c     1       aa, bb, chpts(1,ibox))
c        write(31,*) aa, 1.0d0
c      enddo
c      write(31,*) bb, 1.0d0
c
c-------------------------------------------------
c
c     6) deallocate memory
c
      deallocate(levelbox)
      deallocate(icolbox)
      deallocate(iparentbox)
      deallocate(ichildbox)
      deallocate(nblevel)
      deallocate(iboxlev)
      deallocate(istartlev)
      deallocate(leaflist)
      deallocate(newlist)
c
      deallocate(cftm)
      deallocate(citm)
      deallocate(spdef)
      deallocate(spbx)
      deallocate(spxb)
c
      deallocate(chpts)
      deallocate(xlength)
      deallocate(gl)
      deallocate(gr)
      deallocate(glp)
      deallocate(grp)
      deallocate(phil)
      deallocate(phir)
      deallocate(ui)
      deallocate(uip)
      deallocate(fval)
c
      deallocate(alist)
      deallocate(dlist)
      deallocate(clist)
c
      deallocate(sigma)
      deallocate(quadl)
      deallocate(quadr)
      deallocate(qlocl)
      deallocate(qlocr)
c
      deallocate(ulsol)
      deallocate(ulder)
      deallocate(smoni)
      deallocate(upre)

      end subroutine






c--------------------------------------------------
c     subroutine rsysnoadap
c--------------------------------------------------
c
c     Real BVP system solver, non-adaptive version
c
c     This subroutine solves the two point BVP:
c     (1) - (2):
c
c     u'(x)+ p(x)*u(x)  = f(x) 
c              (x \in [xa, xb])    (1)
c     bca*u(xa) + bcb*u(xb) = bcv  (2)
c
c     where u: [xa,xb] -> R^{ndim} is in C1
c           p: [xa,xb] -> R^{ndim x ndim} is continuous
c           f: [xa,xb] -> R^{ndim} is continuous
c
c           bca, bcb \in R^{ndim x ndim}
c           bcv \in R^{ndim}
c
c
c     INPUT:
c     ndim: dimension of the system
c     ndeg: desired order of the method
c     nch: number of subintervals of the user specified grid
c     endpts: real array of length nch+1, endpoints of 
c             subintervals. endpts(1)=xa, endpts(nch+1)=xb.
c             the user specified grid.
c     bc**: boundary conditions, as in (2) and (3)
c     peval, feval: function evaluation 
c           subroutines for p, q, and f in (1)
c           which have the calling sequence:
c           peval(x,p,pars), feval(x,f,pars)
c           where x is the eval point, p/f is the value
c           returned, and pars is the list of parameters
c
c     OUTPUT:
c     usol: real*8, dim (ndeg,nch), the solution u
c           evaluated at Chebyshev points on each
c           interval (in leaflist)        
c     uder: real*8, dim (ndeg,nch), the derivative
c           of u evaluated at the same grid 
c
c--------------------------------------------------
c
      subroutine rsysnoadap(ndim, ndeg, nch, endpts,
     1           bca, bcb, bcv, peval, feval, pars,
     2           ntarg, xtarg, usol, uder)
      implicit real*8 (a-h,o-z)
      integer ndim, ndeg, nch, ntarg
      real*8 endpts(nch+1), pars(1)
      real*8 xtarg(ntarg)
      real*8 bca(ndim,ndim), bcb(ndim,ndim), bcv(ndim)
      real*8 ulsol(ndim,ndeg,nch), ulder(ndim,ndeg,nch)
      real*8 usol(ndim, ntarg), uder(ndim, ntarg)
      external peval, feval
c     local vars (allocatable)
c     binary tree and leaflist
      integer nboxes, maxid, nlev, nleaf
      integer,allocatable:: levelbox(:)
      integer,allocatable:: icolbox(:) 
      integer,allocatable:: iparentbox(:)
      integer,allocatable:: ichildbox(:,:)
      integer,allocatable:: nblevel(:)
      integer,allocatable:: iboxlev(:)
      integer,allocatable:: istartlev(:)
      integer,allocatable:: leaflist(:)
      integer,allocatable:: newlist(:)
c     chebyshev matrices      
      real*8, allocatable::cftm(:,:), citm(:,:) 
      real*8, allocatable::spdef(:), spbx(:,:), spxb(:,:)
      real*8, allocatable::svlvr(:,:), sdvl(:,:), sdvr(:,:)
c     data on leaf nodes
      real*8, allocatable::chpts(:,:), xlength(:)
      real*8, allocatable::ui(:)
      real*8, allocatable::vl(:,:), vr(:,:)
      real*8, allocatable::fval(:,:,:)
      real*8, allocatable::phival(:,:,:,:)
c     inner products and coupling coeffs
      real*8, allocatable:: alist(:,:,:,:), dlist(:,:,:)
      real*8, allocatable:: clist(:,:)
c     solution related
      real*8, allocatable::sigma(:,:,:), eta(:,:,:)
      real*8, allocatable::xi(:,:,:,:)
      real*8, allocatable::quadl(:,:), quadr(:,:)
      real*8, allocatable::qloc(:,:)
c
c---------------------------
c
      xa=endpts(1)
      xb=endpts(nch+1)
c
      xsize0=xb-xa
      cent0=xa+xsize0/2.0d0
c
      write(*,*) 'xa=',xa
      write(*,*) 'xb=',xb
      write(*,*) 'cent0=', cent0
      write(*,*) 'xsize0=', xsize0
c
c     1) allocate memory
c     memory query, to create a quasiunitree
      nleaf=nch
c      call getnumqutree(nleaf, nboxes, nlev)
c      write(*,*) 'nleaf=',nleaf
c      write(*,*) 'nboxes=',nboxes
c      write(*,*) 'nlev=',nlev
c
      maxboxes=1000000
      maxlevel=100
c
      allocate(levelbox(maxboxes))
      allocate(icolbox(maxboxes))
      allocate(iparentbox(maxboxes))
      allocate(ichildbox(2,maxboxes))
      allocate(nblevel(0:maxlevel))
      allocate(iboxlev(maxboxes))
      allocate(istartlev(0:maxlevel))
      allocate(leaflist(maxboxes))
      allocate(newlist(maxboxes))
c
      allocate(cftm(ndeg,ndeg))
      allocate(citm(ndeg,ndeg))
      allocate(spdef(ndeg))
      allocate(spbx(ndeg,ndeg))
      allocate(spxb(ndeg,ndeg))
c
      allocate(svlvr(ndeg*ndim,ndeg*ndim))
      allocate(sdvl(ndim,ndeg*ndim))
      allocate(sdvr(ndim,ndeg*ndim))
c
      allocate(chpts(ndeg,maxboxes))
      allocate(xlength(maxboxes))
      allocate(ui(ndim))
      allocate(vl(ndim,ndim))
      allocate(vr(ndim,ndim))
      allocate(fval(ndim,ndeg,maxboxes))
      allocate(phival(ndim,ndim,ndeg,maxboxes))
c
      allocate(alist(ndim,ndim,2,maxboxes))
      allocate(dlist(ndim,2,maxboxes))
      allocate(clist(ndim,maxboxes))
c
      allocate(sigma(ndim,ndeg,maxboxes))
      allocate(eta(ndim,ndeg,maxboxes))
      allocate(xi(ndim,ndim,ndeg,maxboxes))
      allocate(quadl(ndim,maxboxes))
      allocate(quadr(ndim,maxboxes))
      allocate(qloc(ndim,ndeg))
c
c     2) create a quasi-uniform tree whose leaf nodes
c        corresponds to the subintervals
c     ISSUE: quasi-uniform tree doesn't always work
c            for the user given data
c            change to mktreelf
c
c      call quasiunitree(levelbox, icolbox, nboxes, 
c     1     maxid, nlev, iparentbox, ichildbox, 
c     2     nblevel, iboxlev, istartlev, nleaf)
c
      call mktreelf(levelbox, icolbox, nboxes, 
     1     maxid, nlev, iparentbox, ichildbox, 
     2     nblevel, iboxlev, istartlev, cent0, 
     3     xsize0, maxboxes, maxlevel, 
     4     nch, endpts)
      write(*,*) 'nlev=',nlev
      write(*,*) 'nch=',nch
      write(*,*) 'nboxes=',nboxes
c
      call getleaflist(levelbox, icolbox, nboxes,
     1     maxid, nlev, iparentbox, ichildbox, 
     2     nblevel, iboxlev, istartlev, 
     3     nleaf, leaflist)
      write(*,*) 'nleaf=',nleaf
c
c     set the newlist to be the leaflist
      nnew=nleaf
      do i=1, nleaf
        newlist(i)=leaflist(i)
      enddo
c
c     3) prepare chebyshev matrices and piecewise
c        chebyshev grid on each leaf node
c
      call setchmat(ndeg, cftm, citm, spdef, spbx, spxb)
      do ii=1,nleaf
        ibox=leaflist(ii)
        aa=endpts(ii)
        bb=endpts(ii+1)
        xlength(ii)=bb-aa
        call chnodes(aa, bb, ndeg, chpts(1,ii), u, v, u1, v1)
        do k=1, ndeg
          write(301,*) chpts(k,ii), k, ii, bb-aa
        enddo
        write(305,*) bb-aa
      enddo
c
c     4) choose the right background Green's 
c        function and inhomogeneous term, 
c        and compute phil, phir, and fval (the rhs)
c
c     call ssetbc to compute the constant matrices
c     RMK: for the singular case, don't forget to 
c          do a transform here. not implemented.
      call ssetbc(ndim, bca, bcb, bcv, ui, vl, vr)
c
c     form the spectral integration (with vl, vr) matrices
c     for the system's case
c
      call formsmat(ndim, ndeg, spdef, spbx, spxb, vl,
     1              vr, sdvl, sdvr, svlvr)
c
c     call bctab to compute the rhs: fval and phival
      do j=1, nleaf
        do i=1, ndeg
          call sbctab(ndim,chpts(i,j),ui,fval(1,i,j),
     1         phival(1,1,i,j),peval,feval,pars)
        enddo
      enddo
c
c     5) call rsysmain to solve the BVP
c
      call rsysmain(ndim, ndeg, levelbox, icolbox, 
     1    nboxes, maxid, nlev, iparentbox, ichildbox,
     2    nblevel, iboxlev, istartlev, nleaf, leaflist,
     3    nnew, newlist, svlvr, sdvl, sdvr, chpts,
     4    xlength, vl, vr, phival, fval, ui, alist, dlist, 
     5    clist, sigma, eta, xi, quadl, quadr, qloc, 
     6    ulsol, ulder)
c
c     print out to test
c      do k=1, nch
c        aa=endpts(k)
c        bb=endpts(k+1)
c        call chnodes(aa, bb, ndeg, chpts(1,k), u, v, u1, v1)
cc       x=xtarg(k)
c        do j=1, ndeg
c          x=chpts(j,k)
c          y1=ulsol(1,j,k)
c          y2=ulsol(2,j,k)
c          u=(x-xa)*y1+y2
c          write(222,*) x, u
c          write(223,*) x, y1, y2
c        enddo
c      enddo
c
c
c     now interpolate to the user-specified grid
c     using sinterpf, where function values are
c     saved in the order of the leaf nodes,
c     not in the order of the tree
c
        write(*,*) 'calling sinterpf'
        call sinterpf(nlev, levelbox, iparentbox,
     1       ichildbox, icolbox, nboxes, maxid,
     2       nblevel, iboxlev, istartlev, cent0,
     3       xsize0, nleaf, leaflist, ndim, ndeg, 
     4       ulsol, ntarg, xtarg, usol)
c
c        call sinterpf(nlev, levelbox, iparentbox,
c     1       ichildbox, icolbox, nboxes, maxid,
c     2       nblevel, iboxlev, istartlev, cent0,
c     3       xsize0, nleaf, leaflist, ndim, ndeg, 
c     4       ulder, ntarg, xtarg, uder)
c
c
      deallocate(levelbox)
      deallocate(icolbox)
      deallocate(iparentbox)
      deallocate(ichildbox)
      deallocate(nblevel)
      deallocate(iboxlev)
      deallocate(istartlev)
      deallocate(leaflist)
      deallocate(newlist)
c
      deallocate(cftm)
      deallocate(citm)
      deallocate(spdef)
      deallocate(spbx)
      deallocate(spxb)
c
      deallocate(svlvr)
      deallocate(sdvl)
      deallocate(sdvr)
c
      deallocate(chpts)
      deallocate(xlength)
      deallocate(ui)
      deallocate(vl)
      deallocate(vr)
      deallocate(fval)
      deallocate(phival)
c
      deallocate(alist)
      deallocate(dlist)
      deallocate(clist)
c
      deallocate(sigma)
      deallocate(eta)
      deallocate(xi)

      deallocate(quadl)
      deallocate(quadr)
      deallocate(qloc)


      end subroutine





c--------------------------------------------------
c     subroutine rsysmain
c--------------------------------------------------
c
c     This subroutine does the main work of bvp solver
c     (real system's case)
c
c--------------------------------------------------
c
      subroutine rsysmain(ndim, ndeg, levelbox, icolbox, 
     1    nboxes, maxid, nlev, iparentbox, ichildbox,
     2    nblevel, iboxlev, istartlev, nleaf, leaflist,
     3    nnew, newlist, svlvr, sdvl, sdvr, chpts,
     4    xlength, vl, vr, phival, fval, ui, alist, dlist,
     5    clist, sigma, eta, xi, quadl, quadr, qloc, 
     6    usol, uder)
      implicit real*8 (a-h,o-z)
      integer ndim, ndeg
c     binary tree and leaflist
      integer nboxes, nlev, nleaf, nnew, maxid
      integer levelbox(nboxes), icolbox(nboxes)
      integer iparentbox(nboxes), ichildbox(2,nboxes)
      integer nblevel(0:nlev)
      integer iboxlev(nboxes), istartlev(0:nlev)
      integer leaflist(nleaf), newlist(nnew)
c     const matrices
      real*8 ui(ndim)
      real*8 vl(ndim,ndim), vr(ndim,ndim)
c     chebyshev matrices
      real*8 svlvr(ndeg*ndim,ndeg*ndim)
      real*8 sdvl(ndim,ndeg*ndim), sdvr(ndim,ndeg*ndim)
c     data on leaf nodes
      real*8 chpts(ndeg,nleaf), xlength(nleaf)
      real*8 fval(ndim,ndeg,nleaf)
      real*8 phival(ndim,ndim,ndeg,nleaf)
c     inner products and coupling coeffs
      real*8 alist(ndim,ndim,2,nboxes)
      real*8 dlist(ndim,2,nboxes)
      real*8 clist(ndim,nboxes)
c     local sol related
c     solution related
      real*8 sigma(ndim,ndeg,nleaf)
      real*8 eta(ndim,ndeg,nleaf)
      real*8 xi(ndim,ndim,ndeg,nleaf)
      real*8 quadl(ndim,nleaf), quadr(ndim,nleaf)
      real*8 qloc(ndim, ndeg)
cccccc      real*8 usol(ndim,ndeg,nleaf), uder(ndim,ndeg,nleaf)
      real*8 usol(ndim,ndeg,nboxes), uder(ndim,ndeg,nboxes)
c
c     1) on leaf nodes, do the local solve and 
c        compute the inner products
c        update: change to the newlist instead of
c        the leaflist
c
      do ii=1, nnew
        ibox=newlist(ii)
        call slocsolve(ndim, ndeg, svlvr, phival(1,1,1,ii), 
     1       fval(1,1,ii), xlength(ii), eta(1,1,ii), xi(1,1,1,ii))
c
        call smkprod(ndim, ndeg, xlength(ii), eta(1,1,ii), xi(1,1,1,ii),
     1       sdvl, sdvr, alist(1,1,1,ibox), alist(1,1,2,ibox), 
     2       dlist(1,1,ibox), dlist(1,2,ibox))
      enddo
c
c     2) upward sweep to compute inner products
c
      do l=nlev,0,-1
        istart=istartlev(l)
        iend=istart+nblevel(l)-1
        do ii=istart,iend
          ibox=iboxlev(ii)
          if(ichildbox(1,ibox) .gt.0 ) then
            icha=ichildbox(1,ibox)
            ichb=ichildbox(2,ibox)
            call schd2par(ndim, ibox, icha, ichb, maxid,
     1           alist, dlist)
          endif
        enddo
      enddo
c
c     3) downward sweep to compute coupling coefficients
c
      l=0
      iroot=istartlev(l) 
      do i=1, ndim
        clist(i,iroot)=0.0d0
      enddo
c
      do l=0,nlev
        istart=istartlev(l)
        iend=istart+nblevel(l)-1
        do ii=istart,iend
          ibox=iboxlev(ii)
          if(ichildbox(1,ibox) .gt. 0) then
            icha=ichildbox(1,ibox)
            ichb=ichildbox(2,ibox)
            call spar2chd(ndim, ibox, icha, ichb, maxid,
     1           alist, dlist, clist)
          endif
        enddo
      enddo
c
c     4) recover the solution and first derivative
c
      do ii=1,nleaf
        ibox=leaflist(ii)
        call sevaldens(ndim, ndeg, clist(1,ibox), eta(1,1,ii),
     1       xi(1,1,1,ii), sigma(1,1,ii))
c       call evaldens to patch together the density
c       sigma is stored in the order of leaflist
      enddo
c     correct up to here
c
      call sprecompq(ndim, nleaf, leaflist, alist, dlist, 
     1     clist, quadl, quadr)
c     call sprecompq to precompute global integrals
c     quadl and quadr for each (leaf) interval
c
      do ii=1, nleaf
        ibox=leaflist(ii)
        call squadloc(ndim, ndeg, xlength(ii), sigma(1,1,ii), 
     1       svlvr, qloc)

c       go through the leaflist to compute the local
c       integrals qlocl and qlocr 
c
        do k=1, ndeg
          do j=1, ndim
            usol(j,k,ii)=qloc(j,k)+quadl(j,ii)+quadr(j,ii)+ui(j)
            uder(j,k,ii)=0.0d0
            do jj=1, ndim
              uder(j,k,ii)=uder(j,k,ii)+(vl(j,jj)-vr(j,jj))
     1                                 *sigma(jj,k,ii)
            enddo
          enddo
        enddo
      enddo
c
      end subroutine





c--------------------------------------------------
c     subroutine rnlinadap
c--------------------------------------------------
c
c     Real nonlinear BVP system solver, 
c     adaptive version
c
c     this subroutine solves the two point BVP
c     (1) - (2):
c
c     y'(x) = f(x,y(x)) 
c              (x \in [xa, xb])    (1)
c     bca*y(xa) + bcb*y(xb) = bcv  (2)
c
c     where y: [xa,xb] -> R^{ndim} is in C1
c           f: R^{ndim+1} -> R^{ndim} is continuous
c
c           bca, bcb \in R^{ndim x ndim}
c           bcv \in R^{ndim}
c
c
c     INPUT:
c     ndim: dimension of the system
c     ndeg: degree of Chebyshev approx on each subinterval
c     nch: number of subintervals of the initial grid.
c     endpts: real array of length nch+1, endpoints of 
c             subintervals of the initial grid.
c             endpts(1)=xa, endpts(nch+1)=xb.
c     bc**: boundary conditions, as in (2)
c     feval: function evaluation subroutines for
c            the function f in (1),
c            which have the calling sequence:
c            feval(x,y,f,pars)
c            where x,y is the input, f is the output
c            returned, and pars is the list of parameters
c     fyeval: function evaluation subroutine for
c             df/dy, where f is in (1),
c             whose calling sequence is the same as
c             feval
c     yeval0: function evaluation subroutine for the 
c         initial guess, 
c         whose calling sequence is yeval0(x,y0,pars)
c     ydeval0: function evaluation subroutine for the 
c          derivative of the initial guess,
c          whose calling sequence is 
c          ydeval0(x,yd0,pars)
c
c     maxdiv: maximum rounds of subdivisions and mergings
c             allowed
c     rtol: error tolerance
c     maxstep: maximum number of newton iterations
c     ntarg: length of xtarg
c     xtarg: user specified grid where the solution is needed
c    
c     OUTPUT:
c     ysol: the solution y evaluated at xtarg
c     yder: the derivative of y evaluated at xtarg
c
c--------------------------------------------------
c
      subroutine rnlinadap(ndim, ndeg, nch, endpts,
     1           bca, bcb, bcv, feval, fyeval, yeval0,
     2           ydeval0, pars, maxdiv, rtol, maxstep,
     3           ntarg, xtarg, ysol, yder)
      implicit real*8 (a-h,o-z)
      integer ndim, ndeg, nch, ntarg
      real*8 endpts(nch+1), pars(1), xtarg(ntarg)
      real*8 bca(ndim,ndim), bcb(ndim,ndim), bcv(ndim)
      real*8 ysol(ndim,ntarg), yder(ndim,ntarg)
      real*8 yy(2), rtol
      external feval, fyeval, yeval0, ydeval0
c     local vars (allocatable)
c     binary tree and leaflist
      integer nboxes, maxid, maxstep, nlev, nleaf
      integer,allocatable:: levelbox(:)
      integer,allocatable:: icolbox(:) 
      integer,allocatable:: iparentbox(:)
      integer,allocatable:: ichildbox(:,:)
      integer,allocatable:: nblevel(:)
      integer,allocatable:: iboxlev(:)
      integer,allocatable:: istartlev(:)
      integer,allocatable:: leaflist(:)
      integer,allocatable:: newlist(:)
c     chebyshev matrices      
      real*8, allocatable::cftm(:,:), citm(:,:) 
      real*8, allocatable::spdef(:), spbx(:,:), spxb(:,:)
      real*8, allocatable::svlvr(:,:), sdvl(:,:), sdvr(:,:)
c     bc related data and such
      real*8, allocatable::chpts(:,:), xlength(:)
      real*8, allocatable::ui(:)
      real*8, allocatable::vl(:,:), vr(:,:)
      real*8, allocatable::fval(:,:,:)
      real*8, allocatable::phival(:,:,:,:)
c     inner products and coupling coeffs
      real*8, allocatable:: alist(:,:,:,:), dlist(:,:,:)
      real*8, allocatable:: clist(:,:)
c     solution related
      real*8, allocatable::sigma0(:,:,:), ul0(:,:,:)
      real*8, allocatable::sigma(:,:,:), eta(:,:,:)
      real*8, allocatable::xi(:,:,:,:)
      real*8, allocatable::quadl(:,:), quadr(:,:)
      real*8, allocatable::qloc(:,:)
      real*8, allocatable::ulsol(:,:,:), ulder(:,:,:)
      real*8, allocatable::smoni(:), upre(:,:)
c
c---------------------------
c     STEP 0: initialization
c---------------------------
c
c     1) allocate memory, increase nmax and maxlev
c        if necessary
      nmax=1000000
      maxlev=20
c
      allocate(levelbox(nmax))
      allocate(icolbox(nmax))
      allocate(iparentbox(nmax))
      allocate(ichildbox(2,nmax))
      allocate(nblevel(0:maxlev))
      allocate(iboxlev(nmax))
      allocate(istartlev(0:maxlev))
      allocate(leaflist(nmax))
      allocate(newlist(nmax))
c
      allocate(cftm(ndeg,ndeg))
      allocate(citm(ndeg,ndeg))
      allocate(spdef(ndeg))
      allocate(spbx(ndeg,ndeg))
      allocate(spxb(ndeg,ndeg))
c
      allocate(svlvr(ndeg*ndim,ndeg*ndim))
      allocate(sdvl(ndim,ndeg*ndim))
      allocate(sdvr(ndim,ndeg*ndim))
c
      allocate(chpts(ndeg,nmax))
      allocate(xlength(nmax))
      allocate(ui(ndim))
      allocate(vl(ndim,ndim))
      allocate(vr(ndim,ndim))
      allocate(fval(ndim,ndeg,nmax))
      allocate(phival(ndim,ndim,ndeg,nmax))
c
      allocate(alist(ndim,ndim,2,nmax))
      allocate(dlist(ndim,2,nmax))
      allocate(clist(ndim,nmax))
c
      allocate(sigma0(ndim,ndeg,nmax))
      allocate(ul0(ndim,ndeg,nmax))
      allocate(sigma(ndim,ndeg,nmax))
      allocate(eta(ndim,ndeg,nmax))
      allocate(xi(ndim,ndim,ndeg,nmax))
      allocate(quadl(ndim,nmax))
      allocate(quadr(ndim,nmax))
      allocate(qloc(ndim,ndeg))
c
      allocate(ulsol(ndim,ndeg,nmax))
      allocate(ulder(ndim,ndeg,nmax))
      allocate(smoni(nmax))
      allocate(upre(ndim,ntarg))
c     there are two types of ordering:
c     quadl, quadr, sigma, ulsol, ulder are stored
c     in the order of the (current) leaflist,
c     while others are stored in the order of the tree
c
c     prepare the chebyshev matrices
      xa=endpts(1)
      xb=endpts(nch+1)
      call setchmat(ndeg, cftm, citm, spdef, spbx, spxb)
c
c     choose the right Green's function and
c     inhomogeneous term, call ssetbc to compute 
c     the constants 
      call ssetbc(ndim, bca, bcb, bcv, ui, vl, vr)
c
c     don't forget to call subroutine formsmat here
      call formsmat(ndim, ndeg, spdef, spbx, spxb, vl,
     1              vr, sdvl, sdvr, svlvr)
c
c     2) create a quasi-uniform tree whose leaf nodes
c        corresponds to the subintervals
      nleaf=nch
      call getnumqutree(nleaf, nboxes, nlev)
c
      xsize0=xb-xa
      cent0=xa+xsize0/2.0d0
      call quasiunitree(levelbox, icolbox, nboxes, 
     1     maxid, nlev, iparentbox, ichildbox, 
     2     nblevel, iboxlev, istartlev, nleaf)
c
      call getleaflist(levelbox, icolbox, nboxes,
     1     maxid, nlev, iparentbox, ichildbox, 
     2     nblevel, iboxlev, istartlev, 
     3     nleaf, leaflist)
c
c     set the newlist to be the leaflist
      nnew=nleaf
      do i=1, nleaf
        newlist(i)=leaflist(i)
      enddo
c
c-------------------------------------------------
c
c     Newton iteration control
c     initialize (sigma0,ul0) (Q:how about ulder0?)
c     in each newton step, solve an integral equation
c
c     1. initialization -> (sigma0, ul0)
c     by calling the user specified subroutines:
c     yeval0(x,ul0,pars), 
c     ydeval0(x,sigma0,pars)
c     RMK: p0(x) =0 fixed in this code
c
      do i=1, nleaf
        ibox = leaflist(i)
        lev=levelbox(ibox)
        icol=icolbox(ibox)
c       set the cheby pts
        call mkgrid(lev, icol, xa, xb, ndeg,
     1         aa, bb, chpts(1,ibox))
c       call yeval0 and ydeval0 at cheby pts
        do k=1, ndeg
          xx=chpts(k,ibox)
          call yeval0(xx,ul0(1,k,ibox),pars)
          call ydeval0(xx,sigma0(1,k,ibox),pars)
c          write(301,*) xx, ul0(1,k,ibox), ul0(2,k,ibox),
c     1                     ul0(3,k,ibox)
c          write(302,*) xx, sigma0(1,k,ibox), sigma0(2,k,ibox),
c     1                     sigma0(3,k,ibox)
        enddo
      enddo
c
c     2. call rasysnwtn until err_rel < rtol
      err_rel = 100.0d0
      nstep = 0 
      do while((err_rel .gt. rtol).and.(nstep .lt. maxstep))
cccccc      do while((nstep .lt. maxstep))
        write(*,*) '*********************************'
        write(*,*) 'calling rasysnwtn:'
        call rasysnwtn(ndim, ndeg, levelbox, icolbox, 
     1    nboxes, maxid, nlev, iparentbox, ichildbox,
     2    nblevel, iboxlev, istartlev, cent0, xsize0, nmax,
     3    nleaf, leaflist, nnew, newlist, svlvr, sdvl, sdvr, 
     4    chpts, xlength, vl, vr, phival, fval, ui, alist, 
     5    dlist, clist, sigma, eta, xi, quadl, quadr, qloc, 
     6    feval, fyeval, pars, ntarg, xtarg, maxdiv, rtol,
     7    sigma0, ul0, ulsol, ulder, err_rel)
c
        nstep = nstep+1
        write(*,*) 'newton iteration step:', nstep
        write(*,*) 'err_rel=', err_rel
      enddo
c
c     3. interpolate the solution to the user-specified grid
c
      call sinterpf2(nlev, levelbox, iparentbox,
     1     ichildbox, icolbox, nboxes, maxid,
     2     nblevel, iboxlev, istartlev, cent0,
     3     xsize0, nleaf, leaflist, ndim, ndeg, 
     4     ulsol, ntarg, xtarg, ysol)
c
      call sinterpf2(nlev, levelbox, iparentbox,
     1     ichildbox, icolbox, nboxes, maxid,
     2     nblevel, iboxlev, istartlev, cent0,
     3     xsize0, nleaf, leaflist, ndim, ndeg, 
     4     ulder, ntarg, xtarg, yder)
c
c-------------------------------------------------
c
      deallocate(levelbox)
      deallocate(icolbox)
      deallocate(iparentbox)
      deallocate(ichildbox)
      deallocate(nblevel)
      deallocate(iboxlev)
      deallocate(istartlev)
      deallocate(leaflist)
      deallocate(newlist)
c
      deallocate(cftm)
      deallocate(citm)
      deallocate(spdef)
      deallocate(spbx)
      deallocate(spxb)
c
      deallocate(svlvr)
      deallocate(sdvl)
      deallocate(sdvr)
c
      deallocate(chpts)
      deallocate(xlength)
      deallocate(ui)
      deallocate(vl)
      deallocate(vr)
      deallocate(fval)
      deallocate(phival)
c
      deallocate(alist)
      deallocate(dlist)
      deallocate(clist)
c
      deallocate(sigma)
      deallocate(eta)
      deallocate(xi)
      deallocate(quadl)
      deallocate(quadr)
      deallocate(qloc)
c
      deallocate(ulsol)
      deallocate(ulder)
      deallocate(smoni)
      deallocate(upre)

c
      end subroutine 






c--------------------------------------------------
c     subroutine rasysmain
c--------------------------------------------------
c
c     This subroutine does the main work of the
c     adaptive integral equation solver
c     (for the system's case)
c
c     input: 
c     the old tree
c     omega and g in the integral equation (sampled on the tree)
c     solution sigma of the last step (=0 by default)
c
c     output:
c     the new tree
c     sigma: solution sigma of the current step (of the integral equation)
c     ulsol, ulder:solution u of the bvp, on the new tree
c     err_rel: relative l2 error of sigma when compared to the last step
c     
c     something like that
c
c     feval, peval: leave them here for the moment, get rid of them later
c     sigma0, u0: solution of the integral equation and the bvp
c                 of the last newton step, =0 if used without newton
c                 sampled on the old tree
c
c--------------------------------------------------
c
      subroutine rasysmain(ndim, ndeg, levelbox, icolbox, 
     1    nboxes, maxid, nlev, iparentbox, ichildbox,
     2    nblevel, iboxlev, istartlev, cent0, xsize0,
     3    nmax, nleaf, leaflist, nnew, newlist, svlvr, sdvl, 
     4    sdvr, chpts, xlength, vl, vr, phival, fval, 
     5    ui, alist, dlist, clist, sigma, eta, xi, quadl, 
     6    quadr, qloc, peval, feval, pars, ntarg, xtarg, 
     7    maxdiv, rtol, usol, uder)
      implicit real*8 (a-h,o-z)
      integer ndim, ndeg, ntarg, maxdiv
c     binary tree and leaflist
      integer nboxes, nlev, nmax, nleaf, nnew, maxid
      integer levelbox(nboxes), icolbox(nboxes)
      integer iparentbox(nboxes), ichildbox(2,nboxes)
      integer nblevel(0:nlev)
      integer iboxlev(nboxes), istartlev(0:nlev)
      integer leaflist(nleaf), newlist(nnew)
      external feval, peval
c     const matrices
      real*8 ui(ndim)
      real*8 vl(ndim,ndim), vr(ndim,ndim)
c     chebyshev matrices
      real*8 svlvr(ndeg*ndim,ndeg*ndim)
      real*8 sdvl(ndim,ndeg*ndim), sdvr(ndim,ndeg*ndim)
c     data on leaf nodes
      real*8 chpts(ndeg,nleaf), xlength(nleaf)
      real*8 fval(ndim,ndeg,nleaf)
      real*8 phival(ndim,ndim,ndeg,nleaf)
c     inner products and coupling coeffs
      real*8 alist(ndim,ndim,2,nboxes)
      real*8 dlist(ndim,2,nboxes)
      real*8 clist(ndim,nboxes)
c     solution related
      real*8 rtol, pars(1), xsize0, cent0
      real*8 sigma(ndim,ndeg,nboxes)
      real*8 eta(ndim,ndeg,nboxes)
      real*8 xi(ndim,ndim,ndeg,nboxes)
      real*8 quadl(ndim,nleaf), quadr(ndim,nleaf)
      real*8 qloc(ndim,ndeg)
      real*8 xtarg(ntarg)
      real*8 usol(ndim,ntarg), uder(ndim,ntarg)
c     allocatables
      real*8, allocatable::smoni(:,:), upre(:,:), sdiv(:)
      real*8, allocatable:: ulsol(:,:,:), ulder(:,:,:)
c
      allocate(sdiv(ndim))
      allocate(smoni(ndim,nmax))
      allocate(upre(ndim,ntarg))
      allocate(ulsol(ndim,ndeg,nmax))
      allocate(ulder(ndim,ndeg,nmax))
c
c     initialization 
c
c     set the newlist to be the leaflist
      nnew=nleaf
      do i=1, nleaf
        newlist(i)=leaflist(i)
      enddo
c
c     initialize the solution at the previous time step
c     to be zero
      do i=1, ntarg
      do ii=1, ndim
        upre(ii,i)=0.0d0
      enddo
      enddo
c
c-------------------------------------------------
c
      xa=cent0-xsize0/2.0d0
      xb=cent0+xsize0/2.0d0
c
      nstep=0
      idbled=0
      do while((nnew .gt. 0) .and. (nstep .le. maxdiv))
c       adapt control: newlist is nonempty, and
c       the number of adaptive refinement and coarsening
c       hasn't exceeded maxdiv. Do the following steps:
c
        nstep=nstep+1     
        write(*,*) ''
        write(*,*) '****** nstep=', nstep
        write(*,*) '****** nboxes=', nboxes
        write(*,*) '****** nnew=', nnew
c -------------------------------
c       STEP 1: local solve, for nodes in the newlist
c -------------------------------
c        write(*,*) 'setting up chebyshev nodes' 
c        write(*,*) 'doing the local solve'
        do ii=1, nnew
          ibox=newlist(ii)
          lev=levelbox(ibox)
          icol=icolbox(ibox)
c         set the cheby pts
          call mkgrid(lev, icol, xa, xb, ndeg,
     1         aa, bb, chpts(1,ibox))
          xlength(ibox)=bb-aa
c          write(456,*) aa, 0.0d0
c           attention!!! xa, xb uninitialized here!
c        
c         compute the green's function and 
c         related function values on this node
          do i=1, ndeg
            call sbctab(ndim,chpts(i,ibox),ui,fval(1,i,ibox),
     1           phival(1,1,i,ibox),peval,feval,pars)
cccccc      stop here to test:
cccccc      in the linear case, the subroutines sbctab and sbcnwtn should return
cccccc      exactly the same output
c            do kk=1, ndim
c              write(41,*) kk,i,ibox,fval(kk,i,ibox)
c              do kkk=1, ndim
c                write(42,*) kk,i,ibox,phival(kkk,kk,i,ibox)
c              enddo
c            enddo
cccccc
          enddo
c
c         now do the local solve
          call slocsolve(ndim, ndeg, svlvr, phival(1,1,1,ibox), 
     1         fval(1,1,ibox), xlength(ibox), eta(1,1,ibox), 
     2         xi(1,1,1,ibox))
c
c         eval inner products alist and dlist
          call smkprod(ndim, ndeg, xlength(ibox), eta(1,1,ibox), 
     1         xi(1,1,1,ibox), sdvl, sdvr, alist(1,1,1,ibox), 
     2         alist(1,1,2,ibox), dlist(1,1,ibox), dlist(1,2,ibox))
        enddo
c
        nnew=0
c       remove the processed nodes from the newlist
c
c -------------------------------
c     STEP 2A: an upward sweep to compute inner products
c -------------------------------
        write(*,*) 'upward sweep'
        do l=nlev,0,-1
          istart=istartlev(l)
          iend=istart+nblevel(l)-1
          do ii=istart,iend
            ibox=iboxlev(ii)
            if(ichildbox(1,ibox) .gt.0 ) then
              icha=ichildbox(1,ibox)
              ichb=ichildbox(2,ibox)
              call schd2par(ndim, ibox, icha, ichb, maxid,
     1             alist, dlist)
            endif
          enddo
        enddo
c
c -------------------------------
c     STEP 2B: a downward sweep to compute 
c              the coupling coeffs
c -------------------------------
        write(*,*) 'downward sweep'
        l=0
        iroot=istartlev(l) 
        do i=1, ndim
          clist(i,iroot)=0.0d0
        enddo
c
        do l=0,nlev
          istart=istartlev(l)
          iend=istart+nblevel(l)-1
          do ii=istart,iend
            ibox=iboxlev(ii)
            if(ichildbox(1,ibox) .gt. 0) then
              icha=ichildbox(1,ibox)
              ichb=ichildbox(2,ibox)
              call spar2chd(ndim, ibox, icha, ichb, maxid,
     1           alist, dlist, clist)
            endif
          enddo
        enddo
c
c -------------------------------
c       STEP 2C: recover the solution of
c            the global integral equation 
c -------------------------------
        write(*,*) 'evaluating the sol of the bie'
        do ii=1, nleaf
          ibox=leaflist(ii)
          call sevaldens(ndim, ndeg, clist(1,ibox), eta(1,1,ibox),
     1       xi(1,1,1,ibox), sigma(1,1,ibox))
        enddo
c
c -------------------------------
c       STEP 3: recover the solution of the
c       ODE and its derivative
c -------------------------------
c
        write(*,*) 'precomputing local quads'
        call sprecompq(ndim, nleaf, leaflist, alist, dlist, 
     1     clist, quadl, quadr)
c
        do ii=1, nleaf
          ibox=leaflist(ii)
          call squadloc(ndim, ndeg, xlength(ibox), sigma(1,1,ibox), 
     1       svlvr, qloc)
c          
c         go through the leaflist to compute the 
c         local integrals qlocl and qlocr 
c
          do k=1, ndeg
            do j=1, ndim
c              ulsol(j,k,ii)=qloc(j,k)+quadl(j,ii)+quadr(j,ii)+ui(j)
c              ulder(j,k,ii)=0.0d0
              ulsol(j,k,ibox)=qloc(j,k)+quadl(j,ii)+quadr(j,ii)+ui(j)
              ulder(j,k,ibox)=0.0d0
              do jj=1, ndim
c                ulder(j,k,ii)=ulder(j,k,ii)+(vl(j,jj)-vr(j,jj))
c     1                                   *sigma(jj,k,ii)
                ulder(j,k,ibox)=ulder(j,k,ibox)+(vl(j,jj)-vr(j,jj))
     1                                   *sigma(jj,k,ibox)
              enddo
            enddo
          enddo
c         recover the solution and its der on
c         the leaf nodes of the tree: 
c         ulsol, ulder
        enddo
c
c       now interpolate to the user-specified grid
c       by calling sinterpf
c       (now we use a modified version sinterpf2)
c
cccccccccc
c       print out ulsol for debugging 
c        do ii=1, nleaf
c          ibox=leaflist(ii)
c          do k=1, ndeg
c          do j=1, ndim
c            write(302,*) ulsol(j,k,ibox), j,k,ibox,nstep
c          enddo
c          enddo
c        enddo
cccccccccc
c
        write(*,*) 'calling sinterpf2'
        write(*,*) 'cent0=', cent0
        write(*,*) 'xsize0=', xsize0
        call sinterpf2(nlev, levelbox, iparentbox,
     1       ichildbox, icolbox, nboxes, maxid,
     2       nblevel, iboxlev, istartlev, cent0,
     3       xsize0, nleaf, leaflist, ndim, ndeg, 
     4       ulsol, ntarg, xtarg, usol)
c
        call sinterpf2(nlev, levelbox, iparentbox,
     1       ichildbox, icolbox, nboxes, maxid,
     2       nblevel, iboxlev, istartlev, cent0,
     3       xsize0, nleaf, leaflist, ndim, ndeg, 
     4       ulder, ntarg, xtarg, uder)

c
c -------------------------------
c       STEP 4: the adaptivity control
c -------------------------------
c
        etest=0.0d0
        um2=0.0d0
        up2=0.0d0
        do i=1, ntarg
        do j=1, ndim
          um2=um2+abs(upre(j,i)-usol(j,i))**2
          up2=up2+abs(upre(j,i)+usol(j,i))**2
c          write(301,*) upre(j,i), usol(j,i), j, i, nstep, xtarg(i)
        enddo
        enddo
        etest=sqrt(um2/up2)
c       etest: l2 norm of the error
c
        write(*,*) 'etest=',etest
        write(*,*) 'rtol=',rtol
c
c       update: upre = usol
        do i=1, ntarg
        do j=1, ndim
          upre(j,i)=usol(j,i)
        enddo
        enddo
c
c-------------------------------------
c
        if(etest .gt. rtol) then
c         not convergent yet, do the following:
          write(*,*) 'not convergent yet'
c
c         1) call sgetmonitors (in bvprouts.f)
          call sgetmonitors(ndim, ndeg, nleaf, 
     1         leaflist, sigma, smoni, sdiv)
          write(*,*) 'sdiv='
          do k=1, ndim
            write(*,*) sdiv(k)
          enddo
c
c         2) do the thing as in testadapt2.f,
c            remember to add nodes to newlist
c-----------------------------------------------------
c
          do i=1, nleaf
            ibox=leaflist(i)
c           set idiv to 0, if there is a k
c           s.t. smoni(k,ibox)>sdiv(k)
c           then subdivide
c            write(*,*) 'smoni:'
            idiv=0
            do k=1, ndim
c              write(*,*) ibox, smoni(k,i), sdiv(k)
              if(smoni(k,i) .gt. sdiv(k)) then
                idiv = 1
              endif
            enddo
c
            if(idiv .eq. 1) then
c             subdivide ibox and add its children
c             into the newlist
              iparbox=ibox
c              write(*,*) 'subdividing box', iparbox
              call subdivide(iparbox, levelbox, icolbox, 
     1             nboxes, maxid, nlev, iparentbox, 
     2             ichildbox, nblevel, iboxlev, istartlev)
c
              do ic=1, 2
                nnew=nnew+1
                newlist(nnew)=ichildbox(ic,ibox)
              enddo
            elseif(i .lt. nleaf) then
              irt=leaflist(i+1)
              call isibling(ibox, irt, iparentbox, isib)
              if(isib .gt. 0) then
                imerge = 1
              else
                imerge = 0
              endif
c             if siblings, attempt to merge
c             but not if ...
              do k=1, ndim
                ski=smoni(k,i)
                skr=smoni(k,i+1)
                ssumk=ski+skr
                if (ssumk .ge. sdiv(k)/2**8) then
                  imerge = 0
                endif
              enddo
c
c             now we've got imerge right
              if(imerge .eq. 1) then
                iparbox=iparentbox(ibox)
                call mergechd(iparbox,levelbox,icolbox, 
     1               nboxes,maxid,nlev,iparentbox, 
     2               ichildbox,nblevel,iboxlev,istartlev)
c               add the parent box, which newly becomes
c               a leaf box, into the newlist
                nnew=nnew+1
                newlist(nnew)=iparbox
              endif
            endif
          enddo
c
c-----------------------------------------------------
c
          idbled=0              
c          
        elseif(idbled .le. 0) then
c         converged, double the grid just to be sure
          do i=1, nleaf
            ibox=leaflist(i)
c           subdivide each leaf box
            call subdivide(ibox, levelbox, icolbox, 
     1           nboxes, maxid, nlev, iparentbox, 
     2           ichildbox, nblevel, iboxlev, istartlev)
c
c           add the new leaf nodes into the newlist
            do ic=1, 2
              nnew=nnew+1
              newlist(nnew)=ichildbox(ic,ibox)
            enddo 
          enddo
c
          idbled=1
        endif
c
        write(*,*) 'converged at nstep=',nstep
c
c-------------------------------------
c
c       end of this round of adaptive refinement 
c       and coarsening
c
c       call getleaflist to get the current leaves
c       after everything
c        write(*,*) 'getting leaf nodes'
        call getleaflist(levelbox, icolbox, nboxes,
     1       maxid, nlev, iparentbox, ichildbox, 
     2       nblevel, iboxlev, istartlev, 
     3       nleaf, leaflist)
c
      enddo
c
c-------------------------------------------------
c
c     6) deallocate memory
c
c
      deallocate(smoni)
      deallocate(upre)
      deallocate(ulsol)
      deallocate(ulder)

      end subroutine





c--------------------------------------------------
c     subroutine rasysnwtn
c--------------------------------------------------
c
c     This subroutine does the main work of one newton
c     step of the nonlinear solver: 
c     it is the adaptive integral equation solver
c     (for the system's case) wrapped into a slightly
c     different interface
c
c
c     input: 
c     the old tree
c     omega and g in the integral equation (sampled on the tree)
c     solution sigma of the last step (=0 by default)
c
c     output:
c     the new tree
c     sigma: solution sigma of the current step (of the integral equation)
c     ulsol, ulder:solution u of the bvp, on the new tree
c     err_rel: relative l2 error of sigma when compared to the last step
c     
c     something like that
c
ccccccccccccccccccccccccccc
c
c     feval: subroutine that evaluates f(x,y) in the ode
c     fyeval: subroutine that evaluates df(d,y)/dy in the ode
c
c     sigma0, u0: solution of the integral equation and the bvp
c                 of the last newton step, sampled on the old tree
c
c--------------------------------------------------
c
      subroutine rasysnwtn(ndim, ndeg, levelbox, icolbox, 
     1    nboxes, maxid, nlev, iparentbox, ichildbox,
     2    nblevel, iboxlev, istartlev, cent0, xsize0, nmax,
     3    nleaf, leaflist, nnew, newlist, svlvr, sdvl, sdvr, 
     4    chpts, xlength, vl, vr, phival, fval, ui, alist, 
     5    dlist, clist, sigma, eta, xi, quadl, quadr, qloc, 
     6    feval, fyeval, pars, ntarg, xtarg, maxdiv, rtol,
     7    sigma0, ul0, ulsol, ulder, err_rel)
      implicit real*8 (a-h,o-z)
      integer ndim, ndeg, ntarg, maxdiv
c     binary tree and leaflist
      integer nboxes, nlev, nmax, nleaf, nnew, maxid
      integer levelbox(nmax), icolbox(nmax)
      integer iparentbox(nmax), ichildbox(2,nmax)
      integer nblevel(0:nlev)
      integer iboxlev(nmax), istartlev(0:nlev)
      integer leaflist(nmax), newlist(nmax)
      external feval, fyeval
c     const matrices
      real*8 ui(ndim)
      real*8 vl(ndim,ndim), vr(ndim,ndim)
c     chebyshev matrices
      real*8 svlvr(ndeg*ndim,ndeg*ndim)
      real*8 sdvl(ndim,ndeg*ndim), sdvr(ndim,ndeg*ndim)
c     data on leaf nodes
      real*8 chpts(ndeg,nmax), xlength(nmax)
      real*8 fval(ndim,ndeg,nmax)
      real*8 phival(ndim,ndim,ndeg,nmax)
c     inner products and coupling coeffs
      real*8 alist(ndim,ndim,2,nmax)
      real*8 dlist(ndim,2,nmax)
      real*8 clist(ndim,nmax)
c     solution related
      real*8 rtol, pars(1), xsize0, cent0
      real*8 sigma0(ndim,ndeg,nmax)
      real*8 ul0(ndim,ndeg,nmax)
      real*8 sigma(ndim,ndeg,nmax)
      real*8 eta(ndim,ndeg,nmax)
      real*8 xi(ndim,ndim,ndeg,nmax)
      real*8 quadl(ndim,nmax), quadr(ndim,nmax)
      real*8 qloc(ndim,ndeg)
      real*8 xtarg(ntarg)
      real*8 ulsol(ndim,ndeg,nmax), ulder(ndim,ndeg,nmax)
c     allocatables
      real*8, allocatable::smoni(:), upre(:,:)
      real*8, allocatable::usol(:,:), uder(:,:)
      real*8, allocatable::signew(:,:,:)
c
      allocate(smoni(nmax))
      allocate(upre(ndim,ntarg))
      allocate(usol(ndim,ntarg))
      allocate(uder(ndim,ntarg))
      allocate(signew(ndim,ndeg,nmax))
c
c-------------------------------------------------
c
c here begins the bulk of the subroutine:
c completely the same as rasysmain, except that we replace
c subroutine call of sbctab by that of sbcnwtn, 
c which takes feval and fyeval and sets the bc related info
c accordingly (to the right linearized integral equation)
c
c
c     initialization 
c
c     set the newlist to be the leaflist
      nnew=nleaf
      do i=1, nleaf
        newlist(i)=leaflist(i)
      enddo
c
c     initialize the solution at the previous time step
c     to be zero
      do i=1, ntarg
      do ii=1, ndim
        upre(ii,i)=0.0d0
      enddo
      enddo
c
c-------------------------------------------------
c
      xa=cent0-xsize0/2.0d0
      xb=cent0+xsize0/2.0d0
c
      nstep=0
      idbled=0
      do while((nnew .gt. 0) .and. (nstep .le. maxdiv))
c       adapt control: newlist is nonempty, and
c       the number of adaptive refinement and coarsening
c       hasn't exceeded maxdiv. Do the following steps:
c
        nstep=nstep+1     
        write(*,*) ''
        write(*,*) '****** nstep=', nstep
        write(*,*) '****** nboxes=', nboxes
        write(*,*) '****** nnew=', nnew
c -------------------------------
c       STEP 1: local solve, for nodes in the newlist
c -------------------------------
c        write(*,*) 'setting up chebyshev nodes' 
c        write(*,*) 'doing the local solve'
        do ii=1, nnew
          ibox=newlist(ii)
          lev=levelbox(ibox)
          icol=icolbox(ibox)
c         set the cheby pts
          call mkgrid(lev, icol, xa, xb, ndeg,
     1         aa, bb, chpts(1,ibox))
          xlength(ibox)=bb-aa
c        
c         compute the green's function and 
c         related function values on this node
          do i=1, ndeg
c            call sbctab(ndim,chpts(i,ibox),ui,fval(1,i,ibox),
c     1           phival(1,1,i,ibox),peval,feval,pars)
            call sbcnwtn(ndim,chpts(i,ibox),ui,sigma0(1,i,ibox),
     1           ul0(1,i,ibox),fval(1,i,ibox),phival(1,1,i,ibox),
     2           feval,fyeval,pars)
          enddo
c
c         now do the local solve
          call slocsolve(ndim, ndeg, svlvr, phival(1,1,1,ibox), 
     1         fval(1,1,ibox), xlength(ibox), eta(1,1,ibox), 
     2         xi(1,1,1,ibox))
c
c         eval inner products alist and dlist
          call smkprod(ndim, ndeg, xlength(ibox), eta(1,1,ibox), 
     1         xi(1,1,1,ibox), sdvl, sdvr, alist(1,1,1,ibox), 
     2         alist(1,1,2,ibox), dlist(1,1,ibox), dlist(1,2,ibox))
        enddo
c
        nnew=0
c       remove the processed nodes from the newlist
c
c -------------------------------
c     STEP 2A: an upward sweep to compute inner products
c -------------------------------
        write(*,*) 'upward sweep'
        do l=nlev,0,-1
          istart=istartlev(l)
          iend=istart+nblevel(l)-1
          do ii=istart,iend
            ibox=iboxlev(ii)
            if(ichildbox(1,ibox) .gt.0 ) then
              icha=ichildbox(1,ibox)
              ichb=ichildbox(2,ibox)
              call schd2par(ndim, ibox, icha, ichb, maxid,
     1             alist, dlist)
            endif
          enddo
        enddo
c
c -------------------------------
c     STEP 2B: a downward sweep to compute 
c              the coupling coeffs
c -------------------------------
        write(*,*) 'downward sweep'
        l=0
        iroot=istartlev(l) 
        do i=1, ndim
          clist(i,iroot)=0.0d0
        enddo
c
        do l=0,nlev
          istart=istartlev(l)
          iend=istart+nblevel(l)-1
          do ii=istart,iend
            ibox=iboxlev(ii)
            if(ichildbox(1,ibox) .gt. 0) then
              icha=ichildbox(1,ibox)
              ichb=ichildbox(2,ibox)
              call spar2chd(ndim, ibox, icha, ichb, maxid,
     1           alist, dlist, clist)
            endif
          enddo
        enddo
c
c -------------------------------
c       STEP 2C: recover the solution of
c            the global integral equation 
c -------------------------------
        write(*,*) 'evaluating the sol of the bie'
        do ii=1, nleaf
          ibox=leaflist(ii)
          call sevaldens(ndim, ndeg, clist(1,ibox), eta(1,1,ibox),
     1       xi(1,1,1,ibox), sigma(1,1,ibox))
        enddo
c
c -------------------------------
c       STEP 3: recover the solution of the
c       ODE and its derivative
c -------------------------------
c
        write(*,*) 'precomputing local quads'
        call sprecompq(ndim, nleaf, leaflist, alist, dlist, 
     1     clist, quadl, quadr)
c
        do ii=1, nleaf
          ibox=leaflist(ii)
          call squadloc(ndim, ndeg, xlength(ibox), sigma(1,1,ibox), 
     1       svlvr, qloc)
c
c         go through the leaflist to compute the 
c         local integrals qlocl and qlocr 
c
          do k=1, ndeg
            do j=1, ndim
              ulsol(j,k,ibox)=qloc(j,k)+quadl(j,ii)+quadr(j,ii)+ui(j)
              ulder(j,k,ibox)=0.0d0
              do jj=1, ndim
                ulder(j,k,ibox)=ulder(j,k,ii)+(vl(j,jj)-vr(j,jj))
     1                                   *sigma(jj,k,ibox)
c
              enddo
            enddo
          enddo
c         recover the solution and its der on
c         the leaf nodes of the tree: 
c         ulsol, ulder
        enddo
c
c       now interpolate to the user-specified grid
c       by calling sinterpf
c       (now we use a modified version sinterpf2)
c
        write(*,*) 'calling sinterpf2'
        call sinterpf2(nlev, levelbox, iparentbox,
     1       ichildbox, icolbox, nboxes, maxid,
     2       nblevel, iboxlev, istartlev, cent0,
     3       xsize0, nleaf, leaflist, ndim, ndeg, 
     4       ulsol, ntarg, xtarg, usol)
c
        call sinterpf2(nlev, levelbox, iparentbox,
     1       ichildbox, icolbox, nboxes, maxid,
     2       nblevel, iboxlev, istartlev, cent0,
     3       xsize0, nleaf, leaflist, ndim, ndeg, 
     4       ulder, ntarg, xtarg, uder)
c
c -------------------------------
c       STEP 4: the adaptivity control
c -------------------------------
c
        etest=0.0d0
        um2=0.0d0
        up2=0.0d0
        do i=1, ntarg
        do j=1, ndim
          um2=um2+abs(upre(j,i)-usol(j,i))**2
          up2=up2+abs(upre(j,i)+usol(j,i))**2
        enddo
        enddo
        etest=sqrt(um2/up2)
c       etest: l2 norm of the error
c
        write(*,*) 'etest=',etest
        write(*,*) 'rtol=',rtol
c
c       update: upre = usol
        do i=1, ntarg
        do j=1, ndim
          upre(j,i)=usol(j,i)
        enddo
        enddo
c
c-------------------------------------
c
        if(etest .gt. rtol) then
c         not convergent yet, do the following:
          write(*,*) 'not convergent yet'
c
c         1) call sgetmonitors (in bvprouts.f)
          call sgetmonitors0(ndim, ndeg, nleaf, sigma, smoni,
     1         sdiv)
          write(*,*) 'sdiv=',sdiv
c
c         2) do the thing as in testadapt2.f,
c            remember to add nodes to newlist
          do i=1, nleaf
            ibox=leaflist(i)
            si=smoni(i)
            if(si .gt. sdiv) then
c             subdivide ibox and add its children
c             into the newlist
              iparbox=ibox
c              write(*,*) 'subdividing box', iparbox
              call subdivide(iparbox, levelbox, icolbox, 
     1             nboxes, maxid, nlev, iparentbox, 
     2             ichildbox, nblevel, iboxlev, istartlev)
c              write(*,*) iparbox, 'subdivided'
c              remember to interpolate sigma0 and ul0 to the 
c              newly generated leaf nodes!
              ikid1=ichildbox(1,ibox)
              ikid2=ichildbox(2,ibox)
c              write(*,*) 'calling schebyp2k'
              call schebyp2k(ndim, ndeg, sigma0(1,1,ibox),
     1             sigma0(1,1,ikid1), sigma0(1,1,ikid2))
              call schebyp2k(ndim, ndeg, ul0(1,1,ibox),
     1             ul0(1,1,ikid1), ul0(1,1,ikid2))
c
              do ic=1, 2
                nnew=nnew+1
                newlist(nnew)=ichildbox(ic,ibox)
              enddo
            elseif(i .lt. nleaf) then
              irt=leaflist(i+1)
              sr=smoni(i+1)
              call isibling(ibox, irt, iparentbox, isib)
              ssum=si+sr
              if((ssum .lt. sdiv/2**8)
cccccc              if((ssum .lt. sdiv*2.0d0)
     1           .and.(isib .gt. 0)) then
c               overresolved, merge siblings
                iparbox=iparentbox(ibox)
                ikid1=ichildbox(1,iparbox)
                ikid2=ichildbox(2,iparbox)
c                write(*,*) 'merging kids of box', iparbox
c
c               remember to interpolate sigma0 and ul0 to the 
c               newly generated leaf nodes --
c               this time we have to do it before the merging
c
c                write(*,*) 'calling schebyk2p'
c                write(*,*) iparbox, ikid1, ikid2, nmax
                call schebyk2p(ndim, ndeg, sigma0(1,1,ikid1),
     1               sigma0(1,1,ikid2), sigma0(1,1,iparbox))
                call schebyk2p(ndim, ndeg, ul0(1,1,ikid1),
     1               ul0(1,1,ikid2), ul0(1,1,iparbox))
c                write(*,*) 'done calling schebyk2p'
                call mergechd(iparbox,levelbox,icolbox, 
     1               nboxes,maxid,nlev,iparentbox, 
     2               ichildbox,nblevel,iboxlev,istartlev)
c               add the parent box, which newly becomes
c               a leaf box, into the newlist
                nnew=nnew+1
                newlist(nnew)=iparbox
              endif
            endif
          enddo
c
          idbled=0              
c          
        elseif(idbled .le. 0) then
c         converged, double the grid just to be sure
          do i=1, nleaf
            ibox=leaflist(i)
c           subdivide each leaf box
c            write(*,*) 'doubling, subdivide box', ibox
            call subdivide(ibox, levelbox, icolbox, 
     1           nboxes, maxid, nlev, iparentbox, 
     2           ichildbox, nblevel, iboxlev, istartlev)
c
              ikid1=ichildbox(1,ibox)
              ikid2=ichildbox(2,ibox)
c              write(*,*) 'calling schebyp2k'
              call schebyp2k(ndim, ndeg, sigma0(1,1,ibox),
     1             sigma0(1,1,ikid1), sigma0(1,1,ikid2))
              call schebyp2k(ndim, ndeg, ul0(1,1,ibox),
     1             ul0(1,1,ikid1), ul0(1,1,ikid2))
c
c           add the new leaf nodes into the newlist
            do ic=1, 2
              nnew=nnew+1
              newlist(nnew)=ichildbox(ic,ibox)
            enddo 
          enddo
c
          idbled=1
        endif
c
        write(*,*) 'converged at nstep=',nstep
c
c-------------------------------------
c
c       end of this round of adaptive refinement 
c       and coarsening
c
c       call getleaflist to get the current leaves
c       after everything
        write(*,*) 'getting leaf nodes'
        call getleaflist(levelbox, icolbox, nboxes,
     1       maxid, nlev, iparentbox, ichildbox, 
     2       nblevel, iboxlev, istartlev, 
     3       nleaf, leaflist)
c
c        write(*,*) nleaf, nmax
      enddo
c
c
c-------------------------------------------------
c
c     integral equation solved for the increment of
c     the solutionm, now get the solution of the current step
c
c     postprocessing:
c
c     1. get err_rel: relative l2 error in usol
c        (for convenience, here we use the discrete l2 norm)
c
c     2. add sigma0 to sigma (which is really delta_sigma)
c        to get the new sigma
c
c     sigma = sigma0 + dsigma
c     ulsol = G * sigma = G * sigma0 + G * dsigma 
c          = ul0 + G * dsigma
c
c     i.e. sigma = sigma0 + sigma
c          ulsol  = ul0 + ulsol
c
c     so, 1 + 2:
c     nd2 = norm(sigma, 2)
c     nold2 = norm(sigma0, 2) 
c     sigma = sigma + sigma0
c     ulsol  = ul0 + ulsol
c
      write(*,*) 'postprocessing'
      sumd2=0.0d0
      sumold2=0.0d0
      do i=1, nleaf
        ibox=leaflist(i)
        do k=1, ndeg
        do j=1, ndim
          sumd2=sumd2+sigma(j,k,ibox)**2
          sumold2=sumold2+sigma0(j,k,ibox)**2
        enddo
        enddo
      enddo      
c      
      write(*,*) 'sumd2=',sumd2
      write(*,*) 'sumold2=',sumold2
      if(abs(sumold2) .gt. 1e-10) then
        err_rel=sqrt(sumd2/sumold2)
      else
        err_rel=sqrt(sumd2)
      endif
c     L2 norm of the change in sigma evaluated
c
      do i=1, nleaf
        ibox=leaflist(i)
        do k=1, ndeg
        do j=1, ndim
          sigma(j,k,ibox)=sigma(j,k,ibox)+sigma0(j,k,ibox)
          ulsol(j,k,ibox)=ulsol(j,k,ibox)+ul0(j,k,ibox)
        enddo
        enddo
      enddo 
c     solution in sigma and u updated
c
c-------------------------------------------------
c
c       now remember to set the new initial
c       values of sigma and ulsol for the
c       next iteration:
        do i=1, nleaf
          ibox=leaflist(i)
          do k=1, ndeg
          do j=1, ndim
            sigma0(j,k,ibox)=sigma(j,k,ibox)
            ul0(j,k,ibox)=ulsol(j,k,ibox)-ui(j)
c           attention: the iteration is done for the homogeneous problem
c           without the ui term
c           we need to subtract it, otherwise we need to add it back when
c           the newton iteration stops which is not structurally ideal
          enddo
          enddo
        enddo 
c
c     6) deallocate memory
c
c
      deallocate(smoni)
      deallocate(upre)
      deallocate(usol)
      deallocate(uder)
      deallocate(signew)


      end subroutine





c--------------------------------------------------
c     subroutine rsysadap
c--------------------------------------------------
c
c     Real BVP system solver, adaptive version
c
c     this subroutine solves the two point BVP
c     (1) - (2):
c
c     u'(x)+ p(x)*u(x)  = f(x) 
c              (x \in [xa, xb])    (1)
c     bca*u(xa) + bcb*u(xb) = bcv  (2)
c
c     where u: [xa,xb] -> R^{ndim} is in C1
c           p: [xa,xb] -> R^{ndim x ndim} is continuous
c           f: [xa,xb] -> R^{ndim} is continuous
c
c           bca, bcb \in R^{ndim x ndim}
c           bcv \in R^{ndim}
c
c
c     INPUT:
c     ndim: dimension of the system
c     ndeg: degree of Chebyshev approx on each subinterval
c     nch: number of subintervals of the initial grid.
c     endpts: real array of length nch+1, endpoints of 
c             subintervals of the initial grid.
c             endpts(1)=xa, endpts(nch+1)=xb.
c     bc**: boundary conditions, as in (2)
c     peval, feval: function evaluation subroutines for
c            p, and f in (1)
c            which have the calling sequence:
c            peval(x,p,pars), feval(x,f,pars)
c            where x is the eval point, p/f is the value
c            returned, and pars is the list of parameters
c     maxdiv: maximum rounds of subdivisions and mergings
c             allowed
c     rtol: error tolerance
c     ntarg: length of xtarg
c     xtarg: user specified grid where the solution is needed
c    
c     OUTPUT:
c     usol: the solution u evaluated at xtarg
c     uder: the derivative of u evaluated at xtarg
c
c     RMK: right now the refinement is doing well, but the
c     coarsening seems... not enough
c
c     RMK: a new version calling rasysmain
c
c--------------------------------------------------
c
      subroutine rsysadap(ndim, ndeg, nch, endpts,
     1           bca, bcb, bcv, peval, feval, pars, 
     2           maxdiv, rtol, ntarg, xtarg, usol, uder)
      implicit real*8 (a-h,o-z)
      integer ndim, ndeg, nch, ntarg
      real*8 endpts(nch+1), xtarg(ntarg)
      real*8 bca(ndim,ndim), bcb(ndim,ndim), bcv(ndim)
      real*8 pars(1), usol(ndim,ntarg), uder(ndim,ntarg)
      external peval, feval
c     local vars (allocatable)
c     binary tree and leaflist
      integer nboxes, maxid, nlev, nleaf
      integer,allocatable:: levelbox(:)
      integer,allocatable:: icolbox(:) 
      integer,allocatable:: iparentbox(:)
      integer,allocatable:: ichildbox(:,:)
      integer,allocatable:: nblevel(:)
      integer,allocatable:: iboxlev(:)
      integer,allocatable:: istartlev(:)
      integer,allocatable:: leaflist(:)
      integer,allocatable:: newlist(:)
c     chebyshev matrices      
      real*8, allocatable::cftm(:,:), citm(:,:) 
      real*8, allocatable::spdef(:), spbx(:,:), spxb(:,:)
      real*8, allocatable::svlvr(:,:), sdvl(:,:), sdvr(:,:)
c     bc related data and such
      real*8, allocatable::chpts(:,:), xlength(:)
      real*8, allocatable::ui(:)
      real*8, allocatable::vl(:,:), vr(:,:)
      real*8, allocatable::fval(:,:,:)
      real*8, allocatable::phival(:,:,:,:)
c     inner products and coupling coeffs
      real*8, allocatable:: alist(:,:,:,:), dlist(:,:,:)
      real*8, allocatable:: clist(:,:)
c     solution related
      real*8, allocatable::sigma(:,:,:), eta(:,:,:)
      real*8, allocatable::xi(:,:,:,:)
      real*8, allocatable::quadl(:,:), quadr(:,:)
      real*8, allocatable::qloc(:,:)
      real*8, allocatable::ulsol(:,:,:), ulder(:,:,:)
      real*8, allocatable::smoni(:), upre(:,:)
c
c---------------------------
c     STEP 0: initialization
c---------------------------
c
c     1) allocate memory, increase nmax and maxlev
c        if necessary
      nmax=1000000
      maxlev=20
c
      allocate(levelbox(nmax))
      allocate(icolbox(nmax))
      allocate(iparentbox(nmax))
      allocate(ichildbox(2,nmax))
      allocate(nblevel(0:maxlev))
      allocate(iboxlev(nmax))
      allocate(istartlev(0:maxlev))
      allocate(leaflist(nmax))
      allocate(newlist(nmax))
c
      allocate(cftm(ndeg,ndeg))
      allocate(citm(ndeg,ndeg))
      allocate(spdef(ndeg))
      allocate(spbx(ndeg,ndeg))
      allocate(spxb(ndeg,ndeg))
c
      allocate(svlvr(ndeg*ndim,ndeg*ndim))
      allocate(sdvl(ndim,ndeg*ndim))
      allocate(sdvr(ndim,ndeg*ndim))
c
      allocate(chpts(ndeg,nmax))
      allocate(xlength(nmax))
      allocate(ui(ndim))
      allocate(vl(ndim,ndim))
      allocate(vr(ndim,ndim))
      allocate(fval(ndim,ndeg,nmax))
      allocate(phival(ndim,ndim,ndeg,nmax))
c
      allocate(alist(ndim,ndim,2,nmax))
      allocate(dlist(ndim,2,nmax))
      allocate(clist(ndim,nmax))
c
      allocate(sigma(ndim,ndeg,nmax))
      allocate(eta(ndim,ndeg,nmax))
      allocate(xi(ndim,ndim,ndeg,nmax))
      allocate(quadl(ndim,nmax))
      allocate(quadr(ndim,nmax))
      allocate(qloc(ndim,ndeg))
c
      allocate(ulsol(ndim,ndeg,nmax))
      allocate(ulder(ndim,ndeg,nmax))
      allocate(smoni(nmax))
      allocate(upre(ndim,ntarg))
c     there are two types of ordering:
c     quadl, quadr, sigma, ulsol, ulder are stored
c     in the order of the (current) leaflist,
c     while others are stored in the order of the tree
c
c     prepare the chebyshev matrices
      xa=endpts(1)
      xb=endpts(nch+1)
      call setchmat(ndeg, cftm, citm, spdef, spbx, spxb)
c
c     choose the right Green's function and
c     inhomogeneous term, call ssetbc to compute 
c     the constants 
      call ssetbc(ndim, bca, bcb, bcv, ui, vl, vr)
c
c     don't forget to call subroutine formsmat here
      call formsmat(ndim, ndeg, spdef, spbx, spxb, vl,
     1              vr, sdvl, sdvr, svlvr)
c
c     2) create a quasi-uniform tree whose leaf nodes
c        corresponds to the subintervals
      nleaf=nch
      call getnumqutree(nleaf, nboxes, nlev)
c
      xsize0=xb-xa
      cent0=xa+xsize0/2.0d0
      call quasiunitree(levelbox, icolbox, nboxes, 
     1     maxid, nlev, iparentbox, ichildbox, 
     2     nblevel, iboxlev, istartlev, nleaf)
c
      call getleaflist(levelbox, icolbox, nboxes,
     1     maxid, nlev, iparentbox, ichildbox, 
     2     nblevel, iboxlev, istartlev, 
     3     nleaf, leaflist)
c
      nnew=nleaf
c
c-------------------------------------------------
c
      call rasysmain(ndim, ndeg, levelbox, icolbox, 
     1     nboxes, maxid, nlev, iparentbox, ichildbox,
     2     nblevel, iboxlev, istartlev, cent0, xsize0, 
     3     nmax, nleaf, leaflist, nnew, newlist, svlvr, 
     4     sdvl, sdvr, chpts, xlength, vl, vr, phival, 
     5     fval, ui, alist, dlist, clist, sigma, eta, xi, 
     6     quadl, quadr, qloc, peval, feval, pars, 
     7     ntarg, xtarg, maxdiv, rtol, usol, uder)
c     compare with rsysmain:
c     the tree is changing, output: usol, uder at xtarg
c     needs feval and peval since new leaf nodes are
c     created and new evals are needed
c
c     remember to return chpts as an output!
c
c      write(*,*) 'nleaf = ', nleaf
c
c      do i=1, nleaf
c        ii=leaflist(i)
c        do j=1, ndeg
c          write(41,*) chpts(j,ii), 1.0d0
c        enddo
c        write(42,*) chpts(1,ii), 0.0d0
c        write(42,*) chpts(ndeg,ii), 0.0d0
c      enddo
c
c-------------------------------------------------
c
c     2) deallocate memory
c
      deallocate(levelbox)
      deallocate(icolbox)
      deallocate(iparentbox)
      deallocate(ichildbox)
      deallocate(nblevel)
      deallocate(iboxlev)
      deallocate(istartlev)
      deallocate(leaflist)
      deallocate(newlist)
c
      deallocate(cftm)
      deallocate(citm)
      deallocate(spdef)
      deallocate(spbx)
      deallocate(spxb)
c
      deallocate(svlvr)
      deallocate(sdvl)
      deallocate(sdvr)
c
      deallocate(chpts)
      deallocate(xlength)
      deallocate(ui)
      deallocate(vl)
      deallocate(vr)
      deallocate(fval)
      deallocate(phival)
c
      deallocate(alist)
      deallocate(dlist)
      deallocate(clist)
c
      deallocate(sigma)
      deallocate(eta)
      deallocate(xi)
      deallocate(quadl)
      deallocate(quadr)
      deallocate(qloc)
c
      deallocate(ulsol)
      deallocate(ulder)
      deallocate(smoni)
      deallocate(upre)

      end subroutine






