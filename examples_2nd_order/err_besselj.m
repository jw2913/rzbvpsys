%dat=load('fort.21');
%dat2=load('fort.22');
%dat=load('fort.23');
dat=load('fort.111');
dat2=load('fort.112');
yf=chebfun(@(x) besselj(100,x), [0,600]);
yf=yf/besselj(100,600);
yp=diff(yf);

xa=0.0;
xb=600.0;

x=dat(:,1);
y=dat(:,2);
u1=dat2(:,2);
u2=dat2(:,3);
%y0=besselj(100,x)/besselj(100,600);
y0=yf(x);
y1=yp(x);
y2=(xa-x).*yp(x)+yf(x);

err2_abs=norm(y0-y,2)
uexact_norm2=norm(y0,2)
err2_rel=norm(y0-y,2)/norm(y0,2)
plot(x,y,'r')
hold on
plot(x,y0,'b-.')
%
%
err2_abs=norm(y1-u1,2)
uexact_norm2=norm(y1,2)
err2_rel=norm(y1-u1,2)/norm(y1,2)
%
err2_abs=norm(y2-u2,2)
uexact_norm2=norm(y2,2)
err2_rel=norm(y2-u2,2)/norm(y2,2)
%
% convert this to a 2x2 system and use rsysadap to solve it
% to debug the issue with the system's case
