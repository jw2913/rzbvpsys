c     tests the adaptive solver
c     the besselj function example
c
      program test
      implicit real*8 (a-h,o-z)
      integer ndeg, nch
      parameter(ndeg=16)
      parameter(nch=2)
      real*8 endpts(nch+1), pars(1), eps
      real*8, allocatable:: xtarg(:), usol(:), uder(:)
      external peval, qeval, feval
c
      write(*,*) 'bessel function example'
      write(*,*) 'adaptive version'
c
      endpts(1)=0.0d0
      endpts(nch+1)=600.0d0
      h=(endpts(nch+1)-endpts(1))/nch
c
      do i=2, nch
        endpts(i)=endpts(1)+(i-1)*h
      enddo
c     set end points: initial grid
c
      ntarg=10000
      hh=(endpts(nch+1)-endpts(1))/(ntarg+1)
      allocate(xtarg(ntarg))      
      allocate(usol(ntarg))      
      allocate(uder(ntarg))      
c
      do i=1, ntarg
        xtarg(i)=endpts(1)+hh*i
      enddo
c     set the target points
c
      bcl0=1.0d0
      bcl1=0.0d0
      bclv=0.0d0

      bcr0=1.0d0
      bcr1=0.0d0
      bcrv=1.0d0
c     boundary conditions
c
      nu=100
      pars(1)=nu     
c     order of the bessel function
c
c
      maxdiv=30
      rtol=1.0d-11
c     maximum number of subdivision 
c     and error tolerance
c
      call rbvpadap(ndeg, nch, endpts,
     1     bcl0, bcl1, bclv, bcr0, bcr1, bcrv, 
     2     peval, qeval, feval, pars, maxdiv, 
     3     rtol, ntarg, xtarg, usol, uder)
c
      do i=1, ntarg
        write(21, *) xtarg(i), usol(i), uder(i)
      enddo
      write(*,*) 'solution and derivative written in fort.21'


      deallocate(xtarg)
      deallocate(usol)
      deallocate(uder)
c
c 
      end program      





c---------------------------------
c     user provided functions
c
      subroutine peval(x, p, nu)
      implicit real*8 (a-h,o-z)
      real*8 x, p, nu

      p=1.0d0/x

      end subroutine




      subroutine qeval(x, q, nu)
      implicit real*8 (a-h,o-z)
      real*8 x, q, nu

      q=(x**2-nu**2)/x**2

      end subroutine




      subroutine feval(x, f, eps)
      implicit real*8 (a-h,o-z)
      real*8 x, f, eps

      f=0.0d0

      end subroutine
