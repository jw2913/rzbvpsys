c     tests the adaptive solver
c     the besselj function example
c
      program test
      implicit real*8 (a-h,o-z)
      integer ndeg, nch
      parameter(ndeg=16)
      parameter(nch=107)
      real*8 endpts(nch+1), pars(1), eps
      real*8, allocatable:: xtarg(:), usol(:), uder(:)
      external peval, qeval, feval
c
      write(*,*) 'turning point example'
      write(*,*) 'adaptive version'
      write(*,*) '----------------------------------------'
c
      open(unit=12,file='endpts3.dat') 
      do i=1,nch+1
        read(12,*) endpts(i)
      enddo
      close(12)
c     set end points: initial grid
c
      ntarg=10000
c      hh=(endpts(nch+1)-endpts(1))/ntarg
      hh=(endpts(nch+1)-endpts(1))/(ntarg+1)
      allocate(xtarg(ntarg))      
      allocate(usol(ntarg))      
      allocate(uder(ntarg))      
c
      do i=1, ntarg
        xtarg(i)=endpts(1)+hh*i
      enddo
c     set the target points
c
      bcl0=1.0d0
      bcl1=0.0d0
      bclv=1.0d0

      bcr0=1.0d0
      bcr1=0.0d0
      bcrv=1.0d0
c     boundary conditions
c
      eps=1.0d-6
      pars(1)=eps     
c     order of the bessel function
c
c
      maxdiv=30
      rtol=1.0d-12
c     maximum number of subdivision 
c     and error tolerance
c
      call rbvpnoadap(ndeg, nch, endpts,
     1     bcl0, bcl1, bclv, bcr0, bcr1, bcrv, 
     2     peval, qeval, feval, pars, 
     3     ntarg, xtarg, usol, uder)
c
      do i=1, ntarg
        write(21, *) xtarg(i), usol(i), uder(i)
      enddo
      write(*,*) 'solution and derivative written in fort.21'


      deallocate(xtarg)
      deallocate(usol)
      deallocate(uder)
c
c 
      end program      





c---------------------------------
c     user provided functions
c
      subroutine peval(x, p, eps)
      implicit real*8 (a-h,o-z)
      real*8 x, p, eps

      p=0.0d0

      end subroutine




      subroutine qeval(x, q, eps)
      implicit real*8 (a-h,o-z)
      real*8 x, q, eps

      q=-x/eps

      end subroutine




      subroutine feval(x, f, eps)
      implicit real*8 (a-h,o-z)
      real*8 x, f, eps

      f=0.0d0

      end subroutine
