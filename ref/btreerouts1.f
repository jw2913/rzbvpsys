c**************************************************
c
c     Binary tree related subroutine
c
c**************************************************
c
c     primary routines:
c
c     settree: initializes the tree structure
c               (a tree with root node only)
c     unitree: generates a uniform tree of a given level
c     quasiunitree: generate a 'quasi'-uniform tree
c                   given the number of leaf nodes
c     getnumqutree: memory query for a quasi-uniform tree
c
c     refinelv1: refine by one more level
c
c     mktreef: generate a tree to resolve a given
c              function to a given accuracy
c     mktreeff: generate a tree to resolve a given function
c               (given by a subroutine, with opt params)
c
c     mktreep1: given a set of points (O(1) points),
c               this routine generates a tree so that
c               each leaf box containing points is no
c               more than a given size
c
c     refinetree1: given a binary tree and a set of
c                  points, reffine the tree until each
c                  leaf box containing points is no more
c                  than a given size
c
c--------------------------------
c
c     adaptivity routines: 
c
c     subdivide: subdivides a give leaf node of the tree
c     subdivide1: same as subdivide, but this version
c                 doesn't concern itself with the 
c                 colleague list
c     coarsen1: merges children (that are leaf boxes) 
c               of a given parent box 
c               (it doesn't concern itself with the 
c                colleague list)
c     reorder: to clean up the mess caused by merging
c              boxes (all the box numbers might be
c              reassigned)
c     reorderf: reorder the array fval, according to the
c               reordering of the tree
c
c--------------------------------
c     to be added:
c     spreadtree: given a quad-tree, the center and side
c                 length of the root box, spread out
c                 the tree by one or two levels, so that
c                 it covers a bigger region
c     checkedges
c     adaptree: adaptively refine and coarsen a tree
c               so that an underlying function (sampled
c               on the original tree) is resolved but
c               not over-resolved
c     adaptreef: adaptively refine and coarsen a tree
c                so that an underlying function (given
c                as a function handle) is resolved but
c                not over-resolved
c               
c--------------------------------
c
c     tree operations:
c
c     mkgrid: returns the chebyshev nodes of degree
c             ndeg-1, on a given interval of a given
c             binary tree
c     mkcolls: generates colleagues 
c              (including the periodic case)
c     mkcolls_shift: same as mkcolls, except that
c              for the periodic case, it also returns
c              the units of 'shift'
c
c     setf: sets the function value array
c
c     mkcoeffs: gets the coefficients of chebyshev poly  
c               approx of a given function
c     treesort: sorts points into leaf boxes of a
c               given tree. on output, the array
c               of points is overwritten so that
c               points of the same box are adjacent
c
c     printtree: prints out the tree
c
c     posbox: returns the end points of a box 
c
c     getleaflist: returns the list of leaf nodes,
c           in ascending order of position
c
c     get_rel_coords1d: given a tree and an array of points sorted
c                     in the tree (leaf nodes), return the relative
c                     coordinates w.r.t. box centers
c
c     get_cent_box1d: returns the center of a given box in the
c                   tree (assuming that the unit box is centered
c                   at the origin)

c
c--------------------------------------------------
c
c     level-restriction related:
c
c     restriction: checks if the tree is level restricted
c
c     fixtree: adapts the tree into a level restricted one
c
c     fixtreenf: same as fixtree, except that this version
c                deals with array of function values at the
c                same time (after fixing, interpolate to 
c                newly generated boxes)
c
c--------------------------------------------------
c
c     chebyshev transform related:
c     (wrapped around routines from utils/chebex.f)
c
c     getcoeff: returns the cheby coeffs of a given
c               function (given its function vals)
c     geterror: returns an estimated error for a
c               given cheby series (sum of tails)
c     chebyk2p: chebyshev interp of a function from
c               children to parent
c     chebyp2k: chebyshev interp of a function from
c               parent to children
c     interppot: interpolate the function sampled
c               on the leaf nodes to a regular gridc
c--------------------------------------------------
c
c     (internal) utility routines:
c
c     sortboxes: sorts boxes into one long array
c                in ascending order of level
c     rvecold2new: reorder points from old order to new order
c              given permutation ipold that maps new indices 
c              to the old ones
c     rvecnew2old: reorder points from new order to old order
c              given permutation ipold that maps new indices 
c              to the old ones
c
c
c--------------------------------------------------
c
c     (RMK: same as F. Ethridge's data structure of 
c      a quad-tree, except in this case we don't need
c      the array 'irowbox')
c
c***************************************************





c---------------------------------------------------
c     subroutine settree
c---------------------------------------------------
c
c     This subroutine initializes a binary tree structure
c     which contains only a root node
c
c     output:
c     the binary tree structure:
c     levelbox: an array determining the level of each box
c     icolbox: the column of each box (within its level)
c     nboxes: total number of boxes in the tree
c     nlev: total number of levels in the tree
c     iparentbox: parent of each box
c     ichildbox: the two children of each box
c     nblevel: total number of boxes per level
c     iboxlev: the array in which the boxes are arranged
c              (in ascending order of level) 
c     istartlev: the pointer to where each level begins in
c                the iboxlev array
c
c---------------------------------------------------
c
      subroutine settree(levelbox,icolbox,nboxes, 
     1           nlev,iparentbox,ichildbox,nblevel, 
     2           iboxlev,istartlev)
      implicit none
      integer*4 nboxes, nlev
      integer*4 levelbox(1)
      integer*4 icolbox(1) 
      integer*4 iparentbox(1), ichildbox(2,1)
      integer*4 nblevel(0:1)
      integer*4 iboxlev(1), istartlev(0:1)
c
      levelbox(1)=0
      icolbox(1)=1
      nboxes=1
      nlev=0
      iparentbox(1)=-1
      ichildbox(1,1)=-1
      ichildbox(2,1)=-1
      nblevel(0)=1
      iboxlev(1)=1
      istartlev(0)=1

      end subroutine




c---------------------------------------------------
c     subroutine unitree
c---------------------------------------------------
c
c     This subroutine generates a uniform binary
c     tree of a given level
c
c     input:
c     nlev: total number of levels in the tree
c
c     output:
c     the binary tree structure:
c     levelbox: an array determining the level of each box
c     icolbox: the column of each box (within its level)
c     nboxes: total number of boxes in the tree
c     iparentbox: parent of each box
c     ichildbox: the two children of each box
c     nblevel: total number of boxes per level
c     iboxlev: the array in which the boxes are arranged
c              (in ascending order of level) 
c     istartlev: the pointer to where each level begins in
c                the iboxlev array
c
c---------------------------------------------------
c
      subroutine unitree(levelbox,icolbox,nboxes, 
     1           nlev,iparentbox,ichildbox,nblevel, 
     2           iboxlev,istartlev)
      implicit none
      integer*4 nboxes, nlev
      integer*4 levelbox(1)
      integer*4 icolbox(1) 
      integer*4 iparentbox(1), ichildbox(2,1)
      integer*4 nblevel(0:1)
      integer*4 iboxlev(1), istartlev(0:1)
c     local vars
      integer*4 i, l, ii, id
      integer*4 istart, iend, idlev
      
      nboxes=2**(nlev+1)-1
      do i=1,nboxes
        iparentbox(i)=i/2
        ichildbox(1,i)=i*2
        ichildbox(2,i)=i*2+1
c         simple parent-child relation
        iboxlev(i)=i
c         naturally in ascending order of level
      enddo
      iparentbox(1)=-1
c
      do l=0,nlev
        nblevel(l)=2**l
        istartlev(l)=2**l
      enddo
c
      do l=0,nlev
        istart=istartlev(l)
        iend=istart+nblevel(l)-1
        idlev=1
        do ii=istart,iend
          levelbox(ii)=l
          icolbox(ii)=idlev
          idlev=idlev+1 
        enddo
      enddo
c
      l=nlev
      istart=istartlev(l)
      iend=istart+nblevel(l)-1
      do ii=istart,iend
        id=iboxlev(ii)
        ichildbox(1,id)=-1
        ichildbox(2,id)=-1
      enddo

      end subroutine




c---------------------------------------------------
c     subroutine subdivide1
c---------------------------------------------------
c
c     This subroutine subdivides a given leaf box of a
c     binary tree into two children boxes
c
c     INPUT:
c     iparbox: id of the box being divided
c     levelbox-istartlev: the binary tree structure
c           (see settree for details)
c     
c     OUTPUT:
c     the tree structure updated to reflect the change
c
c-------------------------------------------------------
c
      subroutine subdivide1(iparbox, levelbox, icolbox, nboxes,
     1           nlev, iparentbox, ichildbox, nblevel, 
     2           iboxlev, istartlev)
      implicit real*8 (a-h,o-z)
      integer*4 iparbox
      integer*4 nboxes, nlev
      integer*4 levelbox(1)
      integer*4 icolbox(1) 
      integer*4 iparentbox(1), ichildbox(2,1)
      integer*4 nblevel(0:1)
      integer*4 iboxlev(1), istartlev(0:1)

      levp=levelbox(iparbox)
      icolp=icolbox(iparbox)
      if((levp+1) .gt. nlev) then
        istartlev(levp+1)=nboxes+1
        nblevel(levp+1)=0
c       if a new level is created
c       initialize these two numbers as such
      endif
c
c     set up the first child
      ibox=nboxes+1
      levelbox(ibox)=levp+1
      iparentbox(ibox)=iparbox
      ichildbox(1,ibox)=-1
      ichildbox(2,ibox)=-1
      icolbox(ibox)=2*(icolp-1)+1
c
c     set up the second child
      ibox=nboxes+2
      levelbox(ibox)=levp+1
      iparentbox(ibox)=iparbox
      ichildbox(1,ibox)=-1
      ichildbox(2,ibox)=-1
      icolbox(ibox)=2*(icolp-1)+2
c
      ichildbox(1,iparbox)=nboxes+1
      ichildbox(2,iparbox)=nboxes+2
c
c     if levp+2 exists
c     move things on and below this level forward
      if(nlev .ge. levp+2) then
        do ii=nboxes, istartlev(levp+2),-1
          iboxlev(ii+2)=iboxlev(ii)
        enddo
c       move things 2 spots forward
        
        do l=levp+2,nlev
          istartlev(l)=istartlev(l)+2
        enddo
      endif

      inew=istartlev(levp+1)+nblevel(levp+1)
      iboxlev(inew)=nboxes+1
      iboxlev(inew+1)=nboxes+2

      nblevel(levp+1)=nblevel(levp+1)+2
      nboxes=nboxes+2
      if(levp+1 .gt. nlev) then
        nlev=nlev+1
      endif
      
      end subroutine




 
c---------------------------------------------------
c     subroutine subdivide
c---------------------------------------------------
c
c     This subroutine subdivides a given leaf box of a
c     binary tree into two children boxes
c
c     INPUT:
c     iparbox: id of the box being divided
c     levelbox-istartlev: the binary tree structure
c           (see settree for details)
c     
c     OUTPUT:
c     the tree structure updated to reflect the change
c
c--------------------------------------------------------
c
      subroutine subdivide(iparbox, levelbox, icolbox, nboxes,
     1           nlev, iparentbox, ichildbox, nblevel, 
     2           iboxlev, istartlev, icolleagbox, iperiod)
      implicit real*8 (a-h,o-z)
      integer iparbox, iperiod
      integer nboxes, nlev
      integer levelbox(1)
      integer icolbox(1) 
      integer iparentbox(1), ichildbox(2,1)
      integer nblevel(0:1)
      integer iboxlev(1), istartlev(0:1)
      integer icolleagbox(3,1)

      levp=levelbox(iparbox)
      icolp=icolbox(iparbox)
      if((levp+1) .gt. nlev) then
        istartlev(levp+1)=nboxes+1
        nblevel(levp+1)=0
c       if a new level is created
c       initialize these two numbers as such
      endif
c
c     set up the first child
      ibox=nboxes+1
      levelbox(ibox)=levp+1
      iparentbox(ibox)=iparbox
      ichildbox(1,ibox)=-1
      ichildbox(2,ibox)=-1
      icolbox(ibox)=2*(icolp-1)+1
c
c     set up the second child
      ibox=nboxes+2
      levelbox(ibox)=levp+1
      iparentbox(ibox)=iparbox
      ichildbox(1,ibox)=-1
      ichildbox(2,ibox)=-1
      icolbox(ibox)=2*(icolp-1)+2
c
      ichildbox(1,iparbox)=nboxes+1
      ichildbox(2,iparbox)=nboxes+2
c
c     if levp+2 exists
c     move things on and below this level forward
      if(nlev .ge. levp+2) then
        do ii=nboxes, istartlev(levp+2),-1
          iboxlev(ii+2)=iboxlev(ii)
        enddo
c       move things 2 spots forward
        
        do l=levp+2,nlev
          istartlev(l)=istartlev(l)+2
        enddo
      endif

      inew=istartlev(levp+1)+nblevel(levp+1)
      iboxlev(inew)=nboxes+1
      iboxlev(inew+1)=nboxes+2

      nblevel(levp+1)=nblevel(levp+1)+2
      nboxes=nboxes+2
      if(levp+1 .gt. nlev) then
        nlev=nlev+1
      endif
c
c     now let's go through the process of reforming any
c     necessary colleagues. for each of the child boxes
c     that we just formed, all we need to do is scan 
c     through the boxes that are children of the above
c     parent boxes' colleagues and test the column and
c     row numbers. the two newly formed boxes are colls
c     of each other
c
      if(iperiod .eq. 0) then
        do i=1,2
          if(ichildbox(1,iparbox) .lt. 0)goto 200
          ibox=ichildbox(i,iparbox) 
          icolleagbox(2,ibox)=ibox
          do j=1,3,2
            icolleagbox(j,ibox)=-1
          enddo 
c
          ipartemp=iparentbox(ibox)
          icoltemp=icolbox(ibox)
c         icoltemp: ibox
c
          do jcntr=1,3
            icolleague=icolleagbox(jcntr,ipartemp)
            if(icolleague .lt. 0)goto 100
            if(ichildbox(1,icolleague) .lt. 0)goto 100
c           otherwise scan the two children:
            do icntr=1,2
              j=ichildbox(icntr,icolleague)
              icoltest=icolbox(j)
c             icoltest: j
              if(icoltemp .eq. icoltest+1) then
                icolleagbox(1,ibox)=j
                icolleagbox(3,j)=ibox
              elseif(icoltemp .eq. icoltest-1) then
                icolleagbox(3,ibox)=j
                icolleagbox(1,j)=ibox
              endif
            enddo            
            
100         continue
          enddo

200       continue
        enddo
      else
c       the periodic case
        level=levelbox(iparbox)
        ilev=level+1
        nside=2**ilev
c
c       initialize
        do l=1,2
          ibox=ichildbox(l,iparbox)
          icolleagbox(2,ibox)=ibox
          do j=1,3,2
            icolleagbox(j,ibox)=-1
          enddo
c
        icoltemp=icolbox(ibox)
        do jcntr=1,3,2
          if(jcntr .eq. 1) then
            icoltest=icoltemp-1
            isister=3
          elseif(jcntr .eq. 3) then
            icoltest=icoltemp+1
            isister=1
          endif
c
          if(icoltest .lt. 1) then
            icoltest=icoltest+nside
          elseif(icoltest .gt. nside) then
            icoltest=icoltest-nside
          endif
c
          do j=1,3
            if(icolleagbox(j,iparentbox(ibox)) .lt. 0)
     1        goto 300
            if(ichildbox(1,icolleagbox(j,iparentbox(ibox))) .lt. 0)
     1        goto 300
            do icntr=1,2
              itest=ichildbox(icntr,icolleagbox(j,iparentbox(ibox)))
              if(icolbox(itest) .eq. icoltest) then
                icolleagbox(jcntr,ibox)=itest
                icolleagbox(isister,itest)=ibox
              endif
            enddo

300         continue
400         continue
          enddo
c
        enddo
        enddo
      endif
c
c
      end subroutine




      
c--------------------------------------------------------
c     subroutine mkgrid
c--------------------------------------------------------
c
c     This subroutine returns the Chebyshev nodes of a 
c     given box on a given binary tree
c
c     INPUT:
c     lev: level of the box
c     icol: column index of the box
c     aroot, broot: left and right end points of the root
c           box
c     ndeg: number of chebyshev points
c
c     OUTPUT:
c     a, b: left and right endpoints of the interval
c     chpts: chebyshev points on the interval
c
c--------------------------------------------------------
c
      subroutine mkgrid(ndeg,xf,icol,level,cent0,xsize0)
      implicit real*8 (a-h, o-z)
      integer ndeg, icol, level 
      real*8 xf(ndeg), cent0, xsize0
c
      temp1=xsize0/dble(2**level)
      pi2n=dacos(-1.0d0)/(2.0d0*ndeg)
c
      do i=1,ndeg
        xf(i)=dcos((2*ndeg-2*i+1)*pi2n) /2.0d0
     1       *temp1
      enddo
c
      xshift=cent0-xsize0/2.0d0+(icol-0.5d0)*temp1
c
      do i=1,ndeg
        xf(i)=xf(i)+xshift
      enddo
      
      end subroutine





c--------------------------------------------------------
c     subroutine getleaflist
c--------------------------------------------------------
c
c     This subroutine returns the list of ids of leaf nodes
c     of a given binary tree (in ascending order of position)
c
c     INPUT:
c     levelbox-istartlev: the btree structure
c           (see subroutine settree for details)
c
c     OUTPUT:
c     nleaf: number of leaf boxes
c     leaflist: an array containing ids of the leaf boxes
c           (on input, make sure length >= nboxes)
c
c--------------------------------------------------------
c
      subroutine getleaflist(levelbox, icolbox, nboxes,
     1           nlev, iparentbox, ichildbox, 
     2           nblevel, iboxlev, istartlev, 
     3           nleaf, leaflist)
      implicit real*8 (a-h,o-z)
      integer*4 nboxes, nlev, nleaf
      integer*4 levelbox(1)
      integer*4 icolbox(1) 
      integer*4 iparentbox(1), ichildbox(2,1)
      integer*4 nblevel(0:1)
      integer*4 iboxlev(1), istartlev(0:1)
      integer*4 leaflist(1)
      integer*4, allocatable:: istack(:)
      integer*4 nstack
c
      nleaf=0
      allocate(istack(nboxes))
      iroot=iboxlev(1) 
      nstack=1
      istack(nstack)=iroot

      do while(nstack .gt. 0)
        ibox=istack(nstack)
        nstack=nstack-1
        if(ichildbox(1,ibox).lt.0) then
          nleaf=nleaf+1
          leaflist(nleaf)=ibox
c         is a leaf node, add into the leaflist
        else
          icha=ichildbox(1,ibox)
          ichb=ichildbox(2,ibox)
          nstack=nstack+1
          istack(nstack)=ichb
          nstack=nstack+1
          istack(nstack)=icha
c         not a leaf node, add the children (right first)
c         into the stack
        endif
      enddo
      
      deallocate(istack)

      end subroutine






c--------------------------------------------------------
c     subroutine quadiunitree
c--------------------------------------------------------
c
c     This subroutine generates a 'quasi' uniform binary
c     tree with a given number of leaf nodes
c     so that 2**(nlev-1) < nleaf <= 2**nlev
c
c     INPUT:
c     nlev: total numver of levels in the tree
c
c     OUTPUT:
c     the binary tree structure:
c     levelbox: an array determining the level of each box
c     icolbox: the column of each box (within its level)
c     nboxes: total number of boxes in the tree
c     iparentbox: parent of each box
c     ichildbox: the two children of each box
c     nblevel: total number of boxes per level
c     iboxlev: the array in which the boxes are arranged
c              (in ascending order of level) 
c     istartlev: the pointer to where each level begins in
c                the iboxlev array
c
c--------------------------------------------------------
c
      subroutine quasiunitree(levelbox, icolbox, nboxes, 
     1           nlev, iparentbox, ichildbox, 
     2           nblevel, iboxlev, istartlev, nleaf)
      implicit real*8 (a-h,o-z)
      integer nboxes, nlev, nleaf
      integer levelbox(1)
      integer icolbox(1) 
      integer iparentbox(1), ichildbox(2,1)
      integer nblevel(0:1)
      integer iboxlev(1), istartlev(0:1)
c
      nlev=0
      nnds=1
      do while(nnds .lt. nleaf) 
        nlev=nlev+1
        nnds=nnds*2
      enddo
c
      if(nnds .eq. nleaf) then
c       nleaf = 2**nlev, a real uniform tree        
        call unitree(levelbox, icolbox, nboxes,
     1       nlev, iparentbox, ichildbox, nblevel, 
     2       iboxlev, istartlev)
      else
c       some extra nodes on the finest level
        nextra= nleaf-nnds/2
c       make a uniform tree of level nlev-1
        nlevuni=nlev-1
        call unitree(levelbox, icolbox, nboxes,
     1       nlevuni, iparentbox, ichildbox, nblevel, 
     2       iboxlev, istartlev)

        lev=nlevuni
        istart=istartlev(lev)
        iend=istartlev(lev)+nextra-1

c       subdivide the first few boxes 
        do ii=istart,iend
          ibox=iboxlev(ii)
          call subdivide1(ibox,levelbox,icolbox,nboxes,
     1         nlevuni,iparentbox,ichildbox,nblevel, 
     2         iboxlev, istartlev)
        enddo 
      endif

      end subroutine





c--------------------------------------------------------
c     subroutine getnumqutree
c--------------------------------------------------------
c
c     Memory query for a quasi-uniform tree
c     
c     INPUT:
c     nleaf: number of leaf nodes
c
c     OUTPUT:
c     nboxes: number of boxes
c     nlev: number of levels
c
c--------------------------------------------------------
c
      subroutine getnumqutree(nleaf, nboxes, nlev)
      implicit real*8 (a-h,o-z)
      integer nleaf, nboxes, nlev

      nlev=0
      nnds=1
      nboxes=1
      do while(nnds .lt. nleaf) 
        nlev=nlev+1
        nnds=nnds*2
        nboxes=nboxes+nnds
      enddo
c
      if(nnds .gt. nleaf) then
        nboxes=nboxes+2*nleaf-2*nnds
      endif


      end subroutine 





c--------------------------------------------------
c     subroutine mktreef
c--------------------------------------------------
c
c     the following subroutine is used to generate 
c     a tree given a right hand side, fright.  
c     the algorithm works by testing the function 
c     values vs. an approximating polynomial and 
c     dividing if necessary.  
c
c     it is important to note that the tree generated 
c     by this algorithm may not satisfy the level 
c     restriction.  
c
c     it may be necessary to call the routine fixtree 
c     after this to make it compatible with certain
c     fast transform codes
c
c     input:
c
c     maxboxes denotes the maximum number of boxes allowed
c     maxlevel denotes the deepest level allowed
c     itemparray is just a dummy array
c
c     eps denotes the error bound that determines 
c         when refinement take place
c     h is the real function that is the right hand side
c       of the poisson equation
c
c     cent0: center of the region covered
c     xsize0: side length of the unit box 
c
c     output:
c
c     istartlev is the pointer to where each level
c               begins in the iboxlev array
c     levelbox is an array determining the level of each box
c     nboxes is the total number of boxes
c     nlev is the finest level
c     icolbox denotes the column of each box
c     iparentbox denotes the parent of each box
c     ichildbox denotes the four children of each box
c     nblevel is the total number of boxes per level
c     iboxlev is the array in which the boxes are arranged
c
c     RMK: ndeg=8 fixed, but can be modified later
c
c--------------------------------------------------
c
      subroutine mktreef(levelbox, icolbox, nboxes,
     1           nlev, iparentbox, ichildbox,
     2           nblevel, iboxlev, istartlev,
     3           maxboxes, itemparray, maxlevel,
     4           eps, h, cent0, xsize0)
c-----global variables
      implicit real*8 (a-h,o-z)
      integer nlev, nboxes, maxlevel, maxboxes
      integer levelbox(1), icolbox(1)
      integer iparentbox(1), ichildbox(2,1)
      integer nblevel(0:1), iboxlev(1)
      integer istartlev(0:1), itemparray(1)
      real *8  eps, h
      real *8 cent0, xsize0
c-----local variables
      real *8  coefftemp(8), xf(8), ftemp(8)
      real *8  wsave2(1000)
c
      do i = 0, maxlevel
        nblevel(i) = 0
        istartlev(i) = 0
      enddo
c
      do i = 1, maxboxes
        iboxlev(i) = 0
      enddo
c
c     first set the big parent box to the 
c     appropriate settings:
c     (initially, there is just one box and
c     it is the big parent box at level 0)
c
      ibox = 1
      levelbox(ibox) = 0
      icolbox(ibox) = 1
      iparentbox(ibox) = -1
      ichildbox(1,ibox) = -1
      ichildbox(2,ibox) = -1
c
      nboxes = 1
      nlev = 0
c
c     we also need to initialize the adaptive 'ladder'
c     structures to the correct initial values:
      nblevel(0) = 1
      istartlev(0) = 1
      iboxlev(1) = 1
c
      call chxcin(8,wsave2)
c
      do i = 0, maxlevel - 1
      iflag = 0
      istart = istartlev(i)
      iend = istart + nblevel(i) - 1
      do j = istart, iend
       ibox = iboxlev(j)

       call mkgrid(8,xf,icolbox(ibox),
     1      levelbox(ibox),cent0,xsize0)

       do jj = 1, 8
         ftemp(jj) = h(xf(jj))
       end do
c
c      compute chebyshev transforms
       call getcoeff(8,ftemp,coefftemp)

       call geterror(8,coefftemp,error)

       hh = dble(2**levelbox(ibox))
       epscaled = eps * hh

       if(error .ge. epscaled)then
         call subdivide1(ibox,levelbox,icolbox,
     1        nboxes,nlev,iparentbox,ichildbox,
     2        nblevel,iboxlev,istartlev)
         iflag = 1
       endif
      end do
      if(iflag .eq. 0)then
c      nothing was divided at the
c      last level, so exit the loop.
       return
      endif
      end do

      return
      end subroutine




c--------------------------------------------------
c     subroutine mktreeff
c--------------------------------------------------
c
c     the following subroutine is used to generate 
c     a tree given a right hand side (given by a
c     subroutine with optional parameters)
c     the algorithm works by testing the function 
c     values vs. an approximating polynomial and 
c     dividing if necessary.  
c
c     it is important to note that the tree generated 
c     by this algorithm may not satisfy the level 
c     restriction.  
c
c     it may be necessary to call the routine fixtree 
c     after this to make it compatible with certain
c     fast transform codes
c
c     input:
c
c     maxboxes denotes the maximum number of boxes allowed
c     maxlevel denotes the deepest level allowed
c     itemparray is just a dummy array
c
c     eps denotes the error bound that determines 
c         when refinement take place
c     ftest is the real function that is the right hand
c           side of the poisson equation
c     pars: list of (optional) parameters in ftest
c
c     cent0: center of the region covered
c     xsize0: side length of the unit box 
c
c     output:
c
c     istartlev is the pointer to where each level
c               begins in the iboxlev array
c     levelbox is an array determining the level of each box
c     nboxes is the total number of boxes
c     nlev is the finest level
c     icolbox denotes the column of each box
c     iparentbox denotes the parent of each box
c     ichildbox denotes the four children of each box
c     nblevel is the total number of boxes per level
c     iboxlev is the array in which the boxes are arranged
c
c     RMK: ndeg=8 fixed, but can be modified later
c
c--------------------------------------------------
c
      subroutine mktreeff(levelbox, icolbox, nboxes,
     1           nlev, iparentbox, ichildbox,
     2           nblevel, iboxlev, istartlev,
     3           maxboxes, itemparray, maxlevel,
     4           eps, ftest, pars, cent0, xsize0)
c-----global variables
      implicit real*8 (a-h,o-z)
      integer nlev, nboxes, maxlevel, maxboxes
      integer levelbox(1), icolbox(1)
      integer iparentbox(1), ichildbox(2,1)
      integer nblevel(0:1), iboxlev(1)
      integer istartlev(0:1), itemparray(1)
      real *8  eps, pars(1)
      real *8 cent0, xsize0
c-----local variables
      real *8  coefftemp(8), xf(8), ftemp(8)
      real *8  wsave2(1000)
      external ftest
c
      do i = 0, maxlevel
        nblevel(i) = 0
        istartlev(i) = 0
      enddo
c
      do i = 1, maxboxes
        iboxlev(i) = 0
      enddo
c
c     first set the big parent box to the 
c     appropriate settings:
c     (initially, there is just one box and
c     it is the big parent box at level 0)
c
      ibox = 1
      levelbox(ibox) = 0
      icolbox(ibox) = 1
      iparentbox(ibox) = -1
      ichildbox(1,ibox) = -1
      ichildbox(2,ibox) = -1
c
      nboxes = 1
      nlev = 0
c
c     we also need to initialize the adaptive 'ladder'
c     structures to the correct initial values:
      nblevel(0) = 1
      istartlev(0) = 1
      iboxlev(1) = 1
c
      call chxcin(8,wsave2)
c
      do i = 0, maxlevel - 1
      iflag = 0
      istart = istartlev(i)
      iend = istart + nblevel(i) - 1
      do j = istart, iend
       ibox = iboxlev(j)

       call mkgrid(8,xf,icolbox(ibox),
     1      levelbox(ibox),cent0,xsize0)

       do jj = 1, 8
c         ftemp(jj) = h(xf(jj))
         call ftest(xf(jj),pars,ftemp(jj))
       end do
c
c      compute chebyshev transforms
       call getcoeff(8,ftemp,coefftemp)

       call geterror(8,coefftemp,error)

       hh = dble(2**levelbox(ibox))
       epscaled = eps * hh

       if(error .ge. epscaled)then
         call subdivide1(ibox,levelbox,icolbox,
     1        nboxes,nlev,iparentbox,ichildbox,
     2        nblevel,iboxlev,istartlev)
         iflag = 1
       endif
      end do
      if(iflag .eq. 0)then
c      nothing was divided at the
c      last level, so exit the loop.
       return
      endif
      end do

      return
      end subroutine





c--------------------------------------------------
c     subroutine mkcolls
c--------------------------------------------------
c
c     the following subroutine is used to generate the colleagues
c     for all of the boxes in the tree structure.  if a colleague
c     doesn't exist it is set to -1.  each box has nine colleagues
c     and they are ordered as follows:
c
c                        1     2     3
c
c     you are your own colleague number 2.
c     the algorithm used here is recursive and takes advantage of
c     the fact that your colleagues can only be the children of 
c     your parents colleagues.  there is no need to scan all of the
c     boxes.  iperiod denotes whether or not we are in a periodic
c     or free space case.  the basic algorithm is the same, but in the
c     periodic case we have to look for boxes that are 'outside' of
c     the standard size box.
c
c
c     input:
c
c     levelbox is an array determining the level of each box
c
c     icolbox denotes the column of each box
c
c     nboxes is the total number of boxes
c
c     nlev is the finest level
c
c     iparentbox denotes the parent of each box
c
c     ichildbox denotes the four children of each box
c
c     nblevel is the total number of boxes per level
c
c     iboxlev is the array in which the boxes are arranged
c
c     istartlev is the pointer to where each level begins in the
c               iboxlev array
c
c     iperiod denotes what kind of colleagues are to be generated
c             iperiod = 0 : free space
c             iperiod = 1 : periodic
c
c     output:
c
c     icolleagbox denotes the colleagues of a given box
c
c---------------------------------------------------
c
      subroutine mkcolls(icolbox, icolleagbox, 
     1           nboxes, nlev, iparentbox, 
     2           ichildbox, nblevel, iboxlev, 
     3           istartlev, iperiod)
      implicit real*8 (a-h,o-z)
c-----global variables
      integer *4 icolleagbox(3,1)
      integer *4 icolbox(1)
      integer *4 nboxes, nlev, iparentbox(1)
      integer *4 ichildbox(2,1)
      integer *4 nblevel(0:1), iboxlev(1), istartlev(0:1)
      integer *4 iperiod
c
c     initialize colleague number 2 to
c     yourself and all other colleagues to
c     -1.  -1 is the flag for the case when
c     the colleagues don't exist.  it can 
c     be overwritten below. 
c
      do ibox=1, nboxes
        icolleagbox(2,ibox)=ibox
        do j=1,3,2
          icolleagbox(j,ibox)=-1
        enddo
      enddo
c
c     scan through all of the levels except the coarsest level.
c     the one box at this level cannot have any colleagues.
c     do the uniform case first:
c
      if(iperiod .eq. 0) then
        do ilev=1, nlev
c        scan through all of the boxes on each level.  for each test
c        box, scan the parent's colleagues and test to see if 
c        their children are in contact with the box being tested.
c        each colleague is placed in the correct order, so there is
c        no need to 'shuffle' them later on.
          do l= istartlev(ilev), istartlev(ilev)+nblevel(ilev)-1
            ibox = iboxlev(l) 
            ipartmp = iparentbox(ibox)
            icoltmp =  icolbox(ibox)
c           go through each box, get the parent
c           check the parent's colleagues' children 
c           since it goes down level by level
c           the parents' colleagues are available
c
            do jcntr=1,3
              icolleague=icolleagbox(jcntr,ipartmp)
              if(icolleague .lt. 0) goto 100
              if(ichildbox(1,icolleague) .lt. 0) goto 100
              do icntr=1,2
                j=ichildbox(icntr,icolleague)
                icoltest=icolbox(j)
c
                if(icoltmp .eq. icoltest+1) then
                  icolleagbox(1,ibox)=j
                elseif(icoltmp .eq. icoltest-1) then
                  icolleagbox(3,ibox)=j
                endif
              enddo

100           continue            
            enddo
          enddo
        enddo
      else
c       the periodic case: the root box has itself
c       as the colleagues
        ibox=iboxlev(istartlev(0)) 
        icolleagbox(1,ibox)=ibox
        icolleagbox(2,ibox)=ibox
        icolleagbox(3,ibox)=ibox
c
        do ilev=1, nlev
          nside=2**ilev
c         scan through all the levels, be careful about
c         the wrapping around
          do l=istartlev(ilev), istartlev(ilev)+nblevel(ilev)-1
            ibox=iboxlev(l)
            icoltemp=icolbox(ibox)
c
          do jcntr=1,3,2
            if(jcntr .eq. 1) then
              icoltest=icoltemp-1
            elseif(jcntr .eq. 3) then
              icoltest=icoltemp+1
            endif
c
c           wrap around
            if(icoltest .lt. 1) then
              icoltest=icoltest+nside
            elseif(icoltest .gt. nside) then
              icoltest=icoltest-nside
            endif
c
            do j=1,3
              if(icolleagbox(j,iparentbox(ibox)) .lt. 0)
     1          goto 200
              if(ichildbox(1,icolleagbox(j,iparentbox(ibox))) .lt. 0)
     1          goto 200
              do icntr=1,2
                itest=ichildbox(icntr,icolleagbox(j,iparentbox(ibox)))
                if(icolbox(itest) .eq. icoltest) then
                  icolleagbox(jcntr,ibox)=itest
                endif
              enddo
c
200           continue
            enddo
          enddo
          enddo
        enddo
      endif

      end subroutine





c--------------------------------------------------
c     subroutine mkcolls_shift
c--------------------------------------------------
c
c     the following subroutine is used to generate the colleagues
c     for all of the boxes in the tree structure.  if a colleague
c     doesn't exist it is set to -1.  each box has nine colleagues
c     and they are ordered as follows:
c
c                        1     2     3
c
c     you are your own colleague number 2.
c     the algorithm used here is recursive and takes advantage of
c     the fact that your colleagues can only be the children of 
c     your parents colleagues.  there is no need to scan all of the
c     boxes.  iperiod denotes whether or not we are in a periodic
c     or free space case.  the basic algorithm is the same, but in the
c     periodic case we have to look for boxes that are 'outside' of
c     the standard size box.
c
c     in the periodic case, it also returns the units of
c     'shift' needed to make the two boxes physically colleagues
c
c
c     input:
c
c     levelbox is an array determining the level of each box
c
c     icolbox denotes the column of each box
c
c     nboxes is the total number of boxes
c
c     nlev is the finest level
c
c     iparentbox denotes the parent of each box
c
c     ichildbox denotes the four children of each box
c
c     nblevel is the total number of boxes per level
c
c     iboxlev is the array in which the boxes are arranged
c
c     istartlev is the pointer to where each level begins in the
c               iboxlev array
c
c     iperiod denotes what kind of colleagues are to be generated
c             iperiod = 0 : free space
c             iperiod = 1 : periodic
c
c     output:
c
c     icolleagbox denotes the colleagues of a given box
c     ishift denotes the units of shift
c
c--------------------------------------------------
c
      subroutine mkcolls_shift(icolbox, icolleagbox, 
     1           nboxes, nlev, iparentbox, 
     2           ichildbox, nblevel, iboxlev, 
     3           istartlev, iperiod, ishift)
      implicit real*8 (a-h,o-z)
c-----global variables
      integer *4 icolleagbox(3,1)
      integer *4 icolbox(1)
      integer *4 nboxes, nlev, iparentbox(1)
      integer *4 ichildbox(2,1)
      integer *4 nblevel(0:1), iboxlev(1), istartlev(0:1)
      integer *4 iperiod, ishift(3,1)
c
c     initialize colleague number 5 to
c     yourself and all other colleagues to
c     -1.  -1 is the flag for the case when
c     the colleagues don't exist.  it can 
c     be overwritten below. 
c
      do ibox=1, nboxes
        icolleagbox(2,ibox)=ibox
        do j=1,3,2
          icolleagbox(j,ibox)=-1
        enddo
      enddo
c
c     assign ishift to zeros
c     for the non-periodic case      
      do ibox=1, nboxes
        do j=1,3
          ishift(j,ibox)=0
        enddo
      enddo
c
c     scan through all of the levels except the coarsest level.
c     the one box at this level cannot have any colleagues.
c     do the uniform case first:
c
      if(iperiod .eq. 0) then
        do ilev=1, nlev
c        scan through all of the boxes on each level.  for each test
c        box, scan the parent's colleagues and test to see if 
c        their children are in contact with the box being tested.
c        each colleague is placed in the correct order, so there is
c        no need to 'shuffle' them later on.
          do l= istartlev(ilev), istartlev(ilev)+nblevel(ilev)-1
            ibox = iboxlev(l) 
            ipartmp = iparentbox(ibox)
            icoltmp =  icolbox(ibox)
c           go through each box, get the parent
c           check the parent's colleagues' children 
c           since it goes down level by level
c           the parents' colleagues are available
c
            do jcntr=1,3
              icolleague=icolleagbox(jcntr,ipartmp)
              if(icolleague .lt. 0) goto 100
              if(ichildbox(1,icolleague) .lt. 0) goto 100
              do icntr=1,2
                j=ichildbox(icntr,icolleague)
                icoltest=icolbox(j)
c
                if(icoltmp .eq. icoltest+1) then
                  icolleagbox(1,ibox)=j
                elseif(icoltmp .eq. icoltest-1) then
                  icolleagbox(3,ibox)=j
                endif
              enddo

100           continue            
            enddo
          enddo
        enddo
      else
c       the periodic case: the root box has itself
c       as the colleagues
        ibox=iboxlev(istartlev(0)) 
        icolleagbox(1,ibox)=ibox
        icolleagbox(2,ibox)=ibox
        icolleagbox(3,ibox)=ibox
c
        ishift(1,ibox)=-1
        ishift(2,ibox)=0
        ishift(3,ibox)=1
c
        do ilev=1, nlev
          nside=2**ilev
c         scan through all the levels, be careful about
c         the wrapping around
          do l=istartlev(ilev), istartlev(ilev)+nblevel(ilev)-1
            ibox=iboxlev(l)
            icoltemp=icolbox(ibox)
c
          do jcntr=1,3,2
            if(jcntr .eq. 1) then
              icoltest=icoltemp-1
            elseif(jcntr .eq. 3) then
              icoltest=icoltemp+1
            endif
            ishiftx=0
c
c           wrap around
            if(icoltest .lt. 1) then
              icoltest=icoltest+nside
              ishiftx=-1
            elseif(icoltest .gt. nside) then
              icoltest=icoltest-nside
              ishiftx=1
            endif
c
            do j=1,3
              if(icolleagbox(j,iparentbox(ibox)) .lt. 0)
     1          goto 200
              if(ichildbox(1,icolleagbox(j,iparentbox(ibox))) .lt. 0)
     1          goto 200
              do icntr=1,2
                itest=ichildbox(icntr,icolleagbox(j,iparentbox(ibox)))
                if(icolbox(itest) .eq. icoltest) then
                  icolleagbox(jcntr,ibox)=itest
                  ishift(jcntr,ibox)=ishiftx
                endif
              enddo
c
200           continue
            enddo
          enddo
          enddo
        enddo
      endif

      end subroutine






c--------------------------------------------------
c     subroutine mkcoeffs
c--------------------------------------------------
c
c     the following subroutine defines the array coeffs that contains
c     the polynomial coefficients for the polynomial that approximates
c     the right hand side.
c
c     input:
c
c     ndeg is the order of approximation
c     fright is the right hand side defined on the old grid
c     levelbox is an array determining the level of each box
c     nboxes is the total number of boxes
c     nlev is the finest level
c     ichildbox denotes the four children of each box
c     nblevel is the total number of boxes per level
c     iboxlev is the array in which the boxes are arranged
c     istartlev is the pointer to where each level begins
c               in the iboxlev array
c
c     output:
c
c     coeffs is the array of coefficients for the basis functions
c
c--------------------------------------------------
c
      subroutine mkcoeffs(ndeg,coeffs,fright,
     1         nlev, ichildbox, nblevel, 
     2         iboxlev, istartlev)
      implicit real*8 (a-h,o-z)
c-----global variables
      integer ndeg,nlev
      integer ichildbox(2,1)
      integer nblevel(0:1), iboxlev(1)
      integer istartlev(0:1)
      real*8  coeffs(0:ndeg-1,1), fright(ndeg,1)
c-----local variables
      real *8  ftemp(ndeg)
      real *8  coefftemp(ndeg)
c
      do l=0,nlev
        do i=istartlev(l),istartlev(l)+nblevel(l)-1
          ibox = iboxlev(i)
          if (ichildbox(1,ibox) .lt.0)then
c           no need to do chxcin
            do j=1, ndeg
              ftemp(j)=fright(j,ibox)
            enddo
c
            call getcoeff(ndeg,ftemp,coefftemp)
c
            do j=0,ndeg-1
              coeffs(j,ibox)=coefftemp(j+1)
            enddo
          endif
        enddo
      enddo
c

      end subroutine





c--------------------------------------------------
c     subroutine treesort
c--------------------------------------------------
c
c     Given the tree structure and a set of discrete
c     points, sort the points into boxes of the tree
c
c     INPUT:
c     nlev-istartlev: the tree structure, see
c          subroutine mktree for details
c     xpts: coordinates of the points
c     npts: number of points
c     cent0: center of the root box
c     xsize0: size of the root box
c     
c     OUTPUT:
c     npbox: number of sources in each box 
c                     (including non-leaf boxes)
c     ipold: ids of points on input
c            (before the overwriting)
c     istartbox: starting point in the array xpts
c                for each box
c     ibofp: indices of leaf boxes that the point
c            belongs to (after overwriting)
c
c     (xpts: overwritten, reordered so that points in
c            a box are stored in adjacent places)
c
c--------------------------------------------------
c
      subroutine treesort(nlev,levelbox,iparentbox,ichildbox,
     1           icolbox,nboxes,nblevel,iboxlev,istartlev,
     2           xpts,npts,npbox,ipold,istartbox,ibofp,
     3           cent0,xsize0)
      implicit real*8 (a-h,o-z)
c     global vars
      integer nlev, nboxes, npts
      integer levelbox(nboxes), iparentbox(nboxes)
      integer ichildbox(2,nboxes)
      integer icolbox(nboxes) 
      integer nblevel(0:nlev), iboxlev(nboxes)
      integer istartlev(0:nlev)
      integer npbox(nboxes), ipold(npts)
      integer istartbox(nboxes), ibofp(npts)
      real*8 xpts(npts), cent0, xsize0
c     local vars
      integer nsinchild(2)
      integer, allocatable:: isinchild(:,:)
      integer, allocatable:: itmp(:)
      real*8, allocatable:: xtmp(:)
c
      allocate(isinchild(npts,2))
      allocate(itmp(npts))
      allocate(xtmp(npts))
c
      do i=1,nboxes
        npbox(i)=0
        istartbox(i)=-1
      enddo
c      
      do j=1,npts
        ipold(j)=j
      enddo
c
      iroot=iboxlev(istartlev(0))

      npbox(iroot)=npts
      istartbox(iroot)=1
c
c    the root box contains all the points
c    RMK: in the current version, root box doesn't
c         necessarily have index 1
c         use iroot=iboxlev(istartlev(0))
c         instead of 1
c
c----------------------------------
c
      ix=1
      xlength=xsize0
      x0=cent0-xsize0/2.0d0
      nside=1
c
      do l=0,nlev-1
c       go through all the levels
        xlength=xlength/2.0d0
        nside=nside*2

        istart=istartlev(l)
        iend=istartlev(l)+nblevel(l)-1

        do ii=istart,iend
          ibox=iboxlev(ii)
c         go through all non-empty boxes that have children
          if((npbox(ibox) .gt. 0) .and. 
     1               (ichildbox(1,ibox)).gt.0) then
c-------------------
            iss=istartbox(ibox)
            ies=istartbox(ibox)+npbox(ibox)-1
c
            do kk=1,2
              nsinchild(kk)=0
              ic=ichildbox(kk,ibox)
              npbox(ic)=0
            enddo
c
c             go through all the sources points in the box
c             (which occupy adjacent spots in issorted)      
            do j=iss,ies
              xx=xpts(j)
              xtmp(j)=xx
              itmp(j)=ipold(j)
c                 iss-th to ies-th entries of xpts
c                 and ipold copied to xtmp and itmp at
c                 corresponding places
              ix=ceiling((xx-x0)/xlength)

              if(ix.le.0) then
                ix=1
              endif

              if(ix.gt.nside) then
                ix=nside
              endif
c
              do kk=1,2
                ic=ichildbox(kk,ibox)
                if(icolbox(ic).eq.ix) then
                  nsinchild(kk)=nsinchild(kk)+1
                  npbox(ic)=npbox(ic)+1
                  isinchild(nsinchild(kk),kk)=j
                endif
              enddo
            enddo
c
c           all points in box ibox sorted into isinchild,
c           indicating which child the points belong to. 
c           now overwrite the iss-th to ies-th spot in issorted
c
            jpt=iss
            do jc=1,2
              ic=ichildbox(jc,ibox)
              istartbox(ic)=jpt
              do js=1,nsinchild(jc)
                idnow=isinchild(js,jc)
                xpts(jpt)=xtmp(idnow)  
                ipold(jpt)=itmp(idnow)
                jpt=jpt+1
              enddo
            enddo
c-------------------
          endif
        enddo
c       go to the next level
      enddo
c
c     now get information for ibofp
      do l=0,nlev
        istart=istartlev(l)
        iend=istartlev(l)+nblevel(l)-1
        do ii=istart,iend
          ibox=iboxlev(ii)
          if((ichildbox(1,ibox).lt.0).and.(npbox(ibox).gt.0)) then
            iss=istartbox(ibox)
            ies=iss+npbox(ibox)-1
            do kk=iss,ies
              ibofp(kk)=ibox
            enddo
          endif
        enddo
      enddo
c
c
      deallocate(isinchild)
      deallocate(itmp)
      deallocate(xtmp)


      end subroutine






c--------------------------------------------------
c     subroutine posbox
c--------------------------------------------------
c
c     This subroutine gives the end points of a 
c     given box in a given tree
c     
c     INPUT:
c     icol: number of column (within the current level)
c     level: the current level
c
c     OUTPUT:
c     xa, xb: end points of the box
c      
c--------------------------------------------------
c
      subroutine posbox(xa,xb,icol,level,cent0,xsize0)
      implicit real*8 (a-h,o-z)
      integer icol, level
      real*8 cent0, xsize0, xa, xb
      
      xlength = xsize0 / dble(2**level)
      x0 = cent0-xsize0/2.0d0

      xa=x0+(icol-1)*xlength
      xb = xa+xlength

      end subroutine





      
c--------------------------------------------------
c     subroutine printtree
c--------------------------------------------------
c
c     this subroutine prints end points of leaf
c     boxes into a given file, it also returns
c     the number of leaf boxes in the tree
c
c--------------------------------------------------
c
      subroutine printtree(levelbox,icolbox,nboxes,
     1           nlev,iparentbox,ichildbox,nblevel,
     2           iboxlev,istartlev,iprint,nleaves,
     3           cent0,xsize0)
      implicit real*8 (a-h,o-z)
      integer levelbox(1)
      integer nlev, nboxes
      integer icolbox(1) 
      integer iparentbox(1), ichildbox(2,1)
      integer nblevel(0:1), iboxlev(1)
      integer istartlev(0:1)
      real*8 cent0, xsize0 
c
      nleaves=0
      do l=0,nlev
        istart=istartlev(l)
        iend=istart+nblevel(l)-1
        do i=istart,iend
          ibox=iboxlev(i)
          if(ichildbox(1,ibox).lt.0) then
            nleaves=nleaves+1
            if(iprint .gt. 0) then
c             if a leaf node, print out end points
              call posbox(xa,xb,icolbox(ibox),l,
     1             cent0,xsize0)
              write(iprint,*) xa,xb
            endif
          endif
        enddo
      enddo
c
      if(iprint .gt. 0) then
        write(*,*) 'tree data saved in file',iprint
        close(iprint)
      endif
c
      end subroutine





c--------------------------------------------------
c     subroutine restriction
c--------------------------------------------------
c
c     this subroutine will determine whether or not a given tree satisfies 
c     the level restriction.  if it doesn't, call fixtree to fix the
c     tree.
c
c     icolleagbox is a working array (input) that
c     doesn't need to be initialized
c
c--------------------------------------------------
c
      subroutine restriction(levelbox,iparentbox,
     1           ichildbox,icolbox,icolleagbox,
     2           nboxes,nlev,nblevel,iboxlev,
     3           istartlev,iperiod,ifixflag)
      implicit real*8 (a-h,o-z)
c-----global variables
      integer *4 levelbox(1), icolleagbox(3,1)
      integer *4 iparentbox(1), ichildbox(2,1)
      integer *4 icolbox(1) 
      integer *4 nboxes, nlev
      integer *4 nblevel(0:1), iboxlev(1), istartlev(0:1)
      integer *4 iperiod, ifixflag
c-----local variables
      integer *4 ichild(2),icoll(3), ibox
      integer *4 i, ipar, itest, j, nb
      integer *4 itemp
c
c     let's sort all of the boxes by level.
c     this takes the place of the ladder structure
c     in the uniform case.  all boxes are sorted
c     into the array and placed in their proper places.
c     (need to do this inside? it doesn't hurt though)
c
      ifixflag=0
c     initialize to be zero
c
      call sortboxes(levelbox,nboxes,nlev,
     1     nblevel,iboxlev,istartlev)
c
c     first let's call a subroutine that will
c     generate all of the colleagues for each
c     box.  the colleagues are generated in the
c     correct order so there is no need to 'shuffle'
c     them later on.
      call mkcolls(icolbox,
     1     icolleagbox,nboxes,nlev,
     2     iparentbox,ichildbox,nblevel,
     3     iboxlev, istartlev,iperiod)
c
      do i=nlev,2,-1
        do j=istartlev(i),istartlev(i)+nblevel(i)- 1
          ibox = iboxlev(j)
          ipar  = iparentbox(ibox)
          itest = iparentbox(ipar)
c           itest: the grandparent
c
          icoll(1) = icolleagbox(1,itest)
          icoll(2) = icolleagbox(2,itest)
          icoll(3) = icolleagbox(3,itest)
c
          ichild(1) = ichildbox(1,itest)
          ichild(2) = ichildbox(2,itest)
c
c-----------------
          do nb=1,3
            itemp=icoll(nb)
            if(ichildbox(1,itemp) .lt. 0) then
c             the neighboring box is not divided
c             we could have problems.
              if(nb .eq. 1) then
                if(ipar .eq. ichild(1)) then
                  ifixflag=1
                endif
              elseif(nb .eq. 3) then
                if(ipar .eq. ichild(2)) then
                  ifixflag=1
                endif
              endif
            endif
          enddo 
c-----------------
        enddo
      enddo

      end subroutine





c--------------------------------------------------
c     subroutine fixtree
c--------------------------------------------------
c
c     the following subroutine is designed to take a correctly defined
c     tree and alter it so that no two boxes that contact each other
c     are more than one level apart.  this is corrected only by adding
c     boxes.  the procedure involves flagging down bigger boxes and
c     dividing them and their children as necessary.
c     this routine also produces an array of colleagues for each box
c     that is numbered in the correct order.  all of the children are set
c     so that they satisfy our ordering convention.
c     the algorithm in the periodic case works the same way, it is just 
c     that upon subdivision the new colleagues must be put down to 
c     account for the periodicity.
c
c     input:
c
c     levelbox is an array determining the level of each box
c
c     iparentbox denotes the parent of each box
c
c     ichildbox denotes the four children of each box
c
c     icolbox denotes the column of each box
c
c     icolleagbox denotes the colleagues of a given box
c
c     nboxes is the total number of boxes
c
c     nlev is the finest level
c
c     nblevel is the total number of boxes per level
c
c     iboxlev is the array in which the boxes are arranged
c
c     istartlev is the pointer to where each level begins in the
c               iboxlev array
c
c     iperiod denotes what kind of colleagues are to be generated
c             iperiod = 0 : free space
c             iperiod = 1 or 2 : periodic
c
c     iflag is just a dummy array. but be careful
c     -- an array! not a scalar
c
c     maxboxes is the maximum number of boxes we have storage for
c
c     output:
c
c     icolbox, icolleagbox, nboxes, and all other
c     arrays describing the tree may be change on output
c
c--------------------------------------------------
c
      subroutine fixtree(levelbox,iparentbox,ichildbox,
     1           icolbox,icolleagbox,nboxes,nlev,
     2           nblevel,iboxlev,istartlev,iperiod,
     3           iflag,maxboxes)
      implicit none
c-----global variables
      integer levelbox(1), icolleagbox(3,1)
      integer maxboxes
      integer iparentbox(1), ichildbox(2,1)
      integer icolbox(1)
      integer nboxes, nlev
      integer nblevel(0:1), iboxlev(1), istartlev(0:1)
      integer iperiod
c-----local variables
      integer iflag(maxboxes)
      integer ichild(2),icoll(3), ibox
      integer i, ipar, itest, j, nb
      integer itemp, ntemp, jcntr, icntr
      integer start, istop
c
c     let's sort all of the boxes by level.
c     this takes the place of the ladder structure
c     in the uniform case.  all boxes are sorted
c     into the array and placed in their proper places.
      call sortboxes(levelbox,nboxes,nlev,
     1     nblevel,iboxlev,istartlev)
c
c     first let's call a subroutine that will
c     generate all of the colleagues for each
c     box.  the colleagues are generated in the
c     correct order so there is no need to 'shuffle'
c     them later on.
      call mkcolls(icolbox,
     1     icolleagbox,nboxes,nlev,
     2     iparentbox,ichildbox,nblevel,
     3     iboxlev, istartlev,iperiod)
c
c     let's initialize all of the flags to zero.
      do i = 1, maxboxes
        iflag(i) = 0
      enddo
c
c     find all of the boxes that need to be
c     flagged.  a flagged box will be denoted by 
c     setting iflag(box) = 1.
c     this refers to any box that is directly touching 
c     a box that is more than one level smaller than
c     it.  it is found by performing an upward pass
c     and looking a box's parents parents and seeing
c     if they are childless and contact the given box.
c     note that we only need to get up to level two, as
c     we will not find a violation at a coarser level
c     than that.
c
      do i=nlev, 2, -1
        do j=istartlev(i),istartlev(i)+nblevel(i)- 1
          ibox = iboxlev(j)
          ipar  = iparentbox(ibox)
          itest = iparentbox(ipar)
c
          icoll(1) = icolleagbox(1,itest)
          icoll(2) = icolleagbox(2,itest)
          icoll(3) = icolleagbox(3,itest)
c
          ichild(1) = ichildbox(1,itest)
          ichild(2) = ichildbox(2,itest)
c
          do nb=1, 3
            itemp=icoll(nb)
            if(ichildbox(1,itemp) .lt. 0) then
c             the neighboring box is not divided
c             we could have problems.
              if(nb .eq. 1) then
                if(ipar .eq. ichild(1)) then
                  iflag(itemp)=1
                endif
              elseif(nb .eq. 3) then
                if(ipar .eq. ichild(2)) then
                  iflag(itemp)=1
                endif
              endif
            endif
          enddo
        enddo
      enddo
c
c     find all of the boxes that need to be
c     given a flag+.  a flag+ box will be denoted by 
c     setting iflag(box) = 2.
c     this refers to any box that is not already flagged
c     and is bigger than and is contacting a flagged box
c     or another box that has already been given a flag+.
c     it is found by performing an upward pass
c     and looking at a flagged box's parents colleagues
c     and a flag+ box's parents colleagues and seeing if
c     they are childless and present the case where a 
c     bigger box is contacting a flagged or a flag+ box.
c
      do i=nlev, 2, -1
        do j=istartlev(i),istartlev(i)+nblevel(i)- 1
          ibox=iboxlev(j)
          if(iflag(ibox) .eq. 1 .or. iflag(ibox) .eq. 2)then
            ipar=iparentbox(ibox)
            icoll(1) = icolleagbox(1,ipar)
            icoll(2) = icolleagbox(2,ipar)
            icoll(3) = icolleagbox(3,ipar)
c
            ichild(1) = ichildbox(1,ipar)
            ichild(2) = ichildbox(2,ipar)
c
            do nb=1, 3
              itemp=icoll(nb)
c             let's check using the same criteria as above, but noting that
c             a flag will take precedence over a flag+.
              if(ichildbox(1,itemp) .lt. 0 
     1           .and. iflag(itemp) .ne. 1)then
                if(nb .eq. 1) then
                  if(ibox .eq. ichild(1)) then
                    iflag(itemp)=2
                  endif
                elseif(nb .eq. 3) then
                  if(ibox .eq. ichild(2)) then
                    iflag(itemp)=2
                  endif
                endif
              endif
            enddo
          endif
        enddo
      enddo
c
c     done with flagging
c
c     now let's divide the boxes that need to be immediately
c     divided up.  all of the flagged and flag+ boxes need to
c     be divided one time.  the distinction lies in the fact
c     that the children of a flag+ box will never need to be
c     divided but the children of a flagged box may need to 
c     be divided further.
c     below, all flagged and flag+ boxes are divided once.  the
c     children of a flag+ box are left unflagged while those of
c     the flagged boxes are given a flag++ (denoted by setting
c     iflag(box) = 3) which will be needed in the downward pass.     
c
      ntemp = nboxes
      do i = 1, ntemp
c      divide flagged boxes:
       if (iflag(i) .eq. 1)then

         if(ichildbox(1,i) .lt. 0)then
         call subdivide(i,levelbox,icolbox,nboxes,
     1        nlev,iparentbox,ichildbox,nblevel, 
     2        iboxlev,istartlev,icolleagbox,iperiod)
         endif


c        give flag++ to children of flagged boxes.
         itemp = ichildbox(1,i)
         iflag(itemp) = 3

         itemp = ichildbox(2,i)
         iflag(itemp) = 3
c
c
c      divide flag+ boxes.
       elseif (iflag(i) .eq. 2)then
         if(ichildbox(1,i) .lt. 0)then
         call subdivide(i,levelbox,icolbox,nboxes,
     1        nlev,iparentbox,ichildbox,nblevel, 
     2        iboxlev,istartlev,icolleagbox,iperiod)
         endif
       endif
      end do 
c
c     now we need to do a downward pass.
c     we will concern ourselves only with the children of
c     flagged boxes and their children.  at each level,
c     for each flag++ box, test colleagues children and see
c     if they have children that are contacting you.  if so,
c     divide and flag++ all children that are created.     
c
      do i = 0, nlev
      ntemp = nboxes
      start = istartlev(i)
      istop  = istartlev(i) + nblevel(i) - 1
      do 500 j = start, istop
       ibox = iboxlev(j)
c      only be concerned with boxes on this level and
c      boxes that are given a flag++:
       if(iflag(ibox) .ne. 3)goto 500

         icoll(1) = icolleagbox(1,ibox)
         icoll(2) = icolleagbox(2,ibox)
         icoll(3) = icolleagbox(3,ibox)
c
c       scan colleagues.
        do 400 jcntr = 1, 3
        if(icoll(jcntr) .lt. 0)goto 400
        if(ichildbox(1,icoll(jcntr)) .lt. 0)goto 400

         ichild(1) = ichildbox(1,icoll(jcntr))
         ichild(2) = ichildbox(2,icoll(jcntr))

c          scan colleague's children.
cccccc
           do 300 icntr = 1, 2

           if (ichildbox(1,ichild(icntr)) .lt. 0)goto 300 
           if(jcntr .eq. 1 .and. icntr .eq. 2)then

c           call subdivide
         if(ichildbox(1,ibox) .lt. 0)then
            call subdivide(ibox,levelbox,icolbox,
     1      nboxes,nlev,iparentbox,ichildbox,
     2      nblevel,iboxlev,istartlev,icolleagbox,iperiod)
         endif

c           flag++ all children created
            itemp = ichildbox(1,ibox)
            iflag(itemp) = 3
            itemp = ichildbox(2,ibox)
            iflag(itemp) = 3
c
           elseif(jcntr .eq. 3 .and. 
     1        icntr .eq. 1 )then

c           call subdivide
         if(ichildbox(1,ibox) .lt. 0)then
            call subdivide(ibox,levelbox,icolbox,
     1        nboxes,nlev,iparentbox,ichildbox,
     2        nblevel,iboxlev,istartlev,icolleagbox,iperiod)
         endif

c           flag++ all children created
            itemp = ichildbox(1,ibox)
            iflag(itemp) = 3
            itemp = ichildbox(2,ibox)
            iflag(itemp) = 3

           endif
300     continue   
400     continue
500     continue
      end do

      end subroutine







c--------------------------------------------------
c     subroutine sortboxes
c--------------------------------------------------
c
c     the following subroutine sets up a structure that is analogous
c     to the 'ladder' structure in the nonadaptive case.
c     it is just a way of organizing the boxes by level in one long
c     array and denoting where in the array the levels change.
c
c     input:
c
c     levelbox is an array determining the level of each box
c
c     nboxes is the total number of boxes
c
c     nlev is the finest level
c
c     ichildbox denotes the four children of each box
c
c     output:
c
c     nblevel is the total number of boxes per level
c
c     iboxlev is the array in which the boxes are arranged
c
c     istartlev is the pointer to where each level begins in the
c               iboxlev array
c
c--------------------------------------------------
c
      subroutine sortboxes(levelbox,nboxes,nlev,
     1           nblevel,iboxlev,istartlev)
      implicit none
c-----global variables
      integer *4 levelbox(1), nboxes, nlev
      integer *4 nblevel(0:1), iboxlev(1), istartlev(0:1)
c-----local variables
      integer *4 ncntr, i, j

        ncntr = 1
        do i = 0, nlev
          nblevel(i) = 0
          istartlev(i) = ncntr
          do j = 1, nboxes
            if(levelbox(j) .eq. i)then
             iboxlev(ncntr) = j
             ncntr = ncntr + 1
             nblevel(i) = nblevel(i) + 1
            endif
          end do
        end do
      return
      end subroutine







c---------------------------------------------------
c     subroutine getcoeff
c---------------------------------------------------
c     a wrapper of utils/chebex.f/chexfc
c     for the convenience of the calling seq
c
c     RMK: chxcin is called inside
c          no need to do it before calling getcoeff
c
c---------------------------------------------------
c
      subroutine getcoeff(n, fdat, coeff)
      implicit real *8 (a-h,o-z)
      integer n
      real*8 fdat(n), coeff(n) 
      real *8 wsave2(1000), work(1000)
c
      call chxcin(n, wsave2)
      call chexfc(fdat, n, coeff, wsave2, work)
c
      end subroutine





c---------------------------------------------------
c     subroutine geterror
c---------------------------------------------------
c
      subroutine geterror(n, coeff, error)
      implicit none
      integer n
      real*8 coeff(n), error
c
      error=abs(coeff(n))+abs(coeff(n-1))
c
      end subroutine





c---------------------------------------------------
c     subroutine chebyk2p
c---------------------------------------------------
c
c     This subroutine interpolates a function from
c     the two children boxes to the parent box
c     given the function values on the ndeg cheby 
c     nodes of the children box, it returns the 
c     function values and cheby coeffs of the 
c     parent box
c
c---------------------------------------------------
c
      subroutine chebyk2p(ndeg, valk1, valk2, valp,
     1           coeffp)
      implicit real*8 (a-h,o-z)
      integer ndeg
      real*8 valk1(ndeg), valk2(ndeg)
      real*8 valp(ndeg), coeffp(ndeg)
c     local vars
      real*8 wsave(1000), work(1000)
      real*8 coeffk1(ndeg), coeffk2(ndeg), xf(ndeg)
c
      call chxcin(ndeg, wsave)
      call chexfc(valk1, ndeg, coeffk1, wsave, work)
      call chexfc(valk2, ndeg, coeffk2, wsave, work)
c          got the cheby coeffs of each kid
c
      call mkgrid(ndeg,xf,1,0,0.0d0,1.0d0)
c
      do k=1,ndeg
        x=xf(k)
        if(x .gt. 0.0d0) then
          a=0.0d0
          b=0.5d0
          call cheval(x,valp(k),coeffk2,ndeg,a,b)
        else
          a=-0.5d0
          b=0.0d0
          call cheval(x,valp(k),coeffk1,ndeg,a,b)
        endif
      enddo
c
      call chexfc(valp,ndeg,coeffp,wsave,work)
      
      end subroutine






c---------------------------------------------------
c     subroutine chebyp2k
c---------------------------------------------------
c
c     This subroutine interpolates function values on a 
c     ndeg chebyshev grid to the ndeg chebyshev grids of
c     the four children boxes 
c
c     INPUT:
c     ndeg: order of Chebyshev approx
c     valp: function values on parent box
c
c     OUTPUT:
c     valk1: function values on kid 1
c     valk2: function values on kid 2
c
c---------------------------------------------------
c
      subroutine chebyp2k(ndeg,valp,valk1,valk2)
      implicit real*8 (a-h,o-z)
      integer ndeg
      real*8 valp(ndeg), valk1(ndeg), valk2(ndeg)
c     local vars
      real*8 wsave(1000), work(1000)
      real*8 xf(ndeg), coeffp(ndeg), valtmp(ndeg)
c
      cent0=0.0d0
      xsize0=1.0d0
c
      do k=1, ndeg
        valtmp(k)=valp(k)
      enddo
c     not sure if the following calls would destroy
c     the original values, so I copy it over to be safe
c
      call chxcin(ndeg, wsave)
      call chexfc(valtmp,ndeg,coeffp,wsave,work)
c          got the chebyshev coefficients of the 
c          parent box. now it remains to evaluate the
c          chebyshev series on the grids of children
c
      a=-0.5d0
      b=0.5d0
c
      call mkgrid(ndeg,xf,1,1,cent0,xsize0)
      do k=1,ndeg
        x=xf(k)
        call cheval(x,valk1(k),coeffp,ndeg,a,b)
      enddo
c
      call mkgrid(ndeg,xf,2,1,cent0,xsize0)
      do k=1,ndeg
        x=xf(k)
        call cheval(x,valk2(k),coeffp,ndeg,a,b)
      enddo
c
c
      end subroutine





c--------------------------------------------------
c     subroutine interppot
c--------------------------------------------------
c
c     This subroutine interpolates the function sampled
c     on the Chebyshev grids of leaf nodes of an 
c     adaptive quadtree to a regular grid 
c
c     INPUT: 
c     levelbox - istartlev: the tree structure
c     ndeg: degree of function approx on leaf nodes
c     pot: the array of function values
c     ng: number of grid points 
c     xg: the regular grid
c     fg: the function values on the regular grid
c     cent0: center of the root box
c     xsize0: size of the root box
c
c--------------------------------------------------
c
      subroutine interppot(levelbox, icolbox, nboxes, 
     1           nlev, iparentbox, ichildbox, nblevel, 
     2           iboxlev, istartlev, ndeg, pot, ng,
     3           xg, fg, cent0, xsize0)
      implicit real*8 (a-h,o-z)
      integer nlev, nboxes, ndeg, ng
      integer levelbox(1), icolbox(1)
      integer iparentbox(1), ichildbox(2,1)
      integer nblevel(0:1), iboxlev(1), istartlev(0:1)
      real*8 pot(ndeg,nboxes)
      real*8 xg(ng), fg(ng)
      real*8 cent0, xsize0
c     local vars
      integer,allocatable:: npbox(:), ipold(:)
      integer,allocatable:: istartbox(:), ibofp(:)
      real*8 wsave(1000), chwork(1000)
      real*8, allocatable:: xpts(:), chpot(:)
c
      npts=ng
      allocate(xpts(npts))
      allocate(chpot(ndeg))
      allocate(npbox(nboxes))
      allocate(ipold(npts))
      allocate(istartbox(nboxes))
      allocate(ibofp(npts))
c
      do i=1,npts
        xpts(i)=xg(i)
      enddo
c
c     sort the grid points into the tree
      call treesort(nlev,levelbox,iparentbox,
     1     ichildbox,icolbox,nboxes,nblevel,
     2     iboxlev,istartlev,xpts,npts,npbox,
     3     ipold,istartbox,ibofp,cent0,xsize0)
c
c     go through leaf nodes, 
c     if nonempty, do chebyshev interpolation
c
      call chxcin(ndeg, wsave)
      do ib=1, nboxes
        npb=npbox(ib)
c        write(*,*) ib, npb
        if((ichildbox(1,ib).lt.0) .and. (npb.gt.0)) then
          call chexfc(pot(1,ib),ndeg,chpot,wsave,chwork)
c
          lev=levelbox(ib)
          icol=icolbox(ib)
  
          xlength=0.5d0**lev*xsize0
          x0=cent0-xsize0/2.0d0
c
          a=dble(icol-1)*xlength+x0
          b=a+xlength
c           basic parameters (of current box)
c           RMK: for a generic unit box, treesort needs to 
c                change, and these lines need to change
         
          istart=istartbox(ib) 
          iend=istart+npb-1
          do ip=istart, iend
            xx=xpts(ip) 
            call cheval(xx,val,chpot,ndeg,a,b)
            iold=ipold(ip)
            fg(iold)=val
          enddo
        endif
      enddo
      t2=second()
c      write(*,*) 't_interp=',t2-t1

      deallocate(xpts)
      deallocate(npbox)
      deallocate(ipold)
      deallocate(istartbox)
      deallocate(ibofp)
      deallocate(chpot)
c
c
      end subroutine





c-----------------------------------------------------
c     subroutine rvecold2new
c-----------------------------------------------------
c
c Reorder a vector from the old order to the new order
c given permutation ipold that maps new indices 
c to the old ones
c
c INPUT:
c ndim: dimension of the vector
c nvec: length of the vector
c rvec: the real vector  
c ipold: permutation vector that maps the new indices
c        to the old ones
c
c OUTPUT:
c rvec: rewritten in the old order
c
c-----------------------------------------------------
c
      subroutine rvecold2new(ndim, nvec, rvec, ipold)
      implicit real*8 (a-h,o-z)
      integer ndim, nvec
      integer ipold(nvec)
      real*8 rvec(ndim,nvec)    
      real*8, allocatable:: rold(:,:)
c
      allocate(rold(ndim,nvec))
c
      do i=1,nvec
      do j=1,ndim
        rold(j,i)=rvec(j,i)
      enddo
      enddo
c
      do i=1,nvec
        io=ipold(i)
        do j=1,ndim
          rvec(j,i)=rold(j,io)
        enddo
      enddo
c
      deallocate(rold)
      
      end subroutine






c-----------------------------------------------------
c     subroutine rvecnew2old
c-----------------------------------------------------
c
c Reorder a vector from the new order to the old order
c given permutation ipold that maps new indices 
c to the old ones
c
c INPUT:
c ndim: dimension of the vector
c nvec: length of the vector
c rvec: the real vector  
c ipold: permutation vector that maps the new indices
c        to the old ones
c
c OUTPUT:
c rvec: rewritten in the new order
c
c-----------------------------------------------------
c
      subroutine rvecnew2old(ndim, nvec, rvec, ipold)
      implicit real*8 (a-h,o-z)
      integer ndim, nvec
      integer ipold(nvec)
      real*8 rvec(ndim,nvec)    
      real*8, allocatable:: rnew(:,:)
c
      allocate(rnew(ndim,nvec))
c
      do i=1,nvec
      do j=1,ndim
        rnew(j,i)=rvec(j,i)
      enddo
      enddo
c
      do i=1,nvec
        io=ipold(i)
        do j=1,ndim
          rvec(j,io)=rnew(j,i)
        enddo
      enddo
c
      deallocate(rnew)
      
      end subroutine





c-----------------------------------------------------
c     subroutine setf
c-----------------------------------------------------
c     this subroutine defines the function value array
c     given a function and a tree struct
c
c     input:
c
c     levelbox is an array determining the level of each box
c     nboxes is the total number of boxes
c     icolbox denotes the column of each box
c     ichildbox denotes the four children of each box
c     nlev is the finest level
c     iboxlev is the array in which the boxes are arranged
c     nblevel is the total number of boxes per level
c     istartlev is the pointer to where each level begins in the
c               iboxlev array
c     h is the real function that is the right hand side
c       of the poisson equation
c
c     output:
c
c     fright is the right hand side defined on the tree
c
c-----------------------------------------------------
c
      subroutine setf(ndeg, fright, icolbox, ichildbox,
     1           nlev, nblevel, iboxlev, istartlev, h,
     2           cent0, xsize0)
      implicit real*8 (a-h,o-z)
c-----global variables
      integer  ndeg
      integer  icolbox(1), nlev
      integer  ichildbox(2,1)
      integer  nblevel(0:1), iboxlev(1), istartlev(0:1)
      real*8  fright(ndeg,1), xf(ndeg)
      real*8 cent0, xsize0, x0, y0
c-----external functions
      real*8 h
c
      do l=0,nlev
        istart=istartlev(l)
        iend=istartlev(l)+nblevel(l)-1
        do i=istart, iend
          ibox=iboxlev(i)
          if(ichildbox(1,ibox) .lt. 0) then
            call mkgrid(ndeg,xf,icolbox(ibox),l,cent0,
     1           xsize0)
            do k=1,ndeg
              fright(k,ibox)=h(xf(k))
            enddo
          endif
        enddo 
      enddo


      end subroutine






c--------------------------------------------------
c     subroutine fixtreenf
c--------------------------------------------------
c
c     the following subroutine is designed to take a correctly defined
c     tree and alter it so that no two boxes that contact each other
c     are more than one level apart. At the same time, 
c     it interpolates a function value array to the
c     newly added boxes
c
c     input:
c     levelbox - maxboxes: same as in fixtree
c     ndeg: degree of function approx
c     fval: function value array (defined on leaf nodes
c           of the tree) 
c
c     output:
c     both the tree and fval may be altered
c
c--------------------------------------------------
c
      subroutine fixtreenf(levelbox,iparentbox,
     1           ichildbox,icolbox,icolleagbox,
     2           nboxes,nlev,nblevel,iboxlev,
     3           istartlev,iperiod,iflag,maxboxes,
     4           ndeg,fval)
      implicit none
c-----global variables
      integer levelbox(1), icolleagbox(3,1)
      integer maxboxes, ndeg
      integer iparentbox(1), ichildbox(2,1)
      integer icolbox(1)
      integer nboxes, nlev
      integer nblevel(0:1), iboxlev(1), istartlev(0:1)
      integer iperiod
      real*8 fval(ndeg, 1)
c-----local variables
      integer iflag(maxboxes)
      integer ichild(2),icoll(3), ibox
      integer i, ipar, itest, j, nb
      integer itemp, ntemp, jcntr, icntr
      integer start, istop, ik1, ik2
c
c     let's sort all of the boxes by level.
c     this takes the place of the ladder structure
c     in the uniform case.  all boxes are sorted
c     into the array and placed in their proper places.
      call sortboxes(levelbox,nboxes,nlev,
     1     nblevel,iboxlev,istartlev)
c
c     first let's call a subroutine that will
c     generate all of the colleagues for each
c     box.  the colleagues are generated in the
c     correct order so there is no need to 'shuffle'
c     them later on.
      call mkcolls(icolbox,
     1     icolleagbox,nboxes,nlev,
     2     iparentbox,ichildbox,nblevel,
     3     iboxlev, istartlev,iperiod)
c
c     let's initialize all of the flags to zero.
      do i = 1, maxboxes
        iflag(i) = 0
      enddo
c
c     find all of the boxes that need to be
c     flagged.  a flagged box will be denoted by 
c     setting iflag(box) = 1.
c     this refers to any box that is directly touching 
c     a box that is more than one level smaller than
c     it.  it is found by performing an upward pass
c     and looking a box's parents parents and seeing
c     if they are childless and contact the given box.
c     note that we only need to get up to level two, as
c     we will not find a violation at a coarser level
c     than that.
c
      do i=nlev, 2, -1
        do j=istartlev(i),istartlev(i)+nblevel(i)- 1
          ibox = iboxlev(j)
          ipar  = iparentbox(ibox)
          itest = iparentbox(ipar)
c
          icoll(1) = icolleagbox(1,itest)
          icoll(2) = icolleagbox(2,itest)
          icoll(3) = icolleagbox(3,itest)
c
          ichild(1) = ichildbox(1,itest)
          ichild(2) = ichildbox(2,itest)
c
          do nb=1, 3
            itemp=icoll(nb)
            if(ichildbox(1,itemp) .lt. 0) then
c             the neighboring box is not divided
c             we could have problems.
              if(nb .eq. 1) then
                if(ipar .eq. ichild(1)) then
                  iflag(itemp)=1
                endif
              elseif(nb .eq. 3) then
                if(ipar .eq. ichild(2)) then
                  iflag(itemp)=1
                endif
              endif
            endif
          enddo
        enddo
      enddo
c
c     find all of the boxes that need to be
c     given a flag+.  a flag+ box will be denoted by 
c     setting iflag(box) = 2.
c     this refers to any box that is not already flagged
c     and is bigger than and is contacting a flagged box
c     or another box that has already been given a flag+.
c     it is found by performing an upward pass
c     and looking at a flagged box's parents colleagues
c     and a flag+ box's parents colleagues and seeing if
c     they are childless and present the case where a 
c     bigger box is contacting a flagged or a flag+ box.
c
      do i=nlev, 2, -1
        do j=istartlev(i),istartlev(i)+nblevel(i)- 1
          ibox=iboxlev(j)
          if(iflag(ibox) .eq. 1 .or. iflag(ibox) .eq. 2)then
            ipar=iparentbox(ibox)
            icoll(1) = icolleagbox(1,ipar)
            icoll(2) = icolleagbox(2,ipar)
            icoll(3) = icolleagbox(3,ipar)
c
            ichild(1) = ichildbox(1,ipar)
            ichild(2) = ichildbox(2,ipar)
c
            do nb=1, 3
              itemp=icoll(nb)
c             let's check using the same criteria as above, but noting that
c             a flag will take precedence over a flag+.
              if(ichildbox(1,itemp) .lt. 0 
     1           .and. iflag(itemp) .ne. 1)then
                if(nb .eq. 1) then
                  if(ibox .eq. ichild(1)) then
                    iflag(itemp)=2
                  endif
                elseif(nb .eq. 3) then
                  if(ibox .eq. ichild(2)) then
                    iflag(itemp)=2
                  endif
                endif
              endif
            enddo
          endif
        enddo
      enddo
c
c     done with flagging
c
c     now let's divide the boxes that need to be immediately
c     divided up.  all of the flagged and flag+ boxes need to
c     be divided one time.  the distinction lies in the fact
c     that the children of a flag+ box will never need to be
c     divided but the children of a flagged box may need to 
c     be divided further.
c     below, all flagged and flag+ boxes are divided once.  the
c     children of a flag+ box are left unflagged while those of
c     the flagged boxes are given a flag++ (denoted by setting
c     iflag(box) = 3) which will be needed in the downward pass.     
c
      ntemp = nboxes
      do i = 1, ntemp
c      divide flagged boxes:
       if (iflag(i) .eq. 1)then

         if(ichildbox(1,i) .lt. 0)then
         call subdivide(i,levelbox,icolbox,nboxes,
     1        nlev,iparentbox,ichildbox,nblevel, 
     2        iboxlev,istartlev,icolleagbox,iperiod)
c------------------------
c        after calling subdivide or subdivide1, assign fval
         ik1=ichildbox(1,i)
         ik2=ichildbox(2,i)
         call chebyp2k(ndeg,fval(1,i),fval(1,ik1),
     1                 fval(1,ik2))
c------------------------
         endif


c        give flag++ to children of flagged boxes.
         itemp = ichildbox(1,i)
         iflag(itemp) = 3

         itemp = ichildbox(2,i)
         iflag(itemp) = 3
c
c
c      divide flag+ boxes.
       elseif (iflag(i) .eq. 2)then
         if(ichildbox(1,i) .lt. 0)then
         call subdivide(i,levelbox,icolbox,nboxes,
     1        nlev,iparentbox,ichildbox,nblevel, 
     2        iboxlev,istartlev,icolleagbox,iperiod)
c------------------------
c        after calling subdivide or subdivide1, assign fval
         ik1=ichildbox(1,i)
         ik2=ichildbox(2,i)
         call chebyp2k(ndeg,fval(1,i),fval(1,ik1),
     1                 fval(1,ik2))
c------------------------
         endif
       endif
      end do 
c
c     now we need to do a downward pass.
c     we will concern ourselves only with the children of
c     flagged boxes and their children.  at each level,
c     for each flag++ box, test colleagues children and see
c     if they have children that are contacting you.  if so,
c     divide and flag++ all children that are created.     
c
      do i = 0, nlev
      ntemp = nboxes
      start = istartlev(i)
      istop  = istartlev(i) + nblevel(i) - 1
      do 500 j = start, istop
       ibox = iboxlev(j)
c      only be concerned with boxes on this level and
c      boxes that are given a flag++:
       if(iflag(ibox) .ne. 3)goto 500

         icoll(1) = icolleagbox(1,ibox)
         icoll(2) = icolleagbox(2,ibox)
         icoll(3) = icolleagbox(3,ibox)
c
c       scan colleagues.
        do 400 jcntr = 1, 3
        if(icoll(jcntr) .lt. 0)goto 400
        if(ichildbox(1,icoll(jcntr)) .lt. 0)goto 400

         ichild(1) = ichildbox(1,icoll(jcntr))
         ichild(2) = ichildbox(2,icoll(jcntr))

c          scan colleague's children.
cccccc
           do 300 icntr = 1, 2

           if (ichildbox(1,ichild(icntr)) .lt. 0)goto 300 
           if(jcntr .eq. 1 .and. icntr .eq. 2)then

c           call subdivide
         if(ichildbox(1,ibox) .lt. 0)then
            call subdivide(ibox,levelbox,icolbox,
     1      nboxes,nlev,iparentbox,ichildbox,
     2      nblevel,iboxlev,istartlev,icolleagbox,iperiod)
c------------------------
c          after calling subdivide or subdivide1, assign fval
           ik1=ichildbox(1,ibox)
           ik2=ichildbox(2,ibox)
           call chebyp2k(ndeg,fval(1,ibox),fval(1,ik1),
     1                  fval(1,ik2))
c------------------------


         endif

c           flag++ all children created
            itemp = ichildbox(1,ibox)
            iflag(itemp) = 3
            itemp = ichildbox(2,ibox)
            iflag(itemp) = 3
c
           elseif(jcntr .eq. 3 .and. 
     1        icntr .eq. 1 )then

c           call subdivide
         if(ichildbox(1,ibox) .lt. 0)then
            call subdivide(ibox,levelbox,icolbox,
     1        nboxes,nlev,iparentbox,ichildbox,
     2        nblevel,iboxlev,istartlev,icolleagbox,iperiod)
c------------------------
c          after calling subdivide or subdivide1, assign fval
           ik1=ichildbox(1,ibox)
           ik2=ichildbox(2,ibox)
           call chebyp2k(ndeg,fval(1,ibox),fval(1,ik1),
     1                  fval(1,ik2))
c------------------------
         endif

c           flag++ all children created
            itemp = ichildbox(1,ibox)
            iflag(itemp) = 3
            itemp = ichildbox(2,ibox)
            iflag(itemp) = 3

           endif
300     continue   
400     continue
500     continue
      end do


      end subroutine






c--------------------------------------------------
c     subroutine coarsen1
c--------------------------------------------------
c
c     this subroutine merges the two kids of a given box
c     where all kids are leaf nodes
c     (Attention: this version doesn't alter icolleaguebox!)
c
c     INPUT:
c     iparbox denotes the box whose kids are being merged
c     iparentbox denotes the parent of each box
c     ichildbox denotes the four children of each box
c     nboxes is the total number of boxes
c     icolbox denotes the column of each box
c     levelbox is an array determining the level of each box
c     nlev is the finest level
c     istartlev is the pointer to where each level begins in the
c               iboxlev array
c     nblevel is the total number of boxes per level
c     iboxlev is the array in which the boxes are arranged
c
c---------------------------
c     iempty: array of length nboxes, binary indicator:
c             iempty = 1: empty box
c             iempty = 0: not empty
c
c     OUTPUT:
c     iparentbox, ichildbox, and iempty are altered
c     to reflect the changes
c
c--------------------------------------------------
      subroutine coarsen1(iparbox,iparentbox,ichildbox,
     1           nboxes,icolbox,levelbox,nlev,istartlev,
     2           nblevel,iboxlev,iempty)
      implicit real*8 (a-h,o-z)
c     global vars
      integer nboxes, nlev, iparbox
      integer iparentbox(1), ichildbox(2,1)
      integer icolbox(1), levelbox(1)
      integer istartlev(0:1), nblevel(0:1), iboxlev(1)
      integer iempty(1)
c
      kid1=ichildbox(1,iparbox)
      kid2=ichildbox(2,iparbox)
c
      ichildbox(1,iparbox)=-1
      ichildbox(2,iparbox)=-1
c
      iparentbox(kid1)=-1 
      iparentbox(kid2)=-1 
c
      iempty(kid1)=1
      iempty(kid2)=1

      end subroutine





c-----------------------------------------------------
c     subroutine reorder
c-----------------------------------------------------
c
c     This subroutine cleans up an intermediate tree, to get rid of
c     empty boxes and reassigns box numbers
c
c     INPUT:
c     iparentbox denotes the parent of each box
c     ichildbox denotes the four children of each box
c     nboxes is the total number of boxes
c     icolbox denotes the column of each box
c     levelbox is an array determining the level of each box
c     nlev is the finest level
c     istartlev is the pointer to where each level begins in the
c               iboxlev array
c     nblevel is the total number of boxes per level
c     iboxlev is the array in which the boxes are arranged
c---------------------------
c     iempty: array of length nboxes, binary indicator:
c             iempty = 1: empty box
c             iempty = 0: not empty
c     itemp: temporary working array, length>= nboxes
c     itemp2: temporary working array, dimension(2,nboxes)
c
c     OUTPUT:
c     all the tree structure may be altered to reflect the change
c     idold2new: maps old box id to new box id
c      
c-----------------------------------------------------
c
      subroutine reorder(iparentbox, ichildbox, nboxes, 
     1           icolbox, levelbox, nlev,
     2           istartlev, nblevel, iboxlev, iempty, 
     3           idold2new, nboxesold, itemp, itemp2)
      implicit real*8 (a-h,o-z)
c     global vars
      integer nboxes, nlev, iparbox
      integer iparentbox(1), ichildbox(2,1)
      integer icolbox(1), levelbox(1)
      integer istartlev(0:1), nblevel(0:1), iboxlev(1)
      integer iempty(1), itemp(1), itemp2(2,1)
      integer idold2new(nboxes)
c
c------------------------------
c
c     First, go through the boxes, generate the new id for
c     each box, store them in array idold2new. at the same 
c     time, determine the actual number of boxes on each 
c     level, rewrite nblevel
c
      nshift=0
      do i=1,nboxes
        if(iempty(i) .eq. 0) then
c         box i is not empty
          idold2new(i)=i-nshift
        elseif(iempty(i) .eq. 1) then
c         box i is empty
          nshift=nshift+1
          idold2new(i)=-1 
          lev=levelbox(i)
          nblevel(lev)=nblevel(lev)-1                        
c           update the number of boxes on the current level
        endif
      enddo
      nempty=nshift
c      write(*,*) 'nempty=',nempty
c       got the new number of boxes
c       and updated nblevel
c
c---------------------------------
c
c     second, update the tree structure, using the new numbering      
c
c     1. update iparentbox
c
      do i=1,nboxes
        itemp(i)=iparentbox(i)
      enddo
c
      do iold=1, nboxes
        if(iempty(iold) .eq. 0) then
          jold=itemp(iold)
          i=idold2new(iold)
          if(jold .gt. 0) then
            j=idold2new(jold)
          else
            j=-1
          endif
          iparentbox(i)=j
        endif
      enddo      
c
c---------------------------------
c
c     2. update ichildbox
c
      do i=1,nboxes
        itemp2(1,i)=ichildbox(1,i)
        itemp2(2,i)=ichildbox(2,i)
      enddo
c
      do iold=1, nboxes
        if(iempty(iold) .eq. 0) then
          jold1=itemp2(1,iold)
          jold2=itemp2(2,iold)
c
          i=idold2new(iold) 
          if(jold1 .gt. 0) then
            j1=idold2new(jold1)
            j2=idold2new(jold2)
          else
            j1=-1
            j2=-1
          endif
c
          ichildbox(1,i)=j1
          ichildbox(2,i)=j2
        endif
      enddo 
c
c---------------------------------
c
c     3. update icolbox, and levelbox
c
c
      do i=1,nboxes
        itemp(i)=icolbox(i)
      enddo 
c
      do iold=1,nboxes
        if(iempty(iold) .eq. 0) then
          i=idold2new(iold) 
          icolbox(i)=itemp(iold)
        endif
      enddo
c
c----------------------------
c
      do i=1,nboxes
        itemp(i)=levelbox(i)
      enddo
c
      do iold=1,nboxes
        if(iempty(iold) .eq. 0) then
          i=idold2new(iold)
          levelbox(i)=itemp(iold)
        endif
      enddo
c
c---------------------------------
c
c     4. update istartlev, nblevel, iboxlev 
c
c     4.1 use the new numbering of boxes in: iboxlev
c     
      do i=1,nboxes
        iold=iboxlev(i)
        if(iempty(iold) .eq. 0) then  
          inew=idold2new(iold)
          itemp(i)=inew
        else
          itemp(i)=-1
        endif
      enddo
c
c     4.2 update istartlev, nblevel, and iboxlev.
c         shift boxes in iboxlev first, and then
c         count istartlev and nblevel
c
      nshift=0
      do i=1, nboxes
        if(itemp(i) .gt. 0) then
          iboxlev(i-nshift)=itemp(i)
        else
          nshift=nshift+1
        endif
      enddo
c
c     now count and overwrite istartlev and nblevel      
c
cccccc
c      write(*,*) nboxes, nempty, nboxes-nempty
c      do ii=1,nboxes
c        write(*,*) ii, idold2new(ii)
c      enddo
cccccc
c
      nboxesold=nboxes
      nboxes=nboxes-nempty
      do i=1, nboxes
        iempty(i)=0
      enddo
c           now that almost everything has been overwritten
c           let's finally change nboxes
c
      levpre=0
      istartlev(0)=1
c
      do i=1,nboxes
        ibox=iboxlev(i)
        lev=levelbox(ibox)
        if(lev .ne. levpre) then
          istartlev(lev)=i
          levpre=lev
        endif 
      enddo
c       this is enough since the 'ladder' ordering is preserved
c       
c     one last thing, update nlev
c     (there could be levels at the end disappearing)
c
      nlevnew=nlev
      do l=0,nlev
        if(nblevel(l) .le. 0) then
          nlevnew=nlevnew-1 
        endif
      enddo
      nlev=nlevnew
c

      end subroutine





c-----------------------------------------------------
c     subroutine reorderf
c-----------------------------------------------------
c
c     Given an array fval, and a reordering of box ids
c     reorder the array accordingly
c
c-----------------------------------------------------
c
      subroutine reorderf(na, nold, fval, idold2new)
      implicit real*8 (a-h,o-z)
      integer na, nboxes
      integer idold2new(nold)
      real*8 fval(na, nold)
      real*8, allocatable:: ftemp(:,:)
c
      allocate(ftemp(na,nold))

c     save the old array in ftemp
      do j=1,nold
        do i=1,na 
          ftemp(i,j)=fval(i,j)
        enddo
      enddo
c
      do j=1,nold
        jnew=idold2new(j)
        if(jnew .gt. 0) then
          do i=1,na
            fval(i,jnew)=ftemp(i,j)
          enddo
        endif
      enddo

      deallocate(ftemp)

      end subroutine





c**********************************************************
c
c     Given a tree and an array of points sorted
c     in the tree (leaf nodes), this subroutine returns the 
c     relative coordinates w.r.t. box centers
c
c     INPUT:
c     nlev - istartlev: the tree
c     xpts - ibofp: the points and point-tree relation
c
c     OUTPUT:
c     xrel: relative coordinate of each point w.r.t. the 
c           center of the leaf box it belongs to
c
c**********************************************************
c
      subroutine get_rel_coords1d(nlev,levelbox,iparentbox,
     1           ichildbox,icolbox,nboxes,nblevel,
     2           iboxlev,istartlev,xpts,npts,npbox,ipold,
     3           istartbox,xrel,cent0,xsize0)
      implicit real*8 (a-h,o-z)
      integer nlev, nboxes, npts
      integer levelbox(nboxes), iparentbox(nboxes)
      integer ichildbox(2,nboxes)
      integer icolbox(nboxes)
      integer nblevel(0:nlev), iboxlev(nboxes)
      integer istartlev(0:nlev)
      integer npbox(nboxes), ipold(npts)
      integer istartbox(nboxes)
      real*8 xpts(npts), xrel(npts)
      real*8 cent0, xsize0
c
      do i=1,npts
        xrel(i)=0.0d0
      enddo
c     initialize to zero
c
      do i=1,nboxes
        ic=ichildbox(1,i)
        np=npbox(i)
        if((ic .lt. 0) .and. (np .gt. 0)) then
c       go through non-empty leaf boxes
          call get_cent_box1d(cx,cent0,xsize0,icolbox(i),
     1         levelbox(i)) 
          istart=istartbox(i)
          iend=istart+npbox(i)-1
          do ii=istart, iend
            xrel(ii)=xpts(ii)-cx
          enddo 
        endif
      enddo

      end subroutine
c





c**********************************************************
c     This subroutine returns the center of a given box in the
c     tree (assuming that the unit box is centered at the 
c     origin)
c
c     INPUT:
c     xsize0: side length of the root box
c     icol: column index of the box
c     irow: row index of the box
c     level: level of the box
c
c     OUTPUT:
c     (cx, cy): coordinates of the center of the box
c
c**********************************************************
c
      subroutine get_cent_box1d(cx,cent0,xsize0,icol,level)
      implicit real*8 (a-h,o-z)
      integer icol, level
      real*8 cx, xsize0, cent0
c
      xlength = xsize0 / dble(2**level)
      x0 = cent0-xsize0/2.0d0
      cx = x0 + (icol-0.5d0)*xlength

      end subroutine
c
c




c******************************************************
c     subroutine mktreep1
c******************************************************
c
c     This subroutine is used to generate a tree given
c     a discrete set of points. The algorithm works by
c     refining until there's no more than a given
c     number of points in each leaf box. Points are
c     reordered on output. The original id is saved.
c
c     INPUT:
c     maxboxes: the maximum number of boxes allowed
c     maxlevel: deepest level allowed
c     xsizemax: maximum size for leaf boxes containing
c               points
c     npts: number of points
c     xpts: coordinates of points
c     itemparray: a dummy array to be used in 
c             subroutine subdivide1, which is called within
c     cent0: center of the root box
c     xsize0: side length of the root box 
c     
c     OUTPUT: 
c     (tree structure:)
c     istartlev is the pointer to where each level
c               begins in the iboxlev array
c     levelbox is an array determining the level of each box
c     nboxes is the total number of boxes
c     nlev is the finest level
c     icolbox denotes the column of each box
c     iparentbox denotes the parent of each box
c     ichildbox denotes the four children of each box
c     nblevel is the total number of boxes per level
c     iboxlev is the array in which the boxes are arranged
c
c     (tree-point relation:)
c     npbox: number of sources in each box 
c                     (including non-leaf boxes)
c     ipold: ids of points on input
c            (before the overwriting)
c     istartbox: starting point in the array xpts
c                for each box
c     ibofp: indices of leaf boxes that the point
c            belongs to (after overwriting)
c
c     (xpts: overwritten, reordered so that points in
c            a box are stored in adjacent places)
c
c
c*****************************************************
c
      subroutine mktreep1(levelbox,icolbox,nboxes,
     1           nlev,iparentbox,ichildbox,nblevel,
     2           iboxlev,istartlev,npbox,ipold,
     3           istartbox,ibofp,maxboxes,itemparray,
     4           maxlevel,xsizemax,npts,xpts,
     5           cent0,xsize0)
      implicit real*8 (a-h,o-z)
c-----global variables
      integer nboxes, nlev, maxboxes, maxlevel
      integer maxppl, npts
      integer levelbox(1)
      integer icolbox(1)
      integer iparentbox(1), ichildbox(2,1)
      integer nblevel(0:1), iboxlev(1), istartlev(0:1)
      integer itemparray(1)
      integer npbox(1), ipold(1), istartbox(1), ibofp(1)
      real*8 xpts(npts)
      real*8 cent, xsize0
c-----local variables
      integer nsinchild(2)
      integer, allocatable:: isinchild(:,:)
      integer, allocatable:: itmp(:)
      real*8, allocatable:: xtmp(:)
c
c-----------------------------------------------------
c
      allocate(isinchild(npts,2))
      allocate(itmp(npts))
      allocate(xtmp(npts))
c
      do i = 0, maxlevel
        nblevel(i) = 0
        istartlev(i) = 0
      end do
      do i = 1, maxboxes
        iboxlev(i) = 0
      end do
c
c     first set the big parent box to the 
c     appropriate settings:
c     (initially, there is just one box and
c     it is the big parent box at level 0)
      ibox = 1
      levelbox(ibox) = 0
      icolbox(ibox) = 1
      iparentbox(ibox) = -1
      ichildbox(1,ibox) = -1
      ichildbox(2,ibox) = -1

      nboxes = 1
      nlev = 0

      nblevel(0) = 1
      istartlev(0) = 1
      iboxlev(1) = 1
c
c-----------------------
c
      do i=1,nboxes
        npbox(i)=0
        istartbox(i)=-1
      enddo 
      
      do j=1,npts
        ipold(j)=j
      enddo

      npbox(1)=npts
      istartbox(1)=1
c
c         the root box contains all the points
c
c------------------------------------
c
      ix=1
      iy=1
      xlength=xsize0
      x0=cent0-xsize0/2.0d0
c
      nside=1
c
      do i=0, maxlevel-1
        iflag=0
        xlength=xlength/2.0d0
        nside=nside*2
c       xlength and nside are on the children level
c
        istart=istartlev(i)
        iend=istart+nblevel(i)-1
        do j=istart, iend
          ibox=iboxlev(j)
          if((npbox(ibox) .gt. 0).and.
     1       (2.0d0*xlength .gt. xsizemax)) then
c           call subdivide1 to subdivide the current box
          call subdivide1(ibox, levelbox, icolbox,
     1         nboxes, nlev, iparentbox, ichildbox,
     2         nblevel, iboxlev, istartlev)
c
c           sort points in ibox into its newborn babies
c           attention: this time physically rewrite
c           the array xpts so that points in the same
c           box are stored in adjacent places
c
            iss=istartbox(ibox)
            ies=istartbox(ibox)+npbox(ibox)-1
c
            do kk=1,2
              nsinchild(kk)=0
c             initialize the number of points in each kid
c             to be zero
              ic=ichildbox(kk,ibox)
              npbox(ic)=0
            enddo
c
            do jj=iss,ies
              xx=xpts(jj)
c
              xtmp(jj)=xx
              itmp(jj)=ipold(jj)
c                 iss-th to ies-th entries of xpts
c                 and ipold copied to xtmp and itmp at
c                 corresponding places
c
              ix=ceiling((xx-x0)/xlength)
              if(ix.le.0) then
                ix=1
              endif

              if(ix.gt.nside) then
                ix=nside
              endif
c
              do kk=1,2
                ic=ichildbox(kk,ibox)
                if(icolbox(ic).eq.ix) then
                  nsinchild(kk)=nsinchild(kk)+1
                  npbox(ic)=npbox(ic)+1
                  isinchild(nsinchild(kk),kk)=jj
                endif
              enddo
            enddo
cccccc------
c
c         all points in box ibox sorted into isinchild,
c         indicating which child the points belong to. 
c         now overwrite the iss-th to ies-th spots in
c         xpts and ipold together
c
cccccc------
            jpt=iss
            do jc=1,2
              ic=ichildbox(jc,ibox)
              istartbox(ic)=jpt
              do js=1,nsinchild(jc)
                idnow=isinchild(js,jc)
                xpts(jpt)=xtmp(idnow)  
                ipold(jpt)=itmp(idnow)
                jpt=jpt+1
              enddo
            enddo
c           done with box: ibox
c--------------
            iflag=1
          endif
        enddo
       
        if(iflag .eq. 0) then
c         nothing was divides at the last level,
c         exit the loop (on levels)
          goto 123
        endif
c
      enddo
c
c     now get information for ibofp
123   do l=0,nlev
        istart=istartlev(l)
        iend=istartlev(l)+nblevel(l)-1
        do ii=istart,iend
          ibox=iboxlev(ii)
          if((ichildbox(1,ibox).lt.0).and.(npbox(ibox).gt.0)) then
            iss=istartbox(ibox)
            ies=iss+npbox(ibox)-1
            do kk=iss,ies
              ibofp(kk)=ibox
            enddo
          endif
        enddo
      enddo
c
      deallocate(isinchild)
      deallocate(itmp)
      deallocate(xtmp)

      return 
      end subroutine





c******************************************************
c     subroutine refinetree1
c******************************************************
c
c     given a binary tree and a set of points this
c     subroutine refines the tree until leaf boxes
c     containing points are no more than a given size.
c     Points are reordered on output. The original
c     id is saved.
c
c     INPUT:
c     maxboxes: the maximum number of boxes allowed
c     maxlevel: deepest level allowed
c     xsizemax: maximum size for leaf boxes containing
c               points
c     npts: number of points
c     xpts: coordinates of points
c     itemparray: a dummy array to be used in 
c             subroutine subdivide1, which is called within
c     cent0: center of the root box
c     xsize0: side length of the root box 
c     
c     OUTPUT: 
c     (tree structure:)
c     istartlev is the pointer to where each level
c               begins in the iboxlev array
c     levelbox is an array determining the level of each box
c     nboxes is the total number of boxes
c     nlev is the finest level
c     icolbox denotes the column of each box
c     iparentbox denotes the parent of each box
c     ichildbox denotes the four children of each box
c     nblevel is the total number of boxes per level
c     iboxlev is the array in which the boxes are arranged
c
c     (tree-point relation:)
c     npbox: number of sources in each box 
c                     (including non-leaf boxes)
c     ipold: ids of points on input
c            (before the overwriting)
c     istartbox: starting point in the array xpts
c                for each box
c     ibofp: indices of leaf boxes that the point
c            belongs to (after overwriting)
c
c     (xpts: overwritten, reordered so that points in
c            a box are stored in adjacent places)
c
c
c******************************************************
c
      subroutine refinetree1(levelbox,icolbox,nboxes,
     1           nlev,iparentbox,ichildbox,nblevel,
     2           iboxlev,istartlev,npbox,ipold,
     3           istartbox,ibofp,maxboxes,itemparray,
     4           maxlevel,xsizemax,npts,xpts,
     5           cent0,xsize0)
      implicit real*8 (a-h,o-z)
c-----global variables
      integer nboxes, nlev, maxboxes, maxlevel
      integer maxppl, npts
      integer levelbox(1)
      integer icolbox(1)
      integer iparentbox(1), ichildbox(2,1)
      integer nblevel(0:1), iboxlev(1), istartlev(0:1)
      integer itemparray(1)
      integer npbox(1), ipold(1), istartbox(1), ibofp(1)
      real*8 xpts(npts)
      real*8 cent, xsize0
c-----local variables
      integer nsinchild(2)
      integer, allocatable:: isinchild(:,:)
      integer, allocatable:: itmp(:)
      real*8, allocatable:: xtmp(:)
c
c-----------------------------------------------------
c
      allocate(isinchild(npts,2))
      allocate(itmp(npts))
      allocate(xtmp(npts))
c
      ix=1
      xlength=xsize0
      x0=cent0-xsize0/2.0d0
      nside=1
c
      do i=0,maxlevel-1
        iflag=0
c           iflag: if the last level has been divided at all
        xlength=xlength/2.0d0
        nside=nside*2
c
        istart=istartlev(i)
        iend=istart+nblevel(i)-1
        do j=istart, iend
          ibox=iboxlev(j)
          if(ichildbox(1,ibox) .lt. 0) then
          if((npbox(ibox) .gt. 0).and.
     1       (2.0d0*xlength .gt. xsizemax)) then
c           call subdivide1 to subdivide the current box
          call subdivide1(ibox, levelbox, icolbox,
     1         nboxes, nlev, iparentbox, ichildbox,
     2         nblevel, iboxlev, istartlev)
c
c           sort points in ibox into its newborn babies
c           attention: this time physically rewrite
c           the array xpts so that points in the same
c           box are stored in adjacent places
c
            iss=istartbox(ibox)
            ies=istartbox(ibox)+npbox(ibox)-1
c
            do kk=1,2
              nsinchild(kk)=0
c             initialize the number of points in each kid
c             to be zero
              ic=ichildbox(kk,ibox)
              npbox(ic)=0
            enddo
c
            do jj=iss,ies
              xx=xpts(jj)
c
              xtmp(jj)=xx
              itmp(jj)=ipold(jj)
c                 iss-th to ies-th entries of xpts
c                 and ipold copied to xtmp and itmp at
c                 corresponding places
c
              ix=ceiling((xx-x0)/xlength)
              if(ix.le.0) then
                ix=1
              endif

              if(ix.gt.nside) then
                ix=nside
              endif
c
              do kk=1,2
                ic=ichildbox(kk,ibox)
                if(icolbox(ic).eq.ix) then
                  nsinchild(kk)=nsinchild(kk)+1
                  npbox(ic)=npbox(ic)+1
                  isinchild(nsinchild(kk),kk)=jj
                endif
              enddo
            enddo
cccccc------
c
c         all points in box ibox sorted into isinchild,
c         indicating which child the points belong to. 
c         now overwrite the iss-th to ies-th spots in
c         xpts and ipold together
c
cccccc------
            jpt=iss
            do jc=1,2
              ic=ichildbox(jc,ibox)
              istartbox(ic)=jpt
              do js=1,nsinchild(jc)
                idnow=isinchild(js,jc)
                xpts(jpt)=xtmp(idnow)  
                ipold(jpt)=itmp(idnow)
                jpt=jpt+1
              enddo
            enddo
c           done with box: ibox
c--------------
            iflag=1
          endif
          endif
        enddo

        if((iflag .eq. 0).and.(i .eq. nlev)) then
c         nothing was divides at the last level,
c         exit the loop (on levels)
          goto 234
        endif
      enddo
c
c     now get information for ibofp
234   do l=0,nlev
        istart=istartlev(l)
        iend=istartlev(l)+nblevel(l)-1
        do ii=istart,iend
          ibox=iboxlev(ii)
          if((ichildbox(1,ibox).lt.0).and.(npbox(ibox).gt.0)) then
            iss=istartbox(ibox)
            ies=iss+npbox(ibox)-1
            do kk=iss,ies
              ibofp(kk)=ibox
            enddo
          endif
        enddo
      enddo
c
      deallocate(isinchild)
      deallocate(itmp)
      deallocate(xtmp)

      end subroutine
     




c******************************************************
c     subroutine refinelv1
c******************************************************
c
c     Given a binary tree, refine by one level
c
c     input:
c     istartlev is the pointer to where each level
c               begins in the iboxlev array
c     levelbox is an array determining the level of each box
c     nboxes is the total number of boxes
c     nlev is the finest level
c     icolbox denotes the column of each box
c     iparentbox denotes the parent of each box
c     ichildbox denotes the four children of each box
c     nblevel is the total number of boxes per level
c     iboxlev is the array in which the boxes are arranged
c     maxboxes: the max num of boxes
c
c     output:
c     the tree modified to reflect the change
c
c******************************************************
c
      subroutine refinelv1(levelbox, icolbox, nboxes,
     1           nlev, iparentbox, ichildbox,
     2           nblevel, iboxlev, istartlev, maxboxes)
c-----global variables
      implicit real*8 (a-h,o-z)
      integer nlev, nboxes, maxlevel, maxboxes
      integer levelbox(1), icolbox(1)
      integer iparentbox(1), ichildbox(2,1)
      integer nblevel(0:1), iboxlev(1)
      integer istartlev(0:1)
      integer, allocatable:: isnewbox(:)
c
      allocate(isnewbox(maxboxes))
      do i=1, maxboxes
        isnewbox(i)=0
      enddo
c
      do l=0, nlev
        istart=istartlev(l)
        iend=istart+nblevel(l)-1
        do ibox=istart, iend
          if((isnewbox(ibox) .le. 0) .and.
     1       (ichildbox(1,ibox) .le. 0)) then
            call subdivide1(ibox,levelbox,icolbox,
     1           nboxes,nlev,iparentbox,ichildbox,
     2           nblevel,iboxlev,istartlev)
            do j=1, 2
              ich=ichildbox(j,ibox) 
              isnewbox(ich)=1
            enddo
          endif
        enddo
      enddo


      deallocate(isnewbox)

      end subroutine





c-----------------------------------------------------
c     subroutine adaptree
c-----------------------------------------------------
c
c     This subroutine adaptively refines and coarsens
c     a binary tree so that the underlying function,
c     which is sampled on the original tree,
c     is resolved but not over-resolved
c
c     This subroutine consists of two 'sweeps':
c     1) a downward sweep to refine (until resolved)
c     2) an upward sweep to merge when over-resolved
c        (consider empty boxes only)
c
c
c     RMK: 1) level-restriction may be violated on output
c             call 'fixtree' after this 
c          2) all box ids may be changed due to coarsening
c          3) this subroutine is not optimal, but
c             sufficient for now. optimize when needed.
c             in particular, the 'merge' part is not 
c             stringent. (when an object moves across a
c             region, it may leave a bit of a tail, but
c             not for long)
c
c     INPUT:
c     maxboxes: the maximum number of boxes allowed
c     maxlevel: deepest level allowed
c     maxppl: the maximum number of boundary points allowed
c             in leaf box
c     levelbox - istartlev: the bintree structure
c     ndeg: degree of chebyshev approx 
c     fval: function values on the ndeg x ndeg grid of
c           the leaf nodes
c     eps: error tolerance of function approx
c
c     OUTPUT:
c     the tree overwritten to reflect the changes
c     the function values interpolated to the new 
c         leaf nodes of the tree
c
c-----------------------------------------------------
c
      subroutine adaptree(maxboxes, maxlevel, maxppl,
     1           levelbox, icolbox, irowbox, nboxes,
     2           nlev, iparentbox, ichildbox, nblevel,
     3           iboxlev, istartlev, ndeg, fval, eps)
      implicit real*8 (a-h,o-z)
c-----global variables
      integer maxboxes, maxlevel, maxppl
      integer nboxes, nlev, ndeg
      integer levelbox(1)
      integer icolbox(1)
      integer iparentbox(1), ichildbox(2,1)
      integer nblevel(0:1), iboxlev(1), istartlev(0:1)
      real*8 fval(ndeg,1), eps
      real*8 cent0, xsize0
c-----local variables
      integer kids(2)
      integer, allocatable:: iempty(:)
      integer, allocatable:: itemp(:), itemp2(:,:)
      integer, allocatable:: idold2new(:)
      real*8, allocatable:: ftemp(:), coefftemp(:)
      real*8, allocatable:: valk1(:), valk2(:)
      real*8 wsave(1000), chwork(1000)
c
      allocate(ftemp(ndeg))
      allocate(coefftemp(ndeg))
c
      allocate(iempty(maxboxes))
      allocate(itemp(maxboxes))
      allocate(itemp2(2,maxboxes))
      allocate(idold2new(maxboxes))
c
      allocate(valk1(ndeg))
      allocate(valk2(ndeg))
c
c-------------------------------------------------------
c     1) refinement sweep. go down the tree level by level
c        refine leaf boxes if unresolved
c-------------------------------------------------------
c
      call chxcin(ndeg,wsave)
      do i=0, maxlevel-1
        iflag=0
c          iflag: if the last level has been divided at all
        istart=istartlev(i)
        iend=istart+nblevel(i)-1
        do j=istart, iend
          ibox=iboxlev(j)
          if(ichildbox(1,ibox) .lt. 0) then
            do k=1, ndeg
              ftemp(k)=fval(k,ibox)
            enddo
c
            call getcoeff(ndeg,ftemp,coefftemp)
            call geterror(ndeg,coefftemp,error)
            hh = dble(2**levelbox(ibox))
            epscaled = eps * hh
c           check the resolution of the underlying fval
c
            if(error .ge. epscaled) then
              call subdivide1(ibox, levelbox, icolbox,
     1             nboxes, nlev, iparentbox, ichildbox,
     2             nblevel, iboxlev, istartlev)
c              after calling subdivide1, remember to 
c              assign interpolated function values to children
              ik1=ichildbox(1,ibox)
              ik2=ichildbox(2,ibox)
              call chebyp2k(ndeg,fval(1,ibox),
     1             fval(1,ik1),fval(1,ik2))
c             done with box: ibox
              iflag=1
            endif
          endif
        enddo
c
        if((iflag .eq. 0) .and. (i .eq. nlev)) then
c         nothing was divides at the last level,
c         exit the loop (on levels)
          goto 234
        endif
c     the end of the loop on levels
      enddo
c
c----------------------------------
c     2) an upward sweep to coarsen 
c----------------------------------
c
234   do ii=1,nboxes
        iempty(ii)=0
      enddo
c       before merging, there is no non-existant box
c       (with a slight abuse of language, 
c        we call them empty boxes)
c
      do i=nlev,0,-1
c       to merge, go up the tree level by level
        istart=istartlev(i)
        iend=istart+nblevel(i)-1
        do j=istart, iend
          ibox=iboxlev(j)
c         determine if children are all leaves
          nleafkids=0
          if(ichildbox(1,ibox) .gt. 0) then
            kids(1)=ichildbox(1,ibox)      
            kids(2)=ichildbox(2,ibox)      
            do k=1,2
              if(ichildbox(1,kids(k)).lt.0) then
                nleafkids=nleafkids+1
              endif
            enddo
          endif
c         got nleafkids
c
          if(nleafkids .eq. 2) then
c           both kids are leaves, do the following:
            do k=1, ndeg
              valk1(k)=fval(k,kids(1))
              valk2(k)=fval(k,kids(2))
            enddo
c
            call chebyk2p(ndeg, valk1, valk2, ftemp,
     1           coefftemp)
c
            call geterror(ndeg,coefftemp,error)
            hh = dble(2**levelbox(ibox))
            epscaled = eps * hh
c
            if(error .lt. epscaled) then
c              call coarsen1
              call coarsen1(ibox,iparentbox,ichildbox,
     1           nboxes,icolbox,levelbox,nlev,istartlev,
     2           nblevel,iboxlev,iempty)
              do k=1, ndeg
                fval(k,ibox)=ftemp(k)
              enddo
c             remember to assign interpolated function values
c             to the box that has become a leaf box
            endif
          endif
        enddo
      enddo
c
c------------------------------------------------------
c     3) clean up the intermediate tree with deleted
c        boxes, reorder fval at the same time
c------------------------------------------------------
c
      call reorder(iparentbox, ichildbox, nboxes, 
     1     icolbox, levelbox, nlev,
     2     istartlev, nblevel, iboxlev, iempty, 
     3     idold2new, nold, itemp, itemp2)
c
      call reorderf(ndeg, nold, fval, idold2new)
c
      deallocate(ftemp)
      deallocate(coefftemp)
c
      deallocate(iempty)
      deallocate(itemp)
      deallocate(itemp2)
c
      deallocate(valk1)
      deallocate(valk2)
      deallocate(idold2new)

      end subroutine





c--------------------------------------------------
c     subroutine adaptreef
c--------------------------------------------------
c
c     Given a tree, and a function resolved on the 
c     leaf nodes, and another function (by function
c     evaluation routine), refine and coarsen the tree
c     so that both functions are resolved but not
c     over-resolved on the new tree
c
c     INPUT:
c     maxboxes: the maximum number of boxes allowed
c     maxlevel: deepest level allowed
c     levelbox - istartlev: the quad-tree structure
c     ndeg: degree of chebyshev approx on each leaf node
c     uval: array of function values (resolved already)
c     feval: function evaluation routine of another
c            function
c     eps: error tolerance
c     t: parameter (e.g. time) needed in feval
c
c     OUTPUT:
c     the tree: overwritten to reflect the change
c     uval: interpolated to the new tree
c
c--------------------------------------------------
c
      subroutine adaptreef(maxboxes, maxlevel, 
     1           levelbox, icolbox, nboxes, nlev, 
     2           iparentbox, ichildbox, nblevel, 
     3           iboxlev, istartlev, ndeg, uval, 
     4           feval, t, eps, cent0, xsize0)
      implicit real*8 (a-h,o-z)
c-----global variables
      integer maxboxes, maxlevel
      integer nboxes, nlev, ndeg
      integer levelbox(1)
      integer icolbox(1) 
      integer iparentbox(1), ichildbox(2,1)
      integer nblevel(0:1), iboxlev(1), istartlev(0:1)
      real*8 uval(ndeg,1), eps
      real*8 cent0, xsize0
      external feval
c-----local variables
      integer kids(2)
      integer, allocatable:: iempty(:)
      integer, allocatable:: itemp(:), itemp2(:,:)
      integer, allocatable:: idold2new(:)
      real*8 wsave(1000), chwork(1000)
      real*8 xf(ndeg), ftemp(ndeg)
      real*8 coefftemp(ndeg)
      real*8, allocatable:: valk1(:), valk2(:)
c
      allocate(iempty(maxboxes))
      allocate(itemp(maxboxes))
      allocate(itemp2(2,maxboxes))
      allocate(idold2new(maxboxes))
c
      allocate(valk1(ndeg))
      allocate(valk2(ndeg))
c
c---------------------------------------------------------
c     1) refinement sweep. go down the tree level by level
c        refine leaf boxes if feval(:,t) is unresolved
c        no need to worry about uval, since by assumption
c        it is resolved. do remember to interp tho
c---------------------------------------------------------
c
      ix=1
      xlength=xsize0
      x0=cent0-xsize0/2.0d0
      nside=1
c
      call chxcin(ndeg,wsave)
      do i=0, maxlevel-1
        iflag=0
c          iflag: if the last level has been divided at all
        xlength=xlength/2.0d0
        nside=nside*2
c
        istart=istartlev(i)
        iend=istart+nblevel(i)-1
        do j=istart, iend
          ibox=iboxlev(j)
          if(ichildbox(1,ibox) .lt. 0) then
            call mkgrid(ndeg,xf,icolbox(ibox),
     1           levelbox(ibox),cent0,xsize0)
            do jj=1,ndeg
              call feval(xf(jj),t,ftemp(jj))
            enddo
c
            call getcoeff(ndeg,ftemp,coefftemp)
            call geterror(ndeg,coefftemp,error)
            hh = dble(2**levelbox(ibox))
            epscaled = eps * hh
c
            if(error .ge. epscaled) then
              call subdivide1(ibox, levelbox, icolbox,
     1             nboxes, nlev, iparentbox, ichildbox,
     2             nblevel, iboxlev, istartlev)
c              after calling subdivide1, remember to 
c              assign interpolated function values to children
              ik1=ichildbox(1,ibox)
              ik2=ichildbox(2,ibox)
              call chebyp2k(ndeg,uval(1,ibox),
     1             uval(1,ik1),uval(1,ik2))
c             done with box: ibox
              iflag=1
            endif
          endif
        enddo
c
        if((iflag .eq. 0) .and. (i .eq. nlev))then
          goto 110
        endif
      enddo
c
c------------------------------------------
c 2. go up the tree to coarsen:
c    if on a certain parent box, both functions
c    are resolved, merge its children
c------------------------------------------
c
110   do ii=1,nboxes
        iempty(ii)=0
      enddo   
c
      do i=nlev,0,-1
c       to merge, go up the tree level by level
        istart=istartlev(i)
        iend=istart+nblevel(i)-1
        do j=istart, iend
          ibox=iboxlev(j)
c         determine if children are all leaves
          nleafkids=0
          if(ichildbox(1,ibox) .gt. 0) then
            kids(1)=ichildbox(1,ibox)      
            kids(2)=ichildbox(2,ibox)      
            do k=1,2
              if(ichildbox(1,kids(k)).lt.0) then
                nleafkids=nleafkids+1
              endif
            enddo
          endif
c         got nleafkids
c
          if(nleafkids .eq. 2) then
c           both kids are leaves, do the following:
c            call mkgrid
c
            do jj=1, ndeg
              call feval(xf(jj),t,ftemp(jj))
            enddo
c
            call getcoeff(ndeg,ftemp,coefftemp)
            call geterror(ndeg,coefftemp,ferror)
c           error of feval
c
            do k=1, ndeg
              valk1(k)=uval(k,kids(1))
              valk2(k)=uval(k,kids(2))
            enddo
c
            call chebyk2p(ndeg, valk1, valk2, ftemp,
     1           coefftemp)
c
            call geterror(ndeg,coefftemp,uerror)
c           error of uval
c
            hh = dble(2**levelbox(ibox))
            epscaled = eps * hh
c
            if(uerror .lt. epscaled) then
c              call coarsen1
              call coarsen1(ibox,iparentbox,ichildbox,
     1           nboxes,icolbox,levelbox,nlev,istartlev,
     2           nblevel,iboxlev,iempty)
              do k=1, ndeg
                uval(k,ibox)=ftemp(k)
              enddo
c             remember to assign interpolated function values
c             to the box that has become a leaf box
            endif
          endif
        enddo
      enddo
c
c------------------------------------------
c     3) clean up the intermediate tree with deleted
c        boxes, reorder uval at the same time
c------------------------------------------
c
      call reorder(iparentbox, ichildbox, nboxes, 
     1     icolbox, levelbox, nlev,
     2     istartlev, nblevel, iboxlev, iempty, 
     3     idold2new, nold, itemp, itemp2)
c
      call reorderf(ndeg, nold, uval, idold2new)
c 
      deallocate(iempty)
      deallocate(itemp)
      deallocate(itemp2)
      deallocate(idold2new)
c
      deallocate(valk1)
      deallocate(valk2)
c
c
      end subroutine




c********************************************
c     tree spreading related subroutines
c********************************************




c--------------------------------------------------
c     subroutine spreadtree1
c--------------------------------------------------
c     
c     given a quad-tree, the center and side 
c     length of the root box, spread out the 
c     tree by one level, so that it covers a
c     bigger region 
c
c--------------------------------------------------
c
      subroutine spreadtree1(levelbox,icolbox,
     1           nboxes,nlev,iparentbox,ichildbox,
     2           nblevel,iboxlev,istartlev,xsize0,
     3           cent0,idir,ndeg,fval)
      implicit real*8 (a-h,o-z)
c-----global variables
      integer nboxes, nlev, maxboxes, maxlevel
      integer maxppl, npts, ndeg
      integer levelbox(1)
      integer icolbox(1)
      integer iparentbox(1), ichildbox(2,1)
      integer nblevel(0:1), iboxlev(1), istartlev(0:1)
      integer idir
      real*8 xsize0, cent0
      real*8 fval(ndeg,1), coeffp(ndeg)
c
      iroot=iboxlev(istartlev(0))
      if(abs(idir) .gt. 1.0d-12) then
c       1. deal with level info: existing boxes
        do i=nboxes, 1, -1
          iboxlev(i+2)=iboxlev(i)
c         move back by 2 entries
          levelbox(i)=levelbox(i)+1
c         move down by 1 level
        enddo

        do l=nlev, 1, -1
          nblevel(l+1)=nblevel(l)
          istartlev(l+1)=istartlev(l)+2
c         move down by 1 level
        enddo
c
c       2. deal with level info: new boxes
        levelbox(nboxes+1)=1
        levelbox(nboxes+2)=0
c
        nblevel(0)=1
        nblevel(1)=2
c
        istartlev(0)=1
        istartlev(1)=2
c
        iboxlev(1)=nboxes+2
        iboxlev(2)=nboxes+1
c
c       done with level structure, now deal with parent-child
c       and location info      
c
        icolbox(nboxes+2)=1
        iparentbox(nboxes+2)=-1
c
        iparentbox(iroot)=nboxes+2
        iparentbox(nboxes+1)=nboxes+2
c
        ichildbox(1,nboxes+1)=-1      
        ichildbox(2,nboxes+1)=-1      
c
c       location info, depending on idir,
c       the direction of spreading
        if(idir .gt. 0) then
          cent0=cent0+xsize0/2.0d0
          icolbox(iroot)=1
          icolbox(nboxes+1)=2
c
          ichildbox(1,nboxes+2)=nboxes+1
          ichildbox(2,nboxes+2)=iroot
        else
          cent0=cent0-xsize0/2.0d0
          icolbox(iroot)=2
          icolbox(nboxes+1)=1
c
c         in this case, icolbox is increased
c         for all the other boxes      
c
          do l=2, nlev+1
            istart=istartlev(l)
            iend=istart+nblevel(l)-1
            ntot=2**(l-1)
            do ii=istart, iend
              ibox=iboxlev(ii)
              icolbox(ibox)=icolbox(ibox)+ntot
            enddo
          enddo
c
          ichildbox(1,nboxes+2)=nboxes+1
          ichildbox(2,nboxes+2)=iroot
        endif
c
c     done spreading the tree structure by 1 level
c     now interpolate the function value array fval
c
      do k=1, ndeg
        fval(k,nboxes+1)=0.0d0
      enddo
c
      k1=ichildbox(1,nboxes+2)
      k2=ichildbox(2,nboxes+2)
      call chebyk2p(ndeg,fval(1,k1),fval(1,k2),
     1     fval(1,nboxes+2),coeffp)
c
        xsize0=xsize0*2.0d0
        nlev=nlev+1
        nboxes=nboxes+2
c
      endif

      end subroutine






