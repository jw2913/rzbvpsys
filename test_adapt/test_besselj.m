format long
xx=chebpts(500,[0,600]);
y100=besselj(100,xx);
y99=besselj(99,xx);
y98=besselj(98,xx);
% use besselj to eval
% convert to chebfun later
% IDK why chebfun/besselj alone doesn't work
J100=chebfun(y100,[0,600]);
J99=chebfun(y99,[0,600]);
J98=chebfun(y98,[0,600]);
%
yj100=J100(xx);
yj99=J99(xx);
yj98=J98(xx);
%
norm(yj100-y100,inf)
norm(yj99-y99,inf)
norm(yj98-y98,inf)
%
plot(J100,'r');
hold on
plot(J99,'g');
plot(J98,'b');
% check derivatives
JD100=diff(J100);
JD99=diff(J99);
JD98=diff(J98);
%
JD100(600)
JD99(600)
JD98(600)
%
% all right, check if they are the solution to the ode
x=chebfun('x',[0,600])
n=100;
res1=diff(diff(J100))+(x.^2-n^2-n)./x.^2.*J100+1./x.*J99;
res2=diff(diff(J99))+(x.^2-n^2+n)./x.^2.*J99+1./x.*J98;
res3=diff(diff(J98))-1./x.*J99+(x.^2-n^2+5*n-6)./x.^2.*J98;
xx=xx(10:end); %could be problematic at the end pts doesn't matter
% check quickly if the equation is correct. is all. 
err1=norm(res1(xx),inf)
err2=norm(res2(xx),inf)
err3=norm(res3(xx),inf)
% okay fine the equation is correct
