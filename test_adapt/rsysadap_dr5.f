c     driver code
c     which solves the bessel odes
c     (linear, system, adaptive)
c     Besselj functions of order 98,99,100
c     (from Starr & Rokhlin)
c
c     the bvp:
c
c     u'(x)+p(x)*u(x)=f(x)
c     bca*u(xa)+bcb*u(xb)=bcv
c
c     u\in R^6
c
c     where p and f are functions given by peval and feval
c     bca, bcb, bcv, xa, xb are given at the beginning
c     the exact solution and derivative are given by 
c     subroutines uexact and uder
c
c-------------------------------------------------
c
c     adaptive grid on, rtol = 1e-6, xa = 1e-8
c     ndeg =16, initial grid: nch =2
c     L2 error: 11 digits
c     nboxes = 415, nleaf = 208
c     okay, not that bad...
c
c-------------------------------------------------
c
      program besselj
      implicit real*8 (a-h,o-z)
      integer ndim, ndeg, nmax
      parameter(ndim=6) 
      parameter(ndeg=16)
      parameter(nch=2)
c      parameter(nch=10)
      parameter(nmax=100000)
c     tree related 
      integer nboxes, maxid, nlev
      integer levelbox(nmax)
      integer icolbox(nmax) 
      integer iparentbox(nmax), ichildbox(2,nmax)
      integer nblevel(0:nmax)
      integer iboxlev(nmax), istartlev(0:nmax)
      integer leaflist(nmax), itemparray(nmax)
c     the BC matrices and vec
      real*8 bca(ndim,ndim), bcb(ndim,ndim), bcv(ndim)
      real*8 pars(1), ua(ndim), ub(ndim)
c     the solution and its derivative
      real*8 endpts(nch+1)
      real*8 xtarg(nmax)
      real*8 usol(ndim,nmax), uext(ndim,nmax)
      real*8 ud(ndim,nmax), udext(ndim,nmax)
c     testing
      real*8 chpts(ndeg,nmax)
      external peval, feval, uexact, uder, ftest
c
      write(*,*) 'solving the bessel odes'
      write(*,*) 'adaptive grid, refined at different places'
      write(*,*) 'for each component'
      write(*,*) '----------------------------------------'
c
c     0. define the BC
      do j=1, ndim
      do i=1, ndim
        bca(i,j)=0.0d0
        bcb(i,j)=0.0d0
      enddo
      enddo
c
      bca(1,1)=1.0d0
      bca(2,3)=1.0d0
      bca(3,5)=1.0d0

      bcb(4,2)=1.0d0
      bcb(5,4)=1.0d0
      bcb(6,6)=1.0d0
c
      xa=1.0d-8
      xb=600.0d0
c
      bcv(1)=0.0d0
      bcv(2)=0.0d0
      bcv(3)=0.0d0
      bcv(4)= 0.030598170290372796d0
      bcv(5)= 0.015416721257492013d0
      bcv(6)=-0.025526503991812874d0
c
      write(*,*) 'bcv='
      do i=1, ndim
        write(*,*) bcv(i)
      enddo
c     end of the definition of the problem
c
c-------------------------------------------
c
c     prepare endpts (should be clustered near c1 and c2,
c     but a fine enough grid should be okay too)
      endpts(1)=xa
      endpts(nch+1)=xb
      h=(endpts(nch+1)-endpts(1))/nch
      do i=2, nch
        endpts(i)=(i-1)*h
      enddo
c
c-------------------------------------------
c
c     prepare the target points xtarg
      ntarg=1000
      h=(xb-xa)/ntarg
      do i=1, ntarg
        xtarg(i)=xa+i*h
      enddo
c
      maxdiv=25
      rtol=1.0d-6
      pars(1)=100
      call rsysadap(ndim, ndeg, nch, endpts,
     1     bca, bcb, bcv, peval, feval, pars,
     2     maxdiv, rtol, ntarg, xtarg, usol, ud)
c
      do k=1, ntarg
       x=xtarg(k)
       write(111,*) x, usol(1,k), usol(3,k), usol(5,k)
       write(112,*) x, usol(2,k), usol(4,k), usol(6,k)
c        call uexact(x,uext(1,k)) 
c        err1=abs(usol(1,k)-uext(1,k))
c        err2=abs(usol(2,k)-uext(2,k))
c        write(211,*) x, usol(1,k), uext(1,k), err1 
c        write(212,*) x, usol(2,k), uext(2,k), err2 
c
c        call uder(x,udext(1,k)) 
c        err1=abs(ud(1,k)-udext(1,k))
c        err2=abs(ud(2,k)-udext(2,k))
c        write(221,*) x, ud(1,k), udext(1,k), err1 
c        write(222,*) x, ud(2,k), udext(2,k), err2 
      enddo
c
      write(*,*) 'solution and written in fort.111 and fort.112'
c

      end program

c
c-----------------------------------------
c

      subroutine peval(x,p,pars) 
      implicit real*8 (a-h,o-z)
      real*8 x, pars(1)
      real*8 p(6,6)
c
c     pars(1): the highest order of the besselj functions
      n=pars(1)
      n=100
c
      do j=1, 6
      do i=1, 6
        p(i,j)=0.0d0
      enddo
      enddo
c
      p(1,2)=-1.0d0
      p(2,1)=(x**2-n**2-n)/x**2
      p(2,3)=1.0d0/x
      p(3,4)=-1.0d0
      p(4,3)=(x**2-n**2+n)/x**2
      p(4,5)=1.0d0/x
      p(5,6)=-1.0d0
      p(6,3)=-1.0d0/x
      p(6,5)=(x**2-n**2+5.0d0*n-6.0d0)/x**2

c
      end subroutine




      subroutine feval(x,f,pars)
      implicit real*8 (a-h,o-z)
      real*8 x, pars(1)
      real*8 f(6)
c      
      do j=1, 6
c        f(0)=0.0d0
        f(j)=0.0d0
      enddo
c

      end subroutine





