c     driver code
c     which solves the 3rd example
c     from Lee and Greengard
c     converted to a 2x2 system
c     calling rsysadap
c
c     the bvp:
c
c     u'(x)+p(x)*u(x)=f(x)
c     bca*u(xa)+bcb*u(xb)=bcv
c
c     u\in R^2
c
c     where p and f are functions given by peval and feval
c     bca, bcb, bcv, xa, xb are given at the beginning
c     the exact solution and derivative are given by 
c     subroutines uexact and uder
c
c------------------------------------------------
c     qualitatively correct,
c     using the grid spit out by rbvpadap_dr3.f
c------------------------------------------------
c
      program test3
      implicit real*8 (a-h,o-z)
      integer ndim, ndeg, nmax
      parameter(ndim=2) 
      parameter(ndeg=16)
      parameter(nch=107)
      parameter(nmax=100000)
c     tree related 
      integer nboxes, maxid, nlev
      integer levelbox(nmax)
      integer icolbox(nmax) 
      integer iparentbox(nmax), ichildbox(2,nmax)
      integer nblevel(0:nmax)
      integer iboxlev(nmax), istartlev(0:nmax)
      integer leaflist(nmax), itemparray(nmax)
c     the BC matrices and vec
      real*8 bca(ndim,ndim), bcb(ndim,ndim), bcv(ndim)
      real*8 pars(1), ua(ndim), ub(ndim)
c     the solution and its derivative
      real*8 endpts(nch+1)
      real*8 xtarg(nmax)
c      real*8 usol(ndim,nmax), uext(ndim,nmax)
c      real*8 ud(ndim,nmax), udext(ndim,nmax)

      real*8 usol(ndim,ndeg,nch), uext(ndim,ndeg,nch)
      real*8 ud(ndim,ndeg,nch), udext(ndim,ndeg,nch)
c     testing
      real*8 chpts(ndeg,nch)
      external peval, feval, uexact, uder, ftest
c
      write(*,*) 'solving the bessel odes'
      write(*,*) 'initial grid loaded from endpts3.dat.'
      write(*,*) '----------------------------------------'
      open(unit=12,file='endpts3.dat') 
      do i=1,nch+1
        read(12,*) endpts(i)
      enddo
      close(12)
c
      pars(1)=1.0d-6
      xa=-1.0d0
      xb=1.0d0
c
c     0. define the BC
      do j=1, ndim
      do i=1, ndim
        bca(i,j)=0.0d0
        bcb(i,j)=0.0d0
      enddo
      enddo
c
      bca(1,2)=1.0d0

      bcb(2,1)=2.0d0
      bcb(2,2)=1.0d0
c
      bcv(1)=1.0d0
      bcv(2)=1.0d0
c
      write(*,*) 'bcv='
      do i=1, ndim
        write(*,*) bcv(i)
      enddo
c     end of the definition of the problem
c
      call rsysnoadap(ndim, ndeg, nch, endpts,
     1     bca, bcb, bcv, peval, feval, pars,
     2     usol, ud)
c
      do k=1, nch
        aa=endpts(k)
        bb=endpts(k+1)
        call chnodes(aa, bb, ndeg, chpts(1,k), u, v, u1, v1)
c       x=xtarg(k)
        do j=1, ndeg
          x=chpts(j,k)
          y1=usol(1,j,k)
          y2=usol(2,j,k)
          u=(x-xa)*y1+y2
          write(111,*) x, u
        enddo
      enddo
c
      write(*,*) 'solution and written in fort.111'
c
      end program

c
c-----------------------------------------
c
      subroutine peval(x,p,pars) 
      implicit real*8 (a-h,o-z)
      real*8 x, pars(1)
      real*8 p(2,2)
c
c     pars(1): the highest order of the besselj functions
      eps=pars(1)
      eps=1.0d-6
c
      do j=1, 2
      do i=1, 2
        p(i,j)=0.0d0
      enddo
      enddo
c
      p(1,1)= -(x+1.0d0)*x/eps
      p(2,1)= (x+1.0d0)**2*x/eps
      p(1,2)= -x/eps
      p(2,2)= (x+1.0d0)*x/eps
c
      end subroutine




      subroutine feval(x,f,pars)
      implicit real*8 (a-h,o-z)
      real*8 x, pars(1)
      real*8 f(2)
c      
      do j=1, 2
        f(j)=0.0d0
      enddo
c

      end subroutine





      subroutine uexact(x,u)
      implicit real*8 (a-h,o-z)
      real*8 x, u(2)
c      
      c1=0.0d0
      d1=1.0d-3
c
      c2=1.0d0
      d2=1.0d-2
c
      u(1)=exp(-(x-c1)**2/d1)
      u(2)=exp(-(x-c2)**2/d2)
c
      end subroutine





      subroutine uder(x,ud)
      implicit real*8 (a-h,o-z)
      real*8 s, ud(2)
c
      c1=0.0d0
      d1=1.0d-3
c
      c2=1.0d0
      d2=1.0d-2
c
      ud(1)=-2.0d0*(x-c1)/d1*exp(-(x-c1)**2/d1)
      ud(2)=-2.0d0*(x-c2)/d2*exp(-(x-c2)**2/d2)
c
      end subroutine 



c----------------------------------------------
c
      function ftest(x)
      implicit real*8 (a-h,o-z)
      real*8 x, ftest
c
      c1=0.0d0
      d1=1.0d-3
c
      c2=1.0d0
      d2=1.0d-2
c
      ftest=exp(-(x-c1)**2/d1)+exp(-(x-c2)**2/d2)
c

      end function
