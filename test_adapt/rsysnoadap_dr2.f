c     driver code
c     which solves the bessel odes
c     (linear, adaptive)
c     Besselj functions of order 100,
c     from Lee and Greengard
c     converted to a 2x2 system
c     calling rsysadap
c
c     the bvp:
c
c     u'(x)+p(x)*u(x)=f(x)
c     bca*u(xa)+bcb*u(xb)=bcv
c
c     u\in R^2
c
c     where p and f are functions given by peval and feval
c     bca, bcb, bcv, xa, xb are given at the beginning
c     the exact solution and derivative are given by 
c     subroutines uexact and uder
c
c----------------------------------------------------
c     stuck at 7 digits
c----------------------------------------------------
c
c
      program besselj
      implicit real*8 (a-h,o-z)
      integer ndim, ndeg, nmax
      parameter(ndim=2) 
      parameter(ndeg=16)
c      parameter(nch=286)
      parameter(nch=20000)
      parameter(nmax=100000)
c     tree related 
      integer nboxes, maxid, nlev
      integer levelbox(nmax)
      integer icolbox(nmax) 
      integer iparentbox(nmax), ichildbox(2,nmax)
      integer nblevel(0:nmax)
      integer iboxlev(nmax), istartlev(0:nmax)
      integer leaflist(nmax), itemparray(nmax)
c     the BC matrices and vec
      real*8 bca(ndim,ndim), bcb(ndim,ndim), bcv(ndim)
      real*8 pars(1), ua(ndim), ub(ndim)
c     the solution and its derivative
      real*8 endpts(nch+1)
      real*8 xtarg(nmax)
c      real*8 usol(ndim,nmax), uext(ndim,nmax)
c      real*8 ud(ndim,nmax), udext(ndim,nmax)

      real*8 usol(ndim,ndeg,nch), uext(ndim,ndeg,nch)
      real*8 ud(ndim,ndeg,nch), udext(ndim,ndeg,nch)
c     testing
      real*8 chpts(ndeg,nch)
      external peval, feval, uexact, uder, ftest
c
      write(*,*) 'solving the bessel odes'
      write(*,*) 'initial grid loaded from endpts2.dat.'
      write(*,*) '----------------------------------------'
c      open(unit=12,file='endpts2.dat') 
c      do i=1,nch+1
c        read(12,*) endpts(i)
c      enddo
c      close(12)
c
c
      pars(1)=100.0d0
      xa=0.0d0
      xb=600.0d0
c
      endpts(1)=xa
      h=(xb-xa)/nch
      do i=2, nch
        endpts(i)=xa+(i-1)*h
      enddo
      endpts(nch+1)=xb
c     create nch equispaced intervals
c
c     0. define the BC
      do j=1, ndim
      do i=1, ndim
        bca(i,j)=0.0d0
        bcb(i,j)=0.0d0
      enddo
      enddo
c
      bca(1,2)=1.0d0

      bcb(2,1)=600.0d0
      bcb(2,2)=1.0d0
c
      bcv(1)=0.0d0
      bcv(2)=1.0d0
c
c     define the solution to be two gaussians with c1, d1, c2, d2
c     see also subroutines uexact and uder 
c
c      c1=0.0d0
c      d1=1.0d-3
c
c      c2=1.0d0
c      d2=1.0d-2
c
c      ua(1)=exp(-(xa-c1)**2/d1)
c      ua(2)=exp(-(xa-c2)**2/d2)
c
c      ub(1)=exp(-(xb-c1)**2/d1)
c      ub(2)=exp(-(xb-c2)**2/d2)
c
c      bcv(1)=bca(1,1)*ua(1)+bca(1,2)*ua(2)
c     1      +bcb(1,1)*ub(1)+bcb(1,2)*ub(2)
c
c      bcv(2)=bca(2,1)*ua(1)+bca(2,2)*ua(2)
c     1      +bcb(2,1)*ub(1)+bcb(2,2)*ub(2)
c
c      write(*,*) ua(1), ua(2), ub(1), ub(2)
c
      write(*,*) 'bcv='
      do i=1, ndim
        write(*,*) bcv(i)
      enddo
c     end of the definition of the problem
c
c-------------------------------------------
c
c     prepare endpts (should be clustered near c1 and c2,
c     but a fine enough grid should be okay too)
c      endpts(1)=xa
c      endpts(nch+1)=xb
c      h=(endpts(nch+1)-endpts(1))/nch
c      do i=2, nch
c        endpts(i)=(i-1)*h
c      enddo
c
c-------------------------------------------
c
c     prepare the target points xtarg
c      ntarg=1000
c      h=(xb-xa)/ntarg
c      do i=1, ntarg
c        xtarg(i)=xa+i*h
c      enddo
c
c      maxdiv=25
c      rtol=1.0d-9
c      call rsysadap(ndim, ndeg, nch, endpts,
c     1     bca, bcb, bcv, peval, feval, pars,
c     2     maxdiv, rtol, ntarg, xtarg, usol, ud)
c
      call rsysnoadap(ndim, ndeg, nch, endpts,
     1     bca, bcb, bcv, peval, feval, pars,
     2     usol, ud)
c
      do k=1, nch
        aa=endpts(k)
        bb=endpts(k+1)
        call chnodes(aa, bb, ndeg, chpts(1,k), u, v, u1, v1)
c       x=xtarg(k)
        do j=1, ndeg
          x=chpts(j,k)
          y1=usol(1,j,k)
          y2=usol(2,j,k)
          u=(x-xa)*y1+y2
          write(111,*) x, u, y1, y2
        enddo
c        call uexact(x,uext(1,k)) 
c        err1=abs(usol(1,k)-uext(1,k))
c        err2=abs(usol(2,k)-uext(2,k))
c        write(211,*) x, usol(1,k), uext(1,k), err1 
c        write(212,*) x, usol(2,k), uext(2,k), err2 
c
c        call uder(x,udext(1,k)) 
c        err1=abs(ud(1,k)-udext(1,k))
c        err2=abs(ud(2,k)-udext(2,k))
c        write(221,*) x, ud(1,k), udext(1,k), err1 
c        write(222,*) x, ud(2,k), udext(2,k), err2 
      enddo
c
      write(*,*) 'solution and written in fort.111'
c      write(*,*) 'derivative and error written in fort.221 and fort.222'
c
      end program

c
c-----------------------------------------
c

      subroutine peval(x,p,pars) 
      implicit real*8 (a-h,o-z)
      real*8 x, pars(1)
      real*8 p(2,2)
c
c     pars(1): the highest order of the besselj functions
      n=pars(1)
      n=100
c
      do j=1, 2
      do i=1, 2
        p(i,j)=0.0d0
      enddo
      enddo
c
      p(1,1)= (x**2-n**2+1)/x
      p(2,1)=-(x**2-n**2+1)
      p(1,2)=(x**2-n**2)/x**2
      p(2,2)=-(x**2-n**2)/x
c
      end subroutine




      subroutine feval(x,f,pars)
      implicit real*8 (a-h,o-z)
      real*8 x, pars(1)
      real*8 f(2)
c      
      do j=1, 2
        f(j)=0.0d0
      enddo
c

      end subroutine




      subroutine uexact(x,u)
      implicit real*8 (a-h,o-z)
      real*8 x, u(2)
c      
      c1=0.0d0
      d1=1.0d-3
c
      c2=1.0d0
      d2=1.0d-2
c
      u(1)=exp(-(x-c1)**2/d1)
      u(2)=exp(-(x-c2)**2/d2)
c
      end subroutine





      subroutine uder(x,ud)
      implicit real*8 (a-h,o-z)
      real*8 s, ud(2)
c
      c1=0.0d0
      d1=1.0d-3
c
      c2=1.0d0
      d2=1.0d-2
c
      ud(1)=-2.0d0*(x-c1)/d1*exp(-(x-c1)**2/d1)
      ud(2)=-2.0d0*(x-c2)/d2*exp(-(x-c2)**2/d2)
c
      end subroutine 



c----------------------------------------------
c
      function ftest(x)
      implicit real*8 (a-h,o-z)
      real*8 x, ftest
c
      c1=0.0d0
      d1=1.0d-3
c
      c2=1.0d0
      d2=1.0d-2
c
      ftest=exp(-(x-c1)**2/d1)+exp(-(x-c2)**2/d2)
c

      end function
