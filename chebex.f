c
c     chebyshev expansion routines for real valued functions.
c     at the moment, no intelligence is used in speeding up the cosine
c     transforms. a standard fft is used.
c
c     two sets of nodes are allowed, referred to as classical and
c     practical. the classical nodes contain only interior points,
c     while the practical nodes contain both endpoints.
c
c     i:   node construction routines (chnodc and chnodp).
c
c     ii:  transform routines for classical nodes chexfc and chexbc,
c          which compute interpolant and evaluate it, respectively.
c
c     iii: the analogous transform routines for practical nodes
c          (chexfp and chexbp).
c
c     iv.  chebyshev differentiation, integration and pointwise
c          evaluation routines (chbdif, chbint, cheval).
c
c      v.  differentiation and indefinite integration for functions
c          tabulated at classical chebyshev nodes (fdervc,fintgc,
c          fdicin). the latter is an initialization entry.
c
c      vi. differentiation and indefinite integration for functions
c          tabulated at practical chebyshev nodes (fdervp,fintgp,
c          fdipin). the latter is an initialization entry.
c
c   vii--ix. 2D tensor product version of chexfc, chexbc, and cheval
c
c     x. 2D tensor product version of chbdif
c
c*******************************************************************
c     i.      --------  node construction ----
c*******************************************************************
c
      subroutine chnodc(a,b,m,chpts,sinch,u,v,u1,v1)
c       implicit real *8 (a-h,o-z)
      integer *4 m,k,i,j
      real *8 chpts(1),sinch(1),u1,v1,u,v,a,b
      real *8 zero,done,pi2
c
c     this subroutine constructs the array chpts of classical chebyshev
c     nodes on a user-specified interval [a,b] and the coefficients
c     of the linear mappings from the interval [a,b] onto
c     interval [-1,1] and back.
c
c     the kth point is given by
c
c      (b - a)        (2k - 1)*pi     (b + a)
c       -----  * cos  ----------   +  -------    for k = 1,...,m.
c         2               2m             2
c
c     note: in the output, the point are given in reverse order
c           from left to right across the interval, rather than
c            from right to left as implicit in the above formula.
c
c     it also constructs the array u*sin(theta) for points 
c     corresponding to the chebyshev points which are maps of
c     cos(theta).
c
c     input parameters:
c
c     a,b - the ends of the interval on which the chebychev
c           nodes are being created.
c     m -   the number of chebyshev nodes to be created
c
c     output parameters:
c
c     chpts - the chebychev nodes on the interval [a,b].
c     sinch - u*sin(theta) for nodes representing the translated
c             point cos(theta).
c     u,v -   the coefficients of the linear mapping from the
c             interval [-1,1] onto the interval [a,b], i.e.
c             the mapping has the form  chpts = u*x + v.
c     u1,v1 - the coefficients of the linear mapping from the
c             interval [a,b] onto the interval [-1,1], i.e.
c             the mapping has the form    x= u1*chpts + v1.
c
c     construct the scaling parameters
c
      u=(b-a)/2
      v=(b+a)/2
      u1=2/(b-a)
      v1=1-u1*b
c
c     construct the chebyshev nodes and corresponding sin array.
c
      zero=0
      done=1
      pi2=datan(done)*2
      do 1200 k = 1,m
       chpts(m-k+1)=u*dcos((2*k-1)*pi2/m) + v
       sinch(m-k+1)= u*dsin((2*k-1)*pi2/m)
1200  continue
      return
      end
c
c--------------
      subroutine chnodp(a,b,m,chpts,sinch,u,v,u1,v1)
c--------------
c       implicit real *8 (a-h,o-z)
      integer *4 m,k,m1,k1
      real *8 chpts(1),sinch(1),u1,v1,u,v,a,b
      real *8 zero,done,pi
c
c     this entry constructs the array chpts of practical chebychev
c     nodes on a user-specified interval [a,b] and the coefficients
c     of the linear mappings from the interval [a,b] onto
c     interval [-1,1] and back.
c
c     the kth point is given by
c
c      (b - a)        (k-1)*pi       (b + a)
c       -----  *  cos----------   +  -------    for k = 1,...,m.
c         2             m-1             2
c
c     note: in the output, the point are given in reverse order
c           from left to right across the interval, rather than
c           from right to left as implicit in the above formula.
c
c     note: the "practical" chebyshev points include the two endpoints.
c
c     it also constructs the array u*sin(theta) for points 
c     corresponding to the chebyshev points which are maps of
c     cos(theta).
c
c     input parameters:
c
c     a,b - the ends of the interval on which the chebychev
c           nodes are being created.
c     m -   the number of practical chebyshev nodes created. there
c           are m-1 intervals created.
c
c     output parameters:
c
c     chpts - the chebychev nodes on the interval [a,b].
c     sinch - u*sin(theta) for nodes representing the translated
c             point cos(theta).
c     u,v -   the coefficients of the linear mapping from the
c             interval [-1,1] onto the interval [a,b], i.e.
c             the mapping has the form  chpts = u*x + v.
c     u1,v1 - the coefficients of the linear mapping from the
c             interval [a,b] onto the interval [-1,1], i.e.
c             the mapping has the form    x= u1*chpts + v1.
c
c     construct the scaling parameters
c
      u=(b-a)/2
      v=(b+a)/2
      u1=2/(b-a)
      v1=1-u1*b
c
c     construct the chebyshev nodes and corresponding sin array.
c
      zero=0
      done=1
      pi = 4*datan(done)
      m1 = m-1
      do 1200 k = 0,m1
       chpts(m-k)=u*dcos(k*pi/m1) + v
       sinch(m-k)= u*dsin(k*pi/m1)
1200  continue
      return
      end
c
c*******************************************************************
c     ii.   transformation routines for classical nodes
c***********************************************************
      subroutine chexfc(f,n,texp,wsave,work)
c
c     subroutine computes coefficients of chebyshev expansion
c     of function tabulated at classical chebyshev nodes.
c
c
c     must be preceded by a call to entry chxcin.
c
c     input:
c
c     f    =  function values at chebyshev nodes in increasing order
c             on the interval (-1,1)
c     n    =  number of chebyshev nodes
c
c     wsave = complex workspace of dimension at least 8n + 20.
c             these  array elements must be kept fixed
c             between calls - otherwise chxcin must be called again.
c
c     work = complex workspace of dimension at least 2n.
c
c     output:
c
c     texp = array of expansion coefficients of (n-1)st degree
c            interpolant
c
      implicit real *8 (a-h,o-z)
      integer *4 n,n2,k,km1
      real *8 f(1),texp(1),done,d,scalf,pi
      complex *16 work(1),scalb,wsave(1)
      save
c
c---- form even extension of function values ordered from 1 to -1.
c
ccc      call prin2(' f is *',f,2*n)
      n2 = 2*n
ccc      call prinf(' n2 is *',n2,1)
      do 200 k = 1,n
       work(k) = f(n-k+1)
       work(n2-k+1) = f(n-k+1)
200   continue
c
ccc      call prin2(' even extension of f is *',work(1),2*n2)
c
c---- compute forward transform and scale result as needed to
c     recover coefficients.
c
      call zfftf(n2,work,wsave)
ccc   call prin2(' fft transform of f is *',work(1),2*n2)
      do 300 k = 1,n
       km1 = k-1
       scalf = n*dcos(km1*pi/n2)
       texp(k) = dreal(work(k))/scalf
300   continue
      texp(1) = texp(1)/2
      return
c
c--------------
      entry chexbc(f,n,texp,wsave,work)
c--------------
c
c     entry computes value of chebyshev expansion with
c     coefficients texp(k) at n classical chebyshev nodes.
c
c
c     must be preceded by a call to entry chxcin.
c
c     input:
c
c     texp = array of expansion coefficients of (n-1)st degree
c            interpolant
c     n    = number of chebyshev nodes
c
c     wsave = complex workspace of dimension at least 8n + 20.
c             these  array elements must be kept fixed
c             between calls - otherwise chxcin must be called again.
c
c     work = complex workspace of dimension at least 2n.
c
c     output:
c
c          = function values at n classical chebyshev nodes in
c            increasing order on the interval (-1,1)
c
c---- compute coefficients of forward transform of f from coefficients
c     of texp. this requires a rescaling of the coefficients.
c
ccc   call prin2(' texp is *',texp,n)
      n2 = 2*n
      do 600 k = 1,n
       km1 = k-1
       arg = km1*pi/n2
       scalb = dcmplx(dcos(arg),-dsin(arg))/2
       work(k) = texp(k)*scalb
600   continue
      work(1) = work(1)*2
c
c---- form extension of transform values
c
      do 700 k = 1,n-1
       work(n2-k+1) = dconjg(work(k+1))
700   continue
      work(n+1) = 0
c
ccc   call prin2(' extension of texp is *',work(1),2*n2)
c
c---- compute inverse transform
c
      call zfftb(n2,work,wsave)
ccc   call prin2(' inv fft of extended texp is *',work(1),2*n2)
c
c---- assign function values to array f. note that the function
c     values are recovered in reverse order (from theta = 0 to
c     theta = pi i.e. from 1 to -1 ).
c
      do 800 k = 1,n
       f(n-k+1) = dreal(work(k+1))
800   continue
      return
c
c--------------
      entry chxcin(n,wsave)
c--------------
c
c---- initialization entry .. defines pi and callls initialization
c     routine for ffts. wsave must have length at least 4*n2+15.
c
      pi = 4*datan(1.0d0)
      n2 = 2*n
      call zffti(n2,wsave)
c
      return
      end
c
c*******************************************************************
c     iii.   transformation routines for practical nodes
c***********************************************************
c
      subroutine chexfp(f,n,texp,wsave,work)
c
c     this entry computes coefficients of chebyshev expansion
c     of function tabulated at practical chebyshev nodes.
c
c
c     must be preceded by a call to entry chxpin.
c
c     input:
c
c     f    =  function values at practical chebyshev nodes in
c             increasing order on the interval [-1,1].
c     n    =  number of practical chebyshev nodes
c             for efficiency, n should be of the form 2**k + 1.
c
c
c     wsave = complex workspace of dimension at least 8n + 20.
c             these  array elements must be kept fixed
c             between calls - otherwise chxpin must be called again.
c
c     work = complex workspace of dimension at least 2n.
c
c     output:
c
c     texp = array of expansion coefficients of (n-1)st degree
c            interpolant
c
      implicit real *8 (a-h,o-z)
      integer *4 n,n2,k,km1
      real *8 f(1),texp(1),done,d,scalf
      complex *16 work(1),scalb,wsave(1)
c
c---- form even extension of function values ordered from 1 to -1,
c     recalling that endpoint values are included as f(1), f(n+1).
c
      n2 = 2*(n-1)
      work(1) = f(n)
      work(n) = f(1)
      do 400 k = 2,n-1
       work(k) = f(n-k+1)
       work(n2-k+2) = work(k)
400   continue
c
ccc   call prin2(' even extension of f is *',work(1),2*n2)
c
c---- compute forward transform and scale result as needed to
c     recover coefficients.
c
      call zfftf(n2,work,wsave)
ccc   call prin2(' fft transform of f is *',work(1),2*n2)
      scalf = 1.0d0/(n-1)
      do 500 k = 1,n
       texp(k) = dreal(work(k))*scalf
500   continue
      texp(1) = texp(1)/2
      return
      end
c
c----------
      subroutine chexbp(f,n,texp,wsave,work)
c----------
      implicit real *8 (a-h,o-z)
      integer *4 n,n2,k,km1
      real *8 f(1),texp(1),done,d,scalf
      complex *16 work(1),scalb,wsave(1)
      save
c
c     entry computes value of chebyshev expansion with
c     coefficients texp(k) at n+1 practical chebyshev nodes.
c
c     must be preceded by a call to entry chxpin.
c
c     input:
c
c     texp = array of expansion coefficients of (n-1)st degree
c            interpolant
c     n    = number of practical chebyshev nodes
c            for efficiency, n should be of the form 2**k + 1.
c
c     wsave = complex workspace of dimension at least 8n + 20.
c             these  array elements must be kept fixed
c             between calls - otherwise chxpin must be called again.
c
c     work = complex workspace of dimension at least 2n.
c
c     output:
c
c     f    = function values at n+1 practical chebyshev nodes in
c            increasing order on the interval [-1,1].
c
c---- compute coefficients of forward transform of f from coefficients
c     of texp. this requires a rescaling of the coefficients.
c
ccc   call prin2(' texp is *',texp,n)
      n2 = 2*(n-1)
      scalb = 1.0d0/2.0d0
      work(1) = texp(1)
      do 900 k = 2,n
       work(k) = texp(k)*scalb
900   continue
c
c---- form extension of transform values
c
      do 1000 k = 1,n-2
       work(n2-k+1) = work(k+1)
1000  continue
c
ccc   call prin2(' extension of texp is *',work(1),2*n2)
c
c---- compute inverse transform
c
      call zfftb(n2,work,wsave)
ccc   call prin2(' inv fft of extended texp is *',work(1),2*n2)
c
c---- assign function values to array f. note that the function
c     values are recovered in reverse order (from theta = 0 to
c     theta = pi i.e. from 1 to -1 ).
c
      do 1100 k = 1,n
       f(n-k+1) = dreal(work(k))
1100  continue
      return
      end
c
c--------------
      subroutine chxpin(n,wsave)
c--------------
c
c---- initialization entry ...calls initialization
c     routine for ffts. wsave must have length at least 4*n2+15.
c
      implicit real *8 (a-h,o-z)
      integer *4 n,n2,k,km1
      complex *16 wsave(1)
c
      n2 = 2*(n-1)
      call zffti(n2,wsave)
c
      return
      end
c
c*******************************************************************
c  iv.  chebyshev differentiation, integration and pointwise evaluation
c       routines.
c*******************************************************************
c
      subroutine chbdif(texp,tdiff,n,a,b)
c
c     subroutine computes chebyshev coefficients of derivative of
c     chebyshev expansion. in other words, the (n-1)st degree expansion
c     texp is mapped to the (n-2) degree expansion tdiff which
c     represents the derivative of the original expansion.
c
c     n = number of chebyshev nodes
c     texp = array of expansion coefficients
c     tdiff = array of expansion coefficients of derivative
c
      implicit real *8 (a-h,o-z)
      integer *4 n
      real *8 tdiff(1),texp(1),a,b,u,sc
c
c-----local variables
c
      integer *4 k
c
      u = 2/(b-a)
      sc = 2*u
      if (n .eq. 1) then
       tdiff(1) = 0
      else if (n .eq. 2) then
       tdiff(2) = 0
       tdiff(1) = texp(2)*sc/2
      else
       tdiff(n) = 0
       tdiff(n-1) = (n-1)*texp(n)*sc
       do 400 k = 2,n-1
          tdiff(n-k) = tdiff(n-k+2) + (n-k)*texp(n-k+1)*sc
400      continue
       tdiff(1) = tdiff(1)/2
      endif
ccc   call prin2(' differentiated expansion is *',tdiff,n)
      return
      end
c
c--------------
      subroutine chbint(texp,tint,n,a,b)
c--------------
c
c     subroutine computes chebyshev coefficients of indefinite integral
c     of chebyshev expansion. in other words, the (n-2)nd degree
c     expansion texp is mapped to the (n-1) degree expansion tint which
c     represents the integral of the original expansion (up to a
c     constant). for consistency, we set the zeroth order coefficient
c     to 0, which has the effect that the integrated expansion always
c     passes through the origin.
c
c     n = number of chebyshev nodes
c     texp = array of expansion coefficients
c     tint = array of expansion coefficients of (indefinite) integral
c
      implicit real *8 (a-h,o-z)
      integer *4 n
      real *8 tint(1),texp(1),a,b,u,sc
c
c-----local variables
c
      integer *4 k
c
ccc   call prin2(' original expansion is *',texp,n)
      u = 2/(b-a)
      sc = 1/u
      do 100 k = 1,n
       tint(k) = 0
100   continue
      if (n .eq. 1) then
       tint(2) = texp(1)*sc
      else if (n .eq. 2) then
       tint(2) = texp(1)*sc
       tint(3) = texp(2)*sc/4
      else
       tint(2) = texp(1)*sc
       tint(3) = texp(2)*sc/4
       do 400 k = 3,n-1
          tint(k+1) = tint(k+1) + sc*texp(k)/(2*k)
          tint(k-1) = tint(k-1) - sc*texp(k)/(2*(k-2))
400      continue
      endif
ccc   call prin2(' integrated expansion is *',tint,n)
      return
      end
c
c--------------
      subroutine cheval(x,val,texp,n,a,b)
c--------------
c
c     subroutine evaluates chebyshev expansion with coefficients texp
c     at point x in interval [a,b].
c
c     a = left endpoint of interval
c     b = right endpoint of interval
c     x = evaluation point
c     texp = expansion coefficients
c     n  = order of expansion (0 ,..., n-1)
c     val = computed value
c
      implicit real *8 (a-h,o-z)
      integer *4 n,j
      real *8 texp(1),a,b,x,xscal,u,v,val,tj
c
      u = 2.0d0/(b-a)
      v = 1.0d0 - u*b
      xx = x
cccccc      a better fix?
      if (abs(xx-a).le.1e-14) then
        xx=a
      elseif (abs(xx-b).le.1e-14) then
        xx=b
      endif
cccccc
c      xscal = u*x + v
      xscal = u*xx + v
      val = 0
      do 600 j = 0,n-1
       tj = dcos( j* dacos(xscal) )
       val = val + tj*texp(j+1)
600   continue
      return
      end
c
c
c*******************************************************************
c  v.  differentiation and indefinite integration for functions
c      tabulated at classical chebyshev nodes.
c*******************************************************************
c
      subroutine fdervc(f,fprime,n,a,b,wsave,work)
c
c     must be preceded by a call to fdicin.
c
c     this subroutine computes derivative values of function tabulated
c     at the n classical chebyshev nodes in interval [a,b]. method is
c     to compute chebyshev interpolating polynomial and differentiate
c     it analytically, obtaining coefficients of new chebyshev
c     expansion of fprime. a fourier transform then yields
c     desired derivative values.
c
c     input:
c
c     f = function values at (expanded) classical chebyshev nodes
c     n = number of nodes
c     a = left endpoint of interval
c     b = right endpoint of interval
c     wsave = complex workspace of dimension at least 8n + 20.
c            these array elements must be kept fixed
c            between calls - otherwise fdicin must be called again.
c     work = real workspace of dimension at least 6n.
c
c     output:
c
c     fprime = computed derivative values at same nodes
c
      implicit real *8 (a-h,o-z)
      integer *4 n,j,lcw,lw
      real *8 f(1),fprime(1),finteg(1),a,b,work(1),dint,fa,sgn,fb
      complex *16 wsave(1)
      save
c
c
c---- starting at work(1), we store chebyshev expansion of f.
c---- starting at work(n+1), we store chebyshev expansion of fprime.
c---- starting at work(2n+1), used for scratch space.
c
      call chexfc(f,n,work(1),wsave,work(2*n+1))
ccc   call prin2(' texp is *',work(1),n)
ccc   call chexbc(f,n,work(1),wsave,work(2*n+1))
ccc   call prin2(' after back transform, f is *',f,n)
c
      call chbdif(work(1),work(n+1),n,a,b)
      call chexbc(fprime,n,work(n+1),wsave,work(2*n+1))
      return
c
c--------------
      entry fintgc(f,finteg,n,a,b,dint,fa,fb,wsave,work)
c--------------
c
c     must be preceded by a call to fdicin.
c
c     subroutine computes values of indefinite integral of function
c     tabulated at n classical chebyshev nodes in interval [a,b].
c     method is to compute chebyshev interpolating polynomial and
c     integrate it analytically, obtaining coefficients of new
c     chebyshev expansion of finteg. a fourier transform then yields
c     the  desired integral values.
c     for consistency, the particular choice of indefinite integral
c     is that which passes through the origin, i.e. the indefinite
c     integral whose chebyshev expansion has 0 for constant term.
c
c     the definite integral of f on [a,b] is returned as dint.
c     dint is computed from the chebyshev expansion of the indefinite
c     integral tint as tint(b) - tint(a).
c
c     input:
c
c     f = function values at (expanded) classical chebyshev nodes
c     n = number of nodes
c     a = left endpoint of interval
c     b = right endpoint of interval
c     wsave = complex workspace of dimension at least 8n + 20.
c            these array elements must be kept fixed
c            between calls - otherwise fdicin must be called again.
c     work = real workspace of dimension at least 6n.
c
c     output:
c
c     finteg = computed integral values at same nodes
c     dint = definite integral of f on [a,b].
c     fa =  value of integrated expansion at a.
c     fb =  value of integrated expansion at b.
c
c
ccc   call prin2(' inside fintgr, original data is *',f,n)
c
c---- starting at work(1), we store chebyshev expansion of f.
c---- starting at work(n+1), we store chebyshev expansion of finteg.
c---- starting at work(2n+1), used for scratch space.
c
      call chexfc(f,n,work(1),wsave,work(2*n+1))
c------------------------------------
c
c     test filtering of texp before continuing
c
c      do 50 j = n/2,n
c        work(j) = 0
50    continue
c------------------------------
c
      call chbint(work(1),work(n+1),n,a,b)
ccc      call prin2(' inside fintgr, integrated expansion is *',
ccc     1             work(n+1),n)
ccc   do 70 j = 1,n
ccc      write(1,*) j, dabs(work(n+j))
70    continue
c
c---- compute definite integral
c
      dint = 0
      do 100 j = 2,n,2
       dint = dint + 2*work(n+j)
100   continue
c
c---- compute finteg at endpoints a and b.
c
      fa = 0
      fb = 0
      sgn = 1.
      do 200 j = 1,n
       fa = fa + work(n+j)*sgn
       fb = fb + work(n+j)
       sgn = -sgn
200   continue
c
      call chexbc(finteg,n,work(n+1),wsave,work(2*n+1))
      return
c
c--------------
      entry fdicin(n,wsave,lcw,lw)
c--------------
c
c     initialization entry - calls chxcin which initializes
c     fft routines.
c
      call chxcin(n,wsave)
      return
      end
c
c*******************************************************************
c  vi.  differentiation and indefinite integration for functions
c       tabulated at practical chebyshev nodes.
c*******************************************************************
c
      subroutine fdervp(f,fprime,n,a,b,wsave,work)
c
c     must be preceded by a call to fdipin.
c
c     this subroutine computes derivative values of function tabulated
c     at the n classical chebyshev nodes in interval [a,b]. method is
c     to compute chebyshev interpolating polynomial and differentiate
c     it analytically, obtaining coefficients of new chebyshev
c     expansion of fprime. a fourier transform then yields
c     desired derivative values.
c
c     input:
c
c     f = function values at (expanded) practical chebyshev nodes
c     n = number of nodes
c     a = left endpoint of interval
c     b = right endpoint of interval
c     wsave = complex workspace of dimension at least 8n + 20.
c            these array elements must be kept fixed
c            between calls - otherwise fdiini must be called again.
c     work = real workspace of dimension at least 6n.
c
c     output:
c
c     fprime = computed derivative values at same nodes
c
      implicit real *8 (a-h,o-z)
      integer *4 n,j,lcw,lw
      real *8 f(1),fprime(1),finteg(1),a,b,work(1)
      complex *16 wsave(1)
      save
c
c
c---- starting at work(1), we store chebyshev expansion of f.
c---- starting at work(n+1), we store chebyshev expansion of fprime.
c---- starting at work(2n+1), used for scratch space.
c
      call chexfp(f,n,work(1),wsave,work(2*n+1))
ccc   call prin2(' texp is *',work(1),n)
ccc   call chexbc(f,n,work(1),wsave,work(2*n+1))
ccc   call prin2(' after back transform, f is *',f,n)
c
      call chbdif(work(1),work(n+1),n,a,b)
      call chexbp(fprime,n,work(n+1),wsave,work(2*n+1))
      return
c
c--------------
      entry fintgp(f,finteg,n,a,b,wsave,work)
c--------------
c
c     must be preceded by a call to fdipin.
c
c     subroutine computes values of indefinite integral of function
c     tabulated at n practical chebyshev nodes in interval [a,b].
c     method is to compute chebyshev interpolating polynomial and
c     integrate it analytically, obtaining coefficients of new
c     chebyshev expansion of finteg. a fourier transform then yields
c     the  desired integral values.
c     for consistency, the particular choice of indefinite integral
c     is that which passes through the origin, i.e. the indefinite
c     integral whose chebyshev expansion has 0 for constant term.
c
c     note that the definite integral of f on [a,b] is easily
c     obtained from finteg as finteg(n) - finteg(1).
c
c     input:
c
c     f = function values at (expanded) practical chebyshev nodes
c     n = number of nodes
c     a = left endpoint of interval
c     b = right endpoint of interval
c     wsave = complex workspace of dimension at least 8n + 20.
c            these array elements must be kept fixed
c            between calls - otherwise fdiini must be called again.
c     work = real workspace of dimension at least 6n.
c
c     output:
c
c     finteg = computed integral values at same nodes
c
ccc   call prin2(' inside fintgr, original data is *',f,n)
c
c---- starting at work(1), we store chebyshev expansion of f.
c---- starting at work(n+1), we store chebyshev expansion of finteg.
c---- starting at work(2n+1), used for scratch space.
c
      call chexfp(f,n,work(1),wsave,work(2*n+1))
c------------------------------------
c
c     test filtering of texp before continuing
c
c      do 50 j = n/2,n
c        work(j) = 0
50    continue
c------------------------------
c
      call chbint(work(1),work(n+1),n,a,b)
ccc      call prin2(' inside fintgr, integrated expansion is *',
ccc     1             work(n+1),n)
ccc   do 70 j = 1,n
ccc      write(1,*) j, dabs(work(n+j))
70    continue
c
      call chexbp(finteg,n,work(n+1),wsave,work(2*n+1))
      return
c
c--------------
      entry fdipin(n,wsave,lcw,lw)
c--------------
c
c     initialization entry - calls chxcin which initializes
c     fft routines.
c
      call chxpin(n,wsave)
      return
      end





c*******************************************************************
c  vii.  2D tensor product version of chexfc
c*******************************************************************
c
c     subroutine computes coefficients of chebyshev expansion
c     of function tabulated at classical chebyshev nodes.
c
c
c     attention: must be preceded by a call to entry chxcin!
c
c     input:
c
c     f    =  function values at chebyshev nodes in increasing order
c             on the interval (-1,1)
c     n    =  number of chebyshev nodes (in one dimension)
c
c     wsave = complex workspace of dimension at least 8n + 20.
c             these  array elements must be kept fixed
c             between calls - otherwise chxcin must be called again.
c
c     work = complex workspace of dimension at least 2n.
c
c     output:
c
c     texp = array of expansion coefficients of (n-1)st degree
c            interpolant
c
c*******************************************************************
c
      subroutine chexfc2d(f,n,texp,wsave,work)
      implicit real*8 (a-h,o-z)
      integer n
      real*8 f(n,n), texp(n,n), tmp(n), tmp2(n,n)
      complex*16 wsave(1), work(1)
c
c     1. apply the column transform by calling chexfc
      do j=1,n
        call chexfc(f(1,j),n,tmp,wsave,work)
        do i=1,n
          tmp2(j,i)=tmp(i)
        enddo
c       do the transpose
      enddo

c     2. apply the row transform (column transform of tmp2)
      do j=1,n
        call chexfc(tmp2(1,j),n,tmp,wsave,work)
        do i=1,n
          texp(j,i)=tmp(i)
        enddo
c       do the transpose
      enddo

      return
      end subroutine





c*******************************************************************
c     viii. 2D version of chexbc
c*******************************************************************
c
c     entry computes value of chebyshev expansion with
c     coefficients texp(k) at n classical chebyshev nodes.
c
c
c     attention: must be preceded by a call to entry chxcin!
c
c     input:
c
c     texp = array of expansion coefficients of (n-1)st degree
c            interpolant
c     n    = number of chebyshev nodes
c
c     wsave = complex workspace of dimension at least 8n + 20.
c             these  array elements must be kept fixed
c             between calls - otherwise chxcin must be called again.
c
c     work = complex workspace of dimension at least 2n.
c
c     output:
c
c     f    = function values at n classical chebyshev nodes in
c            increasing order on the interval (-1,1)
c
c*******************************************************************
c
      subroutine chexbc2d(f,n,texp,wsave,work)
      implicit real*8 (a-h,o-z)
      integer n
      real*8  f(n,n), texp(n,n), tmp(n), tmp2(n,n)
      complex*16 wsave(1), work(1)
c
c     1. apply the colum transform by calling chexbc
      do l=1,n
        call chexbc(tmp,n,texp(1,l),wsave,work)
        do i=1,n
          tmp2(l,i)=tmp(i)
        enddo
c       do the transpose
      enddo

c     2. apply the row transform (column transform of tmp2)
      do i=1,n
        call chexbc(tmp,n,tmp2(1,i),wsave,work)
        do j=1,n
          f(i,j)=tmp(j)
        enddo
c       do the transpose
      enddo

      return
      end subroutine






c*******************************************************************
c     ix. 2D version of cheval
c*******************************************************************
c
c     subroutine evaluates Chebyshev expansion with coefficients 
c     texp at point (x,y) in [a,b]*[c,d]      
c
c     INPUT:
c     (a,b,c,d) = the domain 
c     (x,y) = evaluation point
c     texp = expansion coefficients
c     n = order of expansion 
c     
c     OUTPUT:
c     val =  computed value
c
c*******************************************************************
      subroutine cheval2d(x,y,val,texp,n,a,b,c,d)
      implicit real*8 (a-h,o-z)
      integer n
      real*8 texp(n,n), a,b,c,d, val, tmp(n)

c     evaluate in the x-direction
      do l=1,n
        call cheval(x,tmp(l),texp(1,l),n,a,b)
      enddo

c     evaluate in the y-direction   
      call cheval(y,val,tmp,n,c,d)

      return
      end subroutine






c*******************************************************************
c     x. 2D (tensor product) version of chbdif
c*******************************************************************
c
c     subroutine computes chebyshev coefficients of derivative of
c     chebyshev expansion. in other words, the (n-1)st degree expansion
c     texp is mapped to the (n-2) degree expansion tdiff which
c     represents the derivative of the original expansion.
c
c     n = number of chebyshev nodes (in each dimension)
c     (a,b)= the interval where x lies in 
c     (c,d)= the interval where y lies in 
c     texp = array of expansion coefficients
c     tdifx = array of expansion coefficients of d/dx
c     tdify = array of expansion coefficients of d/dy
c
c*******************************************************************
c
      subroutine chbdif2d(texp,tdifx,tdify,n,a,b,c,d)
      implicit real*8 (a-h,o-z)
      integer n
      real*8 a,b,c,d
      real*8 texp(n,n), tdifx(n,n), tdify(n,n)
      real*8 texpt(n,n), tdy(n)
c
c     i. apply column transform of chbdif to texp to 
c        get tdifx      
c
      do j=1,n
        call chbdif(texp(1,j),tdifx(1,j),n,a,b)
      enddo
      
c    ii. apply row transform of chbdif to texp to 
c        get tdify      
c
      do j=1,n
      do i=1,n
        texpt(j,i)=texp(i,j)
      enddo
      enddo
c        do the transpose

      do j=1,n
        call chbdif(texpt(1,j),tdy,n,c,d) 
        do i=1,n
          tdify(j,i)=tdy(i)
        enddo
c         do the transpose
      enddo

      end subroutine






c------------------------------------------------------
c     subroutine chebypol
c------------------------------------------------------
c
c     computes the value of chebyshev polynomial
c     of order n, at the point x
c
c------------------------------------------------------
      subroutine chebypol(n,x,y)
      implicit real*8 (a-h,o-z)
      integer n
      real*8 x,y,sig

c        y=real(cos(n*acos(x)))
c        handle the case with abs(x)>1
c
c        attention: dacosh might not be defined
c                   for every compiler...
        if (abs(x) .le. 1.0d0) then
          y=dcos(n*dacos(x))
        elseif (x .gt. 1.0d0) then
          y=dcosh(n*dacosh(x))
        else
          if (mod(n,2) .eq. 0) then
            sig=1.0d0
          else
            sig=-1.0d0
          endif
          y=sig*dcosh(n*dacosh(-x))
        endif

      end subroutine





c----------------------------------------------
c     some old subroutines with complex version
c----------------------------------------------





c  real version:
c  chindef_bx: left indef integration transf
c  chindef_xb: right indef integration transf 
c  defint: def integration transf
c  chftrans: forward chebyshev transf
c  chbtrans: backward chebyshev transf
c  setchmat: given degree, set up all the chebyshev
c            transform/integration matrices
c  chnodes: returns chebyshev nodes of a given order
c           on a given interval
c
c  complex version:
c  zchindef_bx: left indef integration transf
c  zchindef_xb: right indef integration transf 
c  zdefint: def integration transf
c  zchftrans: forward chebyshev transf
c  zchbtrans: backward chebyshev transf
c  zsetchmat: given degree, set up all the chebyshev
c            transform/integration matrices

c
c---------------------------------------------------





c---------------------------------------------------
c     subroutine chindef_bx
c---------------------------------------------------
c
c this routine calculates the chebyshev coefficients of
c the left (normal) indefinite integrals
c
c---------------------------------------------------
c
      subroutine chindef_bx(chintfl,coeffl,nnd)
      implicit real*8 (a-h,o-z)
      integer i,nnd
      real*8 coeffl(0:nnd-1), chintfl(0:nnd-1)
c
        chintfl(1) = coeffl(0) - coeffl(2)/2d0
        do i = 2,nnd-2
           chintfl(i) = (coeffl(i-1)-coeffl(i+1)) / dble(2*i)
        end do
        chintfl(nnd-1) = coeffl(nnd-2) / dble(2*nnd-2)

        chintfl(0) = (-1)**(nnd+1) * coeffl(nnd-1) / dble(2*nnd)
        do i = nnd-1, 1, -1
          chintfl(0) = chintfl(0) - (-1)**i * chintfl(i)
        end do
      end





c---------------------------------------------------
c     subroutine chindef_xb
c---------------------------------------------------
c
c this routine calculates the chebyshev coefficients of
c the right (backward) indefinite integrals
c
c---------------------------------------------------
      subroutine chindef_xb(chintfr,coeffr,nnd)
      implicit real*8 (a-h,o-z)
      integer i,nnd
      real*8 coeffr(0:nnd-1),chintfr(0:nnd-1)
c
      chintfr(1) = coeffr(2)/2d0 - coeffr(0)
      do i = 2, nnd-2
         chintfr(i) = (coeffr(i+1)-coeffr(i-1)) / dble(2*i)
      end do
      chintfr(nnd-1) = -coeffr(nnd-2) / dble(2*nnd-2)

      chintfr(0) = coeffr(nnd-1) / dble(2*nnd)
      do i = 1, nnd-1
         chintfr(0) = chintfr(0) -  chintfr(i)
      end do
      end






c---------------------------------------------------
c     subroutine defint 
c---------------------------------------------------
c
c     This subroutine computes the definit integral
c     of a function on [-1,1], 
c     given the chebyshev coeffs
c
c---------------------------------------------------
c
      subroutine defint(coef,nnd,res)
      implicit real*8 (a-h,o-z)
      integer  nnd, i
      real*8 res,coef(0:nnd-1)

      res =  2d0 * coef(0)
      do i = 2, nnd-1, 2
         res = res - 2d0 * coef(i) / dble(i*i-1)
      end do

      end subroutine





c---------------------------------------------------
c     subroutine chftrans
c---------------------------------------------------
c
c     This subroutine does the forward cheby transf
c     given function values, returns cheby coeffs
c
c     (the forward transf matrix is precomputed)
c
c---------------------------------------------------
c
      subroutine chftrans(ch, fn, nnd, cftm)
      implicit real*8 (a-h,o-z)
      integer nnd
      real*8 ch(nnd), fn(nnd), cftm(nnd,nnd)
c
      do i = 1, nnd
         ch(i) = 0d0
         do j = 1, nnd
            ch(i) = ch(i) + cftm(i,j)*fn(j) 
         end do
      end do

      end subroutine





c---------------------------------------------------
c     subroutine chbtrans
c---------------------------------------------------
c
c     This subroutine does the backward cheby transf
c     given cheby coeffs, returns function values
c
c     (the backward transf matrix is precomputed)
c
c---------------------------------------------------
c
      subroutine chbtrans(fn, ch, nnd, citm)
      implicit real*8 (a-h,o-z)
      integer nnd
      real*8 ch(nnd), fn(nnd), citm(nnd,nnd)
c
      do i = 1, nnd
         fn(i) = 0d0
         do j = 1, nnd
            fn(i) = fn(i) + citm(j,i)*ch(j) 
         end do
      end do

      end subroutine





c---------------------------------------------------
c     subroutine setchmat
c---------------------------------------------------
c
c     Given degree nnd, this subroutine sets the 
c     chebyshev matrices: 
c     1) forward and backward chebyshev transf
c        matrices: cftm and citm
c     2) chebyshev integration matrices
c        spdef, spbx, spxb (definite, left-indef,
c        and right-indef respectively) 
c
c---------------------------------------------------
c
      subroutine setchmat(nnd, cftm, citm, spdef,
     1           spbx, spxb)
      implicit real*8 (a-h,o-z)
      integer nnd
      real*8 cftm(nnd,nnd), citm(nnd,nnd)
      real*8 spdef(nnd), spbx(nnd,nnd), spxb(nnd,nnd)
c     local vars (assuming nnd<=100)
      real*8 theta(100), ns(100), c(100), chnd01(100) 
      real*8 work(100), pi
c
      pi=3.1415926535897932d0
      do i = 1, nnd
        theta(i) = (nnd-i+.5d0)/nnd * pi
        ns(i) = dsin(theta(i)) * dsqrt(2d0)
        c(i) = dcos(theta(i))
        chnd01(i) = ( dcos(theta(i)) + 1d0 ) / 2d0
      enddo
c------------------------------------------------------------
c computes terms in cosine inverse transform
c and stores them into matrix citm in the transposed format
c to facilitate data access during matrix multiplications.
c------------------------------------------------------------
        do  k = 1,nnd
           do i = 1, nnd
              citm(i,k) = dcos((i-1)*theta(k))
           end do
        end do

c------------------------------------------------------------
c computes terms in cosine forward transform with scaling factors
c and stores them into matrix cftm.
c    cftm(i,k) = 2d0/dble(nnd) * dcos((i-1)*theta(k))
c------------------------------------------------------------
        do k = 1,nnd
           cftm(1,k) = 1d0/dble(nnd)
           do i = 2, nnd
              cftm(i,k) = 2d0/dble(nnd)*citm(i,k)
            end do
        end do

c------------------------------------------------------------
c Spectral integration matrix
c   ctfm(:,i) = Cn En where En = ( 0, 0, ... , 1=i-th, ... , 0 )
c   SPBX Sigma  = Cn^{-1} SPINT_BXn Cn = \INT_{-1}^{x} Sigma(t) dt
c   SPXB Sigma  = Cn^{-1} SPINT_XBn Cn = \INT_{x}^{1} Sigma(t) dt
c------------------------------------------------------------
        do i = 1, nnd
           call chindef_bx(work,cftm(1,i),nnd)
           call chbtrans(spbx(1,i),work,nnd,citm)
           call chindef_xb(work,cftm(1,i),nnd)
           call chbtrans(spxb(1,i),work,nnd,citm)
           call defint(cftm(1,i),nnd,spdef(i))
        enddo
      
      end subroutine
      






c--------------------------------------------------
c     subroutine chnodes
c--------------------------------------------------
c
c     This subroutine returns Chebyshev nodes of a
c     given order on a given interal
c
c--------------------------------------------------
c
      subroutine chnodes(a, b, ndeg, chpts, u, v, u1, v1)
      implicit real*8 (a-h,o-z)
      integer ndeg
      real*8 a, b, chpts(ndeg)
      real*8 u, v, u1, v1
c
      u=(b-a)/2
      v=(b+a)/2
      u1=2/(b-a)
      v1=1-u1*b

      zero=0
      done=1
      pi2=datan(done)*2
      m=ndeg
      do k = 1,m
       chpts(m-k+1)=u*dcos((2*k-1)*pi2/m) + v
      enddo

      end subroutine





c     COMPLEX VERSION
c---------------------------------------------------
c     subroutine zchindef_bx
c---------------------------------------------------
c
c this routine calculates the chebyshev coefficients of
c the left (normal) indefinite integrals
c
c---------------------------------------------------
c
      subroutine zchindef_bx(chintfl,coeffl,nnd)
      implicit real*8 (a-h,o-z)
      integer i,nnd
      complex*16 coeffl(0:nnd-1), chintfl(0:nnd-1)
c
        chintfl(1) = coeffl(0) - coeffl(2)/2d0
        do i = 2,nnd-2
           chintfl(i) = (coeffl(i-1)-coeffl(i+1)) / dble(2*i)
        end do
        chintfl(nnd-1) = coeffl(nnd-2) / dble(2*nnd-2)

        chintfl(0) = (-1)**(nnd+1) * coeffl(nnd-1) / dble(2*nnd)
        do i = nnd-1, 1, -1
          chintfl(0) = chintfl(0) - (-1)**i * chintfl(i)
        end do
      end





c---------------------------------------------------
c     subroutine zchindef_xb
c---------------------------------------------------
c
c this routine calculates the chebyshev coefficients of
c the right (backward) indefinite integrals
c
c---------------------------------------------------
      subroutine zchindef_xb(chintfr,coeffr,nnd)
      implicit real*8 (a-h,o-z)
      integer i,nnd
      complex*16 coeffr(0:nnd-1),chintfr(0:nnd-1)
c
      chintfr(1) = coeffr(2)/2d0 - coeffr(0)
      do i = 2, nnd-2
         chintfr(i) = (coeffr(i+1)-coeffr(i-1)) / dble(2*i)
      end do
      chintfr(nnd-1) = -coeffr(nnd-2) / dble(2*nnd-2)

      chintfr(0) = coeffr(nnd-1) / dble(2*nnd)
      do i = 1, nnd-1
         chintfr(0) = chintfr(0) -  chintfr(i)
      end do
      end






c---------------------------------------------------
c     subroutine zdefint 
c---------------------------------------------------
c
c     This subroutine computes the definit integral
c     of a function on [-1,1], 
c     given the chebyshev coeffs
c
c---------------------------------------------------
c
      subroutine zdefint(coef,nnd,res)
      implicit real*8 (a-h,o-z)
      integer  nnd, i
      complex*16 res,coef(0:nnd-1)

      res =  2d0 * coef(0)
      do i = 2, nnd-1, 2
         res = res - 2d0 * coef(i) / dble(i*i-1)
      end do

      end subroutine






c---------------------------------------------------
c     subroutine zchftrans
c---------------------------------------------------
c
c     This subroutine does the forward cheby transf
c     given function values, returns cheby coeffs
c
c     (the forward transf matrix is precomputed)
c
c---------------------------------------------------
c
      subroutine zchftrans(ch, fn, nnd, cftm)
      implicit real*8 (a-h,o-z)
      integer nnd
      complex*16 ch(nnd), fn(nnd), cftm(nnd,nnd)
c
      do i = 1, nnd
         ch(i) = 0d0
         do j = 1, nnd
            ch(i) = ch(i) + cftm(i,j)*fn(j) 
         end do
      end do

      end subroutine






c---------------------------------------------------
c     subroutine zchbtrans
c---------------------------------------------------
c
c     This subroutine does the backward cheby transf
c     given cheby coeffs, returns function values
c
c     (the backward transf matrix is precomputed)
c
c---------------------------------------------------
c
      subroutine zchbtrans(fn, ch, nnd, citm)
      implicit real*8 (a-h,o-z)
      integer nnd
      complex*16 ch(nnd), fn(nnd), citm(nnd,nnd)
c
      do i = 1, nnd
         fn(i) = 0d0
         do j = 1, nnd
            fn(i) = fn(i) + citm(j,i)*ch(j) 
         end do
      end do

      end subroutine






c---------------------------------------------------
c     subroutine zsetchmat
c---------------------------------------------------
c
c     Given degree nnd, this subroutine sets the 
c     chebyshev matrices: 
c     1) forward and backward chebyshev transf
c        matrices: cftm and citm
c     2) chebyshev integration matrices
c        spdef, spbx, spxb (definite, left-indef,
c        and right-indef respectively) 
c
c---------------------------------------------------
c
      subroutine zsetchmat(nnd, cftm, citm, spdef,
     1           spbx, spxb)
      implicit real*8 (a-h,o-z)
      integer nnd
      complex*16 cftm(nnd,nnd), citm(nnd,nnd)
      complex*16 spdef(nnd), spbx(nnd,nnd), spxb(nnd,nnd)
c     local vars (assuming nnd<=100)
      real*8 theta(100), ns(100), c(100), chnd01(100)
      real*8 pi, xx
      complex*16 work(100)
c
      pi=3.1415926535897932d0
      do i = 1, nnd
        theta(i) = (nnd-i+.5d0)/nnd * pi
        ns(i) = dsin(theta(i)) * dsqrt(2d0)
        c(i) = dcos(theta(i))
        chnd01(i) = ( dcos(theta(i)) + 1d0 ) / 2d0
      enddo
c------------------------------------------------------------
c computes terms in cosine inverse transform
c and stores them into matrix citm in the transposed format
c to facilitate data access during matrix multiplications.
c------------------------------------------------------------
        do  k = 1,nnd
           do i = 1, nnd
              xx = dcos((i-1)*theta(k))
              citm(i,k)=dcmplx(xx,0.0d0)
           end do
        end do

c------------------------------------------------------------
c computes terms in cosine forward transform with scaling factors
c and stores them into matrix cftm.
c    cftm(i,k) = 2d0/dble(nnd) * dcos((i-1)*theta(k))
c------------------------------------------------------------
        do k = 1,nnd
           cftm(1,k) = 1d0/dble(nnd)
           do i = 2, nnd
              cftm(i,k) = 2d0/dble(nnd)*citm(i,k)
            end do
        end do

c------------------------------------------------------------
c Spectral integration matrix
c   ctfm(:,i) = Cn En where En = ( 0, 0, ... , 1=i-th, ... , 0 )
c   SPBX Sigma  = Cn^{-1} SPINT_BXn Cn = \INT_{-1}^{x} Sigma(t) dt
c   SPXB Sigma  = Cn^{-1} SPINT_XBn Cn = \INT_{x}^{1} Sigma(t) dt
c------------------------------------------------------------
        do i = 1, nnd
           call zchindef_bx(work,cftm(1,i),nnd)
           call zchbtrans(spbx(1,i),work,nnd,citm)
           call zchindef_xb(work,cftm(1,i),nnd)
           call zchbtrans(spxb(1,i),work,nnd,citm)
           call zdefint(cftm(1,i),nnd,spdef(i))
        enddo
      
      end subroutine
      







