Two Point BVP solver


bvpmain.f: the user interface subroutines

bvprouts.f: functional subroutines

btreerouts.f: binary tree related subroutines

chebex.f: chebyshev transf related subroutines

dfft.f: fft related subroutines, needed by chebex.f

lu.f: subroutines from linpack, for solving linear system 




